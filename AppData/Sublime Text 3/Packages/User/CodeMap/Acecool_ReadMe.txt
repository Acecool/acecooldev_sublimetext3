//
// XCodeMapper / CodePeer ReadMe - Josh 'Acecool' Moser
//

I'm moving away from using the custom_mappers/ folder... In a short amount of time, all files inside of this folder will be identical ( required because oleg-shiro doesn't want to add an alternate method of inclusion of custom mappers which would result in circular imports - ugly - without the extra method )... Because they are identical, it'll mean the back-end autoloader / inclusion system will read the configuration for the active language and then choose which CLASS to load... The child-class will be the FIRST include aside from the autoloader class ensuring that each parent it imports all the way to the XCodeMapperBase, Definitions and AcecoolST3_Library will mean there are NO circular imports... It also means adding a new mapper for a new language is easy and adding a mapper for an existing language became even easier....



//
// Adding a new language to the mapping system
//
To add a new language, create a copy of any file in the custom_mappers/ folder and rename it to the file-extension used by the new language such as blah.py or bleh.py or new.py, etc...
Next - follow the instructions for Creating a new Language Configuration File Setup... Then proceed to Creating a new Mapping Class...


//
// Creating a new Language Configuration File Setup
//
This requires creating 2 new sublime-settings files inside of the AppData\Sublime Text 3\Packages\User\ folder following the same pattern as the others... Acecool_XCodeMapper_Config_EXT.Default.sublime-settings and Acecool_XCodeMapper_Config_EXT.User.sublime-settings

The Default file can be made by simply copying the Acecool_XCodeMapper_Config_TEMPLATE.Default.sublime-settings and simply updating the language name within the file along with the basics such as which mappers are used..

For example, if it is a language similar to PHP then it uses 6 different languages within the file itself - PHP within PHP tags, SQL within certain function calls within PHP, Regular Expressions ( within PHP and JavaScript ), HTML outside of PHP tags, CSS within Style Tags within HTML, JavaScript within Script Tags within HTML ( PHP, SQL, RegEx, HTML, JavaScript, CSS ).

With this basic setting you enable the language to pull from all of these languages but rules neet to be defined to ensure which is read where.... Following the example of PHP above - Because HTML already has the rules for JavaScript, CSS and RegEx for HTML you would only need a basic rule to be active when PHP isn't active because it is the secondary language... To set PHP active use <? and disable >? - short tags are fine to use because longer tags are identical along with the form post short-tag... and HTML will be active otherwise - HTML will have it's own setting to activate the others... The only reason we define all possible languages in each language is to simplify the logic required and speed up the script... If I were to simply read each setting so PHP and HTML with the others added then it'd have to manually locate all of the other languages and may end up with a circular import - unlikely but it could happen if incorrectly configured... This way you can also state that you don't want to detect JavaScript, CSS, etc... but only PHP and HTML for instance...

"UsesLanguages":
{
	// Primary first
	"PHP": true,

	// Secondary
	"HTML": true,

	// Now the others...
	"JavaScript": true,
	"RegEx": true,
	"SQL": true,
	"CSS": true,
},


//
// Creating a new Mapping Class
//
With this new system, to add a new mapper for an existing class simply open up the Acecool_XCodeMapper_Classes/ folder and copy the template.py file... Then simply name the file XCodeMapper_LanguageOrEXT_ClassExtra.py so py_Acecool.py or js_AcecoolWeb_Framework.js will work perfectly... Just ensure the class-name of the class within the file is named exactly the same as the file... This will ensure you'll be able to use the short-name ( simply the ClassExtra portion of the name in the language config file when choosing which class to load )...


//
// Creating Language Enaable / Disable Rules for languages or file-extensions which allow for multiple within ( PHP / Asp allows 6 each, HTML allows 3 base and so on... )
//
"LanguageRules":
{
	"PHP":
	{
		"activation":
		{
			"SyntaxRules":
			{
				"<?php": true,
				"<?=": true,
				"<?": true,
				"?>": false,
			},
			"LanguageRules":
			{
				// When HTML is false - ie not active, then HTML IS active
				"HTML": false,
				"RegEx": false,
				"CSS": false,
				"JavaScript": false,
				"SQL": false,
			},
		}
	},
	"HTML":
	{
		"activation":
		{
			"LanguageRules":
			{
				// When PHP is false - ie not active, then HTML IS active ie php must be false for html to be true
				"PHP": false,
				"RegEx": false,
				"CSS": false,
				"JavaScript": false,
				"SQL": false,
			},
		}
	},
	"CSS":
	{
		"activation":
		{
			"SyntaxRules":
			{
				// When a style tag is discovered ( options don't matter ) activate CSS
				"<style": true,

				// When the style end tag is found - deactivate CSS
				"</style>": false,
			},
			"LanguageRules":
			{
				// When HTML is active then we have a chance at activating CSS because syntax rules exist too
				"HTML": true,
			},
		}
	},
	"JavaScript":
	{
		"activation":
		{
			"SyntaxRules":
			{
				// When a Script tag is discovered ( options don't matter ) activate JS
				"<script": true,

				// When the style end tag is found - deactivate JS
				"</script>": false,
			},
			"LanguageRules":
			{
				// When HTML is active then we have a chance at activating JS because syntax rules exist too
				"HTML": true,
			},
		}
	},
	"RegEx":
	{
		"activation":
		{
			"SyntaxRules":
			{
				// For JavaScript new RegExp is used - other methods too - activate in here...
				"RegExp(": true,

				// Deactivate outside of the RegExp function - it can only be disabled when enabled - note: because regex uses ); etc.. we use intelligent tracking to ensure this doesn't trigger within...
				");": false,
			},
			"LanguageRules":
			{
				// When JavaScript OR PHP is active we have a chance at activating RegEx detection ( because Syntax Rules Exist )...
				"JavaScript": true,
				"PHP": true,
			},
		}
	},
}


NOTES: All of these aren't required in all languages that use it... Because HTML already defines when RegEx, CSS, etc.. activate and deactivate we don't need them.. We only need to define the primary and secondary languages along with what languages are actually used... so the only things needed are PHP and HTML...