##
## XCodeMapper, Universal Code Mapper extension for 'CodeMap' - Josh 'Acecool' Moser
##
## CodeMap, by oleg-shiro, is a Sublime Text 3 Package / Extension which provides useful functionality to the editor by adding a panel to display
## 	important aspects of code, hotkeys to navigate the output, and support to double-click each output line to jump to the relevant line.
##
## XCodeMapper extends CodeMap by adding features such as categories, category types, callbacks, and more. These features and partnership allows
## 	a concise and beautiful output to be featured within the Code - Map panel while utilizing the least space possible. The callbacks further
## 	the ability to earmark important data by allowing that data to be modified or checked for errors to further aide the developer. Additionally,
## 	and probably the most important feature is the ease in which support for a new language can be integrated along with the ability to tailor
## 	the mapper to your specific project with just a few lines of function calls.
##
## Please read the comments in XCodeMapperBase.py and XCodeMapperCallbackBase.py to understand the extent of the features and abilities...
##
## Each code_map.<ext>.py script requires several things to work:
## 	- ) Standard Imports
## 	1 ) The generate function definition populated with standard calls to create a new Callback and XCodeMapper object. This function is similar to the main function of a program.
## 	2 ) The Callback object definition with the OnSetupSyntax function defined and populated with self.AddSyntax calls to earmark code. The OnSetupConfig function is also required to tell the mapper your local mapper CFG_ configuration data.
## 	3 ) The empty, or not, XCodeMapper definition - This is there so you may overwrite pre-existing XCodeMapper functions for development, testing and or customization purposes.
##
## Optional Elements can be added too:
##	1 ) `map_syntax` is an attribute that defines syntax highlight to be used for the code map text
## 	2 ) Additional functions added to Callback to extend functionality and tailor the mapper to your specific project or language needs..
##
## The Mapper formats output to the Code - Map panel in the following form to create hotlinks to the <line_number> allowing you to jump directly to that specific line in the active file: <data>:<line_number>
##


##
## Task List
##
## Task: If a function is defined across multiple lines - scan ahead to make sure the : exists and scan ahead so OnFunctionArgument can be called with all of the args properly...
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
##


##
## Imports
##

## If Developer Mode is True, the imports will be reloaded...
DEVELOPER_MODE									= True

##
import sys

## Import Sublime Library
import sublime
import sublime_plugin

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from User.CodeMap.Acecool.XCodeMapper_Definitions import *

## Import Acecool's Library of useful functions and globals / constants...
from User.CodeMap.Acecool.AcecoolST3_Library import *

## Import XCodeMapper
from User.CodeMap.Acecool.XCodeMapper import *

## Reload
if ( DEVELOPER_MODE ):
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper_Definitions' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.AcecoolST3_Library' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper' )

## Grab Sublime Text Settings for packages which are ignored and which are installed!
ignored		= sublime.load_settings( 'Preferences.sublime-settings' ).get( 'ignored_packages' )
installed	= sublime.load_settings( 'Package Control.sublime-settings' ).get( 'installed_packages' )

## Determine the best Python Syntax Highlighter File to use for the Mapping Panel - Detected in order: sublime-syntax / tmLanguage - in Packages/: Acecool/syntax_files/ then <PackageName>/ - defaults to Python/Python.sublime-syntax
map_syntax = Acecool.sublime.GetSyntaxFilePath( 'Python' )


##
## Configuration ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Important Default Configuration Options
##

## Debugging Mode - Set this to True to display ALL debugging messages or set it to a tag / id to only print those. By default this is True so a few messages will print until the local mapper config activates and some messages will always be displayed...
## Options: True / False
CFG_DEBUG_MODE									= False


##
## Language we're using...
##
## Options: Any String
CFG_PRIMARY_ACTIVE_LANGUAGE						= 'Python'


##
## Caching System
##

## Do you want to use the caching system? ( Typically 0.5 seconds to process 5000 lines of code and 0.02 to save - 0.01 to load without needing 0.48 to reprocess... - Times from SSD Hard Drive and [ 10 year old Intel Q6600 2.4 GHz Quad OC on AIR @ GHz 3.0 on air for entire lifetime, 8GB DDR2, Asus P5k Mbd ] )
CFG_CACHING_SYSTEM_ENABLED						= True


##
## Category Related
##

## The default for Alphabetizing the data in categories.... Sort method used... By line number is already default... First seen first out... add _DESC to reverse it, or _ALPHABETICAL for A-Z or _ALPHABETICAL_DESC for Z-A
## Options: ENUM_SORT_METHOD_DEFAULT == 0-9 / ENUM_SORT_METHOD_ALPHABETICAL == A-Z / ENUM_SORT_METHOD_ALPHABETICAL_DESC == Z-A / ENUM_SORT_METHOD_LINE_NUMBER == 0-9 / ENUM_SORT_METHOD_LINE_NUMBER_DESC == 9-0
CFG_CATEGORY_DISPLAY_SORT_METHOD				= ENUM_SORT_METHOD_ALPHABETICAL

## Display the Help and Support Category? When set to True it ouputs Download and Support Information for both oleg-shilo, Creator of CodeMap which is incredibly useful Package for Sublime Text Editor, and Josh 'Acecool' Moser who created XCodeMapper which is an extension for CodeMap to add more features!
## Options: True / False
CFG_DISPLAY_CATEGORY_HELP						= False

## Display the Errors List Category - By default it is at the very top and only visible when errors are found?
## Options: True / False
CFG_DISPLAY_CATEGORY_ERRORS						= True

## Display the Tasks / TODOs / Work / etc.. Category? By default it is enabled - Tasks are typically important but they can pile up. If you want to hide the category temporarily or permanently - set it here..
## Options: True / False
CFG_DISPLAY_CATEGORY_TASKS						= True

## Display the Notes / Info / etc.. Category? By default it is enabled - however Notes aren't always important - if notes are getting in the way of other data you can hide the category temporarily or permanently...
## Options: True / False
CFG_DISPLAY_CATEGORY_NOTES						= True


##
## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
##

## Should the line be displayed?
## Options: True / False
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO				= True

## If the line is to be displayed ( above set to True ), how many new-lines should we add afterwards? 2 gives a blank line between it and the first category output..
## Options: Numerical 0 through n
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES		= 2



##
## New-Line preferred for reading Files and for outputting to the Code - Map Panel
##

## New-Line character used for the Code - Map Panel
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_NEW_LINE									= CONST_NEW_LINE_UNIX

## New-Line character used to delimit the active-file.. The new-line type will appear under TOP_MENU > View > Line Endings > [ Windows / Unix / Mac OS 9 ]
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_FILE_NEW_LINE								= CONST_NEW_LINE_UNIX


##
## Indentation
##

## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
## CFG_INDENT_CHARS								= '\t'
## Options: Any String - I'd recommend using spaces if you'll be copying / pasting into Steam, otherwise Tabs..
CFG_INDENT_CHARS								= '    '

## Set this to -1 to disable the column sizing feature ( meaning line :1234 shows up right at the side of the code output to CodeMap Panel ). Set it to 1 to truncate values meaning the column will be max of that plus the line-size. Set it to 0 to try to force it to that column, but don't truncate. If it goes over, use -1 behavior on that line... This way the line numbers line up nicely most of the time..
## Options: ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT / ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE / ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED
CFG_INDENT_CODE_COLUMN_WIDTH_MODE				= ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT

## Configure the maximum width code should take up in the output before being truncated... Then the line column comes... Note, there will be an additional callback so you can define this on the fly for specific categories so if you want notes / tasks to have no limit, no worries... If you want to disable this feature set it to -1 also note that the line one will be turned off too.. Also, you can set this to a large value, just remember that spaces are used!! I'd recommend keeping it under 255 or so chars per line which is about 1080px wide if viewing on a 4k monitor with a few ticks up in font-size... But at that width you'll unlikely see the line number... keep that in mind...
## Options: Numerical 0 through n - Only Active if CFG_INDENT_CODE_COLUMN_WIDTH_MODE is something other than _DEFAULT
CFG_INDENT_CODE_COLUMN_WIDTH					= 125

## Should we limit the Category Name / Description Length? If so, use CFG_INDENT_CODE_COLUMN_WIDTH + 5 for file with 9999 max lines, or 6 with 99,999 for good measure so they'll be lined up properly...
## Options: Numerical 0 through n - Its' best to keep this on paar with CFG_INDENT_CODE_COLUMN_WIDTH
CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH		= CFG_INDENT_CODE_COLUMN_WIDTH + 6


##
## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
##

## If you want to comment out Task / TODO lines, set this to the language comment
## Options: Any String
CFG_TASK_LINE_PREFIX							= '## '

## This suffix is used to ensure the rest of the Code - Map Panel doesn't show up as a single-color.. Simply use a block-comment or comment with a single and double-quote to ensure the syntax highlighter catche sit...
## Options: Any String
CFG_TASK_LINE_SUFFIX							= ' #'


##
## Category Output Configuration For the Code - Map Panel
##

## Category Line Prefix and Suffix - the Description variant is for before and after the description which is hidden if the above is False... This is for the entire line before and after...
CFG_CATEGORY_LINE_PREFIX						= '☼ '
CFG_CATEGORY_LINE_SUFFIX						= ''

## Should we display the Category Description?
## Options: True / False
CFG_CATEGORY_LINE_SHOW_DESC						= True

## The Prefix to be used separating the Category Name from the Description - I'd recommend something similar to ' - ', maybe with tabs, etc... You can also use Acecool.string.FormatColumn( 25, ' - ' ) to add 22 spaces after ' - '... Later I may add support for the category output to use this function so all category names / descriptions line up in the output...
## Options: Any String
CFG_CATEGORY_DESC_PREFIX						= ' = \''

## If we show the Category Description, I highly recommend you set this to use a comment for the language along with ' and " - or simply use CFG_TASK_LINE_SUFFIX - to prevent the highlighter from messing up and coloring everything one color..
## Options: Any String
CFG_CATEGORY_DESC_SUFFIX						= '\':'


##
## Function / Class For-Each Argument Callback Configuration
##

## This is what separates the function or class from the arguments passed through - Example: func<OPENING_DELIMITER> x, y, z )
## Options: Any String
CFG_OBJECT_ARGUMENT_OPENING_DELIMITER			= '('

## This is what separates the arguments from the end function / class call or declaration - Example: func( x, y, z <ENDING_DELIMITER>
## Options: Any String
CFG_OBJECT_ARGUMENT_ENDING_DELIMITER			= ')'

## This is the character used to delimit or split arguments - It isn't recommended to use more than 1 char otherwise other coding-standards may not be properly read...
## Options: Any String
CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER				= ','

## This is to split a default value from an argument... ie func ( x, y, z = true ) in some languages means z, if unset, is set to true...
## Options: Any String
CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER		= '='

## This is the string used to re-join the list of modified arguments back into a string - A comma and space here provide an Airy output: func( x<JOIN_DELIMITER>y<JOIN_DELIMITER>z )
## Options: Any String
CFG_OBJECT_ARGUMENT_JOIN_DELIMITER				= ', '

## These are what comes between the arguments on either side of the rejoined args string, and the opening / ending delimiter - Spaces here provide an Airy output: func(<PREFIX>x, y, z<SUFFIX>)
## Options: Any String
CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX				= ' '
CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX				= ' '


##
## If CFG_FUNC_FIND_TEXT_REPLACEMENT is set then we look for CFG_FUNC_FIND_TEXT and replace it with CFG_FUNC_FIND_TEXT_REPLACEMENT... so def can become function, or function can become fund or f... Space-savings!
##

## If CFG_FUNC_TEXT above is not '' then set this to what you are looking to replace... Add the space in both, or neither
## Options: Any String
CFG_FUNC_NAME_SEARCH							= 'def' #'def'

## Internal helper for GLua - This defines the text to be used for function declarations as function in Callback.PreOnAddEntry is replaced to save space as some of the function / object declarations in Lua and GLua are quite long...
## Options: Any String
CFG_FUNC_NAME_REPLACE							= 'ƒ'


##
## Data-Type Options for Function / Class Arguments...
##
##	<DataType>		==		ENUM_DISPLAY_DATA_TYPE_ONLY		|	<DataType> _arg		==		ENUM_DISPLAY_DATA_TYPE_BOTH		|	_arg	==		ENUM_DISPLAY_DATA_TYPE_DEFAULT
##

## Determine how the Data-Type system should apply data... ENUM_DISPLAY_DATA_TYPE_DEFAULT == Leaves the argument as is / _TYPE_BOTH == displays <DataType> _arg - assumes ' ', '<', '>' cfg / _TYPE_ONLY == <DataType> only
## Options: ENUM_DISPLAY_DATA_TYPE_DEFAULT == _arg / ENUM_DISPLAY_DATA_TYPE_ONLY == <DataType> / ENUM_DISPLAY_DATA_TYPE_BOTH == <DataType> _arg
CFG_DISPLAY_ARG_DATA_TYPE						= ENUM_DISPLAY_DATA_TYPE_ONLY

## How data-types appear - for when they are added to, or replacing, function args... ie if the following 3 are: ' ' '-' 'x' they'd appear: ( -Playerx _p, -Weaponx _w ) or ' ' '<' '>' they'd appear: ( <Player> _p, <Weapon> _w )
## Options: Any String
CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS			= ' '
CFG_DISPLAY_ARG_DATA_TYPE_PREFIX				= '<'
CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX				= '>'


##
## Personal Information
##

## Display my personal story?
## Options: True / False
CFG_DISPLAY_PERSONAL_STORY						= False


##
## Custom Additions ( for Callback class ) Config - ie things you've added - I'd highly recommend using this area instead of hard-coding values in case you decide to share your Callback class for Language <X> with anyone else...
##

## Determines whether or not you want variables which are ALL UPPERCASE to be displayed in the output... It can be useful, but it can also be a wall of text, especially for these custom mappers until... if... I convert to sublime-settings files...
CFG_DISPLAY_CONST_DEFINITIONS					= True


##
## Alternate Function Output Configuration - ie do you want functions / classes to show up inline as in ClassName.Function( ) similar to Lua?
##

## If set to true, instead of [ function string.Lower( _text ); ] being displayed - [ function Lower( _text ) ] is added to the [ Library string ] Class-Function Table... Similar display to how I display Python...
## Note: functions using : have their name defined as Meta-Table <Name> or Object <Name> for the CATEGORY_CLASS_FUNCTION Header... [ self is also to the output ( in some cases ) -- NEXT UPDATE - option to choose it ]
CFG_DISPLAY_FUNCTIONS_ALTERNATIVELY				= False

## If you choose to have the functions displayed similar to Lua ( some_table_as_library_meta_table_or_object.function( _args ) ) by setting the above value to True...
## This allows you to build the FULL DEPTH... IE every table entry is shown ( they're needed to call the function properly anyway - but it can add a lot of depth depending how far nested your classes are )
## In short: ## Should.we.build.the.full.depth.to.function( _args ) or should we simply show the direct parent only? to.function( _args )?
## Note: I recommend trying it as True - And trying as False to see which you prefer... THIS OPTION IS ONLY ACTIVE IF THE ABOVE VALUE IS True...
CFG_DISPLAY_FUNCTIONS_ALTERNATIVE_FULL_DEPTH	= True


##
## Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Which Mapping Variant would you like to use? Each option going downward gives you everything of the level above and itself except for the USER which is for you to customize how you want.
##
## ENUM																	Class Name								Base Class							ClassDescription
ENUM_LANG_THIS_DEV								= SetupLangVariantMap( 'XCodeMapper_Python_Dev',				'XCodeMapper_Python',				'XCodeMapper Development platform to test updates in real-time instead of restarting gmod. This is used to test major updates to the XCodeMapperBase Class!' )
ENUM_LANG_THIS_VANILLA							= SetupLangVariantMap( 'XCodeMapper_Python',					'XCodeMapperBase', 					'Python - Standard Edition - This maps the following: ' )
ENUM_LANG_THIS_ACECOOL_XCODEMAPPER				= SetupLangVariantMap( 'XCodeMapper_Python_Acecool_XCM',		'XCodeMapper_Python',				'Python - With the additional mappings to navigate through these scripts easily!' )
ENUM_LANG_THIS_USER								= SetupLangVariantMap( 'XCodeMapper_Python_User',				'XCodeMapper_Python_Acecool_XCM',	'Python - Everything you need to create your own extension - I added it to the end to make copying / pasting easier, for when this script updates. On Updates of this script, copy the relevent segments ( This, the config element below, and the class... That is it.. )!' )


## This option is the default and will add mappings for the Vanilla Language / Variant.
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_VANILLA

## This mapper is specifically used to read XCodeMapper XCodeMapper Base, Definitions, Acecool ST3 Library, and code_map.<ext>.py files by creating mappings to show much more information.
## CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_ACECOOL_XCODEMAPPER

## This option is for Greatly altering XCodeMapperBase
## CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_DEV

## This option is a blank Class usable as a template to create mappings for your own project. It is left at the end of the file to allow for easier processing of updates - when a new updates comes out, copy and paste the object to the new file..
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_USER



##
## End Base, Custom Additions, and Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Primary / Main function used by the CodeMap back-end. This doesn't need to be edited anymore... Use the Mapper Configuration to choose which Class to run.
##
def generate( _file ):
	## Grab the Mapper via the Mapper ID / Class-Name using either the Globals dictionary or by using the active module [ Preferred ]... Run the mapper and return the output...
	return getattr( sys.modules[ __name__ ], GetLangVariantClassName( CFG_XCODEMAPPER_MAPPER_VARIANT ) )( ).Run( _file, CFG_XCODEMAPPER_MAPPER_VARIANT )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Developer Task List
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

## Task: Add System to check for multi-line declarations
##		 many developers break function declarations up over multiple lines, even if the function declaration is short. What's worse is most of the time, they're not consistent with the practice...

## Task: Detect type of indentation used and use tabs to spaces engine and spaces to tabs to ensure 4 depth doesn't mean 4 tabs when 4 spaces are used...

## Task: Combine XCodeMapper Category Output functions and add additional sort methods, etc...


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Developer Task List
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Developer Test Code
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Note:
##


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Developer Test Code
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class XCodeMapper_Python( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Python'


	##
	##
	##
	def DeveloperFunc( self ):
		## No need running this code / output when not actively developing or working on it..
		if ( True ):
			return

		## Load the mapper configuration - dynamically would be to read the loaded file and get the extension then use ext_config.sublime-settings...
		XCM_MAPPER_SETTINGS	= sublime.load_settings( 'Acecool\'s_XCodeMapper.sublime-settings' )

		## For some reason the file isn't detected... This hould return True, True, "Python", "Alphabetically" - Instead the defaults are being returned and this is a problem!!!
		print( str( XCM_MAPPER_SETTINGS.get( 'DeveloperMode', False ) ) )
		print( str( XCM_MAPPER_SETTINGS.get( 'DebugMode', False ) ) )
		print( str( XCM_MAPPER_SETTINGS.get( 'LanguageName', False ) ) )
		print( str( XCM_MAPPER_SETTINGS.get( 'OutputSortMethod', 'A-Z' ) ) )


	##
	##
	##
	def OnRunDeveloperFunc( self ):
		pass


	##
	## Callback used to alter Code - Map Panel configuration per mapper...
	##
	def OnSetupCodeMapPanel( self, _panel, _settings ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCodeMapPanel' )

		## Disable Scroll Past End... Note: By default I set this to false - if you want it re-enabled, set it here!
		## _panel.settings( ).set( 'scroll_past_end', False )


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		## Comments
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '##', '#' )

		## Functions and Class
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'def ' )
		self.AddSyntax( CATEGORY_CLASS, SEARCH_TYPE_STARTSWITH, 'class ' )

		## Debugging / Optimization Earmarks
		## self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'print(' )


		##
		## Add common typos to look for along with the approximate code to look for and what it should look like...
		##
		self.AddTypoFixEntry( 'startswidth', '_haystack.startswidth( _needle )', '_haystack.startswith( _needle )' )
		self.AddTypoFixEntry( 'startwidth', '_haystack.startwidth( _needle )', '_haystack.startswith( _needle )' )
		self.AddTypoFixEntry( 'startwith', '_haystack.startwith( _needle )', '_haystack.startswith( _needle )' )
		self.AddTypoFixEntry( 'endswidth', '_haystack.endswidth( _needle )', '_haystack.endswith( _needle )' )
		self.AddTypoFixEntry( 'endwidth', '_haystack.endwidth( _needle )', '_haystack.endswith( _needle )' )
		self.AddTypoFixEntry( 'endwith', '_haystack.endwith( _needle )', '_haystack.endswith( _needle )' )


		##
		## Mapper Features
		##
		self.AddFeature( '- Language: Python using ' + self.__name__ )
		self.AddFeature( 'Detection for comments, functions, classes and print statements for debugging / optimization...' )
		self.AddFeature( 'Class Headers. Child Functions sorted into nested categories and global / local to the scope functions in their own category..' )
		self.AddFeature( 'Basic Syntax-Error Detection for Class and Function Definitions - if : doesn\'t exist then [ ERROR ] is added to the output line, if applicable, and a Syntax Error Entry is added!' )
		self.AddFeature( 'Added new feature: Jan 11 2018 - CFG_DISPLAY_FUNCTIONS_ALTERNATIVELY / CFG_DISPLAY_FUNCTIONS_ALTERNATIVE_FULL_DEPTH - These lets you choose to show functions within classes in the nested-category layout, or similar to LuaTable.FunctionName( _args ) style with the second Cfg allowing you to show more than just the direct-parent ( which.will.follow.the.shallowest.class.to.our.FunctionName( _args ) - This can save space, it can cost space... It is a unique way to display the data and it sorts... For those worried about too little vertical room - you may want to try this! )' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )



		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - Called for each line for additional error-checking and for processing rules for class recognition, stack popping, etc...
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Inverted to get rid of duplicate return code... - Make sure there is code on this line... that it isn't a comment and we're not in a comment block...
		## Adding the not in front of the ( ... ) is identical to applying that negation manually - meaning it would become the following ( as we need to make sure none of those exist in order to run within ):
		##		if ( _Code.strip( ) != '' and not _in_comment and not _in_comment_block  )
		##
		if not ( _code.strip( ) == '' or _in_comment or _in_comment_block ):
			##
			## Process potential syntax errors
			##
			self.ProcessSyntaxErrors( _line_number, _code, _depth, _in_comment, _in_comment_block )


			##
			## Process Depths, parents of objects, etc...
			##
			self.SyntaxRuleFunctionClassScopeDepthTracker( _line_number, _code, _depth, _in_comment, _in_comment_block )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		return super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )


	##
	## Process Syntax Errors - Set in its own function to simplify default callbacks and to reinforce building-block style of coding I typically use...
	##
	def ProcessSyntaxErrors( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Create Helpers to determine whether or not we've detected a line containing a class or function definition...
		_bClass = _code.startswith( 'class ' )
		_bFunc = _code.startswith( 'def ' )
		_bIfElIf = _code.startswith( 'if' ) or _code.startswith( 'elif' )
		_bElse = _code.startswith( 'else' )
		_bFor = _code.startswith( 'for' )


		##
		## Check for syntax error - on CATEGORY_CLASS or CATEGORY_FUNCTION - if either doesn't end with : then report syntax error... - Note: This is done here instead of OnProcessLine because we have access to category and patterns, etc.. here and because we are looking for class and function definitions..
		##
		if ( _bClass or _bFunc or _bIfElIf or _bElse or _bFor ):
			## Make sure the line is a valid code-based line and check to see if the line ends with a semi-colon..
			if ( _line_number != '' and not _code.endswith( ':' ) ):
				## If it is a class, asume the worst.. If it is a function, it may be a single-line definition so lets check for a return statement before jumping the gun.. Note: I used Category Original, because we modify functions to class in this, to accurately process this..
				## if ( ( _category_original != CATEGORY_FUNCTION and _category == CATEGORY_CLASS ) or ( _category_original == CATEGORY_FUNCTION and not Acecool.string.indexOfExists( ': return', _code, True ) and not Acecool.string.indexOfExists( ':return', _code, True ) and not Acecool.string.indexOfExists( ': pass', _code, True ) and not Acecool.string.indexOfExists( ':pass', _code, True ) and not Acecool.string.indexOfExists( ': pas s', _code, True ) and not Acecool.string.indexOfExists( ':pa ss', _code, True ) ) ):
				if ( _bClass or ( _bFunc and not Acecool.string.indexOfExists( ': return', _code, True ) and not Acecool.string.indexOfExists( ':return', _code, True ) and not Acecool.string.indexOfExists( ': pass', _code, True ) and not Acecool.string.indexOfExists( ':pass', _code, True ) and not Acecool.string.indexOfExists( ': pas s', _code, True ) and not Acecool.string.indexOfExists( ':pa ss', _code, True ) ) ):
					## Mark the error so the developer can fix it..
					self.AddError( 'Syntax Error: Missing Colon ":" at end of line for a ' + Acecool.logic.ternary( ( _bFunc ), 'Function', 'Class' ) + ' declaration!', _line_number );

					## Add an error tag to the code in question to make it noticeable as the error only describes the type and line number to keep it concise..
					_code = _code + ' [ ERROR ]'
		else:
			## Make sure the line doesn't end with a : because outside of the above cases, it shouldn't be there...
			if ( _line_number != '' and _code.endswith( ':' ) ):
				## Mark the error so the developer can fix it..
				self.AddError( 'Possible Syntax Error: Colon ":" found at the end of a line which does not require it...', _line_number );

				## Add an error tag to the code in question to make it noticeable as the error only describes the type and line number to keep it concise..
				_code = _code + ' [ ERROR ]'


		##
		## Detect Syntax Error with if / elseif / else if / else statements...
		##
		if ( _bIfElIf ):
			## If ! is used instead of not, alert the user..
			## Task: Create self.AddSyntaxRule( SYNTAX_ERROR, IF_STATEMENTS, CONTAINS, "!", WHEN_NOT "!=" ) - ie extend search detection methods
			if ( Acecool.string.indexOfExists( '!', _code, True ) and not Acecool.string.indexOfExists( '!=', _code, True ) ):
				self.AddError( 'Syntax Error: Incorrect usage of "!". Python requires that "not" is used except when using "!=".. if ( not X ): is correct, if ( !X ): does not work!', _line_number );
				_code = _code + ' [ ERROR ]'

			## Detect && and || usage in if statements...
			## Task: Create self.AddSyntaxRule( SYNTAX_ERROR, IF_STATEMENTS, CONTAINS, "&&", "WHEN_NOT_IN_QUOTES, "||", "WHEN_NOT_IN_QUOTES ) - ie extend search detection methods
			if ( ( not Acecool.string.indexOfExists( '"&&"', _code, True ) and not Acecool.string.indexOfExists( '"||"', _code, True ) and not Acecool.string.indexOfExists( "'&&'", _code, True ) and not Acecool.string.indexOfExists( "'||'", _code, True ) ) and ( Acecool.string.indexOfExists( '&&', _code, True ) or Acecool.string.indexOfExists( '||', _code, True ) ) ):
				self.AddError( 'Syntax Error: Incorrect usage of "&&" or "||" - Python requires "and" or "or" as "&&" and "||" does not work!', _line_number );
				_code = _code + ' [ ERROR ]'



	##
	## Note: This has been added temporarily as a stand-alone function until it is converted into a Syntax Rule... or Rule Set... I will be renaming some of the helper functions to better fit what it is I'm actually doing... Expect that update soon.. Converting things, and coding, is easier when you design your code to be a series of building blocks..
	##
	def SyntaxRuleFunctionClassScopeDepthTracker( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Create Helpers to determine whether or not we've detected a line containing a class or function definition...
		_bClass = _code.startswith( 'class ' )
		_bFunc = _code.startswith( 'def ' )

		## If we haven't detected a line containing either, simply prevent the rest of the code from running...
		if ( not _bClass and not _bFunc ):
			return


		##
		## TODO: Add basic rule system so different languages can take advantage of this ( for languages that don't use a prefix - Lua uses function x.func( ) or x.func = function where x acts as the object, library of functions without object or cloning properties, meta-table, etc.. -- PHP for example could benefit from this output but it requires optional { }s to look for scope and it requires looking at more info so a simple rule system to plug into this system would be fantastic - depth isn't always defined by indentation so depth has to be defined other ways )
		## TODO: Create Depth system - as depth isn't defined by indent-level in all languages ( python being the exception ), depth needs to be defined using other methods... But depth doesn't always mean what we have it mean here so we need to target specifically what we want to trigger depth and add a new IndentLevel accessor-func for visual output and let Depth be used for logical / scope depth..
		##


		##
		## Local - Helper Function - Resets the Active Claass data-set / AccessorFuncs to their default values... Used for depth == 0 cases when code isn't a class...
		##
		def ResetActiveClass( self ):
			## Set the Class Depth Value to 0
			self.SetActiveClassDepth( 0 )

			## Set the Active Class Name to None - ie UNSET.. Logic looks for this, if None then don't categorize...
			self.SetActiveClass( None )

			## Reset the List to an empty list... Quickest way to unset the list...
			## Task: Look into Sublime text and their GC for Python. If these aren't properly collected that this can cause issues after days or weeks running non-stop. Unlikely as it may be, if it is an issue that can arise, I don't want it in my code... I prefer to release quality instead of hacks.
			self.SetClassStack( [ ] )


		##
		## Class Depth Handler...
		##
		if ( _bClass ):
			## Depth is 0 - we reset all values ( and then set active class as well as setting depth to 1 )
			if ( _depth == 0 ):
				## If depth is 0 we need to reset - since a class was found it'll update below...
				ResetActiveClass( self )
			elif ( _depth >= self.GetActiveClassDepth( ) ):
				## We're going deeper into the rabbit hole - make sure we leave breadcrumbs to find our way out...
				self.GetClassStack( ).append( self.GetActiveClass( ) )
			## Apparently not needed, but it may be... this should be called for severe nesting when there are a lot of classes on the same level or going up... need to find a code example to test this, or I'll remove it..
			else:
				## _depth is less than ActiveClassDepth without being 0... Because we're less than, we need to pop as many as levels we're going up... How many times do we pop?
				_difference = self.GetActiveClassDepth( ) - _depth

				## For every element we are supposed to pop
				for _i in range( _difference - 1 ):
					## Make sure we can pop - if we can't, we have an issue... else: print( ' >> INVALID LOGIC DETECTED!!!' ) - Add this if looking for broken logic - this shouldn't happen..
					if ( len( self.GetClassStack( ) ) > 0 ):
						self.SetActiveClass( self.GetClassStack( ).pop( ) )
					else:
						self.AddSimpleEntry( CATEGORY_XCM_ERROR, _line_number, 'Logic Error: Attempted to self.GetClassStack( ).pop( ) from an empty list! This should never happen! Code( "' + str( _code ) + '" )', 0 )
						break

			## Now that the old active class is added, if applicable, we need to update the current active class, and the active depth...
			## Added the Line Number - we use Pop, etc.. so line number will be handled properly because we use GetActiveClass to nest the entry under the parent class... Line number allows the category to act as a class header without needing to scroll up...
			self.SetActiveClass( _code + ':' + str( _line_number ) )
			self.SetActiveClassDepth( _depth + 1 )


		##
		## Function Depth Handler
		##
		if ( _bFunc ):
			## We've found a function at depth 0 - it is a global / local-to-file-scope function - Reset depth status..
			## We use our helper - note: update it to make sure entries are in - debugging technique so we can print when it is called and isn't needed... this is to track faulty logic...
			if ( _depth == 0 ):
				ResetActiveClass( self )
			else:
				## Otherwise - note: Depth should never be < 0... So we're deeper...
				if ( _depth < self.GetActiveClassDepth( ) ):
					if ( len( self.GetClassStack( ) ) > 0 ):
						## If the depth == active we need to set active depth to current depth ( as it is a function, and according to the class it'll be depth + 1 by perspective..
						self.SetActiveClass( self.GetClassStack( ).pop( ) )
						self.SetActiveClassDepth( _depth )


	##
	## Callback - Used to either prevent an entry from being added by returning True / False, to alter the code being added by returning a String, to look for Syntax or other errors and add an Error Entry or Warning Entry, etc..
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Gutter Icon System
		##
		## if ( _category == CATEGORY_FUNCTION ):
			## import sublime
			## import sublime_plugin


			## class SetMarkCommand(sublime_plugin.TextCommand):
			##     def run(self, edit):
			##         mark = [s for s in self.view.sel()]
			##         self.view.add_regions("mark", mark, "mark", "dot", sublime.HIDDEN | sublime.PERSISTENT)


			## class SwapWithMarkCommand(sublime_plugin.TextCommand):
			##     def run(self, edit):
			##         old_mark = self.view.get_regions("mark")

			##         mark = [s for s in self.view.sel()]
			##         self.view.add_regions("mark", mark, "mark", "dot", sublime.HIDDEN | sublime.PERSISTENT)

			##         if len(old_mark):
			##             self.view.sel().clear()
			##             for r in old_mark:
			##                 self.view.sel().add(r)


			## class SelectToMarkCommand(sublime_plugin.TextCommand):
			##     def run(self, edit):
			##         mark = self.view.get_regions("mark")

			##         num = min(len(mark), len(self.view.sel()))

			##         regions = []
			##         for i in range(num):
			##             regions.append(self.view.sel()[i].cover(mark[i]))

			##         for i in range(num, len(self.view.sel())):
			##             regions.append(self.view.sel()[i])

			##         self.view.sel().clear()
			##         for r in regions:
			##             self.view.sel().add(r)


			## class DeleteToMark(sublime_plugin.TextCommand):
			##     def run(self, edit):
			##         self.view.run_command("select_to_mark")
			##         self.view.run_command("add_to_kill_ring", {"forward": False})
			##         self.view.run_command("left_delete")

			## self.GetSublimeActiveView( ).add_regions(key, [regions], scope, <icon>, <flags>)
			## self.GetSublimeActiveView( ).add_regions( "gutter", "gutter", "function", "../User/CodeMap/Assets/Icons/char_function_48x48", sublime.PERSISTENT )
			## self.GetSublimeActiveView( ).add_regions("mark", mark, "mark", "dot", sublime.HIDDEN | sublime.PERSISTENT)

		##
		## System to alter the Category system -- Insert Function into Class Nested Category... OR prefix the parent-class to the function similar to Lua... Optionally include all the way to the base class ( useful tool to show that stack system works now )!
		##
		if ( _category == CATEGORY_FUNCTION ):
			## Grab our class category...
			_class_cat = self.GetActiveClass( )

			## If the class has a category of its own - ie the class was set... move our function into it...
			if ( _class_cat != None ):
				if ( CFG_DISPLAY_FUNCTIONS_ALTERNATIVELY ):
					## Prevent Stack Overflow because we call AddSimpleEntry to allow editing further without pattern or mode...
					if ( _mode != None and _pattern != None ):
						## Set up helper var to track progress...
						_output = _code

						## Remove def prefix...
						_output = _output.replace( 'def ', '' )

						## Clean up the class-code so we're only left with the name... ( Maybe I'll add into the rule system a system to ensure only the name is inserted because re-adding the other data is easy ( although extensions can be useful in some cases )
						_class_cat = self.StripAllExceptClassName( _class_cat )

						## This is the var which tracks out progress when we are to build it ( when stack exists > 0 )
						_class_builder = ''

						## Should we build.the.full.depth.to.function( ) or should we simply show to.function( )? - The value needs to be set to true, and the stack has to have data for this to execute - otherwise only the direct-parent is used...
						if ( CFG_DISPLAY_FUNCTIONS_ALTERNATIVE_FULL_DEPTH and len( self.GetClassStack( ) ) > 0 ):
							## _class_build = ''
							for _i in range( len( self.GetClassStack( ) ) ):
								## Grab the stack value and clean the code so we're only left with the Class Name. We're moving from shallowest to deepest, we don't need to reverse the data or anything special..
								_value = self.StripAllExceptClassName( self.GetClassStack( )[ _i ] )

								## We're moving from SHALLOW >> to >> DEEP so we don't need to do anything special... Simply <String> + <NextClass> + .
								_class_builder = _class_builder + _value + '.'

						## Finalized output: def Class.FuncName( args ) - To keep things short, only the top-most class is output.. Later I may add support to go deeper or all the way...
						_output = 'def ' + _class_builder + _class_cat + '.' + _output

						## AddSimpleEntry to ensure the callbacks are called on the new monster we've build ( if full-length is set )...
						self.AddSimpleEntry( CATEGORY_CLASS_FUNCTION, _line_number, _output, _depth )

						return False
				else:
					## Our entry is redirected from CATEGORY_FUNCTION to CATEGORY_CLASS_FUNCTION and nested into _class_cat or ActiveClass...
					self.AddEntry( CATEGORY_CLASS_FUNCTION, _line_number, _code, _depth, _mode, _pattern, _class_cat )

					## Prevent the function from being added to the standard functions table ( although it can be and it can be nested there - although a separate primary category is sometimes cleaner )
					return False


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Callback - Determines the indentation level to use for entries - Categories are at -1 ( not possible to use ), 0 is indented once, nested categories indent further. Default is 4 spaces per level, can be configured to use any character combination.
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		## For functions and objects / classes.. allow depth to be based on indentation - this is primarily for Python until I add the nested categories system...
		## Note: I've left functions and categories to use depth so they'll show you their level of indentation ( useful to know in Python ) while the rest is at 0 so you don't end up with a ton of tabs for other bits of info... If you don't like the depths being shown all over the place for categories and normal functions simply comment it out and the return 0 will be used...
		if ( _category == CATEGORY_FUNCTION or _category == CATEGORY_CLASS ):
			return _depth

		## For the new nested category system - Because I modify the depth elsewhere, I subtract it here in order to re-add depth... Modding depth elsewhere is necessary in order to make the output readable because of category nesting - I subtract it here because the depth is being used instead of 0..
		## Note: Commented out for now so all nested line up... Now that we don't put classes and functions in the same category ( they're category and nested entries meaning they can be sorted without the headers being moved ) the depth-mod already modifies the output depth so 0 is category_depth + 1
		## if ( _category == CATEGORY_CLASS_FUNCTION ):
		## 	return _depth - 1

		## Return 0 for everything else..
		return 0


	##
	## Configuration - Converts Class / Function Arguments based on config ( AddArgDataTypeEntry( FUNC_OR_CLASS, 'DATA_TYPE', 'str', 'blah' ) would appear as function Name( <DATA_TYPE> str, <DATA_TYPE> blah ),  function Name( <DATA_TYPE> <DATA_TYPE> ), or function Name( str, blah )
	##
	def OnSetupArgDataType( self ):
		## Adds a replacement for 'str' argument in CLASS categories - Functions will not receive this modification... This is to show that class TestingString( str ): will be modified properly..
		self.AddArgDataTypeEntry( CATEGORY_TYPE_CLASS, 'Extends Default String Object', 'str' )

		## Adds a replacement for 'self' argument in FUNCTION categories - Classes will not receive this modification... This is to show that all functions with self as an argument will be properly modified!
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, 'This', 'self' )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupArgDataType( )		## self.CallParent( 'OnSetupArgDataType' )


	##
	## OnSetupConfig - Loads the current mapper configuration values into the active instance of the loader..
	##
	## Note: This will likely be OBSOLETE soon!!!
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Setup Current Mapper Configuration!

		## Desired Debug Mode - Set this to the name / tag to print only those, or True to print all debugging statements.. and the Language associated with the extension..
		self.SetCfgAccessorFuncValues( 'DebugMode', CFG_DEBUG_MODE, 'Language', CFG_PRIMARY_ACTIVE_LANGUAGE )

		## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
		self.SetCfgAccessorFuncValues( 'OutputFileEntriesInfo', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO, 'OutputFileEntriesInfoNewLines', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES )

		## New-Line preferred for reading Files and for outputting to the Code - Map Panel
		self.SetCfgAccessorFuncValues( 'NewLine', CFG_NEW_LINE, 'FileNewLine', CFG_FILE_NEW_LINE )

		## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
		self.SetCfgAccessorFuncValues( 'IndentChars', CFG_INDENT_CHARS, 'IndentCodeColumnMode', CFG_INDENT_CODE_COLUMN_WIDTH_MODE, 'IndentCodeColumnWidth', CFG_INDENT_CODE_COLUMN_WIDTH, 'LimitCatagoryColumnWidth', CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH )

		## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
		self.SetCfgAccessorFuncValues( 'TaskLinePrefix', CFG_TASK_LINE_PREFIX, 'TaskLineSuffix', CFG_TASK_LINE_SUFFIX )

		## Category Line Configuration - Show Description, and What to add at the end of the line ( if showing description, definitely use comment and "' to prevent highlighter mixing things up )
		self.SetCfgAccessorFuncValues( 'CategoryLineShowDesc', CFG_CATEGORY_LINE_SHOW_DESC, 'CategoryLinePrefix', CFG_CATEGORY_LINE_PREFIX, 'CategoryLineSuffix', CFG_CATEGORY_LINE_SUFFIX, 'CategoryDescPrefix', CFG_CATEGORY_DESC_PREFIX, 'CategoryDescSuffix', CFG_CATEGORY_DESC_SUFFIX )

		## Category Toggles
		self.SetCfgAccessorFuncValues( 'DisplayPersonalStory', CFG_DISPLAY_PERSONAL_STORY, 'DisplaySortMethod', CFG_CATEGORY_DISPLAY_SORT_METHOD, 'DisplayErrors', CFG_DISPLAY_CATEGORY_ERRORS, 'DisplayHelp', CFG_DISPLAY_CATEGORY_HELP, 'DisplayTasks', CFG_DISPLAY_CATEGORY_TASKS, 'DisplayNotes', CFG_DISPLAY_CATEGORY_NOTES )

		## For Function / Class Arguments Callback Settings
		self.SetCfgAccessorFuncValues( 'ObjectOpeningDelimiter', CFG_OBJECT_ARGUMENT_OPENING_DELIMITER, 'ObjectEndingDelimiter', CFG_OBJECT_ARGUMENT_ENDING_DELIMITER, 'ObjectArgSplitDelimiter', CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER, 'ObjectArgDefaultSplitDelimiter', CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER, 'ObjectArgJoinString', CFG_OBJECT_ARGUMENT_JOIN_DELIMITER, 'ObjectArgJoinPrefix', CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX, 'ObjectArgJoinSuffix', CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX )

		## Replace 'function' or 'def' with something shorter such as 'f' - Only enabled if Search is populated...
		self.SetCfgAccessorFuncValues( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH, 'FuncNameDefinitionReplace', CFG_FUNC_NAME_REPLACE )

		## Alter Function / Class Args to display Data-Types, Args or Both and set the character display data...
		self.SetCfgAccessorFuncValues( 'DisplayArgDataTypes', CFG_DISPLAY_ARG_DATA_TYPE, 'DisplayArgDataTypesSpacer', CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS, 'DisplayArgDataTypesPrefix', CFG_DISPLAY_ARG_DATA_TYPE_PREFIX, 'DisplayArgDataTypesSuffix', CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


	##
	## Accessor - This is required because we process as little as possible in Run when cached file is to be loaded meaning NO AccessorFuncs... We have to Re-add it for each language class because the CFG_ will be different ( this will be removed when sublime-settings files are used )
	##
	def GetCfgCachingSystemEnabled( self ):
		return CFG_CACHING_SYSTEM_ENABLED


	##
	## Accessor - For Developer Mode - pass-through for caching system..
	##
	def GetDeveloperMode( self ):
		return DEVELOPER_MODE


	##
	## Python Mapper Helper Functions / Custom Functions... These are usable in ALL classes which except XCodeMapper_Python!
	##


	##
	## Helper - specifically for Python mapper...
	##
	def StripAllExceptClassName( self, _code ):
		## Remove class prefix...
		_code = _code.replace( 'class ', '' )

		## If we've found (, then ) likely exists - strip it out... all the way through : - otherwise we only need to remove :
		if ( Acecool.string.indexOfExists( '(', _code, True ) ):
			## If ( is included ( for a class extending another ) Then remove ( all the way to ): - otherwise only remove :
			_code = Acecool.string.Strip( _code, Acecool.string.indexOf( '(', _code ) - 1, Acecool.string.indexOf( '):', _code ) )
		else:
			## Remove the :
			_code = _code.replace( ':', '' )

		## Returned the freshly cleaned Class Entry / Code
		return _code




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla - In Development Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



##
## I'm using the Python class for development of new functionality because XCodeMapper uses Python... These functions are place-holders for new functionality planned...
##
class XCodeMapper_Python_Dev( XCodeMapper_Python ):



	##
	## The following are implemented - they're in here for testing..
	##


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		## Note: Symbols should be all of the functions, vars, etc... We'll see... This would be HUGE in creating an automated mapper though..
		pass


	##
	## Callback - Determines the indentation level to use for entries - Categories are at -1 ( not possible to use ), 0 is indented once, nested categories indent further. Default is 4 spaces per level, can be configured to use any character combination.
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		##
		## OnCalcCodeAlignment Development Test -
		##
		## If we use OnCalcDepth and we Add     ++ depth relative to _depth ie ( return _depth + 1 or return X when _depth   <   X ), ex: we indent more on the LEFT than normal, then we DELETE the whitespace between VAR_END<HERE> all the way through <HERE>=... in the amount of how much was added on the left.. ie we simply offset the Delta to ensure the code aligns properly...
		## If we use OnCalcDepth and we Subtract-- depth relative to _depth ie ( return _depth - 1 or return 0 when _depth isn't 0 ), ex: we indent less on the LEFT than normal, then we    ADD the whitespace between the var<HERE> all the way through <HERE>=... in the amount of how much was removed on the left.. ie we simply offset the Delta to ensure the code aligns properly...
		##
		## This is so, no matter how it is written - as long as it is aligned in your code, it will be aligned in the mapper...
		##
		if ( _category == CATEGORY_CONFIG ):
			return _depth - 1

		## Return 0 for everything else..
		return 0


	##
	## The following ARE NOT IMPLEMENTED!!!!
	##


	##
	## OVerride / Helper to modify gutter icons before they're added or to prevent them from being added at all...
	## Task: Create functionality
	##
	def OnCalcGutterIcon( self ):
		## Basically - If function discovered in PreOnAddEntry we can do self.AddGutterIcon( line_number, self.GetGutterIcon( 'function', 'ƒ' ) ) and this function will let you capture it before it is added - so if we want to modify behavior ( for the _User class - to alter to a different icon return string id / or number enum of gutter icon ) or prevent it from being displayed ( Return boolean )
		## if ( _category == CATEGORY_CLASS and _icon_id == ICON_CLASS ):
		## 		## something along these lines...
		##		return ICON_CLASS_HEADER
		##
		## 		## or.. to stop it from showing up...
		## 		return False
		pass


	##
	## Override / Helper - Callback used to override the indent characters used... IE Lets say you use tabs for indenting... Now lets say you want to create a TREE style view.. Use this function... Inject code - ie injecting into tabs is easy, add at the beginning and it is absorbed, up to 3 chars without the tab jumping ( or 7 on computers which have it set to 8 spaces per tab )... Or use spaces and use my FormatColumn code to absorb your changes...
	##
	## Used to create output similar to this ( Callback is currently a WORK IN PROGRESS and is considered non-functional ):
	##
	##	┌─☼ Class Functions Table - Category...
	##	│ ├──☼ Vector Class
	##	│ │  ├─☼ ƒ X( )
	##	│ │  ├─☼ ƒ Y( )
	##	│ │  └─☼ ƒ Z( )
	##	│ │
	##	│ └──☼ Angle Class
	##	│    ├─☼ ƒ P( )
	##	│    ├─☼ ƒ Y( )
	##	│    └─☼ ƒ R( )
	##	│
	## 	└─☼ Functions Category
	##	  ├──☼ ƒ isstring( _data )
	##	  ├──☼ ƒ isnumber( _data )
	##	  ├──☼ ƒ isfunction( _data )
	##	  └──☼ ƒ isbool( _data )
	##
	def OnCalcDepthChars( self, _category, _line_number, _code, _depth, _search_mode, _pattern, _default_chars ):
		pass






##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla - In Development Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin XCodeMapper_Python_Project_AC_XCM
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class XCodeMapper_Python_Acecool_XCM( XCodeMapper_Python ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Python_Acecool_XCM'


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		## XCodeMapper Features we'd like to see.... - Category Order Entries for output order... Syntax, and more...
		self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_STARTSWITH, 'self.AddCategoryOrderEntry' )

		## When we're adding manual entries...
		self.AddSyntax( CATEGORY_XCM_OUTPUT_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddEntry', 'self.AddSimpleEntry', 'self.AddWarning', 'self.AddError', 'self.AddFeature', 'self.AddHelp' )
		self.AddSyntax( CATEGORY_XCM_SYNTAX_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddSyntax' )
		self.AddSyntax( CATEGORY_XCM_SYNTAX_RULE_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddSyntaxRule' )
		self.AddSyntax( CATEGORY_XCM_OUTPUT_ORDER_ENTRY, SEARCH_TYPE_STARTSWITH, 'self.AddCategoryOrderEntry' )

		## Accessor Funcs - These are expanded...
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'Acecool.util.AccessorFunc' )

		## Variables / Declarations
		if ( CFG_DISPLAY_CONST_DEFINITIONS ):
			self.AddSyntax( CATEGORY_CONFIG, SEARCH_TYPE_REGEX, '^CFG_\w+\s+\=' )
			self.AddSyntax( CATEGORY_ENUMERATION, SEARCH_TYPE_REGEX, '^ENUM_\w+\s+\=' )
			self.AddSyntax( CATEGORY_CONSTANT, SEARCH_TYPE_REGEX, '^CONST_\w+\s+\=', '^[A-Z_]+\s+\=' )
			## self.AddSyntax( CATEGORY_CONSTANT, SEARCH_TYPE_REGEX, '^[A-Z_]+\s+\=' )

		self.AddSyntax( CATEGORY_OPTIMIZATION, SEARCH_TYPE_STARTSWITH, 'self.print(' )


		##
		## Mapper Features
		##
		self.AddFeature( 'Detection for configuration, enumeration, constant and all upper-case variables as definitions into config, enum, and constant variable categories..' )
		self.AddFeature( 'AccessorFunc Expanders for: Acecool.util.AccessorFunc' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## Callback - Used to convert found AccessorFunc Calls into Multiple Entries in XCodeMapper so the Getter / Setter and other functions created by the AccessorFunc call are added to the Map rather than the call.
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = Acecool.table.GetValue( _args, _args_count, 0, '<_arg_tab>' )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## Creates several very useful functions...
		##
		## <Class>.Get<Name>( _default, _skip_defaults ), <Class>.Get<Name>ToString( _default ), <Class>.Get<Name>Len( _default )
		## <Class>.Set<Name>( _value ), <Class>.Reset<Name>( _default ), <Class>.Is<Name>Set( _true_on_default )
		##
		if ( _pattern == 'Acecool.util.AccessorFunc' ):
			##
			_arg_name = Acecool.table.GetValue( _args, _args_count, 1, 'NAME_NONE' )
			_arg_default = Acecool.table.GetValue( _args, _args_count, 2, 'DEFAULT_NONE' )

			_name_get		= _arg_tab + '.Get' + _arg_name + '( _default, _skip_defaults )'
			_name_str		= _arg_tab + '.Get' + _arg_name + 'ToString( _default )'
			_name_len		= _arg_tab + '.Get' + _arg_name + 'Len( _default )'
			_name_lenstr	= _arg_tab + '.Get' + _arg_name + 'LenToString( _default )'
			_name_set		= _arg_tab + '.Set' + _arg_name + '( _value )'
			_name_reset		= _arg_tab + '.Reset' + _arg_name + '( _default )'
			_name_isset		= _arg_tab + '.Is' + _arg_name + 'Set( _true_on_default )'

			## _name_get		= 'self.Get' + _name_prefix + _name + '( _default, _skip_defaults )'
			## _name_str		= 'self.Get' + _name_prefix + _name + 'ToString( _default )'
			## _name_len		= 'self.Get' + _name_prefix + _name + 'Len( _default )'
			## _name_lenstr		= 'self.Get' + _name_prefix + _name + 'LenToString( _default )'
			## _name_set		= 'self.Set' + _name_prefix + _name + '( _value )'
			## _name_reset		= 'self.Reset' + _name_prefix + _name + '( _default )'
			## _name_isset		= 'self.Is' + _name_prefix + _name + 'Set( _true_on_default )'

			## Add the Get<Name> and Set<Name> functions to CATEGORY_FUNCTION category - note, you can use CATEGORY_FUNCTION_ACCESSOR if you want it more out of the way...
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_get, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_str, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_len, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_lenstr, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_set, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_reset, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'def ' + _name_isset, _depth, _mode, _pattern )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End XCodeMapper_Python_Project_AC_XCM
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
## class XCodeMapper_Python_User( XCodeMapper_Python ):
class XCodeMapper_Python_User( XCodeMapper_Python_Acecool_XCM ):
## class XCodeMapper_Python_User( XCodeMapper_Python ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Python_User'


	##
	## Callback - Used to reconfigure the output order by returning True / False to disable the default order, or to simply make a few calls to hide categories you don't want to see.
	##
	def OnSetupCategoryOutputOrder( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCategoryOutputOrder' )

		## Call one per line or many per call - These are categories you want to hide - This overrides the CFG_ values...
		## self.AddHideCategoryEntry( CATEGORY_NOTE, CATEGORY_DEBUGGING, CATEGORY_DEFINITION, CATEGORY_OPTIMIZATION, CATEGORY_HELP )

		## Return true or false to prevent all default categories from being added so you can set up your own order from scratch..
		return None


	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		##
		##

		##
		##


		##
		## Mapper Features
		##
		## self.AddFeature( 'User Class - This is what you are to copy / paste when updates come around - Soon you will not need to! I am going to separate the classes from each other and move them so the user folder is for you..' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )
		## self.AddFeature( '' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## Callback - Used to convert found AccessorFunc Calls into Multiple Entries in XCodeMapper so the Getter / Setter and other functions created by the AccessorFunc call are added to the Map rather than the call.
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = Acecool.table.GetValue( _args, _args_count, 0, self.__name__ )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Example AccessorFunc Addition
		## self.AccessorFunc( 'Name', CFG_OR_OTHER_DEFAULT_VALUE_ )

		## Example Config AccessorFunc Addition
		## self.ConfigAccessorFunc( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		##
		## Set the values of the config additions the mapper...
		##

		## Custom A
		## self.SetCfgNameXXXXXXXName( CFG_XLUA_XXXXXXX )

		## Custom B
		## self.SetNonCfgNameXXXXXXXName( CONST_XLUA_XXXXXXX )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------