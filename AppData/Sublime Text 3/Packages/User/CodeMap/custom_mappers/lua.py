##
## XCodeMapper, Universal Code Mapper extension for 'CodeMap' - Josh 'Acecool' Moser
##
## CodeMap, by oleg-shiro, is a Sublime Text 3 Package / Extension which provides useful functionality to the editor by adding a panel to display
## 	important aspects of code, hotkeys to navigate the output, and support to double-click each output line to jump to the relevant line.
##
## XCodeMapper extends CodeMap by adding features such as categories, category types, callbacks, and more. These features and partnership allows
## 	a concise and beautiful output to be featured within the Code - Map panel while utilizing the least space possible. The callbacks further
## 	the ability to earmark important data by allowing that data to be modified or checked for errors to further aide the developer. Additionally,
## 	and probably the most important feature is the ease in which support for a new language can be integrated along with the ability to tailor
## 	the mapper to your specific project with just a few lines of function calls.
##
## Please read the comments in XCodeMapperBase.py and XCodeMapperCallbackBase.py to understand the extent of the features and abilities...
##
## Each code_map.<ext>.py script requires several things to work:
## 	- ) Standard Imports
## 	1 ) The generate function definition populated with standard calls to create a new Callback and XCodeMapper object. This function is similar to the main function of a program.
## 	2 ) The Callback object definition with the OnSetupSyntax function defined and populated with self.AddSyntax calls to earmark code. The OnSetupConfig function is also required to tell the mapper your local mapper CFG_ configuration data.
## 	3 ) The empty, or not, XCodeMapper definition - This is there so you may overwrite pre-existing XCodeMapper functions for development, testing and or customization purposes.
##
## Optional Elements can be added too:
##	1 ) `map_syntax` is an attribute that defines syntax highlight to be used for the code map text
## 	2 ) Additional functions added to Callback to extend functionality and tailor the mapper to your specific project or language needs..
##
## The Mapper formats output to the Code - Map panel in the following form to create hotlinks to the <line_number> allowing you to jump directly to that specific line in the active file: <data>:<line_number>
##


##
## Imports
##

## If Developer Mode is True, the imports will be reloaded...
DEVELOPER_MODE									= True

## Import Sublime Library
import sublime

##
import imp

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from User.CodeMap.Acecool.XCodeMapper_Definitions import *

## Import Acecool's Library of useful functions and globals / constants...
from User.CodeMap.Acecool.AcecoolST3_Library import *

## Import XCodeMapper
from User.CodeMap.Acecool.XCodeMapper import *

## Import XCodeMapper_Lua_*
## XCodeMapper_Lua_GMod = imp.load_source( 'XCodeMapper_Lua_GMod', 'C:/Users/Acecool/AppData/Roaming/Sublime Text 3/Packages/User/CodeMap/custom_mappers/code_map.lua_garrysmod.py' )
## XCodeMapper_Lua_Acecool = imp.load_source( 'XCodeMapper_Lua_Acecool', 'C:/Users/Acecool/AppData/Roaming/Sublime Text 3/Packages/User/CodeMap/custom_mappers/code_map.lua_acecool.py' )
## XCodeMapper_Lua_User = imp.load_source( 'XCodeMapper_Lua_User', 'C:/Users/Acecool/AppData/Roaming/Sublime Text 3/Packages/User/CodeMap/custom_mappers/code_map.lua_user.py' )
## Acecool.util.ImportInvalidlyNamed( 'User.CodeMap.custom_mappers.', 'code_map.lua_garrysmod', 'XCodeMapper_Lua_GMod' )
## Acecool.util.ImportInvalidlyNamed( 'User.CodeMap.custom_mappers.', 'code_map.lua_acecool', 'XCodeMapper_Lua_GMod' )
## Acecool.util.ImportInvalidlyNamed( 'User.CodeMap.custom_mappers.', 'code_map.lua_user', 'XCodeMapper_Lua_GMod' )
## import User.CodeMap.custom_mappers.xcodemapper_lua_garrysmod
## import User.CodeMap.custom_mappers.xcodemapper_lua_acecool
## import User.CodeMap.custom_mappers.xcodemapper_lua_user
## from User.CodeMap.custom_mappers.xcodemapper_lua_garrysmod import *
## from User.CodeMap.custom_mappers.xcodemapper_lua_acecool import *
## from User.CodeMap.custom_mappers.xcodemapper_lua_user import *

## Reload
if ( DEVELOPER_MODE ):
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper_Definitions' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.AcecoolST3_Library' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper' )
	## sublime_plugin.reload_plugin( 'User.CodeMap.custom_mappers.xcodemapper_lua_garrysmod' )
	## sublime_plugin.reload_plugin( 'User.CodeMap.custom_mappers.xcodemapper_lua_acecool' )
	## sublime_plugin.reload_plugin( 'User.CodeMap.custom_mappers.xcodemapper_lua_user' )

## Grab Sublime Text Settings for packages which are ignored and which are installed!
ignored		= sublime.load_settings( 'Preferences.sublime-settings' ).get( 'ignored_packages' )
installed	= sublime.load_settings( 'Package Control.sublime-settings' ).get( 'installed_packages' )

## Determine the best Python Syntax Highlighter File to use for the Mapping Panel - Detected in order: sublime-syntax / tmLanguage - in Packages/: Acecool/syntax_files/ then <PackageName>/ - defaults to Python/Python.sublime-syntax
map_syntax = Acecool.sublime.GetSyntaxFilePath( 'GMod Lua', 'Lua' )

## Because multiple languages are defined in this extension - if GMod Lua isn't found and the default is set above, we have one more chance to look for plain Lua in Lua/
if ( map_syntax == Acecool.sublime.GetDefaultSyntaxFilePath( ) ):
	map_syntax = Acecool.sublime.GetSyntaxFilePath( 'Lua' )


##
## Configuration ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Important Default Configuration Options
##

## Debugging Mode - Set this to True to display ALL debugging messages or set it to a tag / id to only print those. By default this is True so a few messages will print until the local mapper config activates and some messages will always be displayed...
## Options: True / False
CFG_DEBUG_MODE									= False


##
## Language we're using...
##
## Options: Any String
CFG_PRIMARY_ACTIVE_LANGUAGE						= 'Lua + GMod Lua + ADF'


##
## Caching System
##

## Do you want to use the caching system? ( Typically 0.5 seconds to process 5000 lines of code and 0.02 to save - 0.01 to load without needing 0.48 to reprocess... - Times from SSD Hard Drive and [ 10 year old Intel Q6600 2.4 GHz Quad OC on AIR @ GHz 3.0 on air for entire lifetime, 8GB DDR2, Asus P5k Mbd ] )
CFG_CACHING_SYSTEM_ENABLED						= True


##
## Category Related
##

## The default for Alphabetizing the data in categories.... Sort method used... By line number is already default... First seen first out... add _DESC to reverse it, or _ALPHABETICAL for A-Z or _ALPHABETICAL_DESC for Z-A
## Options: ENUM_SORT_METHOD_DEFAULT == 0-9 / ENUM_SORT_METHOD_ALPHABETICAL == A-Z / ENUM_SORT_METHOD_ALPHABETICAL_DESC == Z-A / ENUM_SORT_METHOD_LINE_NUMBER == 0-9 / ENUM_SORT_METHOD_LINE_NUMBER_DESC == 9-0
CFG_CATEGORY_DISPLAY_SORT_METHOD				= ENUM_SORT_METHOD_ALPHABETICAL

## Display the Help and Support Category? When set to True it ouputs Download and Support Information for both oleg-shilo, Creator of CodeMap which is incredibly useful Package for Sublime Text Editor, and Josh 'Acecool' Moser who created XCodeMapper which is an extension for CodeMap to add more features!
## Options: True / False
CFG_DISPLAY_CATEGORY_HELP						= False

## Display the Errors List Category - By default it is at the very top and only visible when errors are found?
## Options: True / False
CFG_DISPLAY_CATEGORY_ERRORS						= True

## Display the Tasks / TODOs / Work / etc.. Category? By default it is enabled - Tasks are typically important but they can pile up. If you want to hide the category temporarily or permanently - set it here..
## Options: True / False
CFG_DISPLAY_CATEGORY_TASKS						= True

## Display the Notes / Info / etc.. Category? By default it is enabled - however Notes aren't always important - if notes are getting in the way of other data you can hide the category temporarily or permanently...
## Options: True / False
CFG_DISPLAY_CATEGORY_NOTES						= True


##
## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
##

## Should the line be displayed?
## Options: True / False
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO				= True

## If the line is to be displayed ( above set to True ), how many new-lines should we add afterwards? 2 gives a blank line between it and the first category output..
## Options: Numerical 0 through n
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES		= 2


##
## New-Line preferred for reading Files and for outputting to the Code - Map Panel
##

## New-Line character used for the Code - Map Panel
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_NEW_LINE									= CONST_NEW_LINE_UNIX

## New-Line character used to delimit the active-file.. The new-line type will appear under TOP_MENU > View > Line Endings > [ Windows / Unix / Mac OS 9 ]
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_FILE_NEW_LINE								= CONST_NEW_LINE_UNIX


##
## Indentation
##

## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
## Options: Any String - I'd recommend using spaces if you'll be copying / pasting into Steam, otherwise Tabs..
CFG_INDENT_CHARS								= '    '

## Set this to -1 to disable the column sizing feature ( meaning line :1234 shows up right at the side of the code output to CodeMap Panel ). Set it to 1 to truncate values meaning the column will be max of that plus the line-size. Set it to 0 to try to force it to that column, but don't truncate. If it goes over, use -1 behavior on that line... This way the line numbers line up nicely most of the time..
## Options: ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT / ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE / ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED
CFG_INDENT_CODE_COLUMN_WIDTH_MODE				= ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT

## Configure the maximum width code should take up in the output before being truncated... Then the line column comes... Note, there will be an additional callback so you can define this on the fly for specific categories so if you want notes / tasks to have no limit, no worries... If you want to disable this feature set it to -1 also note that the line one will be turned off too.. Also, you can set this to a large value, just remember that spaces are used!! I'd recommend keeping it under 255 or so chars per line which is about 1080px wide if viewing on a 4k monitor with a few ticks up in font-size... But at that width you'll unlikely see the line number... keep that in mind...
## Options: Numerical 0 through n - Only Active if CFG_INDENT_CODE_COLUMN_WIDTH_MODE is something other than _DEFAULT
CFG_INDENT_CODE_COLUMN_WIDTH					= 125

## Should we limit the Category Name / Description Length? If so, use CFG_INDENT_CODE_COLUMN_WIDTH + 5 for file with 9999 max lines, or 6 with 99,999 for good measure so they'll be lined up properly...
## Options: Numerical 0 through n - Its' best to keep this on paar with CFG_INDENT_CODE_COLUMN_WIDTH
CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH		= CFG_INDENT_CODE_COLUMN_WIDTH + 6


##
## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
##

## If you want to comment out Task / TODO lines, set this to the language comment
## Options: Any String
CFG_TASK_LINE_PREFIX							= ''

## This suffix is used to ensure the rest of the Code - Map Panel doesn't show up as a single-color.. Simply use a block-comment or comment with a single and double-quote to ensure the syntax highlighter catche sit...
## Options: Any String
CFG_TASK_LINE_SUFFIX							= '\t--\''


##
## Category Output Configuration For the Code - Map Panel
##

## Category Line Prefix and Suffix - the Description variant is for before and after the description which is hidden if the above is False... This is for the entire line before and after...
CFG_CATEGORY_LINE_PREFIX						= '☼ '
CFG_CATEGORY_LINE_SUFFIX						= ''

## Should we display the Category Description?
## Options: True / False
CFG_CATEGORY_LINE_SHOW_DESC						= True

## The Prefix to be used separating the Category Name from the Description - I'd recommend something similar to ' - ', maybe with tabs, etc... You can also use Acecool.string.FormatColumn( 25, ' - ' ) to add 22 spaces after ' - '... Later I may add support for the category output to use this function so all category names / descriptions line up in the output...
## Options: Any String
CFG_CATEGORY_DESC_PREFIX						= ' = \''

## If we show the Category Description, I highly recommend you set this to use a comment for the language along with ' and " - or simply use CFG_TASK_LINE_SUFFIX - to prevent the highlighter from messing up and coloring everything one color..
## Options: Any String
CFG_CATEGORY_DESC_SUFFIX						= '\';' + CFG_TASK_LINE_SUFFIX


##
## Function / Class For-Each Argument Callback Configuration
##

## This is what separates the function or class from the arguments passed through - Example: func<OPENING_DELIMITER> x, y, z )
## Options: Any String
CFG_OBJECT_ARGUMENT_OPENING_DELIMITER			= '('

## This is what separates the arguments from the end function / class call or declaration - Example: func( x, y, z <ENDING_DELIMITER>
## Options: Any String
CFG_OBJECT_ARGUMENT_ENDING_DELIMITER			= ')'

## This is the character used to delimit or split arguments - It isn't recommended to use more than 1 char otherwise other coding-standards may not be properly read...
## Options: Any String
CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER				= ','

## This is to split a default value from an argument... ie func ( x, y, z = true ) in some languages means z, if unset, is set to true...
## Options: Any String
CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER		= '='

## This is the string used to re-join the list of modified arguments back into a string - A comma and space here provide an Airy output: func( x<JOIN_DELIMITER>y<JOIN_DELIMITER>z )
## Options: Any String
CFG_OBJECT_ARGUMENT_JOIN_DELIMITER				= ', '

## These are what comes between the arguments on either side of the rejoined args string, and the opening / ending delimiter - Spaces here provide an Airy output: func(<PREFIX>x, y, z<SUFFIX>)
## Options: Any String
CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX				= ' '
CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX				= ' '


##
## If CFG_FUNC_FIND_TEXT_REPLACEMENT is set then we look for CFG_FUNC_FIND_TEXT and replace it with CFG_FUNC_FIND_TEXT_REPLACEMENT... so def can become function, or function can become fund or f... Space-savings!
##

## If CFG_FUNC_TEXT above is not '' then set this to what you are looking to replace... Add the space in both, or neither
## Options: Any String
CFG_FUNC_NAME_SEARCH							= 'function'

## Internal helper for GLua - This defines the text to be used for function declarations as function in Callback.PreOnAddEntry is replaced to save space as some of the function / object declarations in Lua and GLua are quite long...
## Options: Any String
CFG_FUNC_NAME_REPLACE							= 'ƒ'


##
## Data-Type Options for Function / Class Arguments...
##
##	<DataType>		==		ENUM_DISPLAY_DATA_TYPE_ONLY		|	<DataType> _arg		==		ENUM_DISPLAY_DATA_TYPE_BOTH		|	_arg	==		ENUM_DISPLAY_DATA_TYPE_DEFAULT
##

## Determine how the Data-Type system should apply data... ENUM_DISPLAY_DATA_TYPE_DEFAULT == Leaves the argument as is / _TYPE_BOTH == displays <DataType> _arg - assumes ' ', '<', '>' cfg / _TYPE_ONLY == <DataType> only
## Options: ENUM_DISPLAY_DATA_TYPE_DEFAULT == _arg / ENUM_DISPLAY_DATA_TYPE_ONLY == <DataType> / ENUM_DISPLAY_DATA_TYPE_BOTH == <DataType> _arg
CFG_DISPLAY_ARG_DATA_TYPE						= ENUM_DISPLAY_DATA_TYPE_DEFAULT

## How data-types appear - for when they are added to, or replacing, function args... ie if the following 3 are: ' ' '-' 'x' they'd appear: ( -Playerx _p, -Weaponx _w ) or ' ' '<' '>' they'd appear: ( <Player> _p, <Weapon> _w )
## Options: Any String
CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS			= ' '
CFG_DISPLAY_ARG_DATA_TYPE_PREFIX				= '<'
CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX				= '>'

## If set to True, the SHORT version will be output - ie <Ent> instead of <Entity> or <W> instead of <WEAPON>, etc..
## Options: True / False
CFG_DISPLAY_ARG_DATA_TYPE_SHORT					= False


##
## Personal Information
##

## Display my personal story?
## Options: True / False
CFG_DISPLAY_PERSONAL_STORY						= False


##
## Custom Additions ( for Callback class ) Config - ie things you've added - I'd highly recommend using this area instead of hard-coding values in case you decide to share your Callback class for Language <X> with anyone else...
##

## ALua Addition - Whether or not the internal functions generated by AccessorFunc expansions will be displayed in the Code - Map Panel. The internal functions are prefixed with __s
## Options: True / False
CFG_ALUA_DISPLAY_ACCESSORFUNC_INTERNALS			= True


##
## Alternate Function Output Configuration - Do you want . / : delimited functions to show up as Library <Name> / Meta-Table <Name> / Object <Name> CATEGORY_CLASS_FUNCTION entries? This allows more data to be displayed using less width - more on point with my concise display options...
##

## If set to true, instead of [ function string.Lower( _text ); ] being displayed - [ function Lower( _text ) ] is added to the [ Library string ] Class-Function Table... Similar display to how I display Python...
## Note: functions using : have their name defined as Meta-Table <Name> or Object <Name> for the CATEGORY_CLASS_FUNCTION Header... [ self is also to the output ( in some cases ) -- NEXT UPDATE - option to choose it ]
CFG_DISPLAY_FUNCTIONS_AS_LIB_OBJ_META_PARTS		= True

## If you want most nested categories for functions to be nested under CATEGORY_CLASS_FUNCTION - set this to true.. otherwise they'll be nested under their appropriate functions.. Sometimes with this as True it appears cleaner - other times the more forced order is nice
CFG_DISPLAY_CLASS_FUNCTIONS_IN_SINGLE_CAT		= True


##
## Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Which Mapping Variant would you like to use? Each option going downward gives you everything of the level above and itself except for the USER which is for you to customize how you want.
##
## ENUM																	Class Name								Base Class				ClassDescription
ENUM_LANG_THIS_VANILLA							= SetupLangVariantMap( 'XCodeMapper_Lua',						'XCodeMapperBase',		'Lua - Vanilla / Original - Proud-To-Be-Something-Else Version!' )
ENUM_LANG_THIS_GARRYSMOD						= SetupLangVariantMap( 'XCodeMapper_Lua_GMod',					'XCodeMapper_Lua',		'GLua - Garry\'s Mod / GMod Lua Variant - Re-Added-What-Was-Lost ( continue == inverted if statement wrapping the scope, !, >, <, >=, <=, &&, || - making it easier to transcribe between other languages2222222 ) Version!' )
ENUM_LANG_THIS_GARRYSMOD_DARKRP					= SetupLangVariantMap( 'XCodeMapper_Lua_GMod_DarkRP',			'XCodeMapper_Lua_GMod',	'Garry\'s Mod / GMod Lua Variant with DarkRP Mappings' )
ENUM_LANG_THIS_GARRYSMOD_NUTSCRIPT				= SetupLangVariantMap( 'XCodeMapper_Lua_GMod_NutScript',		'XCodeMapper_Lua_GMod',	'Garry\'s Mod / GMod Lua Variant with NutScript Mappings' )
ENUM_LANG_THIS_ACECOOL							= SetupLangVariantMap( 'XCodeMapper_Lua_Acecool',				'XCodeMapper_Lua_GMod',	'ALua - Garry\'s Mod / GMod Lua Variant - AceoolDev_Framework Redistro Mappings - Utilitarian Version!' )
ENUM_LANG_THIS_USER								= SetupLangVariantMap( 'XCodeMapper_Lua_User',					'XCodeMapper_Lua',		'Lua - Everything you need to create your own extension - I added it to the end to make copying / pasting easier, for when this script updates. On Updates of this script, copy the relevent segments ( This, the config element below, and the class... That is it.. )!' )


## This option is the default and will add mappings for the Vanilla Language / Variant.
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_VANILLA

## This option will map the Garry's Mod / GMod Lua / GLua Variant of the language which adds C Style Operators, continue keyword, and AccessorFunc helper function mapping which extends to creating the <Table>:G/Set<Name>( _default_or_value )
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_GARRYSMOD

## This option will use the Garry's Mod + Vanilla Lua Mappings in addition to DarkRP Mappings
## CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_GARRYSMOD_DARKRP

## This option will use the Garry's Mod + Vanilla Lua Mappings in addition to NutScript Mappings
## CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_GARRYSMOD_NUTSCRIPT

## This option adds everything above, and also adds the AcecoolDev_Framework mappings for my Garry's Mod Framework / Redistro to help build game-modes quickly and to go with my mods. Many helper accessorfuncs are extended, and much more!
## CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_ACECOOL

## This option is a blank Class usable as a template to create mappings for your own project. It is left at the end of the file to allow for easier processing of updates - when a new updates comes out, copy and paste the object to the new file..
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_USER


##
## End Base, Custom Additions, and Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Primary / Main function used by the CodeMap back-end. This doesn't need to be edited anymore... Use the Mapper Configuration to choose which Class to run.
##
def generate( _file ):
	## Grab the Mapper via the Mapper ID / Class-Name using either the Globals dictionary or by using the active module [ Preferred ]... Run the mapper and return the output...
	return getattr( sys.modules[ __name__ ], GetLangVariantClassName( CFG_XCODEMAPPER_MAPPER_VARIANT ) )( ).Run( _file, CFG_XCODEMAPPER_MAPPER_VARIANT )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


## _code = 'testing'
## if ( _code.startswidth( 'test' ) ):
## 	print( 'woot' )
## if ( _code.endswidth( 'test' ) ):
## 	print( 'woot' )
## if ( _code.endwidth( 'test' ) ):
## 	print( 'woot' )
## if ( _code.endwith( 'test' ) ):
## 	print( 'woot' )
## if ( _code.startwith( 'test' ) ):
## 	print( 'woot' )


##
##
##
class XCodeMapper_Lua( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Lua'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		## Lua Vanilla - Note: Because comment-block for Lua starts with comment of Lua, It needs to be in this SPECIFIC ORDER: Comment block begin, COMMENT, COMMENT BLOCK END...
		## If not in this order ie comment first then comment block will bug out... if comment is last then it'll RE INIT comments on lines that have already been processed...
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '--[[' )
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '--' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, ']]' )

		## Capture print statements and add them to the debugging category - print statements can be very poor for performance, use them sparingly and only when debugging or making sparse reports of startup, shutdown, and the like procedures..
		self.AddSyntax( CATEGORY_DEBUGGING, SEARCH_TYPE_CONTAINS, 'print(' )

		## Function - funtion definitions...
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'function ' )
		self.AddSyntax( CATEGORY_FUNCTION_LOCAL, SEARCH_TYPE_STARTSWITH, 'local function ' )
		self.AddSyntax( CATEGORY_FUNCTION_OTHER, SEARCH_TYPE_CONTAINS, '= function' )

		## Constants and Important Variables ( all in uppercase, certain prefixes go in one location, others elsewhere )
		self.AddSyntax( CATEGORY_CONFIG, SEARCH_TYPE_REGEX, '^CFG_\w+\s+\=' )
		self.AddSyntax( CATEGORY_ENUMERATION, SEARCH_TYPE_REGEX, '^ENUM_\w+\s+\=' )
		self.AddSyntax( CATEGORY_CONSTANT, SEARCH_TYPE_REGEX, '^CONST_\w+\s+\=', '^[A-Z_]+\s+\=' )

		## This will detect all variable usage and declaration...
		## self.AddSyntax( CATEGORY_VARIABLE, SEARCH_TYPE_REGEX, '(local\s*)?([a-zA-Z_.0-9]+)(\W*)(([a-zA-Z_.0-9]+)?([, ]*)?)' ) )
		## Sublime text issue? (?!end|nd|d) No - it's because when a match is made, the next match looks at that position forward instead of that
		## _look_ahead = Acecool.RegEx.BuildSublimeTextLookAheadString( 'end', 'then', 'else', 'elif', 'if', 'local', 'return', 'break', 'true', 'false' )
		## self.AddSyntax( CATEGORY_VARIABLE, SEARCH_TYPE_REGEX, '([A-Za-z_]{1}[A-Za-z0-9_.]+)+' )
		## _look_ahead = Acecool.RegEx.BuildSublimeTextLookAheadString( 'end|then|else|elif|if|local|return|break|true|false' )
		##(?!end|then|else|elif|if|local|return|break|true|false)

		## Working for the most part - still need to exclude items within strings instead of only first, etc...
		## self.AddSyntax( CATEGORY_VARIABLE, SEARCH_TYPE_REGEX, '([A-Za-z_\"\']{1}[A-Za-z0-9_.:\(\"\']+)+' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task:
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		print( ' >> Symbols: ' + str( _symbols ) )


	##
	## Callback - Variables detected are passed through here... This is so we can determine errors because of variables in case-sensitive languages are incorrectly typed, not declared, etc... Even in loosely declared languages, they typically need to be set before trying to read a value or perform math, etc..
	##
	def OnVariableDetection( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):

		## ## Basic rules for Lua - if it is a key-word, ignore it... If it starts with a number, ignore it...
		## ## _result = re.findall( _pattern, _code )
		## ## _results = str( _result )

		## ## _result = re.search( _pattern, _code )
		## ## _results = str( _result.group( 0 ) )
		## ## _tab = _result.groups( )

		## _result = Acecool.string.FindAll( _pattern, _code )
		## _results = str( _result )

		## _output = Acecool.string.FormatColumn( 50, _code, 300, 'Variables: ' + _results )
		## print( _output )

		## ## Add the line for debugging purposes...
		## self.AddSimpleEntry( _category, _line_number, _output, _depth )

		pass


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		return 0


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Don't run in the definitions category so we can see what they are in raw form...
		##
		if ( CFG_DISPLAY_FUNCTIONS_AS_LIB_OBJ_META_PARTS and self.GetCategoryType( _category ) == CATEGORY_TYPE_FUNCTION ):
			##
			## Helpers
			##

			## Decimal Delimeter
			_dot_delim = Acecool.string.indexOf( '.', _code )

			## Semi-Colon
			_semi_delim = Acecool.string.indexOfExists( ':', _code, True )

			## VarArgs ...
			_vararg_chars =  Acecool.string.indexOf( '...', _code )

			##
			_parts = None
			_parts_0 = None
			_parts_1 = None

			## Semi first as multiple dots can be used in these instances...
			if ( _semi_delim ):
				## Basic Splitting...
				_parts = _code.split( ':' )
				_parts_0 = _parts[ 0 ]
				_parts_1 = _parts[ 1 ]

			## If . exists, but it isn't where ... is ( if . is at ... then that means . wasn't found in the name...
			elif ( _dot_delim >= 0 and _dot_delim != _vararg_chars ):
				## Basic Splitting...
				_parts_0 = Acecool.string.SubStr( _code, 0, _dot_delim )
				_parts_1 = Acecool.string.SubStr( _code, _dot_delim, len( _code ) )

			##
			if ( _parts_0 != None and _parts_1 != None ):
				##
				_lib_name = _parts_0

				## _lib_name = _lib_name.replace( 'function ', '' )
				_lib_name = _lib_name.replace( 'function', '' )
				## _lib_name = _lib_name.replace( '= ( )', '' )
				_lib_name = _lib_name.replace( CFG_FUNC_NAME_REPLACE + ' ', '' )

				if ( Acecool.string.indexOf( '=', _lib_name ) > 0 ):
					_lib_name = _lib_name.split( '=' )[ 0 ]

				_lib_name = _lib_name.strip( )

				## Grab the function key... - Not all functions use a single decimal which is why this one goes first - : is easier to spot... decimal requires a bit more work to get right...
				_func_name = _parts_1

				## If we want to display all class-functions in a single category - allow it... except for internal functions as these grow to be MANY..
				if ( CFG_DISPLAY_CLASS_FUNCTIONS_IN_SINGLE_CAT and _category != CATEGORY_FUNCTION_INTERNAL ):
					_category = CATEGORY_CLASS_FUNCTION

				self.AddSimpleEntry( _category, _line_number, 'function ' + _func_name, _depth, '' + _lib_name + ' under ' + self.GetCategoryName( _category ) + ' as a Class / Object / Meta-Table' )

				## Prevent the default function from being added as an entry
				return False

		## Shorten function to f.... But only if the search term is populated...
		if ( len( self.GetCfgFuncNameDefinitionSearch( ) ) > 0 ):
			if ( _code.startswith( self.GetCfgFuncNameDefinitionSearch( ) ) ):
				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( 'PreOnAddEntry', 'OnPreDataAdded / Stripping Function' )

				_code = Acecool.string.SubStr( _code, len( self.GetCfgFuncNameDefinitionSearch( ) ), len( _code ) )
				_code = self.GetCfgFuncNameDefinitionReplace( ) + _code


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Create the AccessorFunc which controls whether or not internal functions will be displayed in the Code - Map Panel..
		self.ConfigAccessorFunc( 'DisplayAccessorFuncInternals', CFG_ALUA_DISPLAY_ACCESSORFUNC_INTERNALS )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## OnSetupConfig - Loads the current mapper configuration values into the active instance of the loader..
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Setup Current Mapper Configuration!

		## Desired Debug Mode - Set this to the name / tag to print only those, or True to print all debugging statements.. and the Language associated with the extension..
		self.SetCfgAccessorFuncValues( 'DebugMode', CFG_DEBUG_MODE, 'Language', CFG_PRIMARY_ACTIVE_LANGUAGE )

		## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
		self.SetCfgAccessorFuncValues( 'OutputFileEntriesInfo', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO, 'OutputFileEntriesInfoNewLines', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES )

		## New-Line preferred for reading Files and for outputting to the Code - Map Panel
		self.SetCfgAccessorFuncValues( 'NewLine', CFG_NEW_LINE, 'FileNewLine', CFG_FILE_NEW_LINE )

		## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
		self.SetCfgAccessorFuncValues( 'IndentChars', CFG_INDENT_CHARS, 'IndentCodeColumnMode', CFG_INDENT_CODE_COLUMN_WIDTH_MODE, 'IndentCodeColumnWidth', CFG_INDENT_CODE_COLUMN_WIDTH, 'LimitCatagoryColumnWidth', CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH )

		## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
		self.SetCfgAccessorFuncValues( 'TaskLinePrefix', CFG_TASK_LINE_PREFIX, 'TaskLineSuffix', CFG_TASK_LINE_SUFFIX )

		## Category Line Configuration - Show Description, and What to add at the end of the line ( if showing description, definitely use comment and "' to prevent highlighter mixing things up )
		self.SetCfgAccessorFuncValues( 'CategoryLineShowDesc', CFG_CATEGORY_LINE_SHOW_DESC, 'CategoryLinePrefix', CFG_CATEGORY_LINE_PREFIX, 'CategoryLineSuffix', CFG_CATEGORY_LINE_SUFFIX, 'CategoryDescPrefix', CFG_CATEGORY_DESC_PREFIX, 'CategoryDescSuffix', CFG_CATEGORY_DESC_SUFFIX )

		## Category Toggles
		self.SetCfgAccessorFuncValues( 'DisplayPersonalStory', CFG_DISPLAY_PERSONAL_STORY, 'DisplaySortMethod', CFG_CATEGORY_DISPLAY_SORT_METHOD, 'DisplayErrors', CFG_DISPLAY_CATEGORY_ERRORS, 'DisplayHelp', CFG_DISPLAY_CATEGORY_HELP, 'DisplayTasks', CFG_DISPLAY_CATEGORY_TASKS, 'DisplayNotes', CFG_DISPLAY_CATEGORY_NOTES )

		## For Function / Class Arguments Callback Settings
		self.SetCfgAccessorFuncValues( 'ObjectOpeningDelimiter', CFG_OBJECT_ARGUMENT_OPENING_DELIMITER, 'ObjectEndingDelimiter', CFG_OBJECT_ARGUMENT_ENDING_DELIMITER, 'ObjectArgSplitDelimiter', CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER, 'ObjectArgDefaultSplitDelimiter', CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER, 'ObjectArgJoinString', CFG_OBJECT_ARGUMENT_JOIN_DELIMITER, 'ObjectArgJoinPrefix', CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX, 'ObjectArgJoinSuffix', CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX )

		## Replace 'function' or 'def' with something shorter such as 'f' - Only enabled if Search is populated...
		self.SetCfgAccessorFuncValues( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH, 'FuncNameDefinitionReplace', CFG_FUNC_NAME_REPLACE )

		## Alter Function / Class Args to display Data-Types, Args or Both and set the character display data...
		self.SetCfgAccessorFuncValues( 'DisplayArgDataTypes', CFG_DISPLAY_ARG_DATA_TYPE, 'DisplayArgDataTypesSpacer', CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS, 'DisplayArgDataTypesPrefix', CFG_DISPLAY_ARG_DATA_TYPE_PREFIX, 'DisplayArgDataTypesSuffix', CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


	##
	## Callback - Called for each line for additional error-checking and for processing rules for class recognition, stack popping, etc...
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Proccess data if not in a comment and the line contains data...
		##
		if not ( _code.strip( ) == '' or _in_comment or _in_comment_block ):
			##
			## Process potential syntax errors
			##
			_xcode = self.ProcessSyntaxErrors( _line_number, _code, _depth, _in_comment, _in_comment_block )

			## If our syntax error checker found an error...
			if ( _xcode and not _xcode == True and not _xcode == False ):
				return _xcode


			##
			## Process potential logical errors
			##
			_xcode = self.ProcessLogicalErrors( _line_number, _code, _depth, _in_comment, _in_comment_block )

			## If our syntax error checker found an error...
			if ( _xcode and not _xcode == True and not _xcode == False ):
				return _xcode


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )


	##
	## Helper for Lua - Look for logical errors such as attempting to use a variable which was never defined, etc...
	##
	def ProcessLogicalErrors( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Task List
		##
		## Task: Track all defined variables ( Use RegisterVariable / AddVariableEntry or some other helper function ) and compare them to variables found in the code... If a variable is used which was never defined - add an error ... Errors to track would be if case doesn't match, etc..
		## Task:
		## Task:
		## Task:
		## Task:
		##





		pass


	##
	## Helper for Lua - Look for syntax errors...
	##
	def ProcessSyntaxErrors( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Detect Syntax Error with if / elseif / else if / else statements...
		if ( self.DetectSyntaxError_IfElse( _line_number, _code, _depth, _in_comment, _in_comment_block ) ):
			## Return the modified line - Note: If the line isn't to be added then this doesn't matter.. It won't add it simply because of an error - the error above will be added though..
			return _code + ' [ Error: If / ... / Else ]'




	##
	## Helper - Detect Syntax Error with if / elseif / else if / else statements...
	##
	def DetectSyntaxError_IfElse( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		if ( _code.startswith( 'elseif' ) or _code.startswith( 'else if' ) or _code.startswith( 'if' ) ):
			## Grab code from the next line...
			_code_pp = self.GetFileDataByLine( _line_number + 1 )

			## Helpers
			_bEndsWithEnd	= _code.endswith( 'end' )
			_bEndsWithThen	= _code.endswith( 'then' )
			_bThenExists	= Acecool.string.indexOfExists( 'then', _code, True )
			_bThenNextLine	= Acecool.string.indexOfExists( 'then', _code_pp, True ) #_code_pp.startswith( 'then' )

			## If The line ends with end and then doesn't exist somewhere - error.... If the line doesn't end with end, and then isn't the last word either, and then doesn't exist on this or the next line then error..
			if ( ( _bEndsWithEnd and not _bThenExists ) or ( not _bEndsWithThen and not _bThenExists and not _bThenNextLine ) ):
				## Add the error fix example and modify the code when this type of error is found...
				self.GenerateExample_IfElse( _line_number, _code, _depth, _in_comment, _in_comment_block )

				## Let the ProcessSyntaxErrors function know that an error was found...
				return True

		## The error was not detected...
		return False


	##
	## Helper - Generates the error for If / ... / Else statements and modifies the code...
	##
	def GenerateExample_IfElse( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Add an Error Entry
		self.AddError( 'Syntax Error: ' + _code + ' [ Error - "then" keyword missing in or at the end of the line! ]', _line_number )

		## Add a blank line before the entry to allow space between examples...
		if ( not self.HasCategoryEntries( CATEGORY_ERROR_EXAMPLE ) ):
			self.AddBlankEntry( CATEGORY_ERROR_EXAMPLE )

		## Add an example of how they should appear, but only add the example once...
		if ( not self.HasDisplayedErrorExample( 'Lua::if / ... / else' ) ):
			##
			_line = str( _line_number )

			## Create a simple if / else if structure as a copy / paste example..
			_example = ''
			_example += '-- ' + ':' + _line
			_example += '\n\t-- Lua Structure Example: if / else if / elseif / else end -- From the error on line' + ':' + _line
			_example += '\n\t-- Note: This will only appear once per file even if the same error occurs multiple times - A shorter message will appear for repeats...' + ':' + _line
			_example += '\n\t-- ' + ':' + _line
			_example += '\n\t\t-- Begin the if Statement...'
			_example += '\n\t\tif ( < Boolean Statements > ) then'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\t-- Add an elseif call...'
			_example += '\n\t\telseif ( < Boolean Statements > ) then'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\t-- else if is identical to calling else and then adding an additional if within it - however when they are side by side only a single "end" is needed afterwards...'
			_example += '\n\t\telse if ( < Boolean Statements > ) then'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\t-- The last catch-all for this statement...'
			_example += '\n\t\telse'
			_example += '\n\t\t\t< Code >'
			_example += '\n\t\tend'
			## _example += '\n\t\t'
			## _example += '\n\t\t-- Logic outside and not influenced by the if statement above...'
			## _example += '\n\t\t< Logic / Code > -- End of Example for the error occurring on line:'

			## ## Add error Example Notice of repeated error...
			## self.AddErrorExample( '\n\t[ Error - Repeat: "then" keyword missing in-line ]', _line_number )

			## Add a blank line before the entry to allow space between examples...
			if ( not self.HasCategoryEntries( CATEGORY_ERROR_EXAMPLE ) ):
				self.AddBlankEntry( CATEGORY_ERROR_EXAMPLE )

			## Add the Example...
			self.AddErrorExample( _example, _line_number )



	##
	## Accessor - This is required because we process as little as possible in Run when cached file is to be loaded meaning NO AccessorFuncs... We have to Re-add it for each language class because the CFG_ will be different ( this will be removed when sublime-settings files are used )
	##
	def GetCfgCachingSystemEnabled( self ):
		return CFG_CACHING_SYSTEM_ENABLED


	##
	## Accessor - For Developer Mode - pass-through for caching system..
	##
	def GetDeveloperMode( self ):
		return DEVELOPER_MODE


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin GLua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Custom XCodeMapper for Lua - Garry's Mod Mappings
##
## import imp
## import User.CodeMap.custom_mappers.code_map.lua
## XCodeMapper_Lua = imp.load_source( 'XCodeMapper_Lua', 'C:/Users/Acecool/AppData/Roaming/Sublime Text 3/Packages/User/CodeMap/custom_mappers/code_map.lua.py' )


##
## Garry's Mod Lua Mappings
##
class XCodeMapper_Lua_GMod( XCodeMapper_Lua ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Lua_GMod'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		## Garry's Mod Lua / GMod Lua / GLua - Note: Because comment-block for Lua starts with comment of Lua, It needs to be in this SPECIFIC ORDER: Comment block begin, COMMENT, COMMENT BLOCK END...
		## If not in this order ie comment first then comment block will bug out... if comment is last then it'll RE INIT comments on lines that have already been processed...
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '-/*' )
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '//' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, '*/' )

		##
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'AccessorFunc' )

		## Function - funtion definitions...
		self.AddSyntax( CATEGORY_FUNCTION_HOOK, SEARCH_TYPE_STARTSWITH, 'hook.Add', 'hook.Remove' )

		## Function Callback Related - net.Receive callback and Callback = function, typically used in BulletStruct...
		self.AddSyntax( CATEGORY_FUNCTION_CALLBACK, SEARCH_TYPE_CONTAINS, 'Callback = function' )

		## Networking - net.Receive could also be classified as a Callback because it is, but its classification as a networking object may be more useful...
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'net.Receive' )
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'BroadcastLua', 'self:SendLua' )
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_CONTAINS, ':SendLua' )

		##
		self.AddSyntax( CATEGORY_INFO, SEARCH_TYPE_STARTSWITH, 'cam.Start', 'cam.End' )

		## Console Commands
		self.AddSyntax( CATEGORY_CMD_CONSOLE, SEARCH_TYPE_STARTSWITH, 'concommand.Add' )

		## Realm
		self.AddSyntax( CATEGORY_REALM, SEARCH_TYPE_REGEX, '\(\s*(SERVER|CLIENT)\s*\)' )


		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		## If a function was used as an argument - it gets replaced with FUNC_REF so we can set what the function should look like - including all args - in place of it.. especially if other arguments come after as multi-line scanning isn't enabled in this mapper..
		if ( _arg == 'FUNC_REF' ):
			if ( _pattern == 'concommand.Add' ):
				_arg = 'function( _p, _cmd, _args, _args_text ) ... end, _autocomplete, _help'


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## Garry's Mod: AccessorFunc( _tab, _key, _name, _force )
		##
		if ( _pattern == 'AccessorFunc' ):
			self.AddAccessorFuncEntries_GModAccessorFunc( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Helper to add AccessorFunc data...
	##
	## Garry's Mod lua/includes/util.lua - AccessorFunc adds Get/Set<Name> functions without decent support for forcing data-types..
	##
	## function AccessorFunc( _tab, _key, _name, _crude_force_type )
	## --[[---------------------------------------------------------
	## 	AccessorFunc
	## 	Quickly make Get/Set accessor fuctions on the specified table
	## -----------------------------------------------------------]]
	## function AccessorFunc( tab, varname, name, iForce )
	##
	def AddAccessorFuncEntries_GModAccessorFunc( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Values...
		## Task: Create helper to easily return these on a single line...
		_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_key	= self.GetTableValue( _args, _args_count, 1, 'KEY_NONE' )
		_name	= self.GetTableValue( _args, _args_count, 2, 'NAME_NONE' )
		_force	= self.GetTableValue( _args, _args_count, 3, 'FORCE_NONE' )

		## The data we add to the function output...
		_force_info = ''

		## Forcing tostring, number or boolean
		if ( _force == "FORCE_STRING" ):
			_force_info = ':: Note: _value forced tostring( ) on set!'
		elif ( _force == "FORCE_NUMBER" ):
			_force_info = ':: Note: _value forced tonumber( ) on set!'
		elif ( _force == "FORCE_BOOL" ):
			_force_info = ':: Note: _value forced tobool( ) on set!'

		## // function <Class>.Get<Name>( );
		## // function <Class>.Set<Name>( _value <FORCED_TYPE_INFO> );
		## Add the Get<Name> and Set<Name> functions to CATEGORY_FUNCTION category - note, you can use CATEGORY_FUNCTION_ACCESSOR if you want it more out of the way...
		self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _tab + ':Get' + _name + '( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _tab + ':Set' + _name + '( _value ' + _force_info + ' );', _depth, _mode, _pattern )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## END GLua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin ALua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Custom XCodeMapper for Lua - AcecoolDev_Framework Mappings
##
## import imp
## import User.CodeMap.custom_mappers.code_map.lua
## XCodeMapper_Lua = imp.load_source( 'XCodeMapper_Lua', 'C:/Users/Acecool/AppData/Roaming/Sublime Text 3/Packages/User/CodeMap/custom_mappers/code_map.lua.py' )


##
## Helper Function for Data-Type System: Should we use Long vs Short output?
##
def SetupArgDataTypeOutput( _short, _long = None ):
	## If Config is set to use Short, use short... If set for Long, ie False, then if Long isn't set use Short, otherwise use Long..
	return Acecool.logic.ternary( ( CFG_DISPLAY_ARG_DATA_TYPE_SHORT == True or _long == None ), _short, _long )


##
## To be used for Arg <> DataType Translation...
##
DATA_TYPE_ENUM									= SetupArgDataTypeOutput( 'ENUM', 'ENUM' )
ENUM_TYPE_REALM									= DATA_TYPE_ENUM
ENUM_TYPE_NAMESPACE								= DATA_TYPE_ENUM

DATA_TYPE_ANY									= SetupArgDataTypeOutput( '*', '*Any' )
DATA_TYPE_UNKNOWN								= SetupArgDataTypeOutput( '%', '%Unknown' )

## Basic Data-Types
DATA_TYPE_BOOLEAN								= SetupArgDataTypeOutput( 'b', 'Boolean' )
DATA_TYPE_NUMBER								= SetupArgDataTypeOutput( 'n', 'Number' )
DATA_TYPE_NUMBER_EXP							= SetupArgDataTypeOutput( '^', 'Number EXP' )
DATA_TYPE_INTEGER								= SetupArgDataTypeOutput( 'i', 'INTeger' )
DATA_TYPE_FLOAT									= SetupArgDataTypeOutput( 'f', 'Float' )
DATA_TYPE_DOUBLE								= SetupArgDataTypeOutput( 'd', 'Double' )
DATA_TYPE_STRING								= SetupArgDataTypeOutput( 'S', 'String' )

## Special Data-Types
DATA_TYPE_VARARGS								= SetupArgDataTypeOutput( '...', '...' ) # ☼☼☼
DATA_TYPE_DELTA									= SetupArgDataTypeOutput( '▲', '▲' )

## Object / Other Data-Types
DATA_TYPE_FUNCTION								= SetupArgDataTypeOutput( 'F', 'function' )
DATA_TYPE_FUNCTION_CALLBACK						= SetupArgDataTypeOutput( 'Fc', 'Callback' )
DATA_TYPE_FUNCTION_REFERENCE					= SetupArgDataTypeOutput( 'Fr', 'FUNC_REF' )
DATA_TYPE_TABLE									= SetupArgDataTypeOutput( 'T', 'Table' )
DATA_TYPE_TABLE_INDEX							= SetupArgDataTypeOutput( 'Ti', 'Table-Index' )

DATA_TYPE_VECTOR								= SetupArgDataTypeOutput( 'V', 'Vector' )
DATA_TYPE_VECTOR_NORM							= SetupArgDataTypeOutput( 'Vn', 'VectorNorm' )
DATA_TYPE_ANGLE									= SetupArgDataTypeOutput( 'A', 'Angle' )
DATA_TYPE_COLOR									= SetupArgDataTypeOutput( 'C', 'Color' )
DATA_TYPE_COLOR_ALPHA							= SetupArgDataTypeOutput( 'Ca', 'ColorAlpha' )

DATA_TYPE_BULLET_TABLE							= SetupArgDataTypeOutput( 'BulletS', 'BulletStruct' )
DATA_TYPE_META_TABLE							= SetupArgDataTypeOutput( 'Tm', 'Meta-Table' )
DATA_TYPE_DAMAGEINFO							= SetupArgDataTypeOutput( 'Dmg', 'DmgInfo' )

DATA_TYPE_CAMERA								= SetupArgDataTypeOutput( 'Cam', 'Camera' )

DATA_TYPE_MATRIX								= SetupArgDataTypeOutput( 'Matrix', 'Matrix' )

DATA_TYPE_OBJECT								= SetupArgDataTypeOutput( 'Obj', 'Object' )
DATA_TYPE_TIMEKEEPER							= SetupArgDataTypeOutput( 'TimeK', 'TimeKeeper' )

DATA_TYPE_ENTITY								= SetupArgDataTypeOutput( 'Ent', 'Entity' )
DATA_TYPE_ENTITY_PLAYER							= SetupArgDataTypeOutput( 'P', 'Player' )
DATA_TYPE_ENTITY_PLAYER_LOCAL					= SetupArgDataTypeOutput( 'LP', 'LocalPlayer' )
DATA_TYPE_ENTITY_VEHICLE						= SetupArgDataTypeOutput( 'V', 'Vehicle' )
DATA_TYPE_ENTITY_WEAPON							= SetupArgDataTypeOutput( 'W', 'Weapon' )
DATA_TYPE_ENTITY_NPC							= SetupArgDataTypeOutput( 'NPC', 'NPC' )
DATA_TYPE_ENTITY_PHYSICS						= SetupArgDataTypeOutput( 'PhysE', 'PhysicsEnt' )
DATA_TYPE_ENTITY_NEXTBOT						= SetupArgDataTypeOutput( 'NBot', 'NextBot' )

DATA_TYPE_EFFECT								= SetupArgDataTypeOutput( 'Effect', 'Effect' )
DATA_TYPE_EFFECT_DATA							= SetupArgDataTypeOutput( 'EffectData', 'EffectData' )
DATA_TYPE_EFFECT_EMITTER						= SetupArgDataTypeOutput( 'Emitter', 'Emitter' )
DATA_TYPE_EFFECT_PARTICLE						= SetupArgDataTypeOutput( 'Particle', 'Particle' )

DATA_TYPE_VGUI_PANEL							= SetupArgDataTypeOutput( 'Panel', 'Panel' )
DATA_TYPE_IMATERIAL								= SetupArgDataTypeOutput( 'IMaterial', 'IMaterial' )
DATA_TYPE_TEXTURE								= SetupArgDataTypeOutput( 'ITexture', 'ITexture' )
DATA_TYPE_AUDIO									= SetupArgDataTypeOutput( 'AudioSource', 'AudioSource' )
DATA_TYPE_LOCOMOTION							= SetupArgDataTypeOutput( 'LocoMotion', 'LocoMotion' )
DATA_TYPE_PATH_FOLLOWER							= SetupArgDataTypeOutput( 'PathFollower', 'PathFollower' )

DATA_TYPE_TRACE									= SetupArgDataTypeOutput( 'Tr', 'Trace' )
DATA_TYPE_TRACE_RESULT							= SetupArgDataTypeOutput( 'TrR', 'TraceResult' )

DATA_TYPE_SIZE_BYTES							= SetupArgDataTypeOutput( 'Bytes', 'Bytes' )



##
## AcecoolDev_Framework, Garry's Mod, and Vanilla Lua Mappings
##
class XCodeMapper_Lua_Acecool( XCodeMapper_Lua_GMod ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_ALua'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		## TODO: Turn the line-processing system into a helper function so it can call itself - ie strip comments from a line and call the code in one call ( for classes, functions, AccessorFuncs, etc.. ) and the stripped comment in another ( to look for TODO lines )
		## From python Callback below...
		## Note: When declaring what is a comment, please remember if you set up your comments other than the default ( line 2 ) such as seen below ( line 1 ) you must put the alteration first so it can be stripped to look for TODO lines...
		## Or you can use a more expensive search method for looking for TODO lines which will miss less ( such as if the end of a declaration, on the same line, has a comment ) by using SEARCH_TYPE_CONTAINS - but, soon, I will set up a helper
		## function to handle parsing lines so if a line needs to be called once for the basics, then again on a stripped comment it won't be tedious... ie the loop on each line would be for line, code ... process_line( line, code ) and that'd
		## call itself with the stripped comment, for example...

		## Lua Vanilla - Note: Because comment-block for Lua starts with comment of Lua, It needs to be in this SPECIFIC ORDER: Comment block begin, COMMENT, COMMENT BLOCK END...
		## If not in this order ie comment first then comment block will bug out... if comment is last then it'll RE INIT comments on lines that have already been processed...
		## self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '--[[', '/*' )
		## self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '--', '//' )
		## self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, ']]', '*/' )

		## Networking
		self.AddSyntax( CATEGORY_NETWORKING, SEARCH_TYPE_STARTSWITH, 'Acecool.C.networking:Send', 'Acecool.C.networking:Broadcast' )

		##
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'Acecool:AddClassOverrides', 'Acecool:AccessorFunc', 'Acecool:AccessorSet', 'Acecool:AccessorGet', 'Acecool:AccessorSimple', 'admin:AddTool' )

		## Function Callback Related
		self.AddSyntax( CATEGORY_FUNCTION_CALLBACK, SEARCH_TYPE_STARTSWITH, 'Acecool.C.networking:SetHelper', 'Acecool.C.networking:AddReceiver' )

		## Console Commands
		self.AddSyntax( CATEGORY_CMD_CONSOLE, SEARCH_TYPE_STARTSWITH, 'dev_example.Add' )


		## Registry
		self.AddSyntax( CATEGORY_REGISTRY, SEARCH_TYPE_STARTSWITH, 'Acecool.C.networking:RegisterReceiver', 'Acecool.C.networking:RegisterSyncableFlag' )

		## Function Acecool.C. - funtion definitions... I use the META_ prefix for all of my META_ replacements such as META_ENTITY, META_PLAYER, etc... so they remain in AcecoolDev_Framework...
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'function Acecool.C.' )
		self.AddSyntax( CATEGORY_FUNCTION_HOOK, SEARCH_TYPE_STARTSWITH, 'draw.This', 'render.This', 'hook.AddTemp' )
		self.AddSyntax( CATEGORY_FUNCTION_META, SEARCH_TYPE_STARTSWITH, 'function META_' )

		## For everything else which starts with Acecool.C. - add it to the definitions category
		self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'Acecool.C.' )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupSyntax( )



	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		##
		## If argument is known to my coding standard, replace with the data-type....
		## Note: You can append the arg, or you can replace completely... You can use data-type or other info... I recommend making the info CLEAR when you replace something..
		##

		## Determine which quotes the arg is using, if any...
		_type_quote = Acecool.string.GetQuotesType( _arg )
		_quotes_double = Acecool.logic.ternary( _type_quote == '"', True, False )
		_quotes_single = Acecool.logic.ternary( _type_quote == "'", True, False )

		## Remove the quotes from the arguments... We'll re-add them later..
		_arg = Acecool.logic.ternary( ( _quotes_double == True or _quotes_single == True ), Acecool.string.StripQuotes( _arg ), _arg )

		## If a function was used as an argument - it gets replaced with FUNC_REF so we can set what the function should look like - including all args - in place of it.. especially if other arguments come after as multi-line scanning isn't enabled in this mapper..
		if ( _arg == 'FUNC_REF' ):
			if ( _pattern == 'Acecool.C.networking:SetHelper' ):
				_arg = 'function( _p, _ent, _flag, _value, _private, _category ) ... end'

			if ( _arg == 'FUNC_REF' ):
				_arg = 'function( ... ) ... end'

		##
		if ( not _arg.startswith( '_' ) and not _arg.startswith( 'function' ) and not _arg.startswith( ');' ) and not _arg.startswith( '"' ) and not _arg.startswith( "'" ) and _arg != 'self' and _arg != '...' and _arg != '' and _arg != 'FUNC_REF' ):
			## If it is something all upper-case then allow it...
			if ( _arg !=_arg.upper( ) ):
				self.AddWarning( '[ Coding Standards Issue ] Arg: ' + _arg, _line_number )

		## else:
			## ( _arg != 'If arg doesn\'t exist in list of approved arguments, then we show a warning message..' )
			## self.AddWarning( 'Argument is not in approved Coding Standards List', _line_number )

		if ( _quotes_double == True ):
			_arg = '"' + _arg + '"'

		if ( _quotes_single == True ):
			_arg = "'" + _arg + "'"


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Remove Acecool.C. from entries to shorten them - This isn't necessary when displaying class-functions in nested categories as Acecool.C. will only appear in category names...
		##
		if ( CFG_DISPLAY_FUNCTIONS_AS_LIB_OBJ_META_PARTS == False and _category != CATEGORY_DEFINITION ):
			## The search term we're looking for
			_search = 'Acecool.C.'

			## Remove it...
			_code = Acecool.string.StripAll( _search, _code )


		##
		## Shorten function to f.... But only if the search term is populated...
		##
		if ( len( self.GetCfgFuncNameDefinitionSearch( ) ) > 0 ):
			if ( _code.startswith( self.GetCfgFuncNameDefinitionSearch( ) ) ):
				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( 'PreOnAddEntry', 'OnPreDataAdded / Stripping Function' )

				_code = Acecool.string.SubStr( _code, len( self.GetCfgFuncNameDefinitionSearch( ) ), len( _code ) )
				_code = self.GetCfgFuncNameDefinitionReplace( ) + _code


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )



	##
	## Setup Data-Type to Arg Conversion. This is used for each function / class argument... This feature is to give more information to the overview panel so it can appear similar to a WIKI overview for all of the data without the long descriptions ( depending on the cfg )
	## The entire systems purpose is to give just enough data to be incredibly useful, but not enough to overwhelm or confuse.. Keep it concise.. This feature can inform you of the data-type to expect to use. For this to work, you need to set and follow coding-standards - more specifically: Argument Naming Standards sub-section.
	##
	## If the configuration is set up to do anything ( ie non-default ) then these are processed. If set to Default or _ARG then nothing happens...
	## If set to Both then they'll appear as: '<DataType> _arg' / If set to Data-Type the args will be replaced with only: '<DataType>'
	## The Space after the suffix, prefix as '<', and suffix as '>' chars and the arg is controlled by the Spacer, Prefix and Suffix Configuration Options respectfully.
	##
	def OnSetupArgDataType( self ):
		## VarArgs > Convert ... to the value set to DATA_TYPE_VARARGS but only when inside of a FUNCTION!!!
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_VARARGS, '...', )

		## Entities > Convert _ent to DATA_TYPE_ENTITY, and so on for these uniques but only when inside of a FUNCTION!!!
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY, '_ent' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_PLAYER, '_p', '_ply', '_player', '_attacker', '_victim' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_PLAYER_LOCAL, '_lp', '_client' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_WEAPON, '_w', '_weapon', '_swep' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_VEHICLE, '_v', '_veh', '_vehicle' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_NEXTBOT, '_nextbot' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_NEXTBOT, '_bot' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_PHYSICS, '_phys' )
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENTITY_NPC, '_npc' )

		## Camera > Convert _vm to DATA_TYPE_CAMERA in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_CAMERA, '_vm' )

		## Numbers > Convert these _* values to DATA_TYPE_NUMBER in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_NUMBER, '_delay', '__category', '_start_value', '_target_value', '_target_alpha', '_count', '_radius', '_duration', '_cycle', '_id' )

		## Booleans < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_BOOLEAN, '_private', '_shouldsync', '_autocomplete', '_desc', '_show_delay', '_override', '_request', '_active' )

		## Strings < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_STRING, '_name', '_flag', '_category', '_help', '_text', '_chars', '_hook', '_lang', '_phrase', '_extends_name', '_getter_prefix', '_path', '_file', '_string', '_prefix', '_suffix' )

		## Functions < _* Conversion in Category Type FUNCTION - note, most of these are callbacks...
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_FUNCTION, '_func', '_callback', '_callback_getter', '_callback_setter', '_info', 'FUNC_REF', '_func_oninit', '_func_onrefresh', '_func_onshutdown', '_func_preoninit', '_func_postoninit', '_func_preonrefresh', '_func_postonrefresh', '_func_preonshutdown', '_func_postonshutdown' )

		## Byte-Size < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_SIZE_BYTES, '_bytes' )

		## Meta-Tables < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_META_TABLE, '_meta', '__object', '_xdir', '_time', '_parent' )

		## Tables < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_TABLE, '_map', '_values', '_forced', '_allowed', '_types', '_force_types', '_force_values', '_library', '_table', '_tab', '_namespace', '_players', '_packet', '_flags', '_data_tab', '_options' )

		## Any Data-Type Acceptable < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ANY, '_data', '_default', '_value', '_start', '_key', '_index' )

		## ENUMeration < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ENUM, '_datatype', '_typeid', '_mode', '_type', '_realm', '_instruction' )

		## Angles < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_ANGLE, '_start_angle', '_target_angle', '_ang' )

		## Vectors < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_VECTOR, '_start_vector', '_target_vector', '_pos', '_vector' )

		## Colors < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_COLOR, '_start_color', '_target_color', '_color', '_col' )

		## Color Alpha < _* Conversion in Category Type FUNCTION
		self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, DATA_TYPE_COLOR_ALPHA, '_target_color_alpha' )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupArgDataType( )


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Create the AccessorFunc which controls whether or not internal functions will be displayed in the Code - Map Panel..
		self.ConfigAccessorFunc( 'DisplayAccessorFuncInternals', CFG_ALUA_DISPLAY_ACCESSORFUNC_INTERNALS )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		## Set the values of the config additions the mapper...

		## Define the value of whether or not internal functions will be displayed or not..
		self.SetCfgDisplayAccessorFuncInternals( CFG_ALUA_DISPLAY_ACCESSORFUNC_INTERNALS )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## In an attempt to slim this down to make it easier to read what was added I've converted them into helper functions...
		##

		## Single Entry... the name of the admin tool...
		if ( _pattern == 'admin:AddTool' ):
			self.AddEntry( CATEGORY_FUNCTION_OTHER, _line_number, 'Admin Tool: ' + self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' ), _depth, _mode, _pattern )

		## function Acecool:AccessorSimple( _tab, _name, _callback )
		if ( _pattern == 'Acecool:AccessorSimple' ):
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':__Set' + self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' ) + '( _value );', _depth, _mode, _pattern )

		## 3 to 9 entries based on config values... OnInit, OnShutdown, OnRefresh and Pre / Post of all 3...
		if ( _pattern == 'Acecool:AddClassOverrides' ):
			self.AddAccessorFuncEntries_AcecoolAddClassOverrides( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )

		## Full AccessorFunc functions list - all helpers for the class, setters and getters including internals...
		if ( _pattern == 'Acecool:AccessorFunc' ):
			self.AddAccessorFuncEntries_AcecoolAccessorFunc( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )

		## Setters and Setter Internals only..
		if ( _pattern == 'Acecool:AccessorSet' ):
			self.AddAccessorFuncEntries_AcecoolAccessorSet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )

		## Getters and Getter Internals only...
		if ( _pattern == 'Acecool:AccessorGet' ):
			self.AddAccessorFuncEntries_AcecoolAccessorGet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## function Acecool:AccessorFunc( _tab, _name, _default, _force_types, _force_values, _getter_prefix, _callback_getter, _callback_setter, _key ):1177
	## Note: As Acecool:AccessorFunc uses helper-functions to create everything and since we can call those - we're defining vars and setting them to true so we can ensure we don't repeat code by adding funcs here and in the helpers... this way we only need to update one location...
	##
	## Calls Acecool:AccessorSet( ), Acecool:AccessorGet( ), Acecool:AccessorHelpers( )
	##
	def AddAccessorFuncEntries_AcecoolAccessorFunc( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolHelpersKey( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolAccessorSet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolAccessorGet( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count, self.GetTableValue( _args, _args_count, 5, 'Get' ) )



	##
	## Looks for: function Acecool:AccessorSet( _tab, _name, _force_types, _force_values, _callback, _key, _extends_name )	in order to create the following helper-functions ( Whether or not they show in the Code - Map Panel is a matter of configuration )
	##
	## Always Adds to Code - Map: function <TAB>:Set<Name>( _value );
	## Configuration based additions: function <TAB>:__Has<Name>AllowedTypes( ),	<TAB>:__Get<Name>AllowedTypes( ),	<TAB>:__Set<Name>AllowedTypes( )
	## Configuration based additions: function <TAB>:__Has<Name>AllowedValues( ),	<TAB>:__Get<Name>AllowedValues( ),	<TAB>:__Set<Name>AllowedValues( )
	##
	def AddAccessorFuncEntries_AcecoolAccessorSet( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_arg_name = self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' )

		## Forced Types / Values used in the Setter - if not either the data-type, or value, or both then the data isn't set
		_arg_force_types = self.GetTableValue( _args, _args_count, Acecool.logic.ternary( ( _pattern == 'Acecool:AccessorFunc' ), 3, 2 ), '' )
		if ( _arg_force_types != '' ): _arg_force_types = ' ForcedTypes = [ ' + _arg_force_types + ' ];'

		_arg_force_values = self.GetTableValue( _args, _args_count, Acecool.logic.ternary( ( _pattern == 'Acecool:AccessorFunc' ), 4, 3 ), '' )
		if ( _arg_force_values != '' ): _arg_force_values = ' ForcedValues = [ ' + _arg_force_values + ' ];'

		## A callback to manipulate the value prior to it being returned
		_arg_callback = self.GetTableValue( _args, _args_count, Acecool.logic.ternary( ( _pattern == 'Acecool:AccessorFunc' ), 7, 4 ), '' )
		if ( _arg_callback != '' ): _arg_callback = ' Callback = [ ' + _arg_callback + ' ];'

		##
		## _accessor_helpers = True
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		self.AddAccessorFuncEntries_AcecoolHelpersKey( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		## _accessor_helpers_set = True

		## function Acecool.C.<ClassName>:Set<FuncName>( _value );
		self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':Set' + _arg_name + '( _value );' + _arg_force_types + _arg_force_values + _arg_callback, _depth, _mode, _pattern )

		## If the configuration value is True, then add the internal helper functions to the Code - Map Panel
		if ( self.GetCfgDisplayAccessorFuncInternals( ) == True ):
			## Adds helper-functions to determine whether Allowed-Types were set so they can be enforced elsewhere, to retrieve them and to reassign them using: 	function class:__Has<Name>AllowedTypes( );	class:__Get<Name>AllowedTypes( _default );	class:__Set<Name>AllowedTypes( _types );
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Has' + _arg_name + 'AllowedTypes( );' + _arg_force_types, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'AllowedTypes( _default );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'AllowedTypes( _types );', _depth, _mode, _pattern )

			## Adds helper-functions to determine whether Allowed-Values were set so they can be enforced elsewhere, to retrieve them and to reassign them using: function class:__Has<Name>AllowedValues( );	class:__Get<Name>AllowedValues( _default );	class:__Set<Name>AllowedValues( _types );
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Has' + _arg_name + 'AllowedValues( );' + _arg_force_values, _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'AllowedValues( _default );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'AllowedValues( _values );', _depth, _mode, _pattern )



	##
	## function Acecool:AccessorGet( _tab, _name, _default, _callback, _key ):1058
	##
	def AddAccessorFuncEntries_AcecoolAccessorGet( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count, _arg_getter_prefix = '' ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_arg_name	= self.GetTableValue( _args, _args_count, 1, 'NAME_NONE' )

		##
		## _accessor_helpers = True
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
		_accessor_helpers_get = True

		## function Acecool.C.<ClassName>:<Prefix><FuncName>( _default );
		self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':' + _arg_getter_prefix + _arg_name + '( _default );', _depth, _mode, _pattern )

		## function Acecool.C.<ClassName>:__Get<Name>DefaultValue
		## function Acecool.C.<ClassName>:__Set<Name>DefaultValue
		if ( self.GetCfgDisplayAccessorFuncInternals( ) == True ):
			_arg_name = self.GetTableValue( _args, _args_count, 1, '<_arg_name>' )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'DefaultValue( );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'DefaultValue( _value );', _depth, _mode, _pattern )


	##
	## function Acecool:AddClassOverrides( _tab, _func_oninit, _func_onrefresh, _func_onshutdown, _func_preoninit, _func_postoninit, _func_preonrefresh, _func_postonrefresh, _func_preonshutdown, _func_postonshutdown )
	## Creates: function <TAB>:OnInit( ); <TAB>:Pre<Name>OnInit( ); <TAB>:Post<Name>OnInit( );
	## Creates: function <TAB>:OnRefresh( ); <TAB>:Pre<Name>OnRefresh( ); <TAB>:Post<Name>OnRefresh( );
	## Creates: function <TAB>:OnShutdown( ); <TAB>:Pre<Name>OnShutdown( ); <TAB>:Post<Name>OnShutdown( );
	## Calls: Acecool:AccessorHelpers( );
	##
	def AddAccessorFuncEntries_AcecoolAddClassOverrides( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
		_arg_key	= self.GetTableValue( _args, _args_count, 1, 'KEY_NONE' )
		_arg_name	= self.GetTableValue( _args, _args_count, 2, 'NAME_NONE' )
		_arg_force	= self.GetTableValue( _args, _args_count, 3, 'FORCE_NONE' )
		_arg_class	= Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )

		## Add 3 internal functions: OnInit, OnRefresh and OnShutdown
		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':OnInit( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':OnRefresh( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':OnShutdown( );', _depth, _mode, _pattern )

		## Add 6 hooks / callbacks for developers: Pre / Post <Class> OnInit, OnRefresh and OnShutdown
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Pre' + _arg_class + 'OnInit( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Pre' + _arg_class + 'OnRefresh( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Pre' + _arg_class + 'OnShutdown( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Post' + _arg_class + 'OnInit( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Post' + _arg_class + 'OnRefresh( );', _depth, _mode, _pattern )
		self.AddEntry( CATEGORY_FUNCTION_CALLBACK, _line_number, 'function ' + _arg_tab + ':Post' + _arg_class + 'OnShutdown( );', _depth, _mode, _pattern )

		## Add the helpers
		self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## function Acecool.C.<ClassName>:__GetDataTable( );
	## function Acecool.C.<ClassName>:__SetData( _key, _value );
	## function Acecool.C.<ClassName>:__GetData( _key, _default );
	##
	def AddAccessorFuncEntries_AcecoolHelpers( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Helpers
		_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )

		## Grab the value for the current class... if it exists - otherwise false...
		_exists = self.AccessorFuncHelpersDefined.get( _arg_tab, False )

		## If no entry exists for this table - we can add the helper functions...
		if ( _exists == False ):
			## Add the output for GetDataTable, GetData, SetData if it hasn't been shown before...
			self.AccessorFuncHelpersDefined[ _arg_tab ] = True
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__GetDataTable( );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__SetData( _key, _value );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__GetData( _key, _default );', _depth, _mode, _pattern )


	##
	## function Acecool.C.<ClassName>:__Get<Name>Key
	## function Acecool.C.<ClassName>:__Set<Name>Key
	##
	def AddAccessorFuncEntries_AcecoolHelpersKey( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## If we allow these functions to be added ( can accumulate quickly )...
		if ( self.GetCfgDisplayAccessorFuncInternals( ) ):
			## Helpers
			_arg_tab	= self.GetTableValue( _args, _args_count, 0, 'CLASS_NONE' )
			_arg_name	= self.GetTableValue( _args, _args_count, 1, '<NAME_NONE>' )

			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'Key( );', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'Key( _key );', _depth, _mode, _pattern )


	##
	##
	##
	def Snippets( self ):
		pass
		## As Acecool:AccessorFunc uses helpers and those helpers can be called - this lets us define each addition in one place without repeating code so if the code is updated, we only need to update once...
		## _accessor_helpers = False
		## _accessor_helpers_set = False
		## _accessor_helpers_get = False
		## _accessor_func_set = False
		## _accessor_func_get = False
			## _accessor_helpers = True
			## _accessor_helpers_set = True
			## _accessor_helpers_get = True
			## _accessor_func_set = True
			## _accessor_func_get = True
		## ## Name is used for all of the functions.. Getter / Setter / Internals / Etc..
			## _arg_name = self.GetTableValue( _args, _args_count, 1, '<_arg_name>' )

			## ## Forced Types / Values used in the Setter - if not either the data-type, or value, or both then the data isn't set
			## _arg_force_types = self.GetTableValue( _args, _args_count, Acecool.logic.ternary( ( _pattern == 'Acecool:AccessorFunc' ), 3, 2 ), '' )
			## if ( _arg_force_types != '' ): _arg_force_types = ' ForcedTypes = [ ' + _arg_force_types + ' ];'

			## _arg_force_values = self.GetTableValue( _args, _args_count, Acecool.logic.ternary( ( _pattern == 'Acecool:AccessorFunc' ), 4, 3 ), '' )
			## if ( _arg_force_values != '' ): _arg_force_values = ' ForcedValues = [ ' + _arg_force_values + ' ];'

			## ## A callback to manipulate the value prior to it being returned
			## _arg_callback = self.GetTableValue( _args, _args_count, Acecool.logic.ternary( ( _pattern == 'Acecool:AccessorFunc' ), 7, 4 ), '' )
			## if ( _arg_callback != '' ): _arg_callback = ' Callback = [ ' + _arg_callback + ' ];'

			## ##
			## ## _accessor_helpers = True
			## self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
			## self.AddAccessorFuncEntries_AcecoolHelpersKey( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
			## ## _accessor_helpers_set = True

			## ## function Acecool.C.<ClassName>:Set<FuncName>( _value );
			## self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':Set' + _arg_name + '( _value );' + _arg_force_types + _arg_force_values + _arg_callback, _depth, _mode, _pattern )

			## ## If the configuration value is True, then add the internal helper functions to the Code - Map Panel
			## if ( self.GetCfgDisplayAccessorFuncInternals( ) == True ):
			## 	## Adds helper-functions to determine whether Allowed-Types were set so they can be enforced elsewhere, to retrieve them and to reassign them using: 	function class:__Has<Name>AllowedTypes( );	class:__Get<Name>AllowedTypes( _default );	class:__Set<Name>AllowedTypes( _types );
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Has' + _arg_name + 'AllowedTypes( );' + _arg_force_types, _depth, _mode, _pattern )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'AllowedTypes( _default );', _depth, _mode, _pattern )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'AllowedTypes( _types );', _depth, _mode, _pattern )

			## 	## Adds helper-functions to determine whether Allowed-Values were set so they can be enforced elsewhere, to retrieve them and to reassign them using: function class:__Has<Name>AllowedValues( );	class:__Get<Name>AllowedValues( _default );	class:__Set<Name>AllowedValues( _types );
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Has' + _arg_name + 'AllowedValues( );' + _arg_force_values, _depth, _mode, _pattern )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'AllowedValues( _default );', _depth, _mode, _pattern )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'AllowedValues( _values );', _depth, _mode, _pattern )


			## ## Name is used for all of the functions.. Getter / Setter / Internals / Etc..
			## _arg_name = self.GetTableValue( _args, _args_count, 1, '<_arg_name>' )

			## ##
			## ## _accessor_helpers = True
			## self.AddAccessorFuncEntries_AcecoolHelpers( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )
			## ## _accessor_helpers_get = True
			## self.AddAccessorFuncEntries_AcecoolHelpersKey( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):

			## ## function Acecool.C.<ClassName>:<Prefix><FuncName>( _default );
			## self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function ' + _arg_tab + ':' + _arg_getter_prefix + _arg_name + '( _default );', _depth, _mode, _pattern )

			## ## function Acecool.C.<ClassName>:__Get<Name>DefaultValue
			## ## function Acecool.C.<ClassName>:__Set<Name>DefaultValue
			## if ( self.GetCfgDisplayAccessorFuncInternals( ) == True ):
			## 	_arg_name = self.GetTableValue( _args, _args_count, 1, '<_arg_name>' )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'DefaultValue( );', _depth, _mode, _pattern )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'DefaultValue( _value );', _depth, _mode, _pattern )






		## if ( _accessor_helpers == True ):
		## 	## Add the output for GetDataTable, GetData, SetData if it hasn't been shown before...
		## 	_arr = self.AccessorFuncHelpersDefined.get( _arg_tab, False )
		## 	if ( _arr == False ):
		## 		self.AccessorFuncHelpersDefined[ _arg_tab ] = True
		## 		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__GetDataTable( );', _depth, _mode, _pattern )
		## 		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__SetData( _key, _value );', _depth, _mode, _pattern )
		## 		self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__GetData( _key, _default );', _depth, _mode, _pattern )


		##
		## function Acecool.C.<ClassName>:__Get<Name>Key
		## function Acecool.C.<ClassName>:__Set<Name>Key
		##
		## if ( _accessor_helpers_set == True or _accessor_helpers_get == True ):
		## 	self.AddAccessorFuncEntries_AcecoolHelpersKey( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):

			## if ( self.GetCfgDisplayAccessorFuncInternals( ) == True ):
			## 	_arg_name = self.GetTableValue( _args, _args_count, 1, '<_arg_name>' )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Get' + _arg_name + 'Key( );', _depth, _mode, _pattern )
			## 	self.AddEntry( CATEGORY_FUNCTION_INTERNAL, _line_number, 'function ' + _arg_tab + ':__Set' + _arg_name + 'Key( _key );', _depth, _mode, _pattern )



##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End ALua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Custom XCodeMapper for Lua - with DarkRP Mappings
##
## class XCodeMapper_Lua_GMod_NutScript( XCodeMapper_ALua ):
## class XCodeMapper_Lua_GMod_NutScript( XCodeMapper_Lua ):
class XCodeMapper_Lua_GMod_DarkRP( XCodeMapper_Lua_GMod ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Lua_GMod_DarkRP'






##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End ALua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Custom XCodeMapper for Lua - with NutScript Mappings
##
## class XCodeMapper_Lua_GMod_NutScript( XCodeMapper_ALua ):
## class XCodeMapper_Lua_GMod_NutScript( XCodeMapper_Lua ):
class XCodeMapper_Lua_GMod_NutScript( XCodeMapper_Lua_GMod ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Lua_GMod_NutScript'





##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End ALua
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Custom XCodeMapper for Lua - User Mappings
##
## import imp
## import User.CodeMap.custom_mappers.code_map.lua
## XCodeMapper_Lua = imp.load_source( 'XCodeMapper_Lua', 'C:/Users/Acecool/AppData/Roaming/Sublime Text 3/Packages/User/CodeMap/custom_mappers/code_map.lua.py' )


##
## Everything you need to create your own - The base class which very limited structure, the CFG_ options above and the ENUM value to use to set to it... Simply rename them...
##
## class XCodeMapper_Lua_User( XCodeMapper_ALua ):
## class XCodeMapper_Lua_User( XCodeMapper_Lua_GMod ):
class XCodeMapper_Lua_User( XCodeMapper_Lua ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_Lua_User'


	##
	## Additional Callback used for hiding default categories, or completely changing the category order.
	## Return True / False to prevent the default categories from being output - you would then need to re-add all categories you want in the order you want them to output...
	##
	def OnSetupCategoryOutputOrder( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCategoryOutputOrder' )

		## Call one per line or many per call - These are categories you want to hide - This overrides the CFG_ values...
		## self.AddHideCategoryEntry( CATEGORY_NOTE, CATEGORY_DEBUGGING, CATEGORY_DEFINITION, CATEGORY_OPTIMIZATION, CATEGORY_HELP )

		## Return true or false to prevent all default categories from being added so you can set up your own order from scratch..
		return None


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = self.GetTableValue( _args, _args_count, 0, self.__name__ )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Example AccessorFunc Addition
		## self.AccessorFunc( 'Name', CFG_OR_OTHER_DEFAULT_VALUE_ )

		## Example Config AccessorFunc Addition
		## self.ConfigAccessorFunc( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		##
		## Set the values of the config additions the mapper...
		##

		## Custom A
		## self.SetCfgNameXXXXXXXName( CFG_XLUA_XXXXXXX )

		## Custom B
		## self.SetNonCfgNameXXXXXXXName( CONST_XLUA_XXXXXXX )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------