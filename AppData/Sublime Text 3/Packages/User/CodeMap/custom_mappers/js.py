##
## XCodeMapper, Universal Code Mapper extension for 'CodeMap' - Josh 'Acecool' Moser
##
## CodeMap, by oleg-shiro, is a Sublime Text 3 Package / Extension which provides useful functionality to the editor by adding a panel to display
## 	important aspects of code, hotkeys to navigate the output, and support to double-click each output line to jump to the relevant line.
##
## XCodeMapper extends CodeMap by adding features such as categories, category types, callbacks, and more. These features and partnership allows
## 	a concise and beautiful output to be featured within the Code - Map panel while utilizing the least space possible. The callbacks further
## 	the ability to earmark important data by allowing that data to be modified or checked for errors to further aide the developer. Additionally,
## 	and probably the most important feature is the ease in which support for a new language can be integrated along with the ability to tailor
## 	the mapper to your specific project with just a few lines of function calls.
##
## Please read the comments in XCodeMapperBase.py and XCodeMapperCallbackBase.py to understand the extent of the features and abilities...
##
## Each code_map.<ext>.py script requires several things to work:
## 	- ) Standard Imports
## 	1 ) The generate function definition populated with standard calls to create a new Callback and XCodeMapper object. This function is similar to the main function of a program.
## 	2 ) The Callback object definition with the OnSetupSyntax function defined and populated with self.AddSyntax calls to earmark code. The OnSetupConfig function is also required to tell the mapper your local mapper CFG_ configuration data.
## 	3 ) The empty, or not, XCodeMapper definition - This is there so you may overwrite pre-existing XCodeMapper functions for development, testing and or customization purposes.
##
## Optional Elements can be added too:
##	1 ) `map_syntax` is an attribute that defines syntax highlight to be used for the code map text
## 	2 ) Additional functions added to Callback to extend functionality and tailor the mapper to your specific project or language needs..
##
## The Mapper formats output to the Code - Map panel in the following form to create hotlinks to the <line_number> allowing you to jump directly to that specific line in the active file: <data>:<line_number>
##


##
## Imports
##

## If Developer Mode is True, the imports will be reloaded...
DEVELOPER_MODE									= True

## Import Sublime Library
import sublime

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from User.CodeMap.Acecool.XCodeMapper_Definitions import *

## Import Acecool's Library of useful functions and globals / constants...
from User.CodeMap.Acecool.AcecoolST3_Library import *

## Import XCodeMapper
from User.CodeMap.Acecool.XCodeMapper import *

## Reload
if ( DEVELOPER_MODE ):
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper_Definitions' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.AcecoolST3_Library' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper' )

## Grab Sublime Text Settings for packages which are ignored and which are installed!
ignored		= sublime.load_settings( 'Preferences.sublime-settings' ).get( 'ignored_packages' )
installed	= sublime.load_settings( 'Package Control.sublime-settings' ).get( 'installed_packages' )

## Determine the best Python Syntax Highlighter File to use for the Mapping Panel - Detected in order: sublime-syntax / tmLanguage - in Packages/: Acecool/syntax_files/ then <PackageName>/ - defaults to Python/Python.sublime-syntax
map_syntax = Acecool.sublime.GetSyntaxFilePath( 'JavaScript' )


##
## Configuration ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Important Default Configuration Options
##

## Debugging Mode - Set this to True to display ALL debugging messages or set it to a tag / id to only print those. By default this is True so a few messages will print until the local mapper config activates and some messages will always be displayed...
## Options: True / False
CFG_DEBUG_MODE									= False


##
## Language we're using...
##
## Options: Any String
CFG_PRIMARY_ACTIVE_LANGUAGE						= 'JavaScript'


##
## Caching System
##

## Do you want to use the caching system? ( Typically 0.5 seconds to process 5000 lines of code and 0.02 to save - 0.01 to load without needing 0.48 to reprocess... - Times from SSD Hard Drive and [ 10 year old Intel Q6600 2.4 GHz Quad OC on AIR @ GHz 3.0 on air for entire lifetime, 8GB DDR2, Asus P5k Mbd ] )
CFG_CACHING_SYSTEM_ENABLED						= True


##
## Category Related
##

## The default for Alphabetizing the data in categories.... Sort method used... By line number is already default... First seen first out... add _DESC to reverse it, or _ALPHABETICAL for A-Z or _ALPHABETICAL_DESC for Z-A
## Options: ENUM_SORT_METHOD_DEFAULT == 0-9 / ENUM_SORT_METHOD_ALPHABETICAL == A-Z / ENUM_SORT_METHOD_ALPHABETICAL_DESC == Z-A / ENUM_SORT_METHOD_LINE_NUMBER == 0-9 / ENUM_SORT_METHOD_LINE_NUMBER_DESC == 9-0
CFG_CATEGORY_DISPLAY_SORT_METHOD				= ENUM_SORT_METHOD_DEFAULT

## Display the Help and Support Category? When set to True it ouputs Download and Support Information for both oleg-shilo, Creator of CodeMap which is incredibly useful Package for Sublime Text Editor, and Josh 'Acecool' Moser who created XCodeMapper which is an extension for CodeMap to add more features!
## Options: True / False
CFG_DISPLAY_CATEGORY_HELP						= False

## Display the Errors List Category - By default it is at the very top and only visible when errors are found?
## Options: True / False
CFG_DISPLAY_CATEGORY_ERRORS						= True

## Display the Tasks / TODOs / Work / etc.. Category? By default it is enabled - Tasks are typically important but they can pile up. If you want to hide the category temporarily or permanently - set it here..
## Options: True / False
CFG_DISPLAY_CATEGORY_TASKS						= True

## Display the Notes / Info / etc.. Category? By default it is enabled - however Notes aren't always important - if notes are getting in the way of other data you can hide the category temporarily or permanently...
## Options: True / False
CFG_DISPLAY_CATEGORY_NOTES						= True


##
## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
##

## Should the line be displayed?
## Options: True / False
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO				= True

## If the line is to be displayed ( above set to True ), how many new-lines should we add afterwards? 2 gives a blank line between it and the first category output..
## Options: Numerical 0 through n
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES		= 2



##
## New-Line preferred for reading Files and for outputting to the Code - Map Panel
##

## New-Line character used for the Code - Map Panel
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_NEW_LINE									= CONST_NEW_LINE_UNIX

## New-Line character used to delimit the active-file.. The new-line type will appear under TOP_MENU > View > Line Endings > [ Windows / Unix / Mac OS 9 ]
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_FILE_NEW_LINE								= CONST_NEW_LINE_UNIX


##
## Indentation
##

## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
## CFG_INDENT_CHARS								= '\t'
## Options: Any String - I'd recommend using spaces if you'll be copying / pasting into Steam, otherwise Tabs..
CFG_INDENT_CHARS								= '    '

## Set this to -1 to disable the column sizing feature ( meaning line :1234 shows up right at the side of the code output to CodeMap Panel ). Set it to 1 to truncate values meaning the column will be max of that plus the line-size. Set it to 0 to try to force it to that column, but don't truncate. If it goes over, use -1 behavior on that line... This way the line numbers line up nicely most of the time..
## Options: ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT / ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE / ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED
CFG_INDENT_CODE_COLUMN_WIDTH_MODE				= ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED

## Configure the maximum width code should take up in the output before being truncated... Then the line column comes... Note, there will be an additional callback so you can define this on the fly for specific categories so if you want notes / tasks to have no limit, no worries... If you want to disable this feature set it to -1 also note that the line one will be turned off too.. Also, you can set this to a large value, just remember that spaces are used!! I'd recommend keeping it under 255 or so chars per line which is about 1080px wide if viewing on a 4k monitor with a few ticks up in font-size... But at that width you'll unlikely see the line number... keep that in mind...
## Options: Numerical 0 through n - Only Active if CFG_INDENT_CODE_COLUMN_WIDTH_MODE is something other than _DEFAULT
CFG_INDENT_CODE_COLUMN_WIDTH					= 125

## Should we limit the Category Name / Description Length? If so, use CFG_INDENT_CODE_COLUMN_WIDTH + 5 for file with 9999 max lines, or 6 with 99,999 for good measure so they'll be lined up properly...
## Options: Numerical 0 through n - Its' best to keep this on paar with CFG_INDENT_CODE_COLUMN_WIDTH
CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH		= CFG_INDENT_CODE_COLUMN_WIDTH + 6


##
## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
##

## If you want to comment out Task / TODO lines, set this to the language comment
## Options: Any String
CFG_TASK_LINE_PREFIX							= ''

## This suffix is used to ensure the rest of the Code - Map Panel doesn't show up as a single-color.. Simply use a block-comment or comment with a single and double-quote to ensure the syntax highlighter catche sit...
## Options: Any String
CFG_TASK_LINE_SUFFIX							= '\t//"\''


##
## Category Output Configuration For the Code - Map Panel
##

## Category Line Prefix and Suffix - the Description variant is for before and after the description which is hidden if the above is False... This is for the entire line before and after...
CFG_CATEGORY_LINE_PREFIX						= '☼ '
CFG_CATEGORY_LINE_SUFFIX						= ''

## Should we display the Category Description?
## Options: True / False
CFG_CATEGORY_LINE_SHOW_DESC						= True

## The Prefix to be used separating the Category Name from the Description - I'd recommend something similar to ' - ', maybe with tabs, etc... You can also use Acecool.string.FormatColumn( 25, ' - ' ) to add 22 spaces after ' - '... Later I may add support for the category output to use this function so all category names / descriptions line up in the output...
## Options: Any String
CFG_CATEGORY_DESC_PREFIX						= ' = \''

## If we show the Category Description, I highly recommend you set this to use a comment for the language along with ' and " - or simply use CFG_TASK_LINE_SUFFIX - to prevent the highlighter from messing up and coloring everything one color..
## Options: Any String
CFG_CATEGORY_DESC_SUFFIX						= '\';' + CFG_TASK_LINE_SUFFIX


##
## Function / Class For-Each Argument Callback Configuration
##

## This is what separates the function or class from the arguments passed through - Example: func<OPENING_DELIMITER> x, y, z )
## Options: Any String
CFG_OBJECT_ARGUMENT_OPENING_DELIMITER			= '('

## This is what separates the arguments from the end function / class call or declaration - Example: func( x, y, z <ENDING_DELIMITER>
## Options: Any String
CFG_OBJECT_ARGUMENT_ENDING_DELIMITER			= ')'

## This is the character used to delimit or split arguments - It isn't recommended to use more than 1 char otherwise other coding-standards may not be properly read...
## Options: Any String
CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER				= ','

## This is to split a default value from an argument... ie func ( x, y, z = true ) in some languages means z, if unset, is set to true...
## Options: Any String
CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER		= '='

## This is the string used to re-join the list of modified arguments back into a string - A comma and space here provide an Airy output: func( x<JOIN_DELIMITER>y<JOIN_DELIMITER>z )
## Options: Any String
CFG_OBJECT_ARGUMENT_JOIN_DELIMITER				= ', '

## These are what comes between the arguments on either side of the rejoined args string, and the opening / ending delimiter - Spaces here provide an Airy output: func(<PREFIX>x, y, z<SUFFIX>)
## Options: Any String
CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX				= ' '
CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX				= ' '


##
## If CFG_FUNC_FIND_TEXT_REPLACEMENT is set then we look for CFG_FUNC_FIND_TEXT and replace it with CFG_FUNC_FIND_TEXT_REPLACEMENT... so def can become function, or function can become fund or f... Space-savings!
##

## If CFG_FUNC_TEXT above is not '' then set this to what you are looking to replace... Add the space in both, or neither
## Options: Any String
CFG_FUNC_NAME_SEARCH							= 'function'

## Internal helper for GLua - This defines the text to be used for function declarations as function in Callback.PreOnAddEntry is replaced to save space as some of the function / object declarations in Lua and GLua are quite long...
## Options: Any String
CFG_FUNC_NAME_REPLACE							= 'ƒ'


##
## Data-Type Options for Function / Class Arguments...
##
##	<DataType>		==		ENUM_DISPLAY_DATA_TYPE_ONLY		|	<DataType> _arg		==		ENUM_DISPLAY_DATA_TYPE_BOTH		|	_arg	==		ENUM_DISPLAY_DATA_TYPE_DEFAULT
##

## Determine how the Data-Type system should apply data... ENUM_DISPLAY_DATA_TYPE_DEFAULT == Leaves the argument as is / _TYPE_BOTH == displays <DataType> _arg - assumes ' ', '<', '>' cfg / _TYPE_ONLY == <DataType> only
## Options: ENUM_DISPLAY_DATA_TYPE_DEFAULT == _arg / ENUM_DISPLAY_DATA_TYPE_ONLY == <DataType> / ENUM_DISPLAY_DATA_TYPE_BOTH == <DataType> _arg
CFG_DISPLAY_ARG_DATA_TYPE						= ENUM_DISPLAY_DATA_TYPE_ONLY

## How data-types appear - for when they are added to, or replacing, function args... ie if the following 3 are: ' ' '-' 'x' they'd appear: ( -Playerx _p, -Weaponx _w ) or ' ' '<' '>' they'd appear: ( <Player> _p, <Weapon> _w )
## Options: Any String
CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS			= ' '
CFG_DISPLAY_ARG_DATA_TYPE_PREFIX				= '<'
CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX				= '>'


##
## Personal Information
##

## Display my personal story?
## Options: True / False
CFG_DISPLAY_PERSONAL_STORY						= False


##
## Custom Additions ( for Callback class ) Config - ie things you've added - I'd highly recommend using this area instead of hard-coding values in case you decide to share your Callback class for Language <X> with anyone else...
##


##
##
##

## Default for the last-tag AccessorFunc
## Options: Any String
CFG_ACECOOL_JAVASCRIPT_DEFAULT_LAST_TAG			= 'DEFAULT'


##
## Comment Based Additions
##

## I added this feature for the JavaScript version because I also have a user-script released which has a large example section commented out... If this is True then JavaScript Comment Blocks /* and */ will not be registered in OnSetupSyntax so the examples inside a giant block-comment will appear in the CodeMap panel...
## Options: True / False
CFG_SHOW_COMMENT_BLOCK_DATA						= True;


##
## Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Which Mapping Variant would you like to use? Each option going downward gives you everything of the level above and itself except for the USER which is for you to customize how you want.
##
## ENUM																	Class Name									Base Class					ClassDescription
ENUM_LANG_THIS_VANILLA							= SetupLangVariantMap( 'XCodeMapper_JavaScript',					'XCodeMapperBase',			'JavaScript - Standard Edition' )
ENUM_LANG_THIS_ACECOOL_WEBMOD					= SetupLangVariantMap( 'XCodeMapper_JavaScript_Acecool',			'XCodeMapper_JavaScript',	'JavaScript - With the extras to map my Ultimate Site Script which has the start to an eBay AJAX Shopping Cart, BitBucket Link Mods to always map to master/ for linking others, unless in history mode / diff, etc.... and Steam Workshop downloader - porting that into the script soon!' )
ENUM_LANG_THIS_USER								= SetupLangVariantMap( 'XCodeMapper_JavaScript_User',				'XCodeMapper_JavaScript',	'JavaScript - Everything you need to create your own extension - I added it to the end to make copying / pasting easier, for when this script updates. On Updates of this script, copy the relevent segments ( This, the config element below, and the class... That is it.. )!' )


## This option is the default and will add mappings for the Vanilla Language / Variant.
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_VANILLA

## This option adds the above, and also maps the functions of my 'Ultimate Site Addon Script' Website Modifier for a Live-Wiki style experience with hot-linking to the code in question...
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_ACECOOL_WEBMOD

## This option is a blank Class usable as a template to create mappings for your own project. It is left at the end of the file to allow for easier processing of updates - when a new updates comes out, copy and paste the object to the new file..
## CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_USER


##
## End Base, Custom Additions, and Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Primary / Main function used by the CodeMap back-end. This doesn't need to be edited anymore... Use the Mapper Configuration to choose which Class to run.
##
def generate( _file ):
	## Grab the Mapper via the Mapper ID / Class-Name using either the Globals dictionary or by using the active module [ Preferred ]... Run the mapper and return the output...
	return getattr( sys.modules[ __name__ ], GetLangVariantClassName( CFG_XCODEMAPPER_MAPPER_VARIANT ) )( ).Run( _file, CFG_XCODEMAPPER_MAPPER_VARIANT )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class XCodeMapper_JavaScript( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_JavaScript'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, '//' )

		## If False then we register block-comments to hide the code examples in my universal site feature adder userscript...
		if ( self.GetCfgParseCodeWithinCommentBlock( ) == False ):
			self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_STARTSWITH, '/*' )
			self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_STARTSWITH, '*/' )

		##
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_STARTSWITH, 'function ' )
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_CONTAINS, '= function', '= async function' )

		##
		self.AddSyntax( CATEGORY_DEBUGGING, SEARCH_TYPE_STARTSWITH, 'console.log' )
		## self.AddSyntax( CATEGORY_DEFINITION, SEARCH_TYPE_STARTSWITH, 'var ', 'return ', 'const ' )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		## If a function was used as an argument - it gets replaced with FUNC_REF so we can set what the function should look like - including all args - in place of it.. especially if other arguments come after as multi-line scanning isn't enabled in this mapper..
		if ( _arg == '' ):
			self.AddWarning( 'Possible Syntax Error - function argument empty which may be due to an extra comma..', _line_number );


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		## Check for syntax error
		##
		if ( _line_number != '' ):
			if ( _pattern == 'var ' or _pattern == 'return ' or _pattern == 'const ' ):
				## If a definition doesn't end with ; then mark it - but only if the definition isn't a table or something - ie ends with the opening bracket.. and it isn't a function definition with { on the next line... and comments aren't obscuring the end of the line
				## Task: Ensure _code is the edited code which is passed through with comments STRIPPED!!! Add _code_raw arg for the unedited line..
				if ( not _code.endswith( ';' ) and not _code.endswith( '{' ) and not self.GetFileDataByLine( _line_number + 1 ).startswith( '{' ) and not Acecool.string.indexOfExists( '//', _code, True ) ):
					self.AddError( 'Syntax Error - Definition line does not end with ;', _line_number );
					_code = _code + ' [ ERROR ]'

			##
			if ( _pattern == 'console.log' and Acecool.string.indexOfExists( '..', _code, False ) ):
				## Mark the error so the developer can fix it..
				self.AddError( 'Possible Syntax Error: Using .. for concatenation instead of +', _line_number );

				## Add an error tag to the code in question to make it noticeable as the error only describes the type and line number to keep it concise..
				_code = _code + ' [ ERROR ]'


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Example AccessorFunc Addition
		## self.AccessorFunc( 'Name', CFG_OR_OTHER_DEFAULT_VALUE_ )

		## Example Config AccessorFunc Addition
		## self.ConfigAccessorFunc( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH )

		## Config: Should code within comment-blocks be parsed as code or as comments? If parsed as comments, they're ignored except for notes / tasks, etc.. If parsed, they show up in the mapping panel - Useful for the EXAMPLE tag code for copy / paste in the ultimate site feature injector script...
		self.ConfigAccessorFunc( 'ParseCodeWithinCommentBlock', CFG_SHOW_COMMENT_BLOCK_DATA )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## OnSetupConfig - Loads the current mapper configuration values into the active instance of the loader..
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Setup Current Mapper Configuration!

		## Desired Debug Mode - Set this to the name / tag to print only those, or True to print all debugging statements.. and the Language associated with the extension..
		self.SetCfgAccessorFuncValues( 'DebugMode', CFG_DEBUG_MODE, 'Language', CFG_PRIMARY_ACTIVE_LANGUAGE )

		## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
		self.SetCfgAccessorFuncValues( 'OutputFileEntriesInfo', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO, 'OutputFileEntriesInfoNewLines', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES )

		## New-Line preferred for reading Files and for outputting to the Code - Map Panel
		self.SetCfgAccessorFuncValues( 'NewLine', CFG_NEW_LINE, 'FileNewLine', CFG_FILE_NEW_LINE )

		## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
		self.SetCfgAccessorFuncValues( 'IndentChars', CFG_INDENT_CHARS, 'IndentCodeColumnMode', CFG_INDENT_CODE_COLUMN_WIDTH_MODE, 'IndentCodeColumnWidth', CFG_INDENT_CODE_COLUMN_WIDTH, 'LimitCatagoryColumnWidth', CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH )

		## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
		self.SetCfgAccessorFuncValues( 'TaskLinePrefix', CFG_TASK_LINE_PREFIX, 'TaskLineSuffix', CFG_TASK_LINE_SUFFIX )

		## Category Line Configuration - Show Description, and What to add at the end of the line ( if showing description, definitely use comment and "' to prevent highlighter mixing things up )
		self.SetCfgAccessorFuncValues( 'CategoryLineShowDesc', CFG_CATEGORY_LINE_SHOW_DESC, 'CategoryLinePrefix', CFG_CATEGORY_LINE_PREFIX, 'CategoryLineSuffix', CFG_CATEGORY_LINE_SUFFIX, 'CategoryDescPrefix', CFG_CATEGORY_DESC_PREFIX, 'CategoryDescSuffix', CFG_CATEGORY_DESC_SUFFIX )

		## Category Toggles
		self.SetCfgAccessorFuncValues( 'DisplayPersonalStory', CFG_DISPLAY_PERSONAL_STORY, 'DisplaySortMethod', CFG_CATEGORY_DISPLAY_SORT_METHOD, 'DisplayErrors', CFG_DISPLAY_CATEGORY_ERRORS, 'DisplayHelp', CFG_DISPLAY_CATEGORY_HELP, 'DisplayTasks', CFG_DISPLAY_CATEGORY_TASKS, 'DisplayNotes', CFG_DISPLAY_CATEGORY_NOTES )

		## For Function / Class Arguments Callback Settings
		self.SetCfgAccessorFuncValues( 'ObjectOpeningDelimiter', CFG_OBJECT_ARGUMENT_OPENING_DELIMITER, 'ObjectEndingDelimiter', CFG_OBJECT_ARGUMENT_ENDING_DELIMITER, 'ObjectArgSplitDelimiter', CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER, 'ObjectArgDefaultSplitDelimiter', CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER, 'ObjectArgJoinString', CFG_OBJECT_ARGUMENT_JOIN_DELIMITER, 'ObjectArgJoinPrefix', CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX, 'ObjectArgJoinSuffix', CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX )

		## Replace 'function' or 'def' with something shorter such as 'f' - Only enabled if Search is populated...
		self.SetCfgAccessorFuncValues( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH, 'FuncNameDefinitionReplace', CFG_FUNC_NAME_REPLACE )

		## Alter Function / Class Args to display Data-Types, Args or Both and set the character display data...
		self.SetCfgAccessorFuncValues( 'DisplayArgDataTypes', CFG_DISPLAY_ARG_DATA_TYPE, 'DisplayArgDataTypesSpacer', CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS, 'DisplayArgDataTypesPrefix', CFG_DISPLAY_ARG_DATA_TYPE_PREFIX, 'DisplayArgDataTypesSuffix', CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX )


		## Custom Additions - Add your custom configuration / AccessorFunc values here - Use the name of the function excluding Set* / SetCfg* followed by the value - chain the name / value together as seen above and below to add more on a single line if using the helper - you may also use the standard self.SetCfgName( x ) or self.SetName( x ). I'd recommend keeping this organized as I did above...

		## AccessorFunc Additions
		## self.SetAccessorFuncValues( 'NameOfFunc', CONST_, '', CONST_, '', CONST_, '', CONST_ )

		## Config AccessorFunc Additions
		## self.SetCfgAccessorFuncValues( 'NameOfFuncExcludingCfgPrefix', CFG_, '', CFG_, '', CFG_, '', CFG_ )

		## Set whether or not we should parse code within comment blocks as comments vs code - set above in config...
		self.SetCfgParseCodeWithinCommentBlock( CFG_SHOW_COMMENT_BLOCK_DATA )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


	##
	## Accessor - This is required because we process as little as possible in Run when cached file is to be loaded meaning NO AccessorFuncs... We have to Re-add it for each language class because the CFG_ will be different ( this will be removed when sublime-settings files are used )
	##
	def GetCfgCachingSystemEnabled( self ):
		return CFG_CACHING_SYSTEM_ENABLED


	##
	## Accessor - For Developer Mode - pass-through for caching system..
	##
	def GetDeveloperMode( self ):
		return DEVELOPER_MODE


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin JavaScript using additional Acecool Mappings for Ultimate Site Modding Script, and also AcecoolST3_Library - Maybe turn that into it's own lib...
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##
##
class XCodeMapper_JavaScript_Acecool( XCodeMapper_JavaScript ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_JavaScript_Acecool_WebMod'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		## Accessor Funcs - These are expanded, typically by adding Getter and Setter functions
		## The following 3 are for - https://bitbucket.org/Acecool/acecooldev_userscripts/src/master/__everything_one_script_to_run_everything_through_modules_and_rules__/AcecoolDev_Ultimate_UserScript.user.js?at=master&fileviewer=file-view-default
		## I use the AccessorFunc callback to ensure _tag is replaced with the appropriate previously memorized value etc..
		## These two instead of adding functions, adds a comment for each.. The content of the comments are based on the arguments to these functions...
		self.AddSyntax( CATEGORY_FUNCTION_ACCESSOR, SEARCH_TYPE_STARTSWITH, 'Acecool.util.AccessorFunc', 'AccessorFunc', 'AddFeature', 'SetupTasks', 'SetupProcesses' )

		##
		## Example - This code ( Note, the //s comments are to show which args mean what - they aren't used for the output, they're only for making the code easily understandable for humans )
		##
		##	// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
		##	SetupTasks( _tag, true, true, true, true, true );
		##
		##	// Setup Processes: Anchors, Divs, Frames, Observer
		##	SetupProcesses( _tag, true, true, true, true );
		##	AddFeature( TAG_EXAMPLE, TASK_DEFAULT, function( _task, _data )
		##	AddFeatures( _tag, TASK_ON_PRELOAD, function( _data, _cfg ) {
		##	AddFeatures( _tag, TASK_ON_DOCUMENT_READY, function( _data, _cfg ) {
		##	AddFeatures( _tag, TASK_ON_LOAD, function( _data, _cfg ) {
		##	AddFeatures( _tag, TASK_ON_FRAME_LOAD, function( _data, _cfg ) {
		##	AddFeatures( _tag, TASK_PROCESS_ANCHORS, function( _anchor, _cfg ) {
		##	AddFeatures( _tag, TASK_PROCESS_DIVS, function( _div, _cfg ) {
		##	AddFeatures( _tag, TASK_PROCESS_FRAMES, function( _frame, _cfg ) {
		##	AddFeatures( _tag, TASK_ON_OBSERVER_MUTATION_CALLBACK, function( _data, _cfg, _mutations ) {
		##	AddFeatures( _tag, TASK_SETUP_OBSERVER, function( _data, _cfg, _observer ) {
		##
		##	// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
		##	SetupTasks( _tag, true, true, true, true, true );
		##
		##	// Setup Processes: Anchors, Divs, Frames, Observer
		##	SetupProcesses( _tag, true, true, true, true );
		##	AddFeature( TAG_DEFAULT, TASK_DEFAULT, function( _task, _data )
		##	// Setup Tasks: Preload, OnReady, OnLoad, OnFrameLoad, SetupObserver
		##	SetupTasks( _tag, true, false, false, false, false );
		##
		##	// Setup Processes: Anchors, Divs, Frames, Observer
		##	SetupProcesses( _tag, false, false, false, false );
		##	AddFeature( TAG_LOCALHOST, TASK_ON_PRELOAD, function( _data, _cfg )


		##
		## Turns into the following output, without ## comments...
		##
		## // Tasks:		[ OnPreLoad ][ OnReady ][ OnLoad ][ OnLoadFrame ][ OnSetupObserver ]:746
		## // Processes:	[ Anchor ][ DIV ][ I-Frame ][ Observer ]:749
		## function AddFeature( EXAMPLE, DEFAULT ):758
		## function AddFeature( EXAMPLE, ON_PRELOAD ):774
		## function AddFeature( EXAMPLE, ON_DOCUMENT_READY ):790
		## function AddFeature( EXAMPLE, ON_LOAD ):800
		## function AddFeature( EXAMPLE, ON_FRAME_LOAD ):810
		## function AddFeature( EXAMPLE, PROCESS_ANCHORS ):820
		## function AddFeature( EXAMPLE, PROCESS_DIVS ):830
		## function AddFeature( EXAMPLE, PROCESS_FRAMES ):840
		## function AddFeature( EXAMPLE, ON_OBSERVER_MUTATION_CALLBACK ):850
		## function AddFeature( EXAMPLE, SETUP_OBSERVER ):860

		## // Tasks:		[ OnPreLoad ][ OnReady ][ OnLoad ][ OnLoadFrame ][ OnSetupObserver ]:898
		## // Processes:	[ Anchor ][ DIV ][ I-Frame ][ Observer ]:901
		## function AddFeature( DEFAULT, DEFAULT ):917

		## // Tasks:		[ OnPreLoad ]:1014
		## function AddFeature( LOCALHOST, ON_PRELOAD ):1023


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = Acecool.table.GetValue( _args, _args_count, 0, self.__name__ )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )


		##
		## Acecool.util.AccessorFunc = function( _tab, _name, _default, _getter_prefix = 'Get', _key = _name )
		##
		##
		if ( _pattern == 'AccessorFunc' or _pattern == 'Acecool.util.AccessorFunc' ):## IF _arg_tab is self, then assume XCodeMapper
			if ( _arg_tab == 'self' ):
				_arg_tab = self.__name__

			## Function name so if this is Blah then SetBlah is one function and _getter_prefix + Blah is the second.
			_arg_name	= Acecool.table.GetValue( _args, _args_count, 1, 'NAME_NONE' )

			## Default value for the Getter - however it can be overwritten by using GetBlah( xxx )
			_arg_default = Acecool.table.GetValue( _args, _args_count, 2, 'null' )

			## Getter Function Prefix - Get is the default value for the function definition so we use Get here too as the default...
			_arg_getter_prefix = Acecool.table.GetValue( _args, _args_count, 3, 'Get' )

			## Key is the index / key of a table where data is stored.. as _name arg is used as default if none is provided, we use the same here...
			_arg_key	= Acecool.table.GetValue( _args, _args_count, 4, _arg_name )

			## If _arg_default is set, show the default value in our output.. otherwise simply show that an argument can be used with this getter...
			if ( _arg_default != 'DEFAULT_NONE' ):
				_arg_default = '_default = ' + _arg_default
			else:
				_arg_default = '_default'

			## function <Class>.<GetPrefix><Name>( _default ), function <Class>.Set<Name>( _value )
			## Add the Setter and Getter functions to the primary function category...
			self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _arg_class + 'Set' + _arg_name + '( _value )', _depth, _mode, _pattern )
			self.AddEntry( CATEGORY_FUNCTION, _line_number, 'function ' + _arg_class + _arg_getter_prefix + _arg_name + '( ' + _arg_default + ' )', _depth, _mode, _pattern )


		##
		## function SetupTasks( _tag, _preload, _doc_ready, _on_load, _on_iframe_load, _setup_observer )
		##
		if ( _pattern == 'SetupTasks' ):
			## Arguments for the function along with the defaults ( False ) for each ( in which case we output nothing to the info comment ).. 0 - 5 == 6 total args.
			_arg_tag				= Acecool.table.GetValue( _args, _args_count, 0, False )
			_arg_task_on_preload	= Acecool.table.GetValue( _args, _args_count, 1, False )
			_arg_task_on_ready		= Acecool.table.GetValue( _args, _args_count, 2, False )
			_arg_task_on_load		= Acecool.table.GetValue( _args, _args_count, 3, False )
			_arg_task_on_load_frame	= Acecool.table.GetValue( _args, _args_count, 4, False )
			_arg_task_on_setup_obs	= Acecool.table.GetValue( _args, _args_count, 5, False )

			## Base Data
			_data = ''

			## If any of the args exist, add a tag which is output with this line
			if ( _arg_task_on_preload == 'true' ):		_data = _data + '[ OnPreLoad ]'
			if ( _arg_task_on_ready == 'true' ):		_data = _data + '[ OnReady ]'
			if ( _arg_task_on_load == 'true' ):			_data = _data + '[ OnLoad ]'
			if ( _arg_task_on_load_frame == 'true' ):	_data = _data + '[ OnLoadFrame ]'
			if ( _arg_task_on_setup_obs == 'true' ):	_data = _data + '[ OnSetupObserver ]'

			## If _data has data, output a new entry with the information collected above added to it.. Also, add a blank line before this entry to make everything easier to read / follow..
			if ( _data != '' ):
				## Only add a space if it isn't the first entry...
				if ( self.HasCategoryEntries( CATEGORY_FUNCTION_ACCESSOR ) ):
					self.AddBlankEntry( CATEGORY_FUNCTION_ACCESSOR )
				## // Tasks:		[ OnPreLoad ][ OnReady ][ OnLoad ][ OnLoadFrame ][ OnSetupObserver ]
				self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, '// Tasks:\t\t' + _data, _depth, _mode, _pattern )


		##
		## function SetupProcesses( _tag, _process_anchors, _process_divs, _process_iframes, _process_mutation_observer )
		##
		if ( _pattern == 'SetupProcesses' ):
			## Arguments for the function along with the defaults ( False ) for each ( in which case we output nothing to the info comment ).. 0 - 4 == 5 total args.
			_arg_tag				= Acecool.table.GetValue( _args, _args_count, 0, False )
			_arg_process_anchors	= Acecool.table.GetValue( _args, _args_count, 1, False )
			_arg_process_divs		= Acecool.table.GetValue( _args, _args_count, 2, False )
			_arg_process_iframes	= Acecool.table.GetValue( _args, _args_count, 3, False )
			_arg_process_observer	= Acecool.table.GetValue( _args, _args_count, 4, False )

			## Base Data
			_data = ''

			## If any of the args exist, add a tag which is output with this line
			if ( _arg_process_anchors == 'true' ):		_data = _data + '[ Anchor ]'
			if ( _arg_process_divs == 'true' ):			_data = _data + '[ DIV ]'
			if ( _arg_process_iframes == 'true' ):		_data = _data + '[ I-Frame ]'
			if ( _arg_process_observer == 'true' ):		_data = _data + '[ Observer ]'

			## If _data has data, output a new entry with the information collected above added to it..
			if ( _data != '' ):
				## // Processes:	[ Anchor ][ DIV ][ I-Frame ][ Observer ]
				self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, '// Processes:\t' + _data, _depth, _mode, _pattern )


		##
		## function AddFeature( _tag, _task = TASK_DEFAULT, _func )
		##
		if ( _pattern == 'AddFeature' ):
			##
			_arg_category = Acecool.table.GetValue( _args, _args_count, 0, 'TAG_NONE' )
			_arg_task = Acecool.table.GetValue( _args, _args_count, 1, 'TASK_NONE' )
			_arg_func = Acecool.table.GetValue( _args, _args_count, 2, 'FUNC_NONE' )

			## Add the Get<Name> and Set<Name> functions to CATEGORY_FUNCTION category - note, you can use CATEGORY_FUNCTION_ACCESSOR if you want it more out of the way...
			self.AddEntry( CATEGORY_FUNCTION_ACCESSOR, _line_number, 'function AddFeature( ' + _arg_category + ', ' + _arg_task + ', function( _task, _data ) ... end );', _depth, _mode, _pattern )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Create Custom AccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		##
		## Custom AccessorFunc Additions for this mapper
		##

		## For storing and recalling the last _tag used in a function call so _tag can be replaced with the full-version such as EXAMPLE, DEFAULT, BITBUCKET, etc..
		self.AccessorFunc( 'LastTag', CFG_ACECOOL_JAVASCRIPT_DEFAULT_LAST_TAG )


		##
		## Custom Config Additions for this mapper
		##

		## Example A
		## assignment a


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		return super( ).OnSetupAccessorFuncs( )		## return self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		##
		## Set the values of the config additions the mapper...
		##

		## Set the value for the Last Tag Tracker to
		self.SetLastTag( CFG_ACECOOL_JAVASCRIPT_DEFAULT_LAST_TAG )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _mode, _pattern ):
		## If our argument starts with TAG_ then remmember it for when the argument is _tag so we can ensure to show the developer the active TAG_ for each _tag...
		_word = 'TAG_'
		if ( _arg.startswith( _word ) ):
			_arg = Acecool.string.SubStr( _arg, len( _word ), len( _arg ) )
			self.SetLastTag( _arg )

		## Show the previously remembered TAG_
		if ( _arg == '_tag' ):
			_arg = self.GetLastTag( )

		## Remove TASK_ from each argument which begins with it to keep things short...
		_word = 'TASK_'
		if ( _arg.startswith( _word ) ):
			_arg = Acecool.string.SubStr( _arg, len( _word ), len( _arg ) )


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		return super( ).OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _mode, _pattern )		## return self.CallParent( 'OnFunctionArgument', _arg, _category, _line_number, _code, _depth, _mode, _pattern )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End JavaScript using additional Acecool Mappings
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##
##
## class XCodeMapper_JavaScript_User( XCodeMapper_JavaScript_Project_AC_WebMod ):
class XCodeMapper_JavaScript_User( XCodeMapper_JavaScript ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_JavaScript_User'


	##
	## Additional Callback used for hiding default categories, or completely changing the category order.
	## Return True / False to prevent the default categories from being output - you would then need to re-add all categories you want in the order you want them to output...
	##
	def OnSetupCategoryOutputOrder( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCategoryOutputOrder' )

		## Call one per line or many per call - These are categories you want to hide - This overrides the CFG_ values...
		## self.AddHideCategoryEntry( CATEGORY_NOTE, CATEGORY_DEBUGGING, CATEGORY_DEFINITION, CATEGORY_OPTIMIZATION, CATEGORY_HELP )

		## Call one per line, or many per... It doesn't matter...
		## self.AddHideCategoryEntry( CATEGORY_ERROR )
		## self.AddHideCategoryEntry( CATEGORY_HELP )
		## self.AddHideCategoryEntry( CATEGORY_NOTE )
		## self.AddHideCategoryEntry( CATEGORY_DEFINITION )
		## self.AddHideCategoryEntry( CATEGORY_OPTIMIZATION )
		## self.AddHideCategoryEntry( CATEGORY_DEBUGGING )

		## Return true or false to prevent all default categories from being added so you can set up your own order from scratch..
		return None


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = Acecool.table.GetValue( _args, _args_count, 0, self.__name__ )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		##
		## Set the values of the config additions the mapper...
		##

		## Custom A
		## self.SetCfgNameXXXXXXXName( CFG_XLUA_XXXXXXX )

		## Custom B
		## self.SetNonCfgNameXXXXXXXName( CONST_XLUA_XXXXXXX )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


	##
	## Create Custom AccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		##
		## Custom AccessorFunc Additions for this mapper
		##

		## Example AccessorFunc Addition
		## self.AccessorFunc( 'NonCfgNameXXXXXXXName', CFG_OR_OTHER_DEFAULT_VALUE_ )


		##
		## Custom Config Additions for this mapper
		##

		## Example Config Addition
		## self.ConfigAccessorFunc( 'NameXXXXXXXName', CFG_ALUA_DISPLAY_ACCESSORFUNC_INTERNALS )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------