##
## XCodeMapper, Universal Code Mapper extension for 'CodeMap' - Josh 'Acecool' Moser
##
## CodeMap, by oleg-shiro, is a Sublime Text 3 Package / Extension which provides useful functionality to the editor by adding a panel to display
## 	important aspects of code, hotkeys to navigate the output, and support to double-click each output line to jump to the relevant line.
##
## XCodeMapper extends CodeMap by adding features such as categories, category types, callbacks, and more. These features and partnership allows
## 	a concise and beautiful output to be featured within the Code - Map panel while utilizing the least space possible. The callbacks further
## 	the ability to earmark important data by allowing that data to be modified or checked for errors to further aide the developer. Additionally,
## 	and probably the most important feature is the ease in which support for a new language can be integrated along with the ability to tailor
## 	the mapper to your specific project with just a few lines of function calls.
##
## Please read the comments in XCodeMapperBase.py and XCodeMapperCallbackBase.py to understand the extent of the features and abilities...
##
## Each code_map.<ext>.py script requires several things to work:
## 	- ) Standard Imports
## 	1 ) The generate function definition populated with standard calls to create a new Callback and XCodeMapper object. This function is similar to the main function of a program.
## 	2 ) The Callback object definition with the OnSetupSyntax function defined and populated with self.AddSyntax calls to earmark code. The OnSetupConfig function is also required to tell the mapper your local mapper CFG_ configuration data.
## 	3 ) The empty, or not, XCodeMapper definition - This is there so you may overwrite pre-existing XCodeMapper functions for development, testing and or customization purposes.
##
## Optional Elements can be added too:
##	1 ) `map_syntax` is an attribute that defines syntax highlight to be used for the code map text
## 	2 ) Additional functions added to Callback to extend functionality and tailor the mapper to your specific project or language needs..
##
## The Mapper formats output to the Code - Map panel in the following form to create hotlinks to the <line_number> allowing you to jump directly to that specific line in the active file: <data>:<line_number>
##


##
## Imports
##

## If Developer Mode is True, the imports will be reloaded...
DEVELOPER_MODE									= False

## Import Sublime Library
import sublime

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from User.CodeMap.Acecool.XCodeMapper_Definitions import *

## Import Acecool's Library of useful functions and globals / constants...
from User.CodeMap.Acecool.AcecoolST3_Library import *

## Import XCodeMapper
from User.CodeMap.Acecool.XCodeMapper import *

## Reload
if ( DEVELOPER_MODE ):
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper_Definitions' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.AcecoolST3_Library' )
	sublime_plugin.reload_plugin( 'User.CodeMap.Acecool.XCodeMapper' )

## Grab Sublime Text Settings for packages which are ignored and which are installed!
ignored		= sublime.load_settings( 'Preferences.sublime-settings' ).get( 'ignored_packages' )
installed	= sublime.load_settings( 'Package Control.sublime-settings' ).get( 'installed_packages' )

## Determine the best Python Syntax Highlighter File to use for the Mapping Panel - Detected in order: sublime-syntax / tmLanguage - in Packages/: Acecool/syntax_files/ then <PackageName>/ - defaults to Python/Python.sublime-syntax
map_syntax = Acecool.sublime.GetSyntaxFilePath( 'AutoHotkey' )


##
## Configuration ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Important Default Configuration Options
##

## Debugging Mode - Set this to True to display ALL debugging messages or set it to a tag / id to only print those. By default this is True so a few messages will print until the local mapper config activates and some messages will always be displayed...
## Options: True / False
CFG_DEBUG_MODE									= False


##
## Language we're using...
##
## Options: Any String
CFG_PRIMARY_ACTIVE_LANGUAGE						= 'AutoHotkey'


##
## Caching System
##

## Do you want to use the caching system? ( Typically 0.5 seconds to process 5000 lines of code and 0.02 to save - 0.01 to load without needing 0.48 to reprocess... - Times from SSD Hard Drive and [ 10 year old Intel Q6600 2.4 GHz Quad OC on AIR @ GHz 3.0 on air for entire lifetime, 8GB DDR2, Asus P5k Mbd ] )
CFG_CACHING_SYSTEM_ENABLED						= True


##
## Category Related
##

## The default for Alphabetizing the data in categories.... Sort method used... By line number is already default... First seen first out... add _DESC to reverse it, or _ALPHABETICAL for A-Z or _ALPHABETICAL_DESC for Z-A
## Options: ENUM_SORT_METHOD_DEFAULT == 0-9 / ENUM_SORT_METHOD_ALPHABETICAL == A-Z / ENUM_SORT_METHOD_ALPHABETICAL_DESC == Z-A / ENUM_SORT_METHOD_LINE_NUMBER == 0-9 / ENUM_SORT_METHOD_LINE_NUMBER_DESC == 9-0
CFG_CATEGORY_DISPLAY_SORT_METHOD				= ENUM_SORT_METHOD_DEFAULT

## Display the Help and Support Category? When set to True it ouputs Download and Support Information for both oleg-shilo, Creator of CodeMap which is incredibly useful Package for Sublime Text Editor, and Josh 'Acecool' Moser who created XCodeMapper which is an extension for CodeMap to add more features!
## Options: True / False
CFG_DISPLAY_CATEGORY_HELP						= False

## Display the Errors List Category - By default it is at the very top and only visible when errors are found?
## Options: True / False
CFG_DISPLAY_CATEGORY_ERRORS						= True

## Display the Tasks / TODOs / Work / etc.. Category? By default it is enabled - Tasks are typically important but they can pile up. If you want to hide the category temporarily or permanently - set it here..
## Options: True / False
CFG_DISPLAY_CATEGORY_TASKS						= True

## Display the Notes / Info / etc.. Category? By default it is enabled - however Notes aren't always important - if notes are getting in the way of other data you can hide the category temporarily or permanently...
## Options: True / False
CFG_DISPLAY_CATEGORY_NOTES						= True


##
## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
##

## Should the line be displayed?
## Options: True / False
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO				= True

## If the line is to be displayed ( above set to True ), how many new-lines should we add afterwards? 2 gives a blank line between it and the first category output..
## Options: Numerical 0 through n
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES		= 2



##
## New-Line preferred for reading Files and for outputting to the Code - Map Panel
##

## New-Line character used for the Code - Map Panel
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_NEW_LINE									= CONST_NEW_LINE_UNIX

## New-Line character used to delimit the active-file.. The new-line type will appear under TOP_MENU > View > Line Endings > [ Windows / Unix / Mac OS 9 ]
## Options: CONST_NEW_LINE_WINDOWS / CONST_NEW_LINE_UNIX / CONST_NEW_LINE_MAC_OS_9
CFG_FILE_NEW_LINE								= CONST_NEW_LINE_UNIX


##
## Indentation
##

## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
## CFG_INDENT_CHARS								= '\t'
## Options: Any String - I'd recommend using spaces if you'll be copying / pasting into Steam, otherwise Tabs..
CFG_INDENT_CHARS								= '    '

## Set this to -1 to disable the column sizing feature ( meaning line :1234 shows up right at the side of the code output to CodeMap Panel ). Set it to 1 to truncate values meaning the column will be max of that plus the line-size. Set it to 0 to try to force it to that column, but don't truncate. If it goes over, use -1 behavior on that line... This way the line numbers line up nicely most of the time..
## Options: ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT / ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE / ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED
CFG_INDENT_CODE_COLUMN_WIDTH_MODE				= ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED

## Configure the maximum width code should take up in the output before being truncated... Then the line column comes... Note, there will be an additional callback so you can define this on the fly for specific categories so if you want notes / tasks to have no limit, no worries... If you want to disable this feature set it to -1 also note that the line one will be turned off too.. Also, you can set this to a large value, just remember that spaces are used!! I'd recommend keeping it under 255 or so chars per line which is about 1080px wide if viewing on a 4k monitor with a few ticks up in font-size... But at that width you'll unlikely see the line number... keep that in mind...
## Options: Numerical 0 through n - Only Active if CFG_INDENT_CODE_COLUMN_WIDTH_MODE is something other than _DEFAULT
CFG_INDENT_CODE_COLUMN_WIDTH					= 125

## Should we limit the Category Name / Description Length? If so, use CFG_INDENT_CODE_COLUMN_WIDTH + 5 for file with 9999 max lines, or 6 with 99,999 for good measure so they'll be lined up properly...
## Options: Numerical 0 through n - Its' best to keep this on paar with CFG_INDENT_CODE_COLUMN_WIDTH
CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH		= CFG_INDENT_CODE_COLUMN_WIDTH + 6


##
## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
##

## If you want to comment out Task / TODO lines, set this to the language comment
## Options: Any String
CFG_TASK_LINE_PREFIX							= ''

## This suffix is used to ensure the rest of the Code - Map Panel doesn't show up as a single-color.. Simply use a block-comment or comment with a single and double-quote to ensure the syntax highlighter catche sit...
## Options: Any String
CFG_TASK_LINE_SUFFIX							= '\t;\'"'


##
## Category Output Configuration For the Code - Map Panel
##

## Category Line Prefix and Suffix - the Description variant is for before and after the description which is hidden if the above is False... This is for the entire line before and after...
CFG_CATEGORY_LINE_PREFIX						= '☼ '
CFG_CATEGORY_LINE_SUFFIX						= ''

## Should we display the Category Description?
## Options: True / False
CFG_CATEGORY_LINE_SHOW_DESC						= True

## The Prefix to be used separating the Category Name from the Description - I'd recommend something similar to ' - ', maybe with tabs, etc... You can also use Acecool.string.FormatColumn( 25, ' - ' ) to add 22 spaces after ' - '... Later I may add support for the category output to use this function so all category names / descriptions line up in the output...
## Options: Any String
CFG_CATEGORY_DESC_PREFIX						= ' = \''

## If we show the Category Description, I highly recommend you set this to use a comment for the language along with ' and " - or simply use CFG_TASK_LINE_SUFFIX - to prevent the highlighter from messing up and coloring everything one color..
## Options: Any String
CFG_CATEGORY_DESC_SUFFIX						= '\';' + CFG_TASK_LINE_SUFFIX


##
## Function / Class For-Each Argument Callback Configuration
##

## This is what separates the function or class from the arguments passed through - Example: func<OPENING_DELIMITER> x, y, z )
## Options: Any String
CFG_OBJECT_ARGUMENT_OPENING_DELIMITER			= '('

## This is what separates the arguments from the end function / class call or declaration - Example: func( x, y, z <ENDING_DELIMITER>
## Options: Any String
CFG_OBJECT_ARGUMENT_ENDING_DELIMITER			= ')'

## This is the character used to delimit or split arguments - It isn't recommended to use more than 1 char otherwise other coding-standards may not be properly read...
## Options: Any String
CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER				= ','

## This is to split a default value from an argument... ie func ( x, y, z = true ) in some languages means z, if unset, is set to true...
## Options: Any String
CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER		= ':='

## This is the string used to re-join the list of modified arguments back into a string - A comma and space here provide an Airy output: func( x<JOIN_DELIMITER>y<JOIN_DELIMITER>z )
## Options: Any String
CFG_OBJECT_ARGUMENT_JOIN_DELIMITER				= ', '

## These are what comes between the arguments on either side of the rejoined args string, and the opening / ending delimiter - Spaces here provide an Airy output: func(<PREFIX>x, y, z<SUFFIX>)
## Options: Any String
CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX				= ' '
CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX				= ' '


##
## If CFG_FUNC_FIND_TEXT_REPLACEMENT is set then we look for CFG_FUNC_FIND_TEXT and replace it with CFG_FUNC_FIND_TEXT_REPLACEMENT... so def can become function, or function can become fund or f... Space-savings!
##

## If CFG_FUNC_TEXT above is not '' then set this to what you are looking to replace... Add the space in both, or neither
## Options: Any String
CFG_FUNC_NAME_SEARCH							= 'function ' #'def'

## Internal helper for GLua - This defines the text to be used for function declarations as function in Callback.PreOnAddEntry is replaced to save space as some of the function / object declarations in Lua and GLua are quite long...
## Options: Any String - Note: Specifically for AutoHotkey, if you're running my AHK tmLanguage - I added ƒ, function, func, and def as function keywords to highlight.. Feel free to use any - or set this to '' to make it look as it did before with functions having no prefix and no color to distinguish them..
CFG_FUNC_NAME_REPLACE							= 'ƒ '


##
## Data-Type Options for Function / Class Arguments...
##
##	<DataType>		==		ENUM_DISPLAY_DATA_TYPE_ONLY		|	<DataType> _arg		==		ENUM_DISPLAY_DATA_TYPE_BOTH		|	_arg	==		ENUM_DISPLAY_DATA_TYPE_DEFAULT
##

## Determine how the Data-Type system should apply data... ENUM_DISPLAY_DATA_TYPE_DEFAULT == Leaves the argument as is / _TYPE_BOTH == displays <DataType> _arg - assumes ' ', '<', '>' cfg / _TYPE_ONLY == <DataType> only
## Options: ENUM_DISPLAY_DATA_TYPE_DEFAULT == _arg / ENUM_DISPLAY_DATA_TYPE_ONLY == <DataType> / ENUM_DISPLAY_DATA_TYPE_BOTH == <DataType> _arg
CFG_DISPLAY_ARG_DATA_TYPE						= ENUM_DISPLAY_DATA_TYPE_ONLY

## How data-types appear - for when they are added to, or replacing, function args... ie if the following 3 are: ' ' '-' 'x' they'd appear: ( -Playerx _p, -Weaponx _w ) or ' ' '<' '>' they'd appear: ( <Player> _p, <Weapon> _w )
## Options: Any String
CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS			= ' '
CFG_DISPLAY_ARG_DATA_TYPE_PREFIX				= '<'
CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX				= '>'


##
## Personal Information
##

## Display my personal story?
## Options: True / False
CFG_DISPLAY_PERSONAL_STORY						= False


##
## Custom Additions ( for Callback class ) Config - ie things you've added - I'd highly recommend using this area instead of hard-coding values in case you decide to share your Callback class for Language <X> with anyone else...
##


##
## Key Conversion Output - Default state makes them appear as: [ Alt ! ] [ Shift + ] [ Win # ] [ Ctrl ^ ]
##
## Options: Any String
CFG_KEY_TEXT_PREFIX								= '[ '
CFG_KEY_TEXT_SUFFIX								= ' ] '
CFG_KEY_TEXT_CTRL								= 'Ctrl ^'
CFG_KEY_TEXT_ALT								= 'Alt !'
CFG_KEY_TEXT_SHIFT								= 'Shift +'
CFG_KEY_TEXT_WIN								= 'Win #'
CFG_KEY_TEXT_ALLOW_OTHER_ACTIVE_MODS			= 'Allow With other ModKeys *'
CFG_KEY_TEXT_NO_BLOCK_PASS_THROUGH				= 'Allow Key Passthrough / No Block ~'


##
## Live-Wiki Style System
##

## Do you want to display the additional information on the AutoHotkey Directives?
CFG_WIKI_DISPLAY_DIRECTIVE_INFO					= False



##
## Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Which Mapping Variant would you like to use? Each option going downward gives you everything of the level above and itself except for the USER which is for you to customize how you want.
##
## ENUM																	Class Name									Base Class					ClassDescription
ENUM_LANG_THIS_VANILLA							= SetupLangVariantMap( 'XCodeMapper_AutoHotkey',					'XCodeMapperBase',			'AutoHotkey - Standard Edition' )
ENUM_LANG_THIS_ACECOOLAHK_FRAMEWORK				= SetupLangVariantMap( 'XCodeMapper_AutoHotkey_Acecool',			'XCodeMapper_AutoHotkey',	'AutoHotkey - Adds Acecool AutoHotkey Framework Mappings' )
ENUM_LANG_THIS_USER								= SetupLangVariantMap( 'XCodeMapper_AutoHotkey_User',				'XCodeMapper_AutoHotkey',	'AutoHotkey - Everything you need to create your own extension - I added it to the end to make copying / pasting easier, for when this script updates. On Updates of this script, copy the relevent segments ( This, the config element below, and the class... That is it.. )!' )


## This option is the default and will add mappings for the Vanilla Language / Variant.
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_VANILLA

## This option adds AcecoolAHK_Framework Mappings
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_ACECOOLAHK_FRAMEWORK

## This option is a blank Class usable as a template to create mappings for your own project. It is left at the end of the file to allow for easier processing of updates - when a new updates comes out, copy and paste the object to the new file..
CFG_XCODEMAPPER_MAPPER_VARIANT					= ENUM_LANG_THIS_USER



##
## End Base, Custom Additions, and Mapper Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Primary / Main function used by the CodeMap back-end. This doesn't need to be edited anymore... Use the Mapper Configuration to choose which Class to run.
##
def generate( _file ):
	## Grab the Mapper via the Mapper ID / Class-Name using either the Globals dictionary or by using the active module [ Preferred ]... Run the mapper and return the output...
	return getattr( sys.modules[ __name__ ], GetLangVariantClassName( CFG_XCODEMAPPER_MAPPER_VARIANT ) )( ).Run( _file, CFG_XCODEMAPPER_MAPPER_VARIANT )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##  Class Callbacks - This is an extension to XCodeMapper which only houses the Callback functions which are meant to be manipulated / edited for each language, or implementation...
##
class XCodeMapper_AutoHotkey( XCodeMapperBase ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_AutoHotkey'


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		##
		## Comments - Notice how I added ;; before ; - As ; is the standard comment for AutoHotkey, but because I use ;;, if I were to look for ; first, then ;; would never trigger.
		## This isn't an issue with comments that are of the same character, but other AddSyntax calls may rely on the order to actually show up..
		## For example, if I used startswith function , and then I added another search for startswidth function Acecool.C. ...
		## The second one would never be detected as function  already sees it - this isn't always bad, but if you want to categorize Acecool.C. functions differently, then you'll want to search for them first.
		## Always  make sure you look at the order of your additions to ensure you're not blocking something else from categorizing an earmark..
		##
		self.AddSyntax( CATEGORY_COMMENT, SEARCH_TYPE_STARTSWITH, ';;', ';' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_BEGIN, SEARCH_TYPE_CONTAINS, '/*' )
		self.AddSyntax( CATEGORY_COMMENT_BLOCK_FINISH, SEARCH_TYPE_CONTAINS, '*/' )

		## Classes, Functions and HotKeys
		self.AddSyntax( CATEGORY_CLASS, SEARCH_TYPE_STARTSWITH, 'class ' )
		self.AddSyntax( CATEGORY_FUNCTION, SEARCH_TYPE_REGEX, '^\s*(\w+)(\()(.*)(\))' )
		self.AddSyntax( CATEGORY_ANCHOR, SEARCH_TYPE_REGEX, '^\s*(\w+):' )
		self.AddSyntax( CATEGORY_HOTKEY, SEARCH_TYPE_ENDSWITH, '::' )

		## Config
		self.AddSyntax( CATEGORY_DIRECTIVE, SEARCH_TYPE_STARTSWITH, 'SetWorkingDir', 'Menu, Tray, Icon,', '#ClipboardTimeout', '#CommentFlag', '#ErrorStdOut', '#EscapeChar', '#HotkeyInterval', '#HotkeyModifierTimeout', '#HotString', '#If', '#IfWinActive', '#IfWinExist', '#IfTimeout', '#Include', '#IncludeAgain', '#InputLevel', '#InstallKeybdHook', '#InstallMouseHook', '#KeyHistory', '#MaxHotkeysPerInterval', '#MaxMem', '#MaxThreads', '#MaxThreadsBuffer', '#MaxThreadsPerHotkey', '#MenuMaskKey', '#NoEnv', '#NoTrayIcon', '#Persistent', '#SingleInstance', '#UseHook', '#Warn', '#WinActivateForce' )
		self.AddSyntax( CATEGORY_DEPRECATED, SEARCH_TYPE_STARTSWITH, '#AllowSameLineComments' )






		##
		## Fallthrough to the parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )


	##
	## Helper function for this mapper - Combines the Prefix and Suffix to input text to form the full Key String..
	##
	def GetKeyTextString( self, _text ):
		return self.GetCfgKeyTextPrefix( ) + _text + self.GetCfgKeyTextSuffix( )


	##
	## Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	## Issue: For some reason _code isn't being passed through in stripped form - on top of that, depth isn't being processed properly... It may be limited to AHK though..
	## Todo: Add Code Raw variable...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original ):
		##
		##
		##
		if ( _code.strip( ) != '' ):
			_code = _code.strip( )


		##
		## Because a function call and a function definition in AutoHotkey are similar ( especially as I don't have the RegEx looking at multiple lines yet ) This will ensure the next line has a { if it is a function we're looking at...
		##
		if ( _category_original == CATEGORY_FUNCTION ):
			##
			_code = 'function ' + _code
			## So, we're in the function category... Lets make sure the next line ( or this line ) has a {.. Alternatively, some people add the { on the same line, lets check there too.. Another possibility is a single line definition - check for } at the end of the line...
			if ( not self.GetFileDataByLine( _line_number + 1 ).strip( ).startswith( '{' ) and not _code.strip( ).endswith( '{' ) and not _code.strip( ).endswith( '}' ) ):
				## None of those options were discovered - this is highly likely to be a function call and not a definition... Return True or False to prevent this entry from being used...
				return _code


		##
		## For Classes, Functions and Hotkeys - Add a new line before the entry if it isn't the first and also add the comment line above the output - as AutoHotkey is an odd language, comments help therefore adding them to the output felt appropriate..
		##
		if ( _category_original == CATEGORY_CLASS and _code.startswith( 'class ' ) ) or ( _category_original == CATEGORY_HOTKEY and _code.endswith( '::' ) ) or ( _category_original == CATEGORY_FUNCTION and _code.endswith( ')' ) ):
			## Only add a space if it isn't the first entry...
			if ( self.HasCategoryEntries( _category ) ):
				self.AddBlankEntry( _category )

			## Add the description from the comment using my comment structure ( 3 lines per function / hotkey with the center containing the description ) above the hotkey with a blank line separating the previous..
			self.AddEntry( _category, _line_number - 2, self.GetFileDataByLine( _line_number - 2 ), _depth, _mode, _pattern )


			## Rewrites a bind which looks like !#^+F22 into something easily identifiable ( The chars are always foreign to me exccept + which is intuitive.. # could work as a frame.. ^ as a carrot to control the donkey? ! alt to?? Alternative sentence type? I wonder why they chose these particular chars other than being easily accessible on the keyboard..
			## Note: ! == ALT, ^ == Control, + == SHIFT, # == WIN, * == Allow Other Active Mods, ~ No Block Pass-Through of Keys Pressed
			_code = _code.replace( '^', self.GetKeyTextString( self.GetCfgKeyTextCtrl( ) ) )
			_code = _code.replace( '!', self.GetKeyTextString( self.GetCfgKeyTextAlt( ) ) )
			_code = _code.replace( '+', self.GetKeyTextString( self.GetCfgKeyTextShift( ) ) )
			_code = _code.replace( '#', self.GetKeyTextString( self.GetCfgKeyTextWin( ) ) )
			_code = _code.replace( '*', self.GetKeyTextString( self.GetCfgKeyTextAllowOtherActiveMods( ) ) )
			_code = _code.replace( '~', self.GetKeyTextString( self.GetCfgKeyTextAllowPassThrough( ) ) )


		##
		## Add descriptions to the configuration values...
		##
		if ( _category == CATEGORY_DIRECTIVE and CFG_WIKI_DISPLAY_DIRECTIVE_INFO == True ):
			## Set the working directory to be that of my AHK Scripts folder for easy includes...
			if ( _pattern == 'SetWorkingDir' ):
				_code = _code + ' - Defines the base directory used when including other scripts / files.'

			## ;; Set the default tray icon ( Can only be set 1 time?? )... eg: Menu, Tray, Icon, shell32.dll, 44 ;; Favorites Star
			if ( _pattern == 'Menu, Tray, Icon,' ):
				_code = _code + ' - Sets the default Tray Icon - Example Menu, Tray, Icon, shell32.dll, 44 == Favorites Star.'

			## ClipboardTimeout
			if ( _pattern == '#ClipboardTimeout' ):
				_code = _code + ' - Arg uses Milliseconds. Changes how long the script keeps trying to access the clipboard when the first attempt fails.'

			## CommentFlag
			if ( _pattern == '#CommentFlag' ):
				_code = _code + ' - Arg uses String. Changes the script\'s comment symbol from semicolon to some other string. Note: EscapeChar is related.'

			##  ErrorStdOut
			if ( _pattern == '#ErrorStdOut' ):
				_code = _code + ' - Sends any syntax error that prevents a script from launching to stderr rather than displaying a dialog. Note: As of v1.0.90+ Errors are written to stderr instead of stdout. CmdPrompt and Editors may or may not display both.'

			##  EscapeChar
			if ( _pattern == '#EscapeChar' ):
				_code = _code + ' - Changes the script\'s escape character (e.g. accent vs. backslash).'

			##  HotkeyInterval
			if ( _pattern == '#HotkeyInterval' ):
				_code = _code + ' - Uses Milliseconds - Along with #MaxHotkeysPerInterval, specifies the rate of hotkey activations beyond which a warning dialog will be displayed.'

			##  HotkeyModifierTimeout
			if ( _pattern == '#HotkeyModifierTimeout' ):
				_code = _code + ' - Uses Milliseconds - Can be -1 to never timeout - Affects the behavior of hotkey modifiers: CTRL, ALT, WIN, and SHIFT.'

			##  HotString
			if ( _pattern == '#HotString' ):
				_code = _code + ' - Changes hotstring options or ending characters. - #Hotstring NoMouse - #Hotstring EndChars NewChars - #Hotstring NewOptions'

			##  If
			if ( _pattern == '#If' ):
				_code = _code + ' - Creates context-sensitive hotkeys and hotstrings. Such hotkeys perform a different action (or none at all) depending on the result of an expression. - #If WinActive("ahk_class Notepad") or WinActive(MyWindowTitle)'

			##  IfWinActive
			if ( _pattern == '#IfWinActive' ):
				_code = _code + ' - Creates context-sensitive hotkeys and hotstrings. Such hotkeys perform a different action (or none at all) depending on the type of window that is active or exists.'

			##  IfWinExist
			if ( _pattern == '#IfWinExist' ):
				_code = _code + ' - Creates context-sensitive hotkeys and hotstrings. Such hotkeys perform a different action (or none at all) depending on the type of window that is active or exists.'

			##  IfTimeout
			if ( _pattern == '#IfTimeout' ):
				_code = _code + ' - Uses Milliseconds - Sets the maximum time that may be spent evaluating a single #If expression.'

			##  Include
			if ( _pattern == '#Include' ):
				_code = _code + ' - Causes the script to behave as though the specified file\'s contents are present at this exact position.'

			##  IncludeAgain
			if ( _pattern == '#IncludeAgain' ):
				_code = _code + ' - Causes the script to behave as though the specified file\'s contents are present at this exact position.'

			##  InputLevel
			if ( _pattern == '#InputLevel' ):
				_code = _code + ' - Uses Int between 0 and 100 - Controls which artificial keyboard and mouse events are ignored by hotkeys and hotstrings.'

			##  InstallKeybdHook
			if ( _pattern == '#InstallKeybdHook' ):
				_code = _code + ' - Forces the unconditional installation of the keyboard hook..'

			##  InstallMouseHook
			if ( _pattern == '#InstallMouseHook' ):
				_code = _code + ' - Forces the unconditional installation of the mouse hook...'

			## #KeyHistory -
			if ( _pattern == '#KeyHistory' ):
				_code = _code + ' - Sets the maximum number of keyboard and mouse events displayed by the KeyHistory window. You can set it to 0 to disable key history..'

			##  MaxHotkeysPerInterval
			if ( _pattern == '#MaxHotkeysPerInterval' ):
				_code = _code + ' - The maximum number of hotkeys that can be pressed in the interval specified by #HotkeyInterval without triggering a warning dialog..'

			##  MaxMem
			if ( _pattern == '#MaxMem' ):
				_code = _code + ' - Uses MegaBytes - Sets the maximum capacity of each variable to the specified number of megabytes.'

			##  MaxThreads
			if ( _pattern == '#MaxThreads' ):
				_code = _code + ' - The maximum total number of threads that can exist simultaneously. Specifying a number higher than 255 is the same as specifying 255 (in versions prior to 1.0.48, the limit was 20).'

			##  MaxThreadsBuffer
			if ( _pattern == '#MaxThreadsBuffer' ):
				_code = _code + ' - Causes some or all hotkeys to buffer rather than ignore keypresses when their #MaxThreadsPerHotkey limit has been reached.'

			##  MaxThreadsPerHotkey
			if ( _pattern == '#MaxThreadsPerHotkey' ):
				_code = _code + ' - Sets the maximum number of simultaneous threads per hotkey or hotstring.'

			##  MenuMaskKey
			if ( _pattern == '#MenuMaskKey' ):
				_code = _code + ' - Changes which key is used to mask Win or Alt keyup events. A key name or vkNN sequence which specifies a non-zero virtual keycode. Scan codes are not used.'

			## NoEnv - DEFAULT: Recommended for performance and compatibility with future AutoHotkey releases.
			if ( _pattern == '#NoEnv' ):
				_code = _code + ' - Assists with providing compatibility and overall performance benefits with future AutoHotkey Releases. Avoids checking empty variables to see if they are environment variables (recommended for all new scripts).'

			## NoTrayIcon
			if ( _pattern == '#NoTrayIcon' ):
				_code = _code + ' - Disables the showing of a tray icon, even if the script was compiled into an EXE.'

			## #Persistent
			if ( _pattern == '#Persistent' ):
				_code = _code + ' - Ensures the script does not exist unless ExitApp is called in the script via code-call or by manually exiting the script.'

			## SingleInstance - Disable single-instance ( For Jobs / Processes )
			if ( _pattern == '#SingleInstance' ):
				_code = _code + ' - Ensures only one copy of the script can run at any given time if set to On.'

			##  UseHook
			if ( _pattern == '#UseHook' ):
				_code = _code + ' - Forces the use of the hook to implement all or some keyboard hotkeys.'

			## Warn - Enable warnings to assist with detecting common errors.
			if ( _pattern == '#Warn' ):
				_code = _code + ' - Enables warnings to assist with detecting common errors. Enables or disables warnings for specific conditions which may indicate an error, such as a typo or missing "global" declaration.'

			##  WinActivateForce
			if ( _pattern == '#WinActivateForce' ):
				_code = _code + ' - Skips the gentle method of activating a window and goes straight to the forceful method.'




		##
		##
		##
		if ( _category == CATEGORY_DEPRECATED ):
			##
			if ( _pattern == '#AllowSameLineComments' ):
				_code = _code + ' - DEPRECATED from v1.1.09+'
				self.AddWarning( '#AllowSameLineComments has been DEPRECATED from v1.1.09+ - Directive only for AutoIt Scripts. AutoIt is no longer supported. Note: Same line comments do work using Vanilla AutoHotkey, therefore this is no longer needed anyway..', _line_number )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		## Because this one returns an argument, it needs to return super all the way to XCodeMapperBase to ensure everything remains intact if another injects between...
		##
		return super( ).PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _mode, _pattern, _category_original )


	##
	## Alter Categories when AddEntry is called - Note: This occurs after the Function / Class Arg Callbacks are processed to ensure each occurs in the proper category. This only alters category the item is visible in, no other functionality is altered..
	##
	def PreOnAddEntryCategory( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		## Alter Functions in the Functions Category to the Class / Object Category for Python so indentation will look nice / natural...
		if ( _category == CATEGORY_FUNCTION ):
			## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
			## self.print( 'PreOnAddEntryCategory', 'CATEGORY FUNCTION!!! ' + _code )
			return CATEGORY_CLASS


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _mode, _pattern ):
		if ( _category == CATEGORY_CLASS ):
			return _depth

		return 0


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Set the Key Replacement Prefix and Suffix
		self.ConfigAccessorFunc( 'KeyTextPrefix', CFG_KEY_TEXT_PREFIX, 'KeyTextSuffix', CFG_KEY_TEXT_SUFFIX )

		## Set Key Replacement Text for Ctrl, Alt, Shift and Win
		self.ConfigAccessorFunc( 'KeyTextCtrl', CFG_KEY_TEXT_CTRL, 'KeyTextAlt', CFG_KEY_TEXT_ALT, 'KeyTextShift', CFG_KEY_TEXT_SHIFT, 'KeyTextWin', CFG_KEY_TEXT_WIN,  )

		## Set the Key Replacement Text for the Special Chars - Mods Key: if other modifiers are pressed, allow the hotkey to execute! No Block Char: With this character, AutoHotkey will allow the Keys to pass-through to be interpreted by Windows, or other programs without needing to execute a Send command, or whatever...
		self.ConfigAccessorFunc( 'KeyTextAllowOtherActiveMods', CFG_KEY_TEXT_ALLOW_OTHER_ACTIVE_MODS, 'KeyTextAllowPassThrough', CFG_KEY_TEXT_NO_BLOCK_PASS_THROUGH )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## OnSetupConfig - Loads the current mapper configuration values into the active instance of the loader..
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Setup Current Mapper Configuration!

		## Desired Debug Mode - Set this to the name / tag to print only those, or True to print all debugging statements.. and the Language associated with the extension..
		self.SetCfgAccessorFuncValues( 'DebugMode', CFG_DEBUG_MODE, 'Language', CFG_PRIMARY_ACTIVE_LANGUAGE )

		## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
		self.SetCfgAccessorFuncValues( 'OutputFileEntriesInfo', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO, 'OutputFileEntriesInfoNewLines', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES )

		## New-Line preferred for reading Files and for outputting to the Code - Map Panel
		self.SetCfgAccessorFuncValues( 'NewLine', CFG_NEW_LINE, 'FileNewLine', CFG_FILE_NEW_LINE )

		## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
		self.SetCfgAccessorFuncValues( 'IndentChars', CFG_INDENT_CHARS, 'IndentCodeColumnMode', CFG_INDENT_CODE_COLUMN_WIDTH_MODE, 'IndentCodeColumnWidth', CFG_INDENT_CODE_COLUMN_WIDTH, 'LimitCatagoryColumnWidth', CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH )

		## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
		self.SetCfgAccessorFuncValues( 'TaskLinePrefix', CFG_TASK_LINE_PREFIX, 'TaskLineSuffix', CFG_TASK_LINE_SUFFIX )

		## Category Line Configuration - Show Description, and What to add at the end of the line ( if showing description, definitely use comment and "' to prevent highlighter mixing things up )
		self.SetCfgAccessorFuncValues( 'CategoryLineShowDesc', CFG_CATEGORY_LINE_SHOW_DESC, 'CategoryLinePrefix', CFG_CATEGORY_LINE_PREFIX, 'CategoryLineSuffix', CFG_CATEGORY_LINE_SUFFIX, 'CategoryDescPrefix', CFG_CATEGORY_DESC_PREFIX, 'CategoryDescSuffix', CFG_CATEGORY_DESC_SUFFIX )

		## Category Toggles
		self.SetCfgAccessorFuncValues( 'DisplayPersonalStory', CFG_DISPLAY_PERSONAL_STORY, 'DisplaySortMethod', CFG_CATEGORY_DISPLAY_SORT_METHOD, 'DisplayErrors', CFG_DISPLAY_CATEGORY_ERRORS, 'DisplayHelp', CFG_DISPLAY_CATEGORY_HELP, 'DisplayTasks', CFG_DISPLAY_CATEGORY_TASKS, 'DisplayNotes', CFG_DISPLAY_CATEGORY_NOTES )

		## For Function / Class Arguments Callback Settings
		self.SetCfgAccessorFuncValues( 'ObjectOpeningDelimiter', CFG_OBJECT_ARGUMENT_OPENING_DELIMITER, 'ObjectEndingDelimiter', CFG_OBJECT_ARGUMENT_ENDING_DELIMITER, 'ObjectArgSplitDelimiter', CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER, 'ObjectArgDefaultSplitDelimiter', CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER, 'ObjectArgJoinString', CFG_OBJECT_ARGUMENT_JOIN_DELIMITER, 'ObjectArgJoinPrefix', CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX, 'ObjectArgJoinSuffix', CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX )

		## Replace 'function' or 'def' with something shorter such as 'f' - Only enabled if Search is populated...
		self.SetCfgAccessorFuncValues( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH, 'FuncNameDefinitionReplace', CFG_FUNC_NAME_REPLACE )

		## Alter Function / Class Args to display Data-Types, Args or Both and set the character display data...
		self.SetCfgAccessorFuncValues( 'DisplayArgDataTypes', CFG_DISPLAY_ARG_DATA_TYPE, 'DisplayArgDataTypesSpacer', CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS, 'DisplayArgDataTypesPrefix', CFG_DISPLAY_ARG_DATA_TYPE_PREFIX, 'DisplayArgDataTypesSuffix', CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX )


		## Custom Additions - Add your custom configuration / AccessorFunc values here - Use the name of the function excluding Set* / SetCfg* followed by the value - chain the name / value together as seen above and below to add more on a single line if using the helper - you may also use the standard self.SetCfgName( x ) or self.SetName( x ). I'd recommend keeping this organized as I did above...

		## AccessorFunc Additions
		## self.SetAccessorFuncValues( 'NameOfFunc', CONST_, '', CONST_, '', CONST_, '', CONST_ )

		## Config AccessorFunc Additions
		## self.SetCfgAccessorFuncValues( 'NameOfFuncExcludingCfgPrefix', CFG_, '', CFG_, '', CFG_, '', CFG_ )

		## Set the Key Replacement Prefix and Suffix
		self.SetCfgAccessorFuncValues( 'KeyTextPrefix', CFG_KEY_TEXT_PREFIX, 'KeyTextSuffix', CFG_KEY_TEXT_SUFFIX )

		## Set Key Replacement Text for Ctrl, Alt, Shift and Win
		self.SetCfgAccessorFuncValues( 'KeyTextCtrl', CFG_KEY_TEXT_CTRL, 'KeyTextAlt', CFG_KEY_TEXT_ALT, 'KeyTextShift', CFG_KEY_TEXT_SHIFT, 'KeyTextWin', CFG_KEY_TEXT_WIN )

		## Set the Key Replacement Text for the Special Chars - Mods Key: if other modifiers are pressed, allow the hotkey to execute! No Block Char: With this character, AutoHotkey will allow the Keys to pass-through to be interpreted by Windows, or other programs without needing to execute a Send command, or whatever...
		self.SetCfgAccessorFuncValues( 'KeyTextAllowOtherActiveMods', CFG_KEY_TEXT_ALLOW_OTHER_ACTIVE_MODS, 'KeyTextAllowPassThrough', CFG_KEY_TEXT_NO_BLOCK_PASS_THROUGH )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


	##
	## Callback - Called for each line for additional error-checking and for processing rules for class recognition, stack popping, etc...
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		## Lets avoid comments
		if ( _in_comment or _in_comment_block ):
			return super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )		## self.CallParent( 'OnSetupConfig' )


		##
		## If we're calling a function and we used : instead of . - report the issue...
		##
		if ( re.search( "\w+:\w+\((.*)\)", _code ) ):
			## Add an Error Entry
			self.AddError( 'Syntax Error: ' + _code + ' [ Boolean Statement Error - Proper Usage is "=", not "==" ]', _line_number )

			## Mark the code... If applicable..
			return _code + ' [ Error ]'


		##
		## Detect Common AHK Typos and Errors coming from other lanugages..
		##
		if ( Acecool.string.indexOfExists( '==', _code, True ) ):

			## Add an example of how they should appear, but only add the example once...
			if ( not self.HasDisplayedErrorExample( 'AHK::BooleanStatements' ) ):
				## Convert the line number to string for easily adding it to the example output...
				_line = str( _line_number )

				## Add an Error Entry
				self.AddError( 'Syntax Error: ' + _code + ' [ Boolean Statement Error - Proper Usage is "=", not "==" ]', _line_number )

				## Create a simple if / else if structure as a copy / paste example..
				_example = ''
				_example += ';; ' + ':' + _line
				_example += '\n\t;; AutoHotkey Boolean Statement and Comparison Examples: -- From the error on line' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t;; Notes: ' + ':' + _line
				_example += '\n\t;;\t\tAutoHotkey uses = instead of == or === for is equal or equivalent to, and != for not equals to which is why it can be confusing for the non-evaluated data assignment operator to be used for determining equivalency ..' + ':' + _line
				_example += '\n\t;;\t\tNo error is invoked when using "==" in a boolean statement, and the script will execute. However, the code within the if statement will not be executed making this issue difficult to track down and an annoyance about this language!' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t;; Working Operators and Important Keywords: =, !=, >, >=, <, <=, not, and, or, true, false, break, return, continue' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t;; if ( _a == _b ) { ... } [ ERROR ON THIS LINE - Proper equivalency statements requires "=" to be used instead of "==" ]' + ':' + _line
				_example += '\n\t;; ' + ':' + _line
				_example += '\n\t\t;; Begin Boolean Statement to Compare...'
				_example += '\n\t\tif ( _a = _b && _b = _c ) then'
				_example += '\n\t\t\t< Code >'
				_example += '\n\t\t;; else if is identical to calling else and then adding an additional if within it - however when they are side by side only a single "end" is needed afterwards...'
				_example += '\n\t\telse if ( _c != _b && _b != _a ) then'
				_example += '\n\t\t\t< Code >'
				_example += '\n\t\t;; The last catch-all for this statement...'
				_example += '\n\t\telse'
				_example += '\n\t\t\t< Code >'
				_example += '\n\t\tend'
				## _example += '\n\t\t-- Logic outside and not influenced by the if statement above...'
				## _example += '\n\t\t< Logic / Code > -- End of Example for the error occurring on line:'


				## Add a blank line before the entry to allow space between examples...
				if ( not self.HasCategoryEntries( CATEGORY_ERROR_EXAMPLE ) ):
					self.AddBlankEntry( CATEGORY_ERROR_EXAMPLE )

				## Add the Example...
				self.AddErrorExample( _example, _line_number )

			return _code + ' [ Error ]'



			## Grab code from the next line...
			_code_pp = self.GetFileDataByLine( _line_number + 1 )

			## Helpers
			_bEndsWithEnd	= _code.endswith( 'end' )
			_bEndsWithThen	= _code.endswith( 'then' )
			_bThenExists	= Acecool.string.indexOfExists( 'then', _code, True )
			_bThenNextLine	= Acecool.string.indexOfExists( 'then', _code_pp, True ) #_code_pp.startswith( 'then' )

			## If The line ends with end and then doesn't exist somewhere - error.... If the line doesn't end with end, and then isn't the last word either, and then doesn't exist on this or the next line then error..
			if ( ( _bEndsWithEnd and not _bThenExists ) or ( not _bEndsWithThen and not _bThenExists and not _bThenNextLine ) ):
				## Add an Error Entry
				self.AddError( 'Syntax Error: ' + _code + ' [ Error - "then" keyword missing in or at the end of the line! ]', _line_number )


				## ## Add error Example Notice of repeated error...
				## self.AddErrorExample( '\n\t[ Error - Repeat: "then" keyword missing in-line ]', _line_number )

				## Add an example of how they should appear, but only add the example once...
				if ( not self.HasDisplayedErrorExample( 'Lua::if/ elseif / else if' ) ):
					##
					_line = str( _line_number )

					## Create a simple if / else if structure as a copy / paste example..
					_example = ''
					_example += '-- ' + ':' + _line
					_example += '\n\t-- Lua Structure Example: if / else if / elseif / else end -- From the error on line' + ':' + _line
					## _example += '\n\t-- Note: This will only appear once per file even if the same error occurs multiple times - A shorter message will appear for repeats...' + ':' + _line
					_example += '\n\t-- ' + ':' + _line
					_example += '\n\t\t-- Begin the if Statement...'
					_example += '\n\t\tif ( < Boolean Statements > ) then'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\t-- Add an elseif call...'
					_example += '\n\t\telseif ( < Boolean Statements > ) then'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\t-- else if is identical to calling else and then adding an additional if within it - however when they are side by side only a single "end" is needed afterwards...'
					_example += '\n\t\telse if ( < Boolean Statements > ) then'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\t-- The last catch-all for this statement...'
					_example += '\n\t\telse'
					_example += '\n\t\t\t< Code >'
					_example += '\n\t\tend'
					## _example += '\n\t\t'
					## _example += '\n\t\t-- Logic outside and not influenced by the if statement above...'
					## _example += '\n\t\t< Logic / Code > -- End of Example for the error occurring on line:'

					## Add the Example...
					self.AddErrorExample( _example, _line_number )

				## Return the modified line - Note: If the line isn't to be added then this doesn't matter.. It won't add it simply because of an error - the error above will be added though..



		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		return super( ).OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )		## self.CallParent( 'OnSetupConfig' )


	##
	## Accessor - This is required because we process as little as possible in Run when cached file is to be loaded meaning NO AccessorFuncs... We have to Re-add it for each language class because the CFG_ will be different ( this will be removed when sublime-settings files are used )
	##
	def GetCfgCachingSystemEnabled( self ):
		return CFG_CACHING_SYSTEM_ENABLED


	##
	## Accessor - For Developer Mode - pass-through for caching system..
	##
	def GetDeveloperMode( self ):
		return DEVELOPER_MODE


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End Vanilla Language Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin PHP Project Acecool Content Management System Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
## Acecool AutoHotkey Framework Mappings
##
class XCodeMapper_AutoHotkey_Acecool( XCodeMapper_AutoHotkey ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_AutoHotkey_AcecoolAHK_Framework'



	pass


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End PHP Project Acecool Content Management System Mapper
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## Begin User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


##
##
##
class XCodeMapper_AutoHotkey_User( XCodeMapper_AutoHotkey ):
	##
	## Important Declarations
	##

	## Class Name
	__name__ = 'XCodeMapper_AutoHotkey_User'


	##
	## Additional Callback used for hiding default categories, or completely changing the category order.
	## Return True / False to prevent the default categories from being output - you would then need to re-add all categories you want in the order you want them to output...
	##
	def OnSetupCategoryOutputOrder( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCategoryOutputOrder' )

		## Call one per line or many per call - These are categories you want to hide - This overrides the CFG_ values...
		## self.AddHideCategoryEntry( CATEGORY_NOTE, CATEGORY_DEBUGGING, CATEGORY_DEFINITION, CATEGORY_OPTIMIZATION, CATEGORY_HELP )

		## Return true or false to prevent all default categories from being added so you can set up your own order from scratch..
		return None


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnSetupSyntax( )		## self.CallParent( 'OnSetupSyntax' )


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	def OnAccessorFuncExpansion( self, _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Base Args - _tab will be the first arg in all cases...
		_arg_tab = Acecool.table.GetValue( _args, _args_count, 0, self.__name__ )

		## This is the first arg without any decimals - and we remove everything before last decimal in TitleCase - ClassName ie Timekeeper or Data...
		_arg_class = Acecool.string.SubStr( _arg_tab, 10, len( _arg_tab ) ).title( )

		##
		##

		##
		##


		##
		## Fallthrough to thep parent so their syntax can be added to our own...
		##
		super( ).OnAccessorFuncExpansion( _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )		## self.CallParent( 'OnAccessorFuncExpansion', _mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count )


	##
	## Create Custom AccessorFuncs and ConfigAccessorFuncs here...
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs', 'Creating custom AccessorFuncs for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )

		## Example AccessorFunc Addition
		## self.AccessorFunc( 'Name', CFG_OR_OTHER_DEFAULT_VALUE_ )

		## Example Config AccessorFunc Addition
		## self.ConfigAccessorFunc( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupAccessorFuncs( )		## self.CallParent( 'OnSetupAccessorFuncs' )


	##
	## Setup this mapper configuration values here...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig', 'Applying custom settings for the current mapper ( ' + CFG_PRIMARY_ACTIVE_LANGUAGE + ' )!' )


		##
		## Set the values of the config additions the mapper...
		##

		## Custom A
		## self.SetCfgNameXXXXXXXName( CFG_XLUA_XXXXXXX )

		## Custom B
		## self.SetNonCfgNameXXXXXXXName( CONST_XLUA_XXXXXXX )


		##
		## Fallthrough to the parent - so anything they do can also be processed...
		##
		super( ).OnSetupConfig( )		## self.CallParent( 'OnSetupConfig' )


##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
## End User Language Mapping Class
##----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------