##
## XCodeMapper, an extension for the Sublime Text 3 Package CodeMap by oleg-shilo - Josh 'Acecool' Moser
##


##
## Task List
##
## Task: Add Language System
## Task: Alphabetize category outputs - IF the user config set it up to do so - or if AddCategoryAlphabetize( HELP, FUNCTIONS, CLASSES ) or whatever is called...
## Task: Add new Arg to OnFunction/ClassArg Callback - _arg_default = '', using ArgDefaultSplitDelimiter - Note: May need to check for more than 1 delimiter... will see....
## Task: Create Dynamic nested Category system for Python language or any language with objects so the CLASS category can be the headers, and the functions category can have the class-headers as a category ( with line jump ) and the functions underneath ( which can then be sorted as they're in their own category instead of mixed )
## Task: Create system to grab the Delta Depth / Delta Indentation Length - for when OnCalcDepth returns a different value and definitions or other code is converted thereby offsetting what used to be all indented to the same column to incorrectly offset... Taking the DELTA and re-adding it to the middle where the tabs are is very important...
## Task: Figure out why the TypoFix Entries aren't being processed....
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:
## Task:

##
## Rule system
##
## Task: Add rule system
## Task: Add basic rule-sets such as: Tracking scope ( for classes, if statements, files, etc.. ), variable tracking ( along with a rule to define if the language is case sensitive or not so errors can be pointed out if a variable is used but never declared )
## Task:
## Task:

##
##
##
##
##
## Caching System - Implemented..
##
## X-Task: Add / Create Cache System - If the file we're trying to map isn't cached OR the file being mapped timestamp is newer than cached OR If the mapper timestamp is newer than the file: then reload... otherwise: Simply use the cached version - this should speed certain things up...
## X-Note: Caching System: By default it is set to False because of some other features I need to add - it is quicker than reparsing the data if the data hasn't changed but without processing the timestamp of the mapper which is loaded then editing the mapper ( which would change the data ) won't trigger the system to regenerate the file yet....
## X-Task: Add in a system to delete caches which are over 1 day old for automatic reparsing ( and have it configurable )
## O-Task: Logic needs to be added to determine which custom mapper is being used - then timestamp the mapper too! this way if the mapper has changed, then the
## O-TODO: Add in a system to delete caches which are over 1 day old for automatic reparsing ( and have it configurable ) This is so you don't end up with thousands of files ( only one cache file is generated PER although I may incorporate a backup system which can create a backup every save, every 5 or 10 saves, or how many you want, and delete after having a total of X backups, ie deletes the oldest, or deletes if x days old, etc.. )... This caching system opens the door to a lot of other features which are incredibly useful to have!!!!
## O-TODO: I need to read the MAPPER timestamp too so if the Python mapper has changed, but not the python file we want to map - it'll remap it so if you change the config on how it should output, you'll be able to see it... etc.. Logic needs to be added for this..
##
## Task: Create Backup System
## Task-:	Creates backup every N Saves ( 1, 5, 10, 25 or how ever many you want. 0 and -1 disables )....
## Task-:	Delete oldest backups after N files ( of the same file - 10 backups, 25, 100, 1000, or how ever many.. -1 disables ).
## Task-:	Delete oldest backups after they are N days old ( 1 day, multiple... Note: This will apply to cache output too although it'll be a different variable )
##
## Task: Update Cache System
## Task-:	Add Delete old caches if they are older than N Days ( 7 by default - can be anything you want - Time in seconds so it can be in seconds, minutes, hours, days, months, years, millenia, etc.. or disable deleting by using -1 )..
## Task-DONE:	Add logic to check the Mapper file Timestamp - If it has changed, then reparse a file even if it hasn't changed because the mapper can change the output...
## Task-DONE:	Add logic to check if DEVELOPER_MODE is True - if so then check timestamps for XCodeMapper.py, XCodeMapper_Definitions.py, and AcecoolST3_Library.py because reloading without restarting SublimeText is enabled therefore we need to be able to reparse the file..
##
##
##


##
## Imports
##

## Import Sublime Library
import sublime
import sublime_plugin
import os
import errno
import io
import datetime
import time

## Import Acecool's Library of useful functions and globals / constants...
from User.CodeMap.Acecool.AcecoolST3_Library import *

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from User.CodeMap.Acecool.XCodeMapper_Definitions import *

## Grab Sublime Text Settings for packages which are ignored and which are installed!
ignored		= sublime.load_settings( 'Preferences.sublime - settings' ).get( 'ignored_packages' )
installed	= sublime.load_settings( 'Package Control.sublime - settings' ).get( 'installed_packages' )


##
## Default Configuration ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Important Default Configuration Options
##

## Debugging Mode - Set this to True to display ALL debugging messages or set it to a tag / id to only print those. By default this is True so a few messages will print until the local mapper config activates and some messages will always be displayed...
CFG_DEBUG_MODE									= False
## CFG_DEBUG_MODE									= 'AccessorFunc'


##
## Language we're using...
##
CFG_PRIMARY_ACTIVE_LANGUAGE						= 'LANG_INITIALIZING'


##
## Caching System
##

## Do you want to use the caching system? ( Typically 0.5 seconds to process 5000 lines of code and 0.02 to save - 0.01 to load without needing 0.48 to reprocess... - Times from SSD Hard Drive and [ 10 year old Intel Q6600 2.4 GHz Quad OC on AIR @ GHz 3.0 on air for entire lifetime, 8GB DDR2, Asus P5k Mbd ] )
CFG_CACHING_SYSTEM_ENABLED						= False


##
## Category Related
##

## The default for Alphabetizing the data in categories.... Sort method used... By line number is already default... First seen first out... add _DESC to reverse it, or _ALPHABETICAL for A-Z or _ALPHABETICAL_DESC for Z-A
CFG_CATEGORY_DISPLAY_SORT_METHOD				= ENUM_SORT_METHOD_LINE_NUMBER

## Display the Help and Support Category? When set to True it ouputs Download and Support Information for both oleg-shilo, Creator of CodeMap which is incredibly useful Package for Sublime Text Editor, and Josh 'Acecool' Moser who created XCodeMapper which is an extension for CodeMap to add more features!
CFG_DISPLAY_CATEGORY_HELP						= False

## Display the Errors List Category - By default it is at the very top and only visible when errors are found?
CFG_DISPLAY_CATEGORY_ERRORS						= True

## Display the Tasks / TODOs / Work / etc.. Category? By default it is enabled - Tasks are typically important but they can pile up. If you want to hide the category temporarily or permanently - set it here..
CFG_DISPLAY_CATEGORY_TASKS						= True

## Display the Notes / Info / etc.. Category? By default it is enabled - however Notes aren't always important - if notes are getting in the way of other data you can hide the category temporarily or permanently...
CFG_DISPLAY_CATEGORY_NOTES						= True


##
## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
##

## Should the line be displayed?
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO				= True

## If the line is to be displayed ( above set to True ), how many new-lines should we add afterwards? 2 gives a blank line between it and the first category output..
CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES		= 2



##
## New-Line preferred for reading Files and for outputting to the Code - Map Panel
##

## New-Line character used for the Code - Map Panel
CFG_NEW_LINE									= '\n'

## New-Line character used to delimit the active-file.. The new-line type will appear under TOP_MENU > View > Line Endings > [ Windows / Unix / Mac OS 9 ]
CFG_FILE_NEW_LINE								= '\n'


##
## Indentation
##

## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
## CFG_INDENT_CHARS								= '\t'
CFG_INDENT_CHARS								= '    '

## Set this to -1 to disable the column sizing feature ( meaning line :1234 shows up right at the side of the code output to CodeMap Panel ). Set it to 1 to truncate values meaning the column will be max of that plus the line-size. Set it to 0 to try to force it to that column, but don't truncate. If it goes over, use -1 behavior on that line... This way the line numbers line up nicely most of the time..
CFG_INDENT_CODE_COLUMN_WIDTH_MODE				= ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED

## Configure the maximum width code should take up in the output before being truncated... Then the line column comes... Note, there will be an additional callback so you can define this on the fly for specific categories so if you want notes / tasks to have no limit, no worries... If you want to disable this feature set it to -1 also note that the line one will be turned off too.. Also, you can set this to a large value, just remember that spaces are used!! I'd recommend keeping it under 255 or so chars per line which is about 1080px wide if viewing on a 4k monitor with a few ticks up in font-size... But at that width you'll unlikely see the line number... keep that in mind...
CFG_INDENT_CODE_COLUMN_WIDTH					= 125

## Should we limit the Category Name / Description Length? If so, use CFG_INDENT_CODE_COLUMN_WIDTH + 5 for file with 9999 max lines, or 6 with 99,999 for good measure so they'll be lined up properly...
CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH		= CFG_INDENT_CODE_COLUMN_WIDTH + 6

## ## Deprecated - no point in limiting the line-size - Configure how many chars the code column should take - not, : counts as one of these characters and 4 extra means 9999 line limit... I'd recomment 10 if you work with big files to also allow space... I'll add Right align soon...
## CFG_INDENT_LINE_COLUMN_WIDTH					= 10

## ## Do you want the Category Name, and Description to adhere to the same size? This prevents side-scrollbars...
## CFG_TRUNCATE_CATEGORY_NAME_DESC					= True


##
## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
##

## If you want to comment out Task / TODO lines, set this to the language comment
CFG_TASK_LINE_PREFIX							= ''

## This suffix is used to ensure the rest of the Code - Map Panel doesn't show up as a single-color.. Simply use a block-comment or comment with a single and double-quote to ensure the syntax highlighter catche sit...
CFG_TASK_LINE_SUFFIX							= '\t--"\''


##
## Category Output Configuration For the Code - Map Panel
##

## Category Line Prefix and Suffix - the Description variant is for before and after the description which is hidden if the above is False... This is for the entire line before and after...
CFG_CATEGORY_LINE_PREFIX						= '// '
CFG_CATEGORY_LINE_SUFFIX						= ''

## Should we display the Category Description?
CFG_CATEGORY_LINE_SHOW_DESC						= True

## The Prefix to be used separating the Category Name from the Description - I'd recommend something similar to ' - ', maybe with tabs, etc... You can also use Acecool.string.FormatColumn( 25, ' - ' ) to add 22 spaces after ' - '... Later I may add support for the category output to use this function so all category names / descriptions line up in the output...
CFG_CATEGORY_DESC_PREFIX						= ' - '

## If we show the Category Description, I highly recommend you set this to use a comment for the language along with ' and " - or simply use CFG_TASK_LINE_SUFFIX - to prevent the highlighter from messing up and coloring everything one color..
CFG_CATEGORY_DESC_SUFFIX						= CFG_TASK_LINE_SUFFIX


##
## Function / Class For-Each Argument Callback Configuration
##

## This is what separates the function or class from the arguments passed through - Example: func<OPENING_DELIMITER> x, y, z )
CFG_OBJECT_ARGUMENT_OPENING_DELIMITER			= '('

## This is what separates the arguments from the end function / class call or declaration - Example: func( x, y, z <ENDING_DELIMITER>
CFG_OBJECT_ARGUMENT_ENDING_DELIMITER			= ')'

## This is the character used to delimit or split arguments - It isn't recommended to use more than 1 char otherwise other coding-standards may not be properly read... func( x<SPLIT_DELIMITER> y<SPLIT_DELIMITER> z ) - Split is the first action that takes place...
CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER				= ','

## This is to split a default value from an argument... ie func ( x, y, z = true ) in some languages means z, if unset, is set to true...
CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER		= '='

## This is the string used to re-join the list of modified arguments back into a string - A comma and space here provide an Airy output: func( x<JOIN_DELIMITER>y<JOIN_DELIMITER>z ) - Join occurs after the args have been processed and possibly modified... This can be 2 or more chars, typically a comma and space to allow the args to flow..
CFG_OBJECT_ARGUMENT_JOIN_DELIMITER				= ', '

## These are what comes between the arguments on either side of the rejoined args string, and the opening / ending delimiter - Spaces here provide an Airy output: func(<PREFIX>x, y, z<SUFFIX>)
CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX				= ' '
CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX				= ' '


##
## If CFG_FUNC_FIND_TEXT_REPLACEMENT is set then we look for CFG_FUNC_FIND_TEXT and replace it with CFG_FUNC_FIND_TEXT_REPLACEMENT... so function can become def, func, f, etc.. Space-savings! Can be used for things other than altering function name.. Made so PreOnAddEntry doesn't need to be added to add this feature!
##
## Note: This was made default to make it easier to add a single Add / Replace to PreOnAddEntry - The most common use is to replace 'function' or something with 'func' or 'def' in order to save space... I may extend this later to be comma or tab delimited and alter the var-names..
##

## If CFG_FUNC_TEXT above is not '' then set this to what you are looking to replace... Add the space in both, or neither
CFG_FUNC_NAME_SEARCH							= ''

## Internal helper for GLua - This defines the text to be used for function declarations as function in Callback.PreOnAddEntry is replaced to save space as some of the function / object declarations in Lua and GLua are quite long...
CFG_FUNC_NAME_REPLACE							= ''


##
## Data-Type Arg Injector
##

## Determine how the Data-Type system should apply data... ENUM_DISPLAY_DATA_TYPE_DEFAULT == Leaves the argument as is / _TYPE_BOTH == displays <DataType> _arg - assumes ' ', '<', '>' cfg / _TYPE_ONLY == <DataType> only
CFG_DISPLAY_ARG_DATA_TYPE						= ENUM_DISPLAY_DATA_TYPE_DEFAULT

## How data-types appear - for when they are added to, or replacing, function args... ie if the following 3 are: ' ' '-' 'x' they'd appear: ( -Playerx _p, -Weaponx _w ) or ' ' '<' '>' they'd appear: ( <Player> _p, <Weapon> _w )
CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS			= ' '
CFG_DISPLAY_ARG_DATA_TYPE_PREFIX				= '<'
CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX				= '>'


##
## Personal Information
##

## Display my personal story?
CFG_DISPLAY_PERSONAL_STORY						= False


##
## Custom Additions ( for Callback class ) Config - ie things you've added - I'd highly recommend using this area instead of hard-coding values in case you decide to share your Callback class for Language <X> with anyone else...
##

##
CFG_ENTRY_BLANK									= '\t'


##
## End Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##




##
## Begin Development Example Configuration ( Serves no purpose other than to show off the Code Alignment System ) --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##


##
## Development Test of new OnCalcCodeAlignment System
##
if ( True == False ):
		## These are for Config category ...
		## If we use OnCalcDepth and we ADD depth relative to _depth, ie indent more then we subtract whitespace between var and =...
		## If we use OnCalcDepth and we SUBTRACT depth depth relative to _depth, ie we indent less then we ADD the difference to the whitespace between the var and =...
		## This is so, no matter how it is written - as long as it is aligned in your code, it will be aligned in the mapper...
		CFG_BLAH_T								= True	# 1) Here 2 tabs will be added between CFG_BLAH
		CFG_BLAH_U								= True	# 2) and = True Because depth will be subtracted
		CFG_BLAH_V								= True	# 3) to 0, we need to add whitespace between the
		CFG_BLAH_W								= True	# 4) values to ensure code alignment is continuous..

## Extended
if ( True == False ):
	CFG_BLAH_Y									= True	# Note: 1 tab here because depth == 1 and we return 0

##
CFG_BLAH_Z										= True	# Note: No tabs here because depth == 0


##
## End Development Example Configuration --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##




##
## Begin XCodeMapper Code --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
##



##
## Creates custom helper-functions for the Sublime Region Class
##
## Note: Are regions defined by character / columns? If so then each line needs to be tallied then when going from one line to another - the start for that line is all of the previous lines, then to end is the line on top of that plus all lines to end...
##
class XCodeMapperSublimeTextRegion( sublime.Region ):
	##
	## Creates a region based on line-number instead of character number...
	##
	def DefineByLineNumber( self, _text, _start, _end ):
		pass



##
##
##
class XCodeMapperRunTimeBase( ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperRunTimeBase'


	##
	## Internal Handler / Hook Caller - Called when a new instance is being created...
	##
	##
	def __init__( self ):
		pass


	##
	## Internal Handler / Hook Caller
	##
	def Shutdown( self ):
		pass


##
##
##
class XCodeMapperRunTime( XCodeMapperRunTimeBase ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperRunTime'


##
##
##
class XCodeMapperPanel( sublime.Window ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperPanel'


##
## Used for temporary or developer functions... or for empty callbacks..
##
class XCodeMapperBaseDev:
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperBaseDev'


##
## Used for temporary or developer functions... or for empty callbacks..
##
class XCodeMapperBaseEmptyCallbacks( XCodeMapperBaseDev ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperBaseEmptyCallbacks'

	##
	## Callbacks for initializing local settings so the defaults aren't used...
	##
	def DeveloperFunc( self ):
		pass
	def OnRunDeveloperFunc( self ):
		pass
	def PreOnReset( self ):
		pass
	def PostOnReset( self ):
		pass
	def PreOnSetup( self ):
		pass
	def PostOnSetup( self ):
		pass
	def PreOnShutdown( self ):
		pass
	def PostOnShutdown( self ):
		pass
	def OnInitAccessors( self ):
		pass
	def OnInitConfigAccessors( self ):
		pass
	def PreOnRun( self ):
		pass
	def OnRun( self ):
		pass
	def PostOnRun( self ):
		pass
	def PreOnSetupInternalAccessorFuncs( self ):
		pass
	def PostOnSetupInternalAccessorFuncs( self ):
		pass
	def PreOnSetupConfigAccessorFuncs( self ):
		pass
	def PostOnSetupConfigAccessorFuncs( self ):
		pass
	def Snippets( self ):
		pass


##
## Accessors base...
##
class XCodeMapperBaseAccessors( XCodeMapperBaseEmptyCallbacks ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperBaseAccessors'


	##
	## Accessor - This is required because we process as little as possible in Run when cached file is to be loaded meaning NO AccessorFuncs... We have to Re-add it for each language class because the CFG_ will be different ( this will be removed when sublime-settings files are used )
	##
	def GetCfgCachingSystemEnabled( self ):
		return CFG_CACHING_SYSTEM_ENABLED


	##
	## Accessor - For Developer Mode - pass-through for caching system..
	##
	def GetDeveloperMode( self ):
		return DEVELOPER_MODE


	##
	## Helper - Grabs the Code - Map Panel..
	##
	def GetCodeMapPanel( self ):
		return self.__panel__


##
## Helper base...
##
class XCodeMapperBaseHelpers( XCodeMapperBaseAccessors ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperBaseHelpers'

	##
	## Begin Throw-Away functions list - These functions are for important AccessorFuncs or otherwise which get replaced later on - they exist because some other important function relies on the data from these and are called prior to the default clalbacks...
	##
	## TODO: Ensure the helper AccessorFuncs are created before anything which needs them is called...
	##


	##
	## Assign a default - throwaway function for the functions which call these before they're made
	##
	def GetCfgLanguage( self ):
		return CFG_PRIMARY_ACTIVE_LANGUAGE


	##
	## Assign a default - throwaway function for the functions which call these before they're made
	##
	def GetCfgDebugMode( self ):
		return CFG_DEBUG_MODE


	##
	## Replaceable Helpers
	##
	## Note: This category exists so the custom_mapper can replace these to alter core behavior easily... These are mostly True / False return-type functions...
	##


	##
	## Returns true when in a comment or debugging category type.. This is used as a controlled toggle to ensure parsing occurs in comments and not in non-comment-sections. This allows Notes / Tasks, etc.. items to be located within comments.
	##
	def InCommentDebuggingCategory( self, _category ):
		return ( ( self.GetCategoryType( _category ) == CATEGORY_TYPE_IN_COMMENT ) or ( self.GetCategoryType( _category ) == CATEGORY_TYPE_DEBUGGING ) )





	##
	## Translation / Compiling / etc... helpers..
	##


	##
	## Return the user-friendly category name - used for category header...
	##
	def GetCategoryName( self, _category ):
		return MAP_CATEGORIES.get( _category, CATEGORY_UNKNOWN )


	##
	## Return the Category Type ID
	##
	def GetCategoryType( self, _category ):
		return MAP_CATEGORIES_TYPE.get( _category, CATEGORY_TYPE_UNKNOWN )


	##
	## Return the Category Type ID
	##
	def GetCategoryTypeName( self, _category ):
		return self.GetCategoryName( _category )


	##
	## Return the Category Description String
	##
	def GetCategoryDesc( self, _category ):
		return MAP_CATEGORIES_DESC.get( _category, CATEGORY_DESC_UNKNOWN )


	##
	## Return the Depth String - which is the number of tabs, spaces or whatever the config is set to for chars to multiply when indentation is required...
	##
	def GetDepthString( self, _depth = 0 ):
		return Acecool.string.repeat( self.GetCfgIndentChars( ), _depth )




	##
	## Config System
	##
	def GetConfigValue( ):
		## Base Path... the User folder... We can actually use sublime.load settings because of the user folder use...
		_path = os.path.join( sublime.packages_path( ), 'User' )

		## XCodeMapper Config Files...
		_cfg_default = os.path.join( _path, 'Acecool_XCodeMapper_Config.Default.sublime-settings' )
		_cfg_user = os.path.join( _path, 'Acecool_XCodeMapper_Config.User.sublime-settings' )

		## Custom Mapper Config Files...
		_cfg_mapper_default = os.path.join( _path, 'Acecool_XCodeMapper_Config_' + _ext + '.Default.sublime-settings' )
		_cfg_mapper_user = os.path.join( _path, 'Acecool_XCodeMapper_Config_' + _ext + '.User.sublime-settings' )


	##
	## Simple Helper / Redirect to grab a value from a table of arguments ( for OnFunction/ClassArgument and AccessorExpansion )
	##
	def GetTableValue( self, _args, _args_count, _key, _default ):
		return Acecool.table.GetValue( _args, _args_count, _key, _default )


	##
	##
	##
	def GetSublimeActiveView( self ):
		return self.GetSublimeActiveWindow( ).active_view( )


	##
	##
	##
	def GetSublimeActiveWindow( self ):
		return sublime.active_window( )


	##
	## Helper - Get the view we're looking for...
	##
	def GetSublimeView( self, _name = 'Code - Map' ):
		## Grab all of the active views for the current window...
		_views = sublime.active_window( ).views( )

		## The view we're targeting
		_view = None

		## Test the first view views

		## Code - Map is typically the LAST view, always...
		_view_last = _views[ len( _views ) - 1 ]
		if ( _view == None and _view_last.file_name( ).strip( ).endswith( _name ) ):
			return _view_last

		## First view..
		_view_first = _views[ 0 ]
		if ( _view == None and _view_first.file_name( ).strip( ).endswith( _name ) ):
			return _view_first

		## Active View
		_view_active = sublime.active_window( ).active_view( )
		if ( _view == None and _view_active.file_name( ).strip( ).endswith( _name ) ):
			return _view_active

		## if the last view is what we're looking for, don't continue..
		if ( _view == None ):
			## Let the user know
			## print( 'Suspected Code - Map Panel is incorrent.. loop needed!' )

			## Loop through up to all views - 1 to find the Code - Map Panel if the above one is incorrect... We don't need to look at the last one because we tried that up above...
			for _i in range( len( _views ) - 1 ):
				## Reference the view
				_view = _views[ _i ]

				## If the view is what we we're looking for then we exit the loop so we can return it ( we could return it here too... )
				if ( _view.file_name( ).strip( ).endswith( _name ) ):
					## Notify
					## print( '>> Code - Map Panel Identified: ID: "' + str( _view.id( ) ) + '" / Name: "' + _view.name( ) + '" / File-Name: "' + _view.file_name( ) + '"' )

					## Exit the loop
					## break

					## Return from here..
					return _view

		##
		return _view



##
## Used for a Class Template - holds all of the standard replacement callbacks...
##
class XCodeMapperClassTemplate( XCodeMapperBaseHelpers ):

	##
	## Callback - Return True or False to decide whether or not you want the line-numbers to be added to an entry - by returning False it also means you can't double-click to jump to that line... This was added for the .txt mapper...
	##
	def ShouldDisplayLineJumpLink( self, _category, _line_number, _code, _depth ):
		##
		return True


	##
	## Callback - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	## In Short: If working this should give us all of the information from the Command Palette when we look at @, :, etc.. for function names and everything... A potentially almost perfect way to create a universal mapper...
	##
	## Task:
	##
	def OnMappingFileSymbols( self, _view, _symbols ):
		pass


	##
	## This is used to define the elements of the language or implementation / usage ( ie if using lang with framework, we can track framework components too ) we want to earmark for quick-access!
	##
	## Note: When you add search parameters for more than 1 category ( first arg ) so CATEGORY_FUNCTION .. make sure you add them in the order you want to look for them because when one is found, the rest
	## 	are ignored for that line... For example, if I search for CATEGORY_FUNCTION StartsWith function and CATEGORY_FUNCTION StartsWith function Acecool.C. and function Acecool.C.BLAHBLAH:BLAH( ) is discovered
	## 	then the first CATEGORY_FUNCTION StartsWidth function will CLAIM that line so if my search parameter was going to do anything special to that particular function branch, it becomes moot and is lost to
	## 	the first...			So remember: First come, first serve... first found, all others skipped....
	##
	##
	## Task: Allow OnSetupSyntax to use the Config file for base / vanilla definitions? Maybe on this one...
	#
	##
	## Callback - Used to setup rules to track code, expand AccessorFuncs, etc.. at to display the output in the Code - Map Panel. Priority is handled using standard English Reading direction ie using self.AddSyntax( COMMENT, STARTSWITH, '#', '##' ) means ## would never be handled.
	##
	def OnSetupSyntax( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntax' )

		##
		## The following are added to almost every mapper.. These categories use a category type which is only active inside of comments so they can trigger inside of comments, or out.
		## Comments are stripped from the line and that result is used to search so StartsWidth will work even if the line starts with ;; TODO: Testing..
		##

		## Task Related - I highly recommend adding a space in front to prevent detecting function calls which start with these words in some languages which use : such as Lua..
		self.AddSyntax( CATEGORY_TODO, SEARCH_TYPE_STARTSWITH, 'TODO: ', 'Fix: ', 'Task: ', 'Work: ', 'Update: ', 'Important: ', 'Debug: ' )
		self.AddSyntax( CATEGORY_COMPLETED_TASK, SEARCH_TYPE_STARTSWITH, 'Done: ', 'Fixed: ', 'Worked: ', 'Updated: ', 'Debugged: ' )

		## These are typically added to code for the developer.. they aren't important enough to list in the "important work / todo" area of our list... but they can be added..
		self.AddSyntax( CATEGORY_NOTE, SEARCH_TYPE_STARTSWITH, 'Note: ', 'Info: ', 'Debugging: ' )


	##
	## Same as OnSetupSyntax but kept as a deparate function to set up indentation tracking, and other rules...
	##
	## Task:
	##
	def OnSetupSyntaxRules( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupSyntaxRules' )

		##
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "for" )
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "do" )
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "while" )
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "if" )

		## then is used in many cases - determine which rules I can skip by including then and add the ones which don't use it..
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "then" )

		##
		## self.AddSyntax( CATEGORY_RULE_UNINDENT, SEARCH_TYPE_CONTAINS, "end" )

		## These cancel each other out - and since an IF always preceeds these, it doesn't matter...
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "else if" )
		## self.AddSyntax( CATEGORY_RULE_INDENT, SEARCH_TYPE_CONTAINS, "else" )
		## self.AddSyntax( CATEGORY_RULE_UNINDENT, SEARCH_TYPE_CONTAINS, "else if" )
		## self.AddSyntax( CATEGORY_RULE_UNINDENT, SEARCH_TYPE_CONTAINS, "else" )


	##
	## Setup Data-Type to Arg Conversion. This is used for each function / class argument... This feature is to give more information to the overview panel so it can appear similar to a WIKI overview for all of the data without the long descriptions ( depending on the cfg )
	## The entire systems purpose is to give just enough data to be incredibly useful, but not enough to overwhelm or confuse.. Keep it concise.. This feature can inform you of the data-type to expect to use. For this to work, you need to set and follow coding-standards - more specifically: Argument Naming Standards sub-section.
	##
	## If the configuration is set up to do anything ( ie non-default ) then these are processed. If set to Default or _ARG then nothing happens...
	## If set to Both then they'll appear as: '<DataType> _arg' / If set to Data-Type the args will be replaced with only: '<DataType>'
	## The Space after the suffix, prefix as '<', and suffix as '>' chars and the arg is controlled by the Spacer, Prefix and Suffix Configuration Options respectfully.
	##
	## Task:
	##
	def OnSetupArgDataType( self ):
		##
		## Note: I populated this mapper with several examples to show they are unique - ie - and several to show what this system can do.
		## 	I will likely add more elements to this to map out what each function should receive for developmental purposes or as a sort of quick-wiki output / helper for CodeMap..
		## 	Additionally, If you modify the arguments with OnClassArgument or OnFunctionArgument - this system will NOT overwrite your modifications, this is by design.
		## 	I may modify this system so it reads the modified version into the helper-function to determine whether or not it can modify the modification.. However, being able to have the last-say in OnClass/FunctionArgument can be useful in certain scenarios!!
		##
		## This system was created to simplify modifying arguments - add a single line and modify function / class arguments with ease instead of adding the callbacks and setting up if / elif lines of code...
		##

		## Adds an example which replaces 'str' argument of functions with <BlahBlahBlah> to show that class TestingString( str ): will not be affected by it...
		## self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, 'BlahBlahBlah', 'str' )

		## Adds a replacement for 'str' argument in CLASS categories - Functions will not receive this modification... This is to show that class TestingString( str ): will be modified properly..
		## self.AddArgDataTypeEntry( CATEGORY_TYPE_CLASS, 'Extends Default Object String', 'str' )

		## Adds a replacement for 'self' argument in FUNCTION categories - Classes will not receive this modification... This is to show that all functions with self as an argument will be properly modified!
		## self.AddArgDataTypeEntry( CATEGORY_TYPE_FUNCTION, 'This', 'self' )
		pass


	##
	## Similar to OnFunctionArgument - does the same thing for CLASS arguments - type specific...
	##
	## Task:
	##
	def OnClassArgument( self, _arg, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnClassArgument', _arg )

		## Return the original, or altered, argument.. If you return True or False, the argument is removed. If you return None, then the original argument is used.
		return _arg


	##
	## Called for each function argument - allows you to do anything you want...
	##
	## example: by replacing _p and _w which are hard-coded variable-names in my coding standard..
	##	function GM:PlayerDeath( _p, _w )
	## can become
	##	function GM:PlayerDeath( <Player>, <Weapon> )
	## or
	##	function GM:PlayerDeath( <Player> _p, <Weapon> _w )
	##
	## Task:
	##
	def OnFunctionArgument( self, _arg, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnFunctionArgument', _arg )

		##
		return _arg


	##
	## When an AccessorFunc type is found, this is the callback executed which allows you to expand the AccessorFunc into multiple entries...
	##
	## example: When encountering a GMod Lua / GLua util function, it is expanded into 2 entries representative of the 2 functions AccessorFunc creates..
	##
	## Code:
	## 	AccessorFunc( perp.lighting, "somevar", "LightsEnabled", FORCE_BOOL )
	##
	## XCodeMapper Entries - I set it to add into CATEGORY_FUNCTION_ACCESSOR categories:
	##	function perp.lighting:GetLightsEnabled( );:102
	##	function perp.lighting:SetLightsEnabled( _value :: Note: _value forced tobool( ) on set! );:102
	##
	##
	## Because each AccessorFunc creation method is different for each function we look for... they're all routed through here to be expanded upon...
	## If pattern x then args[ 1 ] = classname, args[ 2 ] = name ( so S/Get<Name> etc.. ), and more... so if args[ n ] is "" then we don't add Get prefix to the Getter.. and so on..
	## Also, we can force the data to show up in different categories.. for example __ functions can be put into CATEGORY_FUNCTION_INTERNAL which appears last in list of functions..
	## or we can determine whether or not x function was already added which has an optional creation function ( ie on first time call of an accessor func, it
	##  creates __GetDataTable, __GetData, __SetData internal function but the next time we call it on the same class, it shouldn't output these ).. etc...
	##
	## The benefit of this function is that you can do ANYTHING you want with the data of AccessorFuncs ( I may add callbacks for each category type or search later on, but right now AccessorFunc is what needs it )
	##
	## Task:
	##
	def OnAccessorFuncExpansion( self, _search_mode, _category, _pattern, _line_number, _code, _depth, _args, _args_count ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnAccessorFuncExpansion', _code )


	##
	## Callback - Called for each line in the file - can also prevent entries from being added ( when found by AddSyntax method ) by returning a boolean. Can also alter entries code ( by returning a string - will only alter if the code being returned was going to be added to a category - otherwise the line is ignored anyway )
	## Note: This is a simple callback so basic info is available only such as the line-number, the code for that line, and the depth of any contents of that line. It is called regardless of comment-status so comment-status is also provided..
	##
	## Task:
	##
	def OnProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		pass


	##
	## Callback - This will be used to enable parsing the file multiple ways to speed up processing... First pass will be in chunks
	##
	## Task: Create
	##
	def OnProcessChunk( self, _line_number_start, _line_number_end, _code, _depth, _in_comment, _in_comment_block ):
		pass


	##
	## Override / Helper - Callback used to override the indent characters used... IE Lets say you use tabs for indenting... Now lets say you want to create a TREE style view.. Use this function... Inject code - ie injecting into tabs is easy, add at the beginning and it is absorbed, up to 3 chars without the tab jumping ( or 7 on computers which have it set to 8 spaces per tab )... Or use spaces and use my FormatColumn code to absorb your changes...
	##
	## Task: Instead of calling this during the whole file processing - I'm going to call this callback during the building-output-data process..
	## Note: Meaning I can simply return the blank-space on the left-side of the _code and include more info such as how many additional entries are in this current category, sub-category data and much much much more meaning the lines below
	##		  would be within reach because all of the necessary info would be available to make it much easier... Returning would simply be the WHITESPACE zone with new info and with a monospaced font, adding the lines won't cause any issues...
	##
	##
	## Useful Chars:
	##
	##	Unknown:		⌐ ¬ ☼ ∙ · ‼ ₧ ¡ µ τ ε ¶ §
	##	Math:			 Φ Θ Ω δ φ α σ Δ
	##		Numeric:	½ ¼  ² ⁿ °
	##		Operators:	 ≥ ≤ ± √ ≡ Σ ≈ ÷
	##		Constants:	π ∞
	##		Currency:	 ¢ £
	##		Other:		ª º ⌠ ⌡
	##	Programming:	 ƒ ∩
	##	French:			« »
	##	German:			 ß
	##	Emojis:			☺ ☻
	##	Genders:		 ♂ ♀
	##	Media:			♪ ♫
	##	Arrows:			 ↨ ↕ ↑ ↓ → ← ↔ ▲ ▼ ► ◄
	##	Cards:			♥ ♦ ♣ ♠
	##	Bullets:		 • ◘ ○ ◙
	##	Fading:			░ ▒ ▓
	##	Blocks:			 █ ▄ ▌ ▐ ▀ ▬ ■
	##	Single:			│ ┤ ┐ └ ┴ ├ ┬ ─ ┼ ┘ ┌ ∟ Γ
	##	Double:			 ╣ ║ ╗ ╝ ╚ ╔ ╩ ╦ ╠ ═ ╬
	##	Both:			╡ ╢ ╖ ╕ ╜ ╛ ╞ ╟ ╧ ╨ ╤ ╥ ╙ ╘ ╒ ╓ ╫ ╪
	##
	## Used to create output similar to this ( Callback is currently a WORK IN PROGRESS and is considered non-functional ):
	##
	##	.			|	First element - others exist... This is a category... Select these chars: [ ┌ ] because of next - and top / [ ─ ] Categories aren't indented, but because of next / [ ☼ ] Represents an end-point... Category icon.. I could simply do because of nested objects:	 [ ┬ ] OR [ ┐ ] if no icon char is used or if I wanted no danglers..
	##
	##
	##	☼ Class Functions Table - Category...		|		##	☼ Class Functions Table - Category...		|	##	☼ Class Functions Table - Category..		|
	##		Vector Class							|		##		☼ Vector Class							|	##		☼ Vector Class							|
	##			ƒ X( )								|		##			function X( )						|	##			function X( )						|
	##			ƒ Y( )								|		##			function Y( )						|	##			function Y( )						|
	##			ƒ Z( )								|		##			function Z( )						|	##			function Z( )						|
	##												|		##												|	##												|
	##		☼ Angle Class							|		##		☼ Angle Class							|	##		☼ Angle Class							|
	##			ƒ P( )								|		##			function P( )						|	##			function P( )						|
	##			ƒ Y( )								|		##			function Y( )						|	##			function Y( )						|
	##			ƒ R( )								|		##			function R( )						|	##			function R( )						|
	##												|		##												|	##												|
	## 	☼ Functions Category						|		## 	☼ Functions Category						|	##	☼ Functions Category						|
	##		ƒ isstring( _data )						|		##		function isstring( _data )				|	##		function isstring( _data )				|
	##		ƒ isnumber( _data )						|		##		function isnumber( _data )				|	##		function isnumber( _data )				|
	##		ƒ isfunction( _data )					|		##		function isfunction( _data )			|	##		function isfunction( _data )			|
	##		ƒ isbool( _data )						|		##		function isbool( _data )				|	##		function isbool( _data )				|

	##												|
	##	┌─☼ Class Functions Table - Category...		|		##	┌─┬☼ Class Functions Table - Category...	|	##	┌─┬☼ Class Functions Table - Category..		|
	##	│ ├──☼ Vector Class							|		##	│ ├─┬☼ Vector Class							|	##	│ ├──☼ Vector Class							|
	##	│ │  ├─☼ ƒ X( )								|		##	│ │ ├──☼ function X( )						|	##	│ │  ├─☼ function X( )						|
	##	│ │  ├─☼ ƒ Y( )								|		##	│ │ ├──☼ function Y( )						|	##	│ │  ├─☼ function Y( )						|
	##	│ │  └─☼ ƒ Z( )								|		##	│ │ └──☼ function Z( )						|	##	│ │  └─☼ function Z( )						|
	##	│ │											|		##	│ │											|	##	│ │											|
	##	│ └──☼ Angle Class							|		##	│ └┬☼ Angle Class							|	##	│ └──☼ Angle Class							|
	##	│    ├─☼ ƒ P( )								|		##	│  ├☼ function P( )							|	##	│    ├─☼ function P( )						|
	##	│    ├─☼ ƒ Y( )								|		##	│  ├☼ function Y( )							|	##	│    ├─☼ function Y( )						|
	##	│    └─☼ ƒ R( )								|		##	│  └☼ function R( )							|	##	│    └─☼ function R( )						|
	##	│											|		##	│											|	##	│											|
	## 	└─☼ Functions Category						|		## 	└┬☼ Functions Category						|	##	└─☼ Functions Category						|
	##	  ├──☼ ƒ isstring( _data )					|		##	 ├─☼ function isstring( _data )				|	##	  ├──☼ function isstring( _data )			|
	##	  ├──☼ ƒ isnumber( _data )					|		##	 ├─☼ function isnumber( _data )				|	##	  ├──☼ function isnumber( _data )			|
	##	  ├──☼ ƒ isfunction( _data )				|		##	 ├─☼ function isfunction( _data )			|	##	  ├──☼ function isfunction( _data )			|
	##	  └──☼ ƒ isbool( _data )					|		##	 └─☼ function isbool( _data )				|	##	  └──☼ function isbool( _data )				|
	##
	## Task: Add more args...
	##
	def OnCalcDepthChars( self, _category, _line_number, _code, _depth ):
		## self.OnCalcDepthChars( _category, _line, _code, _depth )


		## self.GetCfgIndentChars( ) + self.GetDepthString( _depth )
		## return '│' + self.GetCfgIndentChars( ) + '│' + self.GetDepthString( _depth )
		return self.GetCfgIndentChars( ) + self.GetDepthString( _depth )


	##
	## Override / Helper to allow for depth to be all uniform or based on indentation ( if you return _depth )
	##
	def OnCalcDepth( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnCalcDepth' )

		## For functions and objects / classes.. allow depth to be based on indentation - this is primarily for Python until I add the nested categories system...
		if ( _category == CATEGORY_FUNCTION or _category == CATEGORY_CLASS ):
			return _depth

		return 0


	##
	## Callback - Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreOnAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original = None ):
		## Do nothing by default...
		return _code


	##
	## Similar to PreOnEntry except instead of modifying Code, it modifies the Category...
	##
	def PreOnAddEntryCategory( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):
		return _category


	##
	## OVerride / Helper to modify gutter icons before they're added or to prevent them from being added at all...
	## Task: Create functionality
	##
	def OnCalcGutterIcon( self ):
		## Basically - If function discovered in PreOnAddEntry we can do self.AddGutterIcon( line_number, self.GetGutterIcon( 'function', 'ƒ' ) ) and this function will let you capture it before it is added - so if we want to modify behavior ( for the _User class - to alter to a different icon return string id / or number enum of gutter icon ) or prevent it from being displayed ( Return boolean )
		## if ( _category == CATEGORY_CLASS and _icon_id == ICON_CLASS ):
		## 		## something along these lines...
		##		return ICON_CLASS_HEADER
		##
		## 		## or.. to stop it from showing up...
		## 		return False
		pass


	##
	## Callback - Used to hide or completely reset the Category Output Order
	##
	def OnSetupCategoryOutputOrder( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCategoryOutputOrder' )

		return None


	##
	## This replaces OnOutputDataOrder - Instead of _data = '', _data += self.ProcessData( CATEGORY ) for each cat..
	## this function simply calls self.AddOutputCategory( CATEGORY ) and it works by first in, first out so TODO, FUNCTION, REALM would output in that order too..
	## Additionally, this is only called once BEFORE the script processes the code instead of after..
	##
	## Task: Get rid of this hook: OnSetupCategoryOutputOrderDefaults in favor of using the config file - also using the config file to set up categories  MAYBE...
	##
	def OnSetupCategoryOutputOrderDefaults( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCategoryOutputOrderDefaults' )


		##
		## We still have a use for the default config... It just makes it easier though..
		##
		if ( not self.GetCfgDisplayErrors( ) ):	self.AddHideCategoryEntry( CATEGORY_ERROR )
		if ( not self.GetCfgDisplayTasks( ) ):	self.AddHideCategoryEntry( CATEGORY_TODO )
		if ( not self.GetCfgDisplayNotes( ) ):	self.AddHideCategoryEntry( CATEGORY_NOTE )
		if ( not self.GetCfgDisplayHelp( ) ):	self.AddHideCategoryEntry( CATEGORY_HELP )
		## if ( not self.GetCfgDisplayHelp( ) ):	self.AddHideCategoryEntry( CATEGORY_MAPPER_FEATURES )

		## If a boolean is returned, or any non-nil value - then the default order will not be set...
		if ( self.OnSetupCategoryOutputOrder( ) != None ): return False


		##
		## Setup the category order...
		##

		## Standard Issues / Errors / Tasks / Notes
		self.AddCategoryOrderEntry( CATEGORY_ERROR, CATEGORY_TODO, CATEGORY_COMPLETED_TASK, CATEGORY_NOTE, CATEGORY_DEPRECATED )

		## Files being imported...
		self.AddCategoryOrderEntry( CATEGORY_INCLUDE, CATEGORY_INCLUDE_ONCE, CATEGORY_REQUIRE, CATEGORY_REQUIRE_ONCE, CATEGORY_IMPORT )

		## Objects / Functions
		self.AddCategoryOrderEntry( CATEGORY_CLASS, CATEGORY_OBJECT, CATEGORY_CLASS_FUNCTION, CATEGORY_FUNCTION, CATEGORY_FUNCTION_META, CATEGORY_METATABLE, CATEGORY_FUNCTION_LOCAL, CATEGORY_FUNCTION_ACCESSOR )

		## Interface
		self.AddCategoryOrderEntry( CATEGORY_VGUI, CATEGORY_USER_INTERFACE, CATEGORY_INTERFACE, CATEGORY_HUD )

		## Other Functions
		self.AddCategoryOrderEntry( CATEGORY_FUNCTION_HELPER, CATEGORY_FUNCTION_CALLBACK, CATEGORY_FUNCTION_HOOK, CATEGORY_FUNCTION_OTHER )

		## Anchor and Goto, the navigator..
		self.AddCategoryOrderEntry( CATEGORY_ANCHOR, CATEGORY_GOTO )

		## Command Specific
		self.AddCategoryOrderEntry( CATEGORY_CMD_FILE, CATEGORY_CMD_ITERATION, CATEGORY_CMD_REGISTRY )

		## Other Information
		self.AddCategoryOrderEntry( CATEGORY_REGISTRY, CATEGORY_CMD_CONSOLE, CATEGORY_INFO, CATEGORY_NETWORKING, CATEGORY_REALM, CATEGORY_WARNING )

		## Configuration, Definitions, etc..
		self.AddCategoryOrderEntry( CATEGORY_DIRECTIVE, CATEGORY_CONFIG, CATEGORY_ENUMERATION, CATEGORY_CONSTANT, CATEGORY_DEFINITION, CATEGORY_VARIABLE )

		## Keys / HotKeys / Key-Bindings
		self.AddCategoryOrderEntry( CATEGORY_HOTKEY, CATEGORY_KEY_BIND )

		## Internal / Private Functions - Usually LOW priority functions go here
		self.AddCategoryOrderEntry( CATEGORY_FUNCTION_INTERNAL )

		## Formatting
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_IMAGE, CATEGORY_FORMATTING_LINK )
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_QUOTE, CATEGORY_FORMATTING_CODE_SNIPPET, CATEGORY_FORMATTING_CODE, CATEGORY_FORMATTING_LINE_BLOCK )
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_TABLE, CATEGORY_FORMATTING_LIST, CATEGORY_FORMATTING_LIST_DEFINITION, CATEGORY_FORMATTING_LIST_BUTTONED, CATEGORY_FORMATTING_LIST_NUMBERED )
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_HEADER, CATEGORY_FORMATTING_HEADER_1, CATEGORY_FORMATTING_HEADER_2, CATEGORY_FORMATTING_HEADER_3, CATEGORY_FORMATTING_HEADER_4, CATEGORY_FORMATTING_HEADER_5, CATEGORY_FORMATTING_HEADER_6 )
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_BOLD, CATEGORY_FORMATTING_ITALICS, CATEGORY_FORMATTING_STRIKETHROUGH )
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_RULE, CATEGORY_FORMATTING_RULE_HORIZONTAL, CATEGORY_FORMATTING_RULE_VERTICAL )
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_FOOTNOTE, CATEGORY_FORMATTING_TAG )
		self.AddCategoryOrderEntry( CATEGORY_FORMATTING_UNKNOWN, CATEGORY_FORMATTING_OTHER, CATEGORY_FORMATTING_EXAMPLE )

		## Debugging / Optimization and unknown entries
		self.AddCategoryOrderEntry( CATEGORY_ERROR_EXAMPLE, CATEGORY_DEBUGGING, CATEGORY_OPTIMIZATION, CATEGORY_UNKNOWN )

		##
		## self.AddCategoryOrderEntry( CATEGORY_COMPLETED_TASK )

		## Help
		self.AddCategoryOrderEntry( CATEGORY_MAPPER_FEATURES, CATEGORY_HELP )


		##
		## XCodeMapper - Internal Categories
		##

		## Important / Debugging Categories - Tell a programmer type categories...
		self.AddCategoryOrderEntry( CATEGORY_XCM_ERROR, CATEGORY_XCM_INFO, CATEGORY_XCM_DEBUG, CATEGORY_XCM_OPTIMIZATION )

		## XCodeMapper Entry Categories - Used to display AddSyntax, AddSyntaxRule, AddCategoryOutputEntry and AddEntry / AddSimpleEntry / AddHelp / AddError / etc.. calls
		self.AddCategoryOrderEntry( CATEGORY_XCM_SYNTAX_ENTRY, CATEGORY_XCM_SYNTAX_RULE_ENTRY, CATEGORY_XCM_OUTPUT_ORDER_ENTRY, CATEGORY_XCM_OUTPUT_ENTRY )


	##
	## Internal Helper - Reset the internal AccessorFuncs to their default values..
	##
	def OnResetInternalAccessors( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnResetInternalAccessors' )

		## Reset Class Data
		self.ResetAccessorFuncValues( 'ActiveClass' )

		## Are we in a comment block - note, this can't be in the line by line scope as blocks can extend beyond a single line...
		self.ResetAccessorFuncValues( 'InComment', 'InCommentBlock', 'CommentBlockStartLine', 'CommentBlockEndLine', 'CommentBlockIndexStart', 'CommentBlockIndexEnd', 'Depth', 'LastDepth', 'Realm', 'FileData', 'FileLines' )


	##
	## Callback used to alter Code - Map Panel configuration per mapper...
	##
	## @Arg: self:		Reference to the __ACTIVE__ XCodeMapper Object - With inheritence this'll point to XCodeMapper_Python_Default if you're mapping a Python file and you're using the Default Python mapper...
	## @Arg: _panel:	References the "Code - Map" Panel - This is a Sublime Text View Object with ALL of the functions, etc.. associated with it...
	## @Arg: _settings:	This is a shortcut reference - instead of having to type _panel.settings( ).set( 'key', 'value' ) you can simply call _settings.set( 'key', 'value' )
	##
	def OnSetupCodeMapPanel( self, _panel, _settings ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCodeMapPanel' )

		## Disable Scroll Past End...
		## print( ' >> Code - Map Panel >> scroll_pass_end set to False!' )
		## _panel.settings( ).set( 'scroll_past_end', False )


		##
		## Minimap Options..
		##

		## Minimap - Unfortunately this is for the entire window... not just the panel and setting show_minimap doesn't alter it..
		## _panel.window( ).set_minimap_visible( False )
		## _panel.window( ).set_minimap_visible( True )

		## Always visualise the viewport on the minimap, as opposed to only showing it on mouse over.
		## _panel.settings( ).set( "always_show_minimap_viewport", True )

		## Set to true to draw a border around the visible rectangle on the minimap if visible. The color of the border will be determined by the "minimapBorder" key in the color scheme.
		## _panel.settings( ).set( "draw_minimap_border", True )


		##
		## Gutter options
		##

		## If set to true, a gutter will apear which can show line numbers, the fold-arrows, icons and more.... This needs to be true for the other options to work...
		## _code_map.settings( ).set( "gutter", False )

		## Show line-numbers on the gutter
		## _code_map.settings( ).set( "line_numbers", True )

		## Show fold-arrows / buttons on the gutter...
		## _code_map.settings( ).set( "fold_buttons", False )


		##
		## Other Options...
		##

		## If you want a different font-size to appear in the mapping panel compared to the rest of your code - set it here...
		## _code_map.settings( ).set( "font_size", 10.5 )


		##
		## Options which don't exist, but should...
		##

		## _panel.settings( ).set( "draw_minimap", False )
		## _panel.settings( ).set( "show_minimap", False )
		## _panel.settings( ).set( "hide_minimap", True )
		## _panel.settings( ).set( "minimap_hide", True )
		## _panel.settings( ).set( "minimap", False )


	##
	## Internal Processor
	## TODO: Add code to unset all data used by this instance and then destroy itself...
	##
	def OnShutdown( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnShutdown' )






	##
	## Callback for displaying Help & Support Information for XCodeMapper created by Josh 'Acecool' Moser which is an extension of CodeMap created by oleg-shilo for Sublime Text Editor!
	##
	def OnSetupHelp( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupHelp' )

		##
		## Adding help info here!
		##
		self.AddHelp( 'Welcome to \'Acecool\'s Universal Code Mapper, an Extension for the ST3 Package CodeMap by oleg-shilo!' )
		self.AddHelp( 'This help category is to list tips & tricks in addition to providing help and support information!' )
		self.AddHelp( '' )
		self.AddHelp( 'For help and support with CodeMap by oleg-shilo:' )
		## self.AddHelp( '' )
		## self.AddHelp( 'Website:\thttps://github.com/oleg-shilo/' )
		self.AddHelp( self.GetCfgIndentChars( ) + 'Download:' + self.GetCfgIndentChars( ) + 'https://github.com/oleg-shilo/sublime-codemap/' )
		self.AddHelp( self.GetCfgIndentChars( ) + 'Support:' + self.GetCfgIndentChars( ) + 'https://github.com/oleg-shilo/sublime-codemap/issues/' )
		self.AddHelp( '' )
		self.AddHelp( 'For help and support with XCodeMapper and the Acecool Development Library by Josh \'Acecool\' Moser' )
		## self.AddHelp( '' )
		## self.AddHelp( 'Website: http://www.acecoolco.com/' )
		## self.AddHelp( 'Website:\thttps://bitbucket.org/Acecool/' )
		self.AddHelp( self.GetCfgIndentChars( ) + 'Download:' + self.GetCfgIndentChars( ) + 'https://bitbucket.org/Acecool/acecooldev_sublimetext3' )
		self.AddHelp( self.GetCfgIndentChars( ) + 'Support:' + self.GetCfgIndentChars( ) + 'https://bitbucket.org/Acecool/acecooldev_sublimetext3/Issues/' )
		self.AddHelp( self.GetCfgIndentChars( ) + 'Donate:' + self.GetCfgIndentChars( ) + 'https://www.paypal.me/Acecool' )
		self.AddHelp( '' )

		if ( self.GetCfgDisplayPersonalStory( ) ):
			self.AddHelp( 'I have a broken neck, back and severe nerve damage from an automobile accident on May 7, 2011 where' )
			self.AddHelp( ' the other person ran a stop sign and hit me. I\'ve been fighting this for over 6 years and am receiving' )
			self.AddHelp( ' no help from them or the insurance. Any support you can offer would be graceously accepted and would go' )
			self.AddHelp( ' a long way towards helping cover costs of medication, treatment, physical therapy, etc..' )
			self.AddHelp( 'Thank you for taking the time to read this!' )
			self.AddHelp( '' )
			self.AddHelp( self.GetCfgIndentChars( ) + 'Donate:' + self.GetCfgIndentChars( ) + 'https://www.paypal.me/Acecool' )
			self.AddHelp( '' )

		self.AddHelp( '' )
		self.AddHelp( 'To hide this category, update the following CFG_ value at the top of each code_map.<ext>.py file.' )
		self.AddHelp( '	Alter from:' )
		self.AddHelp( '		CFG_DISPLAY_CATEGORY_HELP = True' )
		self.AddHelp( '	To:' )
		self.AddHelp( '		CFG_DISPLAY_CATEGORY_HELP = False' )


	##
	## Empty, by default, Callbacks...
	##



	##
	## Setup your code_map.<ext>.py mapper configuration in this callback - Copy the OnSetupConfigDefaults to give yourself a layout...
	##
	def OnSetupConfig( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfig' )


	def OnSetupClassArgument( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupClassArgument' )

	def OnSetupFunctionArgument( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupFunctionArgument' )

	def OnSetupCategories( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupCategories' )

	def PreOnInit( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'PreOnInit' )

	def PostOnInit( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'PostOnInit' )

	def OnInitConfigAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnInitConfigAccessorFuncs' )




##
## XCodeMapper - Universal CodeMap extension to easily add languages and support for your projects.. - Josh 'Acecool' Moser
## Note: I am rebuilding it here under a different name to ensure it works properly without making changes to the current variant.. And so I can extract things in favor of using a RunTime object so this behaves more like a simple library of functions..
##
class XCodeMapperBase( XCodeMapperClassTemplate ):
	##
	## Important Object Values!
	##

	## Name / ID of the XCodeMapperBase
	__name__ = 'XCodeMapperBase'
	__panel__ = None


	##
	## Internal Handler / Hook Caller - Called when a new instance is being created...
	##
	##
	def __init__( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( '__init__', 'New instance created!' )

		## Calculate how long it takes to process files vs cache...
		self.__timestamp_end__ = None
		self.__timestamp_save__ = None
		self.__timestamp_start__ = self.GetTimeStamp( )

		## Add new marker because we're doing things prior to run which matter...
		print( '[ XCodeMapperBase > Initialization ] ----------------------------------------------------------------------------------------------------------------------------' )

		## Code - Map is typically the LAST view, always...
		_code_map = self.GetSublimeView( 'Code - Map' )

		## Just in case...
		if ( _code_map != None ):
			## Notify the user...
			print( ' >> Code - Map Panel >> Updating Configuration!' )

			## Update the var so it can be grabbed later...
			self.__panel__ = _code_map

			## Callback for editing configuration... Default sets the scroll past end to false...
			self.SetupCodeMapPanel( _code_map )

		## Caching System Notification
		print( ' >> XCodeMapper Caching System is ' + Acecool.logic.ternary( self.GetCfgCachingSystemEnabled( ), 'Enabled', 'Disabled' ) )

		## Ensure all of the folders we need to create exist...
		self.SetupFolderStructure( )

		## This is what is returned after everything has been processed..
		## self.output = ''

		## Entries counter
		self.entries = 0

		#-- Syntax Rules
		self.data_syntax = [ ]
		self.data_syntax_comments = [ ]
		self.output_order = [ ]

		## For typos...
		self.typo_list = [ ]

		## Data-Table for the Arg / Data-Type Addition / Modification / Conversion System...
		self.data_arg_conversion = { }

		## These are the categories we add things to...
		self.categories = { }
		self.category_types = { }
		self.category_count = { }

		## Mapped .. Basically the map is the category name as a string to a key from category_children.append.... This lets us sort the children...
		self.category_children_map = { }
		self.category_children = { }

		self.data_hidden_categories = { }

		##
		self.error_example_log = { }

		##
		self.AccessorFuncHelpersDefined = { }

		## Call the helper Callback functions for Pre / Post which can be overridden by the user - OnInit should be used internally and not overridden..
		## self.CallFunctionHooks( 'Init' )
		self.OnInit( )


	##
	## Creates all necessary folders to ensure the structure exists to avoid issues with the caching system, etc..
	##
	def SetupFolderStructure( self ):
		## Make sure the folder structure exists..
		try:
			os.makedirs( self.GetCachePath( ) )
		except OSError as e:
			pass
			## if e.errno != errno.EEXIST:
			## 	raise


	##
	## Internal function used to pre-define base modifications to the code map panel...
	##
	def SetupCodeMapPanel( self, _panel ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'SetupCodeMapPanel' )

		## Default: Disable Scroll Past End...
		print( ' >> Code - Map Panel >> scroll_pass_end set to False!' )
		_panel.settings( ).set( 'scroll_past_end', False )

		## Call the callback for everyone else
		self.OnSetupCodeMapPanel( _panel, _panel.settings( ) )



	##
	## Return whether or not an error example has been displayed - it is set to true as soon as it is called so the if statement can show the example without having to worry about setting a value to true...
	##
	def HasDisplayedErrorExample( self, _id ):
		##
		_was_shown = self.error_example_log.get( _id, False )

		##
		self.error_example_log[ _id ] = True

		##
		return _was_shown


	##
	## Internal Handler / Hook Caller
	##
	def Shutdown( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'Shutdown', 'This instance of XCodeMapper / XCodeMapperBase is being wiped from memory!' )

		## Call the helper Callback functions for Pre / Post which can be overridden by the user - OnInit should be used internally and not overridden..
		self.CallFunctionHooks( 'Shutdown' )

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		print( '[ XCodeMapper ] Finished ------------------------------------------------------------------------------------------------------------------------------------' )

		## If a DeveloperFunc exists, run it...
		if ( isfunction( self.DeveloperFunc ) ):
			self.DeveloperFunc( )



		## print( ' >> Output Stats: ' )
		## ## print( ' >> self.output length: ' + str( len( self.output ) ) )
		## print( ' >> entries: ' + str( self.entries ) )
		## print( ' >> data_syntax: ' + str( len( self.data_syntax ) ) )
		## print( ' >> output_order: ' + str( len( self.output_order ) ) )
		## print( ' >> data_arg_conversion: ' + str( len( self.data_arg_conversion ) ) )
		## print( ' >> categories: ' + str( len( self.categories ) ) )
		## print( ' >> category_types: ' + str( len( self.category_types ) ) )
		## print( ' >> category_count: ' + str( len( self.category_count ) ) )
		## print( ' >> data_hidden_categories: ' + str( len( self.data_hidden_categories ) ) )
		## print( ' >> AccessorFuncHelpersDefined: ' + str( len( self.AccessorFuncHelpersDefined ) ) )
		## print( ' >> : ' )
		## print( ' >> : ' )


	##
	## Used to set up default replacements for the arguments to data-type to display one, the other or both...
	##
	def OnSetupArgDataTypeDefaults( self ):
		##
		## Add default replacements here..
		##

		## Example A
		## assignment..


		## Run the callback
		self.OnSetupArgDataType( )


	##
	## re.search( _pattern, _code )
	##
	def OnVariableDetection( self, _category, _line_number, _code_search, _depth, _search_mode, _pattern ):
		pass


	##
	## Returns a string with delimited elements such as a comma and space with the optional wrap around the data and then around the entire pack...
	## self.GetJoinedDelimitedString( _i, self.GetCfgObjectArgJoinPrefix( ), _pattern, "'", "'" ) == 'x', 'y', 'z'
	## self.GetJoinedDelimitedString( _i, ',', _pattern, " '", "' ", '[', ']' ) == [ 'x' ][, 'y' ][, 'z' ] ## Note: Maybe alter this one a bit? Or have this as the third set.. for optional args?
	##
	def GetJoinedDelimitedString( self, _i, _delimiter = ', ', _data = '', _data_prefix = '', _data_suffix = '', _text_prefix = '', _text_suffix = '' ):
		return _text_prefix + Acecool.logic.ternary( ( _i > 0 ), _delimiter, '' ) + _data_prefix + _data + _data_prefix + _text_suffix


	##
	## Helper - Outputs data to the console in an identifiable way...
	##
	def print( self, _name, _text = '' ):
		_debug = self.GetCfgDebugMode( )

		## To allow printing only what we need....
		if ( _debug == True or _debug == _name ):
			## Debugging - in a nice formated output..
			_pretext = '[ ' + self.__name__ + ' > ' + self.GetCfgLanguage( ) + ' > ' + _name + ' ]'
			print( Acecool.string.FormatColumn( len( _pretext ) + 4, _pretext ) + _text )


	##
	## Helper - For the standard function which executes Pre<Name>( ), On<Name>( ) and Post<Name>( ) - this calls all 3 so the helper only needs to be a single line...
	##
	def CallFunctionHooks( self, _name, *_args ):
		getattr( self, 'PreOn' + _name )( *_args )
		getattr( self, 'On' + _name )( *_args )
		getattr( self, 'PostOn' + _name )( *_args )


	##
	## Helper to call Super...
	## Task: See if it is possible to alter the super call structure to make it seem more elegant and more in line with my standards...
	##
	def CallParent( self, _func_name, *_args ):
		## return super( )[ _func_name ]( *_args )
		## print( str( super( ) ) )
		## super( ).( _func_name, self, *_args )
		## super( ).getattr( self, '_func_name' )( *_args )
		## return super( )[ _func_name ]( *_args ):
		pass


	##
	## End Callback functions
	##


	##
	## Helper - these are the symbols from the file we're currently editings symbols( ) from the view - ie all extracted symbols from Sublime Text...
	##
	def MappingFileSymbols( self, _view, _symbols ):
		## Call the callback
		return self.OnMappingFileSymbols( _view, _symbols )


	##
	## Returns the timestamp from the actual file to help determine the last time it was modified...
	##
	def GetFileTimeStamp( self, _file ):
		## Default timestamp set at 0
		_timestamp = 0

		## Attempt to read the file timestamp...
		try:
			## We try because numerous issues can arise from file I / O
			_timestamp = os.path.getmtime( _file )
		except ( OSError ):
			## File not found, inaccessible or other error - Set the timestamp to -1 so we know an error was thrown..
			_timestamp = -1

		## Output the data..
		## print( '>> Timestamp for file: ' + _file + ' is ' + str( _timestamp ) )

		## Grab the real-time File TimeStamp if the file exists, otherwise -1 or 0...
		return _timestamp


	##
	## Returns the timestamp from the last time the file was processed, if ever. If not, it'll return 0..
	##
	def GetCachedFileTimeStamp( self, _file ):
		## Return the timestamp..
		return self.GetFileTimeStamp( self.GetCachePath( _file ) )


	##
	## Helper - Converts the full-path to and including the file-name to a single-filename which can be used as a filename saved...
	##
	def GetSafeCachedFileName( self, _file ):
		## Convert the filename to a safe-filename - will be using RegEx later..
		## _file_safe = Acecool.string.Safe( _file ) + '.cache'

		## Same as above except not using RegEx...
		_file_safe = _file
		_file_safe = _file_safe.replace( ':', '_' )
		_file_safe = _file_safe.replace( '\\', '_' )
		_file_safe = _file_safe.replace( '/', '_' )
		_file_safe = _file_safe.replace( ' ', '_' )
		_file_safe = _file_safe.replace( '__', '_' )

		## Return the finally "safe for saving" filename..
		return _file_safe


	##
	## Helper - Returns the Cache directory by itself, or if the filename is provided then provide the full path with the converted file-name...
	##
	def GetCachePath( self, _file = None ):
		## Base path...
		_path = os.path.join( sublime.packages_path( ), 'User', 'CodeMap', 'Cache' )

		## If the filename is provided, we'll add that and convert it to the safe-string..
		if ( _file != None ):
			_path = os.path.join( _path, self.GetSafeCachedFileName( _file ) )

		## Returns %AppData%\Sublime Text 3\Packages\User\CodeMap\Cache\ and adds the filename if _file isn't None...
		return _path


	##
	## Helper - Saves the cache file..
	##
	def SaveFileCache( self, _filename, _data = '' ):
		## Grab the path to and including a safe-filename to save the cached file...
		_path = self.GetCachePath( _filename )

		## Output the information..
		print( ' >> Saving CACHED FILE: ' + _filename + ' as ' + _path )

		## Make sure we're not writing an empty file... I may allow this later...
		if ( _data != None and _data.strip( ) != '' ):
			## Try, try, try again...
			try:
				## Open the file with write-permissions and proper encoding
				with io.open( _path, 'w', encoding = 'utf8' ) as _file:
					## Write the data...
					_file.write( _data )

				## Once we're done, return None to ensure no error output is output to the output panel..
				return None
			except NameError as _error:
				## Naming error? Too long maybe? Invalid chars? -- actually because global unicode didn't exist..
				## return 'NameError( {0} ): {1} ' . format( _error.errno, _error.strerror )
				return 'XCodeMapper > SaveFileCache > NameError: ' + str( sys.exc_info( )[ 0 ] ) + ' / ' + str( _error )
			except UnicodeError as _error:
				## Data Formatting Issue
				return 'XCodeMapper > SaveFileCache > UnicodeError( {0} ): {1} ' . format( str( _error.errno ), str( _error.strerror ) )
			except IOError as _error:
				## General Input / Output Error
				return 'XCodeMapper > SaveFileCache > I / O Error( {0} ): {1} ' . format( str( _error.errno ), str( _error.strerror ) )
			except:
				# Handle all other errors
				return 'XCodeMapper > SaveFileCache > Unexpected error: ' + str( sys.exc_info( )[ 0 ] )


	##
	## Helper - Loads the cache file
	##
	def LoadFileCache( self, _filename ):
		## Grab the path to and including a safe-filename to save the cached file...
		_path = self.GetCachePath( _filename )

		## Output the information
		print( ' >> Trying to LOAD CACHED File: ' + _filename + ' as ' + _path )

		## Try to read the file data... This function should only be called if the file exists...
		try:
			## Open the file with readonly-permissions and proper encoding
			with io.open( _path, 'r', encoding = 'utf8' ) as _file:
				## Read the file and output it..
				return _file.read( )
		except:
			## Output the error to the output window...
			return 'ExceptionThrown: ' + str( sys.exc_info( )[ 0 ] )


	##
	##
	##
	def GetTimeStamp( self ):
		## Returns Micro Seconds? Or an actual string date...
		## return datetime.datetime.now( )

		## Requires os.stat( ) on a file or something - stat_result is what is returned...
		## return os.stat_result.st_atime_ns( ) / 1000
		## return os.stat_result.st_ctime_ns( ) / 1000
		## return os.stat_result.st_mtime_ns( ) / 1000

		## Grab actual epoch timestamp from time...
		return time.time( )



	##
	## Helper - Returns how long it took to execute / parse, etc.. the output generation so I can time caching system vs parsing..
	##
	def GetTimeDelta( self, _start = None, _end = None ):
		## If Start wasn't provided as an argument..
		if ( _start == None ):
			_start = self.__timestamp_start__

		## If _end wasn't provided as an argument...
		if ( _end == None ):
			## If __timestamp_end__ isn't set, we grab the current timestamp... or _start is identical to _end... then we must be grabbing a STEP timestamp... recapture the timestamp for this time but don't update end...
			if ( self.__timestamp_end__ == None or _start == self.__timestamp_end__ ):
				## For loading the cached file..
				_end = self.GetTimeStamp( )
			else:
				## Otherwise we use it..
				_end = self.__timestamp_end__

		## Return the DeltaTime...
		return _end - _start


	##
	## Helper - Returns math.Max of each of the core files ( XCodeMapper.py, XCodeMapper_Definitions.py and AcecoolST3_Library.py )
	##
	def GetTimeStampMaxOfCoreFiles( self ):
		## Base Path
		_path = os.path.join( sublime.packages_path( ), 'User', 'CodeMap', 'Acecool' )

		## Path to each core file..
		_path_xcmb = os.path.join( _path, 'XCodeMapper.py' )
		_path_xcmbd = os.path.join( _path, 'XCodeMapper_Definitions.py' )
		_path_acst3l = os.path.join( _path, 'AcecoolST3_Library.py' )

		## TimeStamp of each core file ( done this way to have a notification if needed )
		_timestamp_xcmb = self.GetFileTimeStamp( _path_xcmb )
		_timestamp_xcmbd = self.GetFileTimeStamp( _path_xcmbd )
		_timestamp_acst3l = self.GetFileTimeStamp( _path_acst3l )

		##  Notify
		## self.print( 'Caching', ' >> Caching > the timestamps for each core file is: XCodeMapper.py( ' + str( _timestamp_xcmb ) + ' ); XCodeMapper_Definitions.py( ' + str( _timestamp_xcmbd ) + ' ); AcecoolST3_Library.py( ' + str( _timestamp_acst3l ) + '); ' )

		## Return the max timestamp of all of These - we only need to prove that cache is greater than all to load cached file...
		return max( _timestamp_xcmb, _timestamp_xcmbd, _timestamp_acst3l )


	##
	## Returns the max timestamp of the configuration files ( Default, User, and Custom Mapper )
	##
	def GetTimeStampMaxOfCfgFiles( self, _ext ):
		## Base Path
		## _path = os.path.join( sublime.packages_path( ), 'User', 'CodeMap' )
		_path = os.path.join( sublime.packages_path( ), 'User' )

		## XCodeMapper Config File...
		_timestamp_cfg_default = self.GetFileTimeStamp( os.path.join( _path, 'Acecool_XCodeMapper_Config.Default.sublime-settings' ) )
		_timestamp_cfg_user = self.GetFileTimeStamp( os.path.join( _path, 'Acecool_XCodeMapper_Config.User.sublime-settings' ) )

		## Custom Mapper Config File...
		_timestamp_cfg_mapper_default = self.GetFileTimeStamp( os.path.join( _path, 'Acecool_XCodeMapper_Config_' + _ext + '.Default.sublime-settings' ) )
		_timestamp_cfg_mapper_user = self.GetFileTimeStamp( os.path.join( _path, 'Acecool_XCodeMapper_Config_' + _ext + '.User.sublime-settings' ) )

		## If the file doesn't exist - 0 is returned... otherwise the actual timestamp is returned..
		return max( _timestamp_cfg_default, _timestamp_cfg_user, _timestamp_cfg_mapper_default, _timestamp_cfg_mapper_user )



	##
	## Helper - Used to determine whether or not we need to load the cached file vs generate...
	##
	def ShouldLoadCachedFile( self, _file ):
		## The Core Files Max placeholder ( although it seems like Python doesn't care about scope if you se it inside of an if in a function if you need it in a function - strange )...
		_timestamp_core_files_max = 0

		## If Developer mode is enabled, then the core files can be reloaded so we need to look at those timestamps...
		if ( self.GetDeveloperMode( ) ):
			## Assign _max the maximum timestamp value of all of the core files...
			_timestamp_core_files_max = self.GetTimeStampMaxOfCoreFiles( )

			## Notify
			## self.print( 'Caching', ' >> Caching > The max timestamp of all of the core files is: ' + str( _timestamp_core_files_max ) )

		## Get the extension of the current file to determine which custom mapper is being used... We know a custom mapper is loaded because we're in XCM...
		_file_split = _file.split( '.' )
		_ext = _file_split[ len( _file_split ) - 1 ].lower( )

		## Notify
		## self.print( 'Caching', ' >> Caching > The extension for our mapper is: ' + _ext )

		## Cached File
		_timestamp_cached = self.GetCachedFileTimeStamp( _file )

		## The file we're mapping...
		_timestamp_file = self.GetFileTimeStamp( _file )

		## Get the current mapper ( using the file extension, explode . and use last )
		_timestamp_mapper = self.GetFileTimeStamp( os.path.join( sublime.packages_path( ), 'User', 'CodeMap', 'custom_mappers', _ext + '.py' ) )

		## Get the configuration files timestamp max...
		_timestamp_config_files_max = self.GetTimeStampMaxOfCfgFiles( _ext )

		## The maximum timestamp of all non-cached files..
		_timestamp_max_all = max( _timestamp_file, _timestamp_mapper, _timestamp_core_files_max, _timestamp_config_files_max )

 		## The easiest way to do this is math.Max all of the values except the cache then see if the cache is newer than all we reload the cache file...
		if ( _timestamp_cached > _timestamp_max_all ):
			## Notify
			## self.print( 'Caching', ' >> Caching > the _cache value: ' + str( _timestamp_cached ) + ' is GREATER THAN all ' + str( _timestamp_core_files_max ) + ' / ' + str( _timestamp_file ) + ' / ' + str( _timestamp_mapper ) )

			## We load the Cache file...
			return True

		## Notify
		## self.print( 'Caching', ' >> Caching > the _cache value: ' + str( _timestamp_cached ) + ' is LESS THAN all ' + str( _timestamp_core_files_max ) + ' / ' + str( _timestamp_file ) + ' / ' + str( _timestamp_mapper ) )

		## Return false - we Re-Process the File...
		return False

 		# or self.GetFileTimeStamp( 'mapper_' + GetLangVariantClassName( _variant ) ) <= self.GetCachedFileTimeStamp( 'mapper_' + GetLangVariantClassName( _variant ) ) ):



	##
	## Internal Handler / Hook Caller
	## Task: Split Run into several helper functions to make reprocessing a line easier...
	##
	def Run( self, _file, _variant ):
		## If a DeveloperFunc exists, specifically designed to run prior to everything else, run it...
		if ( isfunction( self.OnRunDeveloperFunc ) ):
			self.OnRunDeveloperFunc( )

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		print( '[ XCodeMapperBase > ' + self.GetCfgLanguage( ) + ' > generate ] ----------------------------------------------------------------------------------------------------------------------------' )

		## Let them know...
		print( '[ XCodeMapper > generate > Specific Mapping Variant Chosen: ' + GetLangVariantClassName( _variant ) + ' ]' )


		##
		## Caching System - If enabled, save the file...
		##
		if ( self.GetCfgCachingSystemEnabled( ) ):
			## Determine if cached exists, etc...
			if ( self.ShouldLoadCachedFile( _file ) ):
				## Load the file before so we get a more accurate tally of time...
				_data_cached = self.LoadFileCache( _file )

				## Output.. To show how long / short it takes to execute - as of January 21, 2018 a Python file of around 5000 lines takes 0.01 to 0.02 seconds to load the file ( SSD ) and completely finish processing anything to the point of this return ( which then outputs the data ).
				print( ' >> Execution Time took a total of: ' + str( self.GetTimeDelta( ) ) )

				## Return the cached data...
				return _data_cached


		## Get the current file view...
		_file_view = self.GetSublimeView( _file )
		if ( _file_view != None ):
			self.MappingFileSymbols( _file_view, _file_view.symbols( ) )

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'Run' )

		## Task: Consider adding PreOnRun / OnRun / PostOnRun Helpers
		## self.CallFunctionHooks( 'Run' )

		## Reset the values of our internal AccessorFuncs
		self.OnResetInternalAccessors( )

		##
		_name = Acecool.string.GetFilename( _file )
		_ext = Acecool.string.GetExt( _file )


		## Read the code / file into memory - exploded on newline
		_lines, _count = Acecool.file.Read( _file, self.GetCfgFileNewLine( ) )

		## Update some helper Accessors so we can use them in certain situations...
		self.SetFileData( _lines )
		self.SetFileLines( _count )

		## For Each Line...
		for _line_number, _line in enumerate( _lines, 1 ):
			##
			_code = _line.strip( )
			_code_search = _code
			_code_lstrip = _line.lstrip( )
			_code_rstrip = _line.rstrip( )

			## Grab the difference between a left-stripped bit of code and non-stripped to count how many spaces or tabs there are...
			_depth = len( _line ) - len( _code_lstrip )

			## Determine Indentation Character Type and Quantity - if tabs, then set indent-length to the value to 1 so no division occurs - if spaces are used then set to number of spaces used so we can use that to divide to ensure accurate indentation occurs..
			## If they're using spaces, we divide _depth by the tabs-to-spaces value ( default 4 - however if they're using 2 spaces per indent we catch and log it by setting a value the FIRST time indents are seen )
			if ( _depth > 0 and self.GetIndentLength( ) == 0 ):
				## Grab the indent chars
				_indent_chars = Acecool.string.Strip( _line, _depth, len( _line ) - 1 )

				## Debugging...
				self.print( 'Indentation', ' >> Depth: ' + str( _depth ) + ' / using chars: "' + _indent_chars + '"' )

				## Determine if spaces
				if ( _indent_chars == Acecool.string.repeat( ' ', _depth ) ):
					print( ' >> Syntax > Indentation > Using ' + str( _depth ) + '  spaces per Indentation!' )
					self.SetIndentLength( _depth )
				## Determine if Tabs
				elif ( _indent_chars == Acecool.string.repeat( '	', _depth ) ):
					print( ' >> Syntax > Indentation > Using ' + str( _depth ) + ' tabs per indentation!' )
					self.SetIndentLength( 1 )

			## If IndentLength has been set, and it is over 0, then use it to divide...
			if ( self.GetIndentLength( ) > 0 ):
				## Set the depth by using the depth provided and dividing by the number of spaces, if spaces are used for indentation, or by 1 for tabs ie nothing...
				self.SetDepth( int( _depth / self.GetIndentLength( ) ) )
			else:
				self.SetDepth( _depth )

			## Are we in a comment block - note, this can't be in the line by line scope as blocks can extend beyond a single line...
			self.ResetInComment( )


			##
			## TODO: Add comment checks out here... Then if we are supposed to look at data within comment blocks we go deeper... As of right now OnProcessLine isn't receiving the right in comment / in commentblock vars... They're from the previous line..
			##
			## Task: Process comments separately.. and before the other data so we can skip other syntax rules if data is found... meaning specific rules need to be moved if added to comment lines - the beginning of the rule system... It'll make the code quicker though!
			##
			## If we have comment rules...
			if ( len( self.data_syntax_comments ) > 0 ):
				## Process comments...
				for _category, _search_mode, _pattern, in self.data_syntax_comments:
					##
					## If we're processing our comment patterns, check to see if the line starts with a comment...
					##
					if _category == CATEGORY_COMMENT and not self.GetInCommentBlock( ) and not self.GetInComment( ):
						## If our comment is at the start of the line then
						if _code.startswith( _pattern ):
							## Mark that we're in a 1 line comment
							self.SetInComment( True )

							## Strip the comment... Since it starts with we have a different problem....
							_code_search = Acecool.string.Strip( _code_search, 0, len( _pattern ) )
							_code_search = _code_search.strip( )

					##
					## If we're in a comment block, set our controlled toggle to true ( also set _in_comment to true so TODO / Tasks / etc.. can be picked up in comment blocks )
					##
					if _category == CATEGORY_COMMENT_BLOCK_BEGIN and Acecool.string.indexOfExists( _pattern, _code_search, True ):
						## Let the code know we're in a comment and in a comment-block
						self.SetInComment( True )
						self.SetInCommentBlock( True )

						## Let the code know which line the comment block is on ( this is so we can strip it from the same line code output if necessary - as of right now a comment block at the end of the line will prevent code being output )
						self.SetCommentBlockStartLine( _line_number )
						self.SetCommentBlockEndLine( -1 )

						## The index where the comment block started
						self.SetCommentBlockIndexStart( Acecool.string.indexOf( _pattern, _code_search ) - 1 )

						## Strip the comment block starting code from the line - will advance this later so it searches until the end if on the same line...
						if ( self.GetCommentBlockIndexStart( ) > 0 ):
							_code_search = Acecool.string.Strip( _code_search, Acecool.string.indexOf( _pattern, _code_search ) - 1, len( _pattern ) - 1 )
							_code_search = _code_search.strip( )
						else:
							_code_search = Acecool.string.Strip( _code_search, 0, len( _pattern ) )
							_code_search = _code_search.strip( )


					##
					## When a comment block ends, set the controlled toggle to false ( ignore comment-line though )
					##
					if _category == CATEGORY_COMMENT_BLOCK_FINISH and Acecool.string.indexOfExists( _pattern, _code, True ):
						## Strip the end if on different line or strip from start to end if on the same line
						if _line_number == self.GetCommentBlockStartLine( ):
							## Strip the comment block starting point to the comment block end pos
							_code_search = Acecool.string.Strip( _code_search, self.GetCommentBlockIndexStart( ), Acecool.string.indexOf( _pattern, _code_search ) + len( _pattern ) - 1 )
							_code_search = _code_search.strip( )
						else:
							## Otherwise, we strip everything up to the ending..
							_code_search = Acecool.string.Strip( _code_search, 0, Acecool.string.indexOf( _pattern, _code_search ) + len( _pattern ) - 1 )
							_code_search = _code_search.strip( )

						## Reset out comment block line data because we've encountered a comment-block end-point...
						self.ResetAccessorFuncValues( 'InComment', 'InCommentBlock', 'CommentBlockStartLine', 'CommentBlockIndexStart', 'CommentBlockIndexEnd' )
						self.SetCommentBlockEndLine( _line_number )


			##
			## Callback for OnProcessLine - Can completely modify the code for that line... Call it here before Comment Status, etc... is reset.
			## Task: Correct issue of OnProcessLine receiving outdated information regarding comment and commentblock status..
			##
			_xcode = self.ProcessLine( _line_number, _code_search, self.GetDepth( ), self.GetInComment( ), self.GetInCommentBlock( ) )
			if ( _xcode and _xcode != True and _xcode != False ):
				_code = _xcode

			## For each syntax type
			## CATEGORY_COMMENT_BLOCK		CATEGORY_COMMENT_BLOCK_BEGIN	CATEGORY_COMMENT_BLOCK_FINISH	CATEGORY_COMMENT			CATEGORY_CLASS
			## CATEGORY_TODO				CATEGORY_UNKNOWN				CATEGORY_REALM					CATEGORY_DEFINITION
			## CATEGORY_FUNCTION			CATEGORY_FUNCTION_LOCAL			CATEGORY_FUNCTION_CALLBACK		CATEGORY_FUNCTION_ACCESSOR
			## for _category, _pattern, in self.data_syntax:
			for _category, _search_mode, _pattern, in self.data_syntax:
				##
				## Map the search-modes to search-functions and determine whether or not a match has been found. If it did, then continue...
				##
				if ( self.LineContainsWatchedToken( _search_mode, _pattern, _code_search ) ):
					## if ( _search_mode == SEARCH_TYPE_REGEX and re.search( _pattern, _code_search ) ) or ( _search_mode == SEARCH_TYPE_STARTSWITH and _code_search.startswith( _pattern ) ) or ( _search_mode == SEARCH_TYPE_ENDSWITH and _code_search.endswith( _pattern ) ) or ( _search_mode == SEARCH_TYPE_CONTAINS and Acecool.string.indexOfExists( _pattern, _code_search, False ) ) or ( _search_mode == SEARCH_TYPE_CONTAINSI and Acecool.string.indexOfExists( _pattern, _code_search, True ) ):
					## Make sure comments are not looked at - ie a line comment at the end of the line is ok. A block comment anywhere as long as it isn't the entire line is ok. Or no comment is ok...
					if ( not self.LineContainsComment( _category ) ):
						## If we're processing an accessor func, we reroute it through self.OnAccessorFuncExpansion...
						if ( _category == CATEGORY_FUNCTION_ACCESSOR ):
							##
							_args, _args_count, _args_raw = Acecool.table.FromFunctionArgsString( _code )

							##
							self.OnAccessorFuncExpansion( _search_mode, _category, _pattern, _line_number, _code_search, self.GetDepth( ), _args, _args_count )

						elif ( _category == CATEGORY_VARIABLE ):
							## For variable detection... and processing...
							self.OnVariableDetection( _category, _line_number, _code_search, self.GetDepth( ), _search_mode, _pattern )
						else:
							##
							self.AddEntry( _category, _line_number, _code_search, self.GetDepth( ), _search_mode, _pattern )

						## We've discovered an entry ( either an AccessorFunc entry or a regular entry - we break here to avoid writing it twice ) - skip processing the rest...
						break;
					else:
						##
						if ( self.InCommentDebuggingCategory( _category ) ):
							_output = self.GetCfgTaskLinePrefix( ) + _code_search + self.GetCfgTaskLineSuffix( )


							##
							## Task: Turn this into a rule - anything which can screw up the highlighter such as strings left open, open and closing string-elements, multi-line string definitions, etc... make sure simple 1 line rules can be added to make sure they're ended...
							##

							_output_cap = ''

							## Make sure an even number of single-quotes are on each line
							_single_quotes = _output.count( '\'' )
							if ( _single_quotes % 2 != 0 ):
								_output_cap = '\''

							## Make sure an even number of double-quotes are on each line...
							_double_quotes = _output.count( '"' )
							if ( _double_quotes % 2 != 0 ):
								_output_cap = '"'



							## Add the uncommented TODO remark - and add a hack so any syntax highlighting errors are ignored for the next line...
							self.AddEntry( _category, _line_number, _output + _output_cap, self.GetDepth( ), _search_mode, _pattern )

							## We've discovered an entry - skip processing the rest...
							break;


			## End ForEach Pattern Option

		## Capture the output data
		_output_data = self.ProcessOutput( _file, _count, self.entries )

		## Timestamp of finished data
		self.__timestamp_end__ = self.GetTimeStamp( )

		## Output.. To show how long / short it takes to execute - as of January 21, 2018 a Python file of around 5000 lines takes 0.48 seconds to process ( _User Class with nested support enabled ) plus 0.02 seconds to save ( SSD ).
		print( ' >> Execution Time took a total of: ' + str( self.GetTimeDelta( ) ) + ' seconds!' )


		##
		## Caching System - If enabled, save the file...
		##
		if ( self.GetCfgCachingSystemEnabled( ) ):
			## Save it...
			_result = self.SaveFileCache( _file, _output_data )

			## ## and how much time to save the file..
			## self.__timestamp_save__ = self.GetTimeStamp( )

			## And how much time it took to save... We output this to show the time-savings ( typically 0.01 seconds...
			print( ' >> Saving Cache File took a total of: ' + str( self.GetTimeDelta( self.__timestamp_end__, None ) ) + ' seconds!' )

			## If an error occured during cache-save - add it to the output ( data has already been processed so we can't add it to a category and expect it to show up without reprocessing everything and then it'd be saved if it saves that time, etc.. We simply add it to the top! )
			if ( _result != None ):
				_output_data = '[ Caching Error ] ' + _result + '\n\n' + _output_data


		## Everything has been processed - we return ProcessOutput because ProcessOutput will use callbacks to process the order of things the user wants to see and so on...
		return _output_data


	##
	## Note: Update - give a better and more accurate name... Update to return false when the comment is ( A line comment which doesn't start at the beginning of the line... OR if a block-comment is used but doesn't take up the entire line... Ie it begins before the line and ends somewhere on the line with the line containing more code... It starts anywhere on the line and ends on the line with code left-over... Or it starts anywhere on the line and ends elsewhere as long as code is left over after stripping it )
	##
	def LineContainsComment( self, _category ):
		return not ( ( not self.InCommentDebuggingCategory( _category ) ) and ( ( not self.GetInCommentBlock( ) and not self.GetInComment( ) ) or ( self.GetInCommentBlock( ) and self.GetCommentBlockStartLine( ) == self.GetCommentBlockEndLine( ) and self.GetCommentBlockIndexStart( ) > 0 ) ) )


	##
	## Return True or False depending on whether or not the line contains data we're looking for...
	##
	def LineContainsWatchedToken( self, _search_mode, _pattern, _code ):
		##
		_bFoundRegEx			= ( _search_mode == SEARCH_TYPE_REGEX and re.search( _pattern, _code ) )

		##
		_bFoundStartsEndsWith	= ( ( _search_mode == SEARCH_TYPE_STARTSWITH and _code.startswith( _pattern ) ) or ( _search_mode == SEARCH_TYPE_ENDSWITH and _code.endswith( _pattern ) ) )

		##
		_bFoundContains			= ( ( _search_mode == SEARCH_TYPE_CONTAINS and Acecool.string.indexOfExists( _pattern, _code, False ) ) or ( _search_mode == SEARCH_TYPE_CONTAINSI and Acecool.string.indexOfExists( _pattern, _code, True ) ) )

		##
		return ( _bFoundRegEx or _bFoundStartsEndsWith or _bFoundContains )


	##
	## Callback - Used to determine what to do when code isn't properly aligned ( check for =, and more than 1 space or 1 or more tabs.. if so then we add or subtract with a minimum of 1 space the delta depth * our indent char to the center to allow the alignment to remain flush...
	##
	def OnCalcCodeAlignment( self, _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original = None ):
		##
		## Detect if we have something to align ( existing = in line )
		##

		## If the _depth_delta is 0, we don't need to even bother..
		if ( _depth_delta == 0 ):
			## self.print( 'OnCalcCodeAlignment', ' >> OFF DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" );' )
			## print( ' >> OFF DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" );' )
			return _code

		## Depth Delta is either addition or subtraction...
		## self.print( 'OnCalcCodeAlignment', ' >> _ON DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" );' )
		## print( ' >> _ON DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" );' )

		## Track the type...
		_type = 0

		## Grab the location
		_pos = Acecool.string.indexOf( '  = ', _code )

		## Check other chars
		if ( not _pos > 0 ):
			_pos = Acecool.string.indexOf( '\t= ', _code )
			_type = 1

		## Check other chars
		if ( not _pos > 0 ):
			_pos = Acecool.string.indexOf( ':  ', _code )
			_type = 2

		## Check other chars
		if ( not _pos > 0 ):
			_pos = Acecool.string.indexOf( ':\t', _code )
			_type = 3

		## If we have one - and we're not in a comment...
		if ( _pos > 0 and not self.GetInComment( ) and not self.GetInCommentBlock( ) ):
			## We need to add...
			## self.print( 'OnCalcCodeAlignment', ' >> ADD DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" ); - BEFORE' )

			## Determine what we need to do... Add or subtract...

			## Add...
			if ( _depth_delta > 0 ):
				## Type 0: Spaces before =...
				if ( _type == 0 ):
					_code = _code.replace( '  = ', Acecool.string.repeat( ' ', _depth_delta ) + '  = ', 1 )

				## Type 1: Tabs before =
				if ( _type == 1 ):
					_code = _code.replace( '\t= ', Acecool.string.repeat( '\t', _depth_delta ) + '\t= ', 1 )

				## Type 2: Spaces after :
				if ( _type == 2 ):
					_code = _code.replace( ':  ', ':  ' + Acecool.string.repeat( ' ', _depth_delta ), 1 )

				## Type 3: Tabs after :
				if ( _type == 3 ):
					_code = _code.replace( ':\t', ':\t' + Acecool.string.repeat( '\t', _depth_delta ), 1 )

			## Show the result...
			## self.print( 'OnCalcCodeAlignment', ' >> ADD DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" ); - FINISHED' )

			## Subtract Mode...
			if ( _depth_delta < 0 ):
				## Task: Create Subtraction algorithm... Make sure we have whitespace to subtract... Determine depth of data to subtract.. etc...

				## Output... a friendly notice that subtraction isn't implemented... Shouldn't happen often...
				## self.print( 'OnCalcCodeAlignment', ' >> SUB DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" ); - NOT IMPLEMENTED' )
				## print( ' >> SUB DepthDif( ' + str( _depth_delta ) + ' ); line( ' + str( _line_number ) + '); code( "' + _code + '" ); - NOT IMPLEMENTED' )

				## Logic not implemented...
				pass

			## Show the final data - used instead of - FINISHED...
			## self.print( 'OnCalcCodeAlignment', ' >> FLUSH SYSTEM __DONE Diff( ' + str( _depth_delta ) + ' ): ' + _code )

		## Do nothing by default...
		return _code


	##
	## Callback - Called on the code being added to a category so that the code can be altered - ie remove Acecool.C. from functions, or change function to f to make it shorter, etc...
	##
	def PreAddEntry( self, _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original = None ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'PreOnAddEntry', _code )

		##
		## Shorten function to f....
		##
		if ( len( self.GetCfgFuncNameDefinitionSearch( ) ) > 0 ):
			if ( _code.startswith( self.GetCfgFuncNameDefinitionSearch( ) ) ):
				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( 'PreOnAddEntry', 'Stripping Function Before: ' + _code )

				_code = Acecool.string.SubStr( _code, len( self.GetCfgFuncNameDefinitionSearch( ) ), len( _code ) )
				_code = self.GetCfgFuncNameDefinitionReplace( ) + _code

				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( 'PreOnAddEntry', 'Stripping Function After: ' + _code )


		##
		## Determine whether or not we need to modify the code...
		##
		_xcode = self.OnCalcCodeAlignment( _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original )
		if ( _xcode != None and _xcode != '' and _xcode != False and _xcode != True ):
			_code = _xcode
			_xcode = None


		##
		## Special Note Prefixes!
		##

		## ## Task Related
		## if ( _pattern == 'Task: ' or _pattern == 'Fix: ' or _pattern == 'TODO: ' or _pattern == 'Work: ' or _pattern == 'Update: ' ):
		## 	_code = '▲' + _code

		## ## Note Related
		## if ( _pattern == 'Note: ' or _pattern == 'Important: ' or _pattern == 'Important: ' ):
		## 	_code = '☼' + _code


		## _xcode, _xline, _xdepth, _xcategory

		## Return the normal PreOnAddEntry to allow for canceling entries to occur, etc... We do our internal edits prior to the user though - I may change it to after... or add a Post function to give more flexibility to the user...
		return self.PreOnAddEntry( _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original )


	##
	## Helper - Used to add a code entry to a category... This calls various callbacks such as OnFunction/ClassArgument ( For each arg, to modify the arg ), PreOnAddEntry ( to edit the data, add data, look for syntax errors, deny entry from being added, etc.. ), PreOnAddEntryCategory ( to mod the category the entry goes to )
	##
	def AddEntry( self, _category, _line_number, _code, _depth, _search_mode, _pattern, _parent = None ):
		## Allow function / Class args to be processed...
		## Function Arguments Replacement Callback - Will move this out of this callback function into an internal PreOnAddEntry callback or PostOnAddEntryCallbackButPreAddAlterations... but more concise..
		if ( self.GetCategoryType( _category ) == CATEGORY_TYPE_FUNCTION ):
			_code = self.ProcessEntryArgs( _category, _line_number, _code, _depth, _search_mode, _pattern, 'OnFunctionArgument' )

		## Allow function / Class args to be processed...
		## Function Arguments Replacement Callback - Will move this out of this callback function into an internal PreOnAddEntry callback or PostOnAddEntryCallbackButPreAddAlterations... but more concise..
		if ( self.GetCategoryType( _category ) == CATEGORY_TYPE_CLASS ):
			_code = self.ProcessEntryArgs( _category, _line_number, _code, _depth, _search_mode, _pattern, 'OnClassArgument' )


		##
		## Allow modifying the data on entry...
		## Note: Testing with the ability to return '' to also skip it or to add an empty line...
		##

		## Category Replacement... Do this after the function args above to ensure they're applied properly - then change for other reasons..
		_xcategory = self.PreOnAddEntryCategory( _category, _line_number, _code, _depth, _search_mode, _pattern )

		## Make sure we remember the original category in case PreOnAddEntry needs it...
		_category_original = _category

		## If we're to modify the category, update the category - original category will already be set...
		if ( _xcategory != None and _xcategory != '' and _xcategory != False and _xcategory != True ):
			## Update the category to the replacement..
			_category = _xcategory

		## Calculate new depth, and the delta-depth...
		_calc_depth = self.OnCalcDepth( _category, _line_number, _code, _depth, _search_mode, _pattern )
		_depth_delta = self.GetDeltaDepth( _depth, _calc_depth )

		## _xcode, _xline, _xdepth, _xcategory = self.PreOnAddEntry( _category, _line_number, _code, _depth, _search_mode, _pattern )
		_xcode = self.PreAddEntry( _category, _line_number, _code, _depth, _depth_delta, _search_mode, _pattern, _category_original )

		## If the returned data exists, isn't blank and isn't a boolean then update our _code value because we're using what the dev decided to replace _code with..
		if ( _xcode != None and _xcode != '' and _xcode != False and _xcode != True ):
			_code = _xcode

		## If we return '' then add a blank entry...
		if ( _xcode == '' ):
			## self.AddBlankEntry( _category )
			return

		## If we returned a boolean then we don't add anything - we assume the coder who returned True or False in the callback called AddEntry or RecalculateLine...
		if ( _xcode == False or _xcode == True ):
			return

		## If the category doesn't add, we need to add it...MAP_CATEGORIES[ _category ]
		if ( self.categories.get( str( _category ) ) == None ):
			self.categories[ str( _category ) ] = [ ]

		##
		_entry = { 'line_number': _line_number, 'category': _category, 'code': _code, 'depth': _calc_depth, 'realm': REALM_UNKNOWN, 'parent': _parent }
		self.entries = self.entries + 1

		##
		## if ( self.GetCfgDisplaySortMethod( ) == ENUM_SORT_METHOD_LINE_NUMBER_DESC ):
		## 	## Descending - First In Last Out
		## 	self.categories[ str( _category ) ].insert( 0, _entry )
		## else:
		## Default == By Line Number - First In First Out...

		##
		_category_str = str( _category )

		## Convert the string _parent code / id into an id - if the id doesn't exist we make it...
		## _parent_id = self.GetMappedNestedCategoryID( _category, _category_str, _parent )

		## If _parent isset ( must be a string ) - it means we're adding an entry to a primary-category _category CHILD - the entry is inserted into _parent because _parent is - or will be - a nested category WITHIN _category...
		if ( _parent != None ):
			## If the primary category doesn't have a child table - make it...
			if ( self.category_children.get( _category_str, None ) == None ):
				## print( 'Created self.category_chilren[ ' + _category_str + ' ]' )
				self.category_children[ _category_str ] = { }

			## Note: If the parent doesn't exist - add it ( This is where I'd have to map the _parent text to an id )
			if ( self.category_children[ _category_str ].get( _parent, None ) == None ):
				## print( 'Created self.category_chilren[ ' + _category_str + ' ][ ' + _parent + ' ]' )
				self.category_children[ _category_str ][ _parent ] = [ ]

			## Add the entry to the child, of the primary category, category - which is known as the entry parent... So we have ( MainCategory > NestedCategoryKnownAsParent > Entry ) instead of ( MainCategory > Entry ) and instead of using a standard table, we use another - will change soon when I generalize everything...
			self.print( 'AddEntry', 'Added child _entry to self.category_chilren[ ' + _category_str + ' ][ ' + _parent + ' ] = ' + str( _entry ) )
			self.category_children[ _category_str ][ _parent ].append( _entry )
			## self.category_children[ _category_str ][ _parent_id ].append( _entry )
		else:
			##
			self.categories[ _category_str ].append( _entry )

		##
		self.AddCategoryEntryCount( _category )


	## ##
	## ## Takes Category as string-name - returns numerical category id
	## ##
	## def GetMappedNestedCategoryID( self, _category, _category_str, _parent ):
	## 	if ( _parent != None ):
	## 		## Add the primary category id to the beginning in order to ensure no duplicates...
	## 		_parent_str = _category_str + '_' + str( _parent )

	## 		## If the primary category doesn't have a child table - make it...
	## 		if ( self.category_children.get( _category_str, None ) == None ):
	## 			self.category_children[ _category_str ] = { }

	## 		## If we don't have code > ID map for _parent, then we create it... Note: We append the category id to the beginning to avoid duplicates...
	## 		if ( self.category_children_map.get( _parent_str, None ) == None ):
	## 			## Append an empty table into the primary category child table so we can use it for entries... and retrieve the ID generated by append...
	## 			_parent_id = self.category_children[ _category_str ].append( [ ] )

	## 			## Add the entry - self.category_children_map[ "<NUMERIC_CATEGORY_AS_STRING>_<NESTED_CATEGORY_CODE>" ] = NumericParentID
	## 			self.category_children_map[ _parent_str ] = _parent_id

	## 		## Return the ID
	## 		return self.category_children_map[ _parent_str ]


		## 	## Note: If the parent doesn't exist - add it ( This is where I'd have to map the _parent text to an id )
		## 	if ( self.category_children[ _category_str ].get( _parent_id, None ) == None ):
		## 		## print( 'Created self.category_chilren[ ' + _category_str + ' ][ ' + _parent + ' ]' )
		## 		## self.category_children[ _category_str ][ _parent ] = [ ]
		## 		self.category_children[ _category_str ].append( _parent_id )



		## ## Map
		## _map = self.category_children_map
		## ## _children = self.category_children

		## ## Convert the string _category name into numerical id...
		## return _map[ _category ]


	##
	##
	##
	def ProcessCategoryOutput( self, _category, _category_str, _category_name, _category_desc ):
		##
		_data = ''

		## Append <THIS> Category Entry Contents to _data..
		## _data += self.CompileCategoryEntriesString( _category, _category_str, _category_name, _category_desc )

		##
		_children = self.category_children.get( _category_str, None )

		## If there aren't any children, stop at returning only the primary category...
		if ( _children == None or not len( _children ) > 0 ):
			return _data

		## Add the category friendly-name to the output data...
		## Check to see if we have data for the category type...
		_value = self.categories.get( _category_str )

		## Modify the title of the category...
		_category_name += ' - Children'

		## The Extra Length we need to keep to ensure the hack survives and the name - so we trim from within the description only..
		_cat_desc_extra_len = len( _category_name + self.GetCfgCategoryDescPrefix( ) + self.GetCfgCategoryDescSuffix( ) )

		## Simple Redirect...
		_output_desc = _category_desc

		## If we are set to limit the length, then so be it... The length will be a separate config because we know the line-ending is 1 + numbers - so 5 for up to 9,999 line file, 6 for 99,999 which will be assumed..
		if ( self.GetCfgLimitCatagoryColumnWidth( ) >= 0 ):
			## Ensure it is truncated at the right level, then strip the spaces so shorter messages to not extend the suffix so far out of the reasonable area...
			_output_desc = Acecool.string.FormatColumn( self.GetCfgLimitCatagoryColumnWidth( ) - _cat_desc_extra_len, _output_desc ).strip( )

		## If we're showing the category description, then append the Prefix, Description and Suffix together - otherwise use a blank line as the return.. then apply append this var to the center of the data var...
		_cat_desc = Acecool.logic.ternary( ( self.GetCfgCategoryLineShowDesc( ) == True and self.GetCfgLimitCatagoryColumnWidth( ) >= 0 ), self.GetCfgCategoryDescPrefix( ) + _output_desc + self.GetCfgCategoryDescSuffix( ), '' )

		## Add the category friendly-name to the output data...
		_data += self.GetCfgCategoryLinePrefix( ) + _category_name + _cat_desc + self.GetCfgCategoryLineSuffix( ) + self.GetCfgNewLine( )

		## Sort the children by name...
		_method = self.GetCfgDisplaySortMethod( )

		## If a sorting method is defined - use it...
		if ( _method != ENUM_SORT_METHOD_DEFAULT ):
			_children = sorted( _children.keys( ), reverse = Acecool.logic.ternary( ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_ALPHABETICAL ), False, True ) )


		for _tab in _children:
			## _name			= _tab[ 'category_name' ]
			## _entries			= _tab[ 'entries' ]

			## Process each entry...
			_data += self.ProcessTableOutputEntries( self.category_children[ _category_str ][ _tab ], _category, _category_str, _category_name, _category_desc )

			## Todo modify...
			## _data += self.ProcessTableOutputEntries( self.category_children[ _category_str ][ _values ], _category, _category_str, _category_name, _category_desc )

		## Return data..
		return _data


	##
	## Callback the generate function uses to simplify each problem... Simply adds a helping text-notification at the top of the map so we can be sure CodeMap is actually running on our current file ( I've had issues during updates where it'd be open but not active which required a lot of meanuevering to re-engage it ) along with some other information...
	## Note: XCodeMapper.entries is _matches
	##
	## @Arg: _file - The File-Name being processed..
	## @Arg: _lines - The total number of lines in the file being processed.
	## @Arg: _entries - The total number of entries added to XCodeMapper from the current file being processed..
	## @Returns: _data - The ENTIRE CodeMap Output for all of the categories the user wants information on...
	##
	def ProcessOutput( self, _file, _lines, _entries ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'ProcessOutput', 'Compiling output data for each populated category...' )

		## Start with a clean-slate....
		_data = ''

		##
		## Because I want to add additional - real-time info - I actually have to modify this.. I could look at hidden categories on setup - and I may change this to match later, but categories can be hidden on the fly, right up to and including the end...
		##
		## If the user wants the message to appear which shows which file was parsed along with the number of entries added and total number of lines in the file..  then show it...
		if ( self.GetCfgOutputFileEntriesInfo( ) ):
			## Mapping: <FILENAME>
			_data += 'Mapping: ' + Acecool.string.GetFilename( _file )

			##  containing : <TOTAL_LINES> lines and <TOTAL_ENTRIES> entries!<NEW_LINE * CONFIG'd_NUMBER_OF_NEW_LINES>
			_data += ' containing: ' + str( _lines ) + ' lines and ' + str( _entries ) + ' entries!' + Acecool.string.repeat( self.GetCfgNewLine( ), self.GetCfgOutputFileEntriesInfoNewLines( ) )

		## For each category in our output_order list - compile the data...
		for _category, _category_str, _category_name, _category_desc, in self.output_order:
			## If Category should be hidden.. Hide it...
			if ( self.IsCategoryHidden( _category ) ):
				continue

			## Add the Category Data
			_data += self.CompileCategoryEntriesString( _category, _category_str, _category_name, _category_desc )

			## Add the Category Data and Children Data...
			_data += self.ProcessCategoryOutput( _category, _category_str, _category_name, _category_desc )

		## For each category in our output_order list - compile the data...
		## for _category, _category_str, _category_name, _category_desc, in self.output_order:
			## _category_entries = 0
			## _category_entries = len( self.categories.get( str( _category ), { } ) )

			##
			## if ( self.categories[ _category_str ] == None ):
			## 	continue

			## _category_entries = len( self.categories[ _category_str ] )

			## If Category should be hidden.. Hide it...
			## if ( self.IsCategoryHidden( _category ) ):
				## if ( _category_entries > 0 ):
				## 	_hidden_entries += _category_entries
				## 	_hidden_categories_with_entries += 1

				## _hidden_categories += 1

				## continue
			## else:
			## 	##
			## 	if ( _category_entries > 0 ):
			## 		_displayed_categories += 1

			## Add the Category Data
			## _data += self.CompileCategoryEntriesString( _category, _category_str, _category_name, _category_desc )

			## Add the Category Data and Children Data...
			## _data += self.ProcessCategoryOutput( _category, _category_str, _category_name, _category_desc )


		##
		## TODO: Determine whether more information is warranted...
		##
		## ## If the user wants the message to appear which shows which file was parsed along with the number of entries added and total number of lines in the file..  then show it...
		## if ( self.GetCfgOutputFileEntriesInfo( ) ):
		## 	_str_lines = str( _lines )
		## 	_str_entries = str( _entries )
		## 	_str_entries_h = str( _hidden_entries )
		## 	_str_cats = str( len( self.output_order ) )
		## 	_str_cats_h = str( _hidden_categories )
		## 	_str_cats_shown = str( _displayed_categories )
		## 	_str_cats_h_w_e = str( _hidden_categories_with_entries )

		## 	_new_lines = Acecool.string.repeat( self.GetCfgNewLine( ), self.GetCfgOutputFileEntriesInfoNewLines( ) )

		## 	 ## containing : <TOTAL_LINES> lines and <TOTAL_ENTRIES> entries!<NEW_LINE * CONFIG'd_NUMBER_OF_NEW_LINES>
		## 	_data = ' containing: ' + _str_lines + ' lines and ' + _str_entries + ' total discovered entries with ' + _str_entries_h + ' hidden!\n ' + _str_cats + ' Total Non-Dynamic Categories ' + _str_cats_h + ' hidden and ' + _str_cats_shown + ' shown, ' + _str_cats_h_w_e + ' of which have entries!' + _new_lines + _data

		## 	## Mapping: <FILENAME>
		## 	_data = 'Mapping: "' + Acecool.string.GetFilename( _file ) + '"' + _data

		## As our output is now assigned to memory with a variable to reference it, we can shutdown the mapper and properly clean up the memory space
		self.Shutdown( )

		return _data


	##
	## PreProcessor - This is added so common mistakes / syntax errors, etc.. can be detected on each line using a new TypoFixEntry system...
	##
	def ProcessLine( self, _line_number, _code, _depth, _in_comment, _in_comment_block ):
		##
		## Process the typo list.... If there are entries...
		##
		if ( len( self.typo_list ) > 0 ):
			##
			for _pattern, _error, _solution, in self.typo_list:
				## If a typo was found on the line, then notate it... Also, make sure it wasn't on a self.AddTypoFixEntry line...
				if ( not _code.startswith( 'self.AddTypoFixEntry' ) and _code.find( _pattern ) >= 0 ):
					## Add the error to the errors category
					self.AddError( 'Typo: ' + _error + ' is a common typo and doesn\'t exist - use ' + _solution + ' instead!', _line_number );

					## Modify the output code-line by adding TYPO to it...
					_code = _code + ' [ TYPO ]'


		## Pass-Through to our Callback...
		return self.OnProcessLine( _line_number, _code, _depth, _in_comment, _in_comment_block )


	##
	## Task: Update this to be more generic in order to be able to process data recursively from a single table entry point... The category data can be retrieved inside or from a single var...
	##
	def ProcessTableOutputEntries( self, _list, _category, _category_str, _category_name, _category_desc ):
		## if ( _method == ENUM_SORT_METHOD_ALPHABETICAL or _method == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):

		## Make sure we're working with a blank set of data to output...
		_data = ''


		##
		_method = self.GetCfgDisplaySortMethod( )
		def Sorter( _item ):
			##
			_result = ''

			## If we're sorting alphabetically, then return the appropriate key... Modify the data accordinly and then return it...
			## Task: Add a Callback so the data can be modified further during the sorting process - so def / class can be removed from consideration, for example...
			if ( _method == ENUM_SORT_METHOD_ALPHABETICAL or _method == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):
				_code = _item[ 'code' ] ## .replace( 'def', '' ).replace( 'class', '' )
				_stripped = _code.strip( )
				_result = _stripped.upper( )
				if ( _result == '' ):
					return 'zzz'

				return _result

			## If we're sorting by line-number - which is the default method although we only process it if specified so some things can end up in different places despite being 'similar' ie blank lines being moved, and so on... Or line-number in reverse... I don't see much use for this but I'm leaving it here as a place-holder to add callback support for one or more of the keys - code, line_number, depth, etc..
			if ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_LINE_NUMBER_DESC ):
				_code = str( _item[ 'line_number' ] )
				return ( len( _code ), _code )

		## If we chose to sort the category data - The Ternary within sets up Reverse because if it isn't any one of the standards non _DESC then the only one it can be is DESC.. Except default isn't needed...
		if ( _method != ENUM_SORT_METHOD_DEFAULT ):
			## Non-Default means we are going to SORT the data... How and in which order depends on the _method...
			_list = sorted( _list, key = Sorter, reverse = Acecool.logic.ternary( ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_ALPHABETICAL ), False, True ) )

		##
		## Process the table - For each entry in the category... enter add it to the local data pool so it can be returned and added to the global pool..
		##
		_i = 0
		for _values in _list:
			_line			= _values[ 'line_number' ]
			_category		= _values[ 'category' ]
			_code			= _values[ 'code' ]
			_depth			= _values[ 'depth' ] + 1
			_realm			= _values[ 'realm' ]
			_parent			= _values[ 'parent' ]

			##
			## _data += self.GetCfgCategoryLinePrefix( ) + _category_name + _cat_desc + self.GetCfgCategoryLineSuffix( ) + self.GetCfgNewLine( )
			if ( _i == 0 ):
				_data += '\t' + self.GetCfgCategoryLinePrefix( ) + str( _parent ) + self.GetCfgCategoryLineSuffix( ) + self.GetCfgNewLine( )
				_i += 1


			## Helpers
			_code_len		= len( _code )
			_indentation	= self.OnCalcDepthChars( _category, _line, _code, _depth )
			_indent_mode	= self.GetCfgIndentCodeColumnMode( )
			_indent_code_w	= self.GetCfgIndentCodeColumnWidth( ) - len( _indentation )

			## Add Blank Line Entry Support - Blank lines will not supply a line-number or code - ie line number will be a blank string as will code...
			if ( _line != '' and _line >= 0 and _code.strip( ) != '' ):
				## Set the output for the line-number to be ':<line-number>'
				_output_line = ':' + str( _line )

				## If we don't want to output the line-number - ie line number is -1 or so, then set it to an empty string - this is used for Help lines, Empty Lines, etc..
				if ( _line < 0 ):
					_output_line = ''

				## DEFAULT CLAUSE meaning no need for - ie Default doesn't need anything to happen..: if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT ):
				_output = _code + _output_line

				## If we force truncate, then force it... Or, if we are organized, then we apply the truncatable function only if it doesn't truncate.... If it does, we leave it to default above!!
				if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE or ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED and _code_len <= _indent_code_w ) ):
					_output = Acecool.string.FormatColumn( _indent_code_w, _code ) + _output_line

				## Output the data in the format of ( + connects 2 pieces of data ) TAB + ( repeat TAB_CHAR using _depth as multiple ) + LINE_CODE + : + LINE_NUMBER + LINE_BREAK / NEW-LINE
				_data += _indentation + _output + self.GetCfgNewLine( )
			else:
				##  New line no matter what?
				_data += self.GetCfgNewLine( )

		## and after outputting the data for the category, add an additional newline to allow a space between categories..
		_data += self.GetCfgNewLine( )

		return _data


	##
	## Compiles all Category Entries as a string and returns it...
	##
	## @Arg: _category - Category ENUMeration as Integer
	## @Returns: String* - Long string of text returned, if data exists, otherwise an empty string is returned...
	##
	def CompileCategoryEntriesString( self, _category, _category_str, _category_name, _category_desc, _depth_mod = 0 ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'CompileCategoryEntriesString', 'Compiling output data...' )

		## Make sure we're working with a blank set of data to output...
		_data = ''

		## Check to see if we have data for the category type...
		_value = self.categories.get( _category_str )

		## If the data in the category has data then we can output something!
		if ( _value != None and len( _value ) > 0 ):
			## The Extra Length we need to keep to ensure the hack survives and the name - so we trim from within the description only..
			_cat_desc_extra_len = len( _category_name + self.GetCfgCategoryDescPrefix( ) + self.GetCfgCategoryDescSuffix( ) )

			## Simple Redirect...
			_output_desc = _category_desc

			## If we are set to limit the length, then so be it... The length will be a separate config because we know the line-ending is 1 + numbers - so 5 for up to 9,999 line file, 6 for 99,999 which will be assumed..
			if ( self.GetCfgLimitCatagoryColumnWidth( ) >= 0 ):
				## Ensure it is truncated at the right level, then strip the spaces so shorter messages to not extend the suffix so far out of the reasonable area...
				_output_desc = Acecool.string.FormatColumn( self.GetCfgLimitCatagoryColumnWidth( ) - _cat_desc_extra_len, _output_desc ).strip( )

			## If we're showing the category description, then append the Prefix, Description and Suffix together - otherwise use a blank line as the return.. then apply append this var to the center of the data var...
			_cat_desc = Acecool.logic.ternary( ( self.GetCfgCategoryLineShowDesc( ) == True and self.GetCfgLimitCatagoryColumnWidth( ) >= 0 ), self.GetCfgCategoryDescPrefix( ) + _output_desc + self.GetCfgCategoryDescSuffix( ), '' )

			## Add the category friendly-name to the output data...
			_data += self.GetCfgCategoryLinePrefix( ) + _category_name + _cat_desc + self.GetCfgCategoryLineSuffix( ) + self.GetCfgNewLine( )

			## The List to output...
			_list = self.categories[ _category_str ]


			## These should be sorted by entry
			## if ( self.GetCfgDisplaySortMethod( ) == ENUM_SORT_METHOD_LINE_NUMBER_DESC ):
			## 	## Descending - First In Last Out
			## 	self.categories[ str( _category ) ].insert( 0, _entry )
			## else:
			## 	## Default == By Line Number - First In First Out...
			## 	self.categories[ str( _category ) ].append( _entry )
			## from operator import itemgetter

			## These should be sorted on exit...
			## if ( self.GetCfgDisplaySortMethod( ) == ENUM_SORT_METHOD_ALPHABETICAL ):
				## _list = sorted( _list, key = lambda _kv: _kv[ 2 ].lower < _list.next( ) )
				## _list.sort( )

			## ENUM_SORT_METHOD_ALPHABETICAL_DESC
			## ENUM_SORT_METHOD_ALPHABETICAL
			## ENUM_SORT_METHOD_LINE_NUMBER_DESC
			## ENUM_SORT_METHOD_LINE_NUMBER

			## _method = self.GetCfgDisplaySortMethod( )
			## self.SetCfgDisplaySortMethod( ENUM_SORT_METHOD_LINE_NUMBER_DESC )
			## self.SetCfgDisplaySortMethod( ENUM_SORT_METHOD_LINE_NUMBER )
			## self.SetCfgDisplaySortMethod( ENUM_SORT_METHOD_ALPHABETICAL_DESC )
			## self.SetCfgDisplaySortMethod( ENUM_SORT_METHOD_ALPHABETICAL )


			## if ( _method == ENUM_SORT_METHOD_ALPHABETICAL or _method == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):

			##
			_method = self.GetCfgDisplaySortMethod( )
			def Sorter( _item ):
				##
				_result = ''

				## If we're sorting alphabetically, then return the appropriate key... Modify the data accordinly and then return it...
				## Task: Add a Callback so the data can be modified further during the sorting process - so def / class can be removed from consideration, for example...
				if ( _method == ENUM_SORT_METHOD_ALPHABETICAL or _method == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):
					_code = _item[ 'code' ] ## .replace( 'def', '' ).replace( 'class', '' )
					_stripped = _code.strip( )
					_result = _stripped.upper( )
					if ( _result == '' ):
						return 'zzz'

					return _result

				## If we're sorting by line-number - which is the default method although we only process it if specified so some things can end up in different places despite being 'similar' ie blank lines being moved, and so on... Or line-number in reverse... I don't see much use for this but I'm leaving it here as a place-holder to add callback support for one or more of the keys - code, line_number, depth, etc..
				if ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_LINE_NUMBER_DESC ):
					_code = str( _item[ 'line_number' ] )
					return ( len( _code ), _code )

			## If we chose to sort the category data - The Ternary within sets up Reverse because if it isn't any one of the standards non _DESC then the only one it can be is DESC.. Except default isn't needed...
			if ( _method != ENUM_SORT_METHOD_DEFAULT ):
				## Non-Default means we are going to SORT the data... How and in which order depends on the _method...
				_list = sorted( _list, key = Sorter, reverse = Acecool.logic.ternary( ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_ALPHABETICAL ), False, True ) )

			## if ( _method == ENUM_SORT_METHOD_LINE_NUMBER or _method == ENUM_SORT_METHOD_LINE_NUMBER_DESC ):
			## 	def Sorter( _item ):

			## 		_code = str( _item[ 'line_number' ] )
			## 		## _stripped = _code.strip( )
			## 		## _result = _stripped.lower( )
			## 		## if ( _result == '' ):
			## 			## return 'zzz'

			## 		return ( len( _code ), _code )

				##
				## _list = sorted( _list, key = Sorter, reverse = Acecool.logic.ternary( ( self.GetCfgDisplaySortMethod( ) == ENUM_SORT_METHOD_LINE_NUMBER ), False, True ) )

			## _list = sorted( _list, key = itemgetter( 'code' ) )
			## elif ( self.GetCfgDisplaySortMethod( ) == ENUM_SORT_METHOD_ALPHABETICAL_DESC ):
			## 	_list = sorted( _list, key = str.lower, reverse = True )


			##
			## Process the table - For each entry in the category... enter add it to the local data pool so it can be returned and added to the global pool..
			##
			## for _line, _category, _code, _depth, _realm, in _list:
			##
			for _values in _list:
				_line			= _values[ 'line_number' ]
				_category		= _values[ 'category' ]
				_code			= _values[ 'code' ]
				_depth			= _values[ 'depth' ] + _depth_mod
				_realm			= _values[ 'realm' ]

				## Helpers
				_code_len		= len( _code )
				_indentation	= self.OnCalcDepthChars( _category, _line, _code, _depth )
				_indent_mode	= self.GetCfgIndentCodeColumnMode( )
				_indent_code_w	= self.GetCfgIndentCodeColumnWidth( ) - len( _indentation )

				## Add Blank Line Entry Support - Blank lines will not supply a line-number or code - ie line number will be a blank string as will code...
				## if ( _line != -1 and _line != '' and _code.strip( ) != '' and _code != self.GetCfgEntryBlank( ) ):
				if ( _code != self.GetCfgEntryBlank( ) ):
					## Set the output for the line-number to be ':<line-number>'
					_output_line = ':' + str( _line )

					## If we don't want to output the line-number - ie line number is -1 or so, then set it to an empty string - this is used for Help lines, Empty Lines, etc..
					if ( _line < 0 ):
						_output_line = ''

					## DEFAULT CLAUSE meaning no need for - ie Default doesn't need anything to happen..: if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT ):
					_output = _code + _output_line

					## If we force truncate, then force it... Or, if we are organized, then we apply the truncatable function only if it doesn't truncate.... If it does, we leave it to default above!!
					if ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE or ( _indent_mode == ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED and _code_len <= _indent_code_w ) ):
						_output = Acecool.string.FormatColumn( _indent_code_w, _code ) + _output_line

					## Output the data in the format of ( + connects 2 pieces of data ) TAB + ( repeat TAB_CHAR using _depth as multiple ) + LINE_CODE + : + LINE_NUMBER + LINE_BREAK / NEW-LINE
					_data += _indentation + _output + self.GetCfgNewLine( )
				else:
					##  New line no matter what?
					_data += self.GetCfgNewLine( )

			## and after outputting the data for the category, add an additional newline to allow a space between categories..
			_data += self.GetCfgNewLine( )

		return _data


	##
	## Internal Processor
	##
	def OnInit( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnInit' )

		## Reset the data each time Run is called, otherwise the data will continue stacking...
		self.Reset( )

		## Setup the object
		self.Setup( )


	##
	## Internal Processor
	##
	def OnSetup( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetup' )

		## Setup the AccessorFuncs by creating and initializing them..
		self.OnSetupAccessorFuncsDefault( )		# Calls Callback OnSetupAccessorFuncs to create other callbacks..

		## Setup Default AccessorFunc values...
		self.OnSetupConfigDefaults( )			# Calls Callback OnSetupConfig to setup the data in the other callbacks..

		##
		self.OnSetupArgDataTypeDefaults( )		# Calls Callback OnSetupArgDataType( ) to setup the data-types...

		## Callback: This lets you create new categories if you don't want to add them at the top of the script...
		self.OnSetupCategories( )

		## ## Callback: This is how the language is declared - and you can also earmark other special segments of code that you'd like categorized or watched for syntax errors...
		self.OnSetupSyntax( )

		## Setup Rules for indentation tracking, and more..
		self.OnSetupSyntaxRules( )

		## Callback: This lets you choose the order the categories output their information...
		self.OnSetupCategoryOutputOrderDefaults( )

		## Sets up the help section of this script...
		self.OnSetupHelp( )


	##
	## Internal helper for XCodeMapper - This creates helper AccessorFuncs - Getter / Setter functions...
	## TODO: Redefine the config system so while the Helpers are available it uses GetFlag / SetFlag and the Accessors work off that... maybe..
	##
	def OnSetupAccessorFuncsDefault( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncsDefault' )


		## Internal / Primary AccessorFuncs

		## For languages such as ASP and PHP which have markers which designate where its code is vs HTML or other...
		## For non-.js excentions this also applies to JavaScript in php, html and asp extension files..
		## TODO: Extend code / OnSetupSyntax callback and nested calls to specify which language the syntax applies to to allow one file to manage all or many languages...
		## For files which allow multiple languages to be executed - This simply tells the script which language is currently being processed thereby telling the script which syntax to use
		## .php allows HTML / JavaScript and PHP.. .asp allows the same with ASP.. .js allows JavaScript.. .htm / .html allows HTML / JavaScript..
		## 'Lua / JavaScript / PHP / ASP / python / etc..'
		self.AccessorFunc( 'InLanguage', False, 'ActiveLanguage', CFG_PRIMARY_ACTIVE_LANGUAGE )

		## The Entire File stored as a new-line delimited table...
		self.AccessorFunc( 'FileData', CONST_DATA_NONE, 'FileLines', 0 )

		## Class Data
		self.AccessorFunc( 'ActiveClass', None, 'ActiveClassDepth', 0, 'ClassStack', [ ] )

		## Create helper functions to be used as controlled-toggles in certain areas / Manage which line the comment block started on and where it ended / Manage which character the comment-block started with and the index of where the comment-block end is...
		self.AccessorFunc( 'InComment', False, 'InCommentBlock', False, 'CommentBlockStartLine', -1, 'CommentBlockEndLine', -1, 'CommentBlockIndexStart', -1, 'CommentBlockIndexEnd', -1 )

		## Manage the Depth / indentation
		self.AccessorFunc( 'Depth', 0, 'LastDepth', 0, 'IndentLength', 0 )

		## GLua Execution Realm - LIMIT TO REALM_ ENUMeration...
		self.AccessorFunc( 'Realm', REALM_UNKNOWN )


		## Config AccessorFuncs

		## Language we're using...
		self.ConfigAccessorFunc( 'DebugMode', CFG_DEBUG_MODE, 'Language', CFG_PRIMARY_ACTIVE_LANGUAGE )

		## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
		self.ConfigAccessorFunc( 'OutputFileEntriesInfo', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO, 'OutputFileEntriesInfoNewLines', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES )

		## New-Line preferred for reading Files and for outputting to the Code - Map Panel
		self.ConfigAccessorFunc( 'NewLine', CFG_NEW_LINE, 'FileNewLine', CFG_FILE_NEW_LINE )

		## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
		self.ConfigAccessorFunc( 'EntryBlank', CFG_ENTRY_BLANK, 'IndentChars', CFG_INDENT_CHARS, 'IndentCodeColumnMode', CFG_INDENT_CODE_COLUMN_WIDTH_MODE, 'IndentCodeColumnWidth', CFG_INDENT_CODE_COLUMN_WIDTH, 'LimitCatagoryColumnWidth', CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH )

		## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
		self.ConfigAccessorFunc( 'TaskLinePrefix', CFG_TASK_LINE_PREFIX, 'TaskLineSuffix', CFG_TASK_LINE_SUFFIX )

		## Category Output Line Configuration
		self.ConfigAccessorFunc( 'CategoryLineShowDesc', CFG_CATEGORY_LINE_SHOW_DESC, 'CategoryLinePrefix', CFG_CATEGORY_LINE_PREFIX, 'CategoryLineSuffix', CFG_CATEGORY_LINE_SUFFIX, 'CategoryDescPrefix', CFG_CATEGORY_DESC_PREFIX, 'CategoryDescSuffix', CFG_CATEGORY_DESC_SUFFIX )

		## Category Related
		self.ConfigAccessorFunc( 'DisplayPersonalStory', CFG_DISPLAY_PERSONAL_STORY , 'DisplaySortMethod', CFG_CATEGORY_DISPLAY_SORT_METHOD, 'DisplayErrors', CFG_DISPLAY_CATEGORY_ERRORS, 'DisplayHelp', CFG_DISPLAY_CATEGORY_HELP, 'DisplayTasks', CFG_DISPLAY_CATEGORY_TASKS, 'DisplayNotes', CFG_DISPLAY_CATEGORY_NOTES )

		## For Function / Class Arguments Callback Settings
		self.ConfigAccessorFunc( 'ObjectOpeningDelimiter', CFG_OBJECT_ARGUMENT_OPENING_DELIMITER, 'ObjectEndingDelimiter', CFG_OBJECT_ARGUMENT_ENDING_DELIMITER, 'ObjectArgSplitDelimiter', CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER, 'ObjectArgDefaultSplitDelimiter', CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER, 'ObjectArgJoinString', CFG_OBJECT_ARGUMENT_JOIN_DELIMITER, 'ObjectArgJoinPrefix', CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX, 'ObjectArgJoinSuffix', CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX )

		## Replace 'function' or 'def' with something shorter such as 'f' - Only enabled if Search is populated...
		self.ConfigAccessorFunc( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH, 'FuncNameDefinitionReplace', CFG_FUNC_NAME_REPLACE )

		## Whether the arguments in objects and functions should be altered to include the data-type, only show the data-type or only show the arg as it was.
		self.ConfigAccessorFunc( 'DisplayArgDataTypes', CFG_DISPLAY_ARG_DATA_TYPE, 'DisplayArgDataTypesSpacer', CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS, 'DisplayArgDataTypesPrefix', CFG_DISPLAY_ARG_DATA_TYPE_PREFIX, 'DisplayArgDataTypesSuffix', CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX )


		##
		## Call the Individual Code-Mapper Callback
		##
		self.OnSetupAccessorFuncs( )


	##
	##
	##
	def OnSetupAccessorFuncs( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupAccessorFuncs' )



	##
	## Setup your configuration here...
	##
	def OnSetupConfigDefaults( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnSetupConfigDefaults' )


		## AccessorFunc - Defaults
		## TODO: Replace all values with CONST_DEFAULT_XXX or DEFAULT_XXX style vars so the data is consistent and not repeated throughout..

		## Defaults for Language, file-data, etc....
		self.SetAccessorFuncValues( 'InLanguage', False, 'ActiveLanguage', CFG_PRIMARY_ACTIVE_LANGUAGE, 'FileData', CONST_DATA_NONE, 'FileLines', 0 )

		## Class tracking... Soon to be a stack..
		self.SetAccessorFuncValues( 'ActiveClass', '', 'ActiveClassDepth', 0, 'ClassStack', [ ] )

		## Are we in a comment block - note, this can't be in the line by line scope as blocks can extend beyond a single line...
		self.SetAccessorFuncValues( 'InComment', False, 'InCommentBlock', False, 'CommentBlockStartLine', -1, 'CommentBlockEndLine', -1, 'CommentBlockIndexStart', -1, 'CommentBlockIndexEnd', -1 )
		self.SetAccessorFuncValues( 'Depth', 0, 'LastDepth', 0, 'IndentLength', 0, 'Realm', REALM_UNKNOWN )


		## Setup Default Mapper Configuration!

		## Desired Debug Mode - Set this to the name / tag to print only those, or True to print all debugging statements.. and the Language associated with the extension..
		self.SetCfgAccessorFuncValues( 'DebugMode', CFG_DEBUG_MODE, 'Language', CFG_PRIMARY_ACTIVE_LANGUAGE )

		## Simple Config to Enable or Disable the 'Mapping: <filename>.<ext> containing <line_count> lines and <entry_count> entries!
		self.SetCfgAccessorFuncValues( 'OutputFileEntriesInfo', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO, 'OutputFileEntriesInfoNewLines', CFG_DATA_OUTPUT_FILE_ENTRIES_INFO_NEW_LINES )

		## New-Line preferred for reading Files and for outputting to the Code - Map Panel
		self.SetCfgAccessorFuncValues( 'NewLine', CFG_NEW_LINE, 'FileNewLine', CFG_FILE_NEW_LINE )

		## Characters used for indentation - Can be anything from a tab-character to multiple spaces to ascii-art..
		self.SetCfgAccessorFuncValues( 'IndentChars', CFG_INDENT_CHARS, 'IndentCodeColumnMode', CFG_INDENT_CODE_COLUMN_WIDTH_MODE, 'IndentCodeColumnWidth', CFG_INDENT_CODE_COLUMN_WIDTH, 'LimitCatagoryColumnWidth', CFG_INDENT_CODE_LIMIT_CATEGORY_COLUMN_WIDTH )

		## Task / Todo Line Hack to prevent the entire file from turning green, or whichever color strings are, when a ' or " appears...
		self.SetCfgAccessorFuncValues( 'TaskLinePrefix', CFG_TASK_LINE_PREFIX, 'TaskLineSuffix', CFG_TASK_LINE_SUFFIX )

		## Category Line Configuration - Show Description, and What to add at the end of the line ( if showing description, definitely use comment and "' to prevent highlighter mixing things up )
		self.SetCfgAccessorFuncValues( 'CategoryLineShowDesc', CFG_CATEGORY_LINE_SHOW_DESC, 'CategoryLinePrefix', CFG_CATEGORY_LINE_PREFIX, 'CategoryLineSuffix', CFG_CATEGORY_LINE_SUFFIX, 'CategoryDescPrefix', CFG_CATEGORY_DESC_PREFIX, 'CategoryDescSuffix', CFG_CATEGORY_DESC_SUFFIX )

		## Category Toggles
		self.SetCfgAccessorFuncValues( 'DisplayPersonalStory', CFG_DISPLAY_PERSONAL_STORY, 'DisplaySortMethod', CFG_CATEGORY_DISPLAY_SORT_METHOD, 'DisplayErrors', CFG_DISPLAY_CATEGORY_ERRORS, 'DisplayHelp', CFG_DISPLAY_CATEGORY_HELP, 'DisplayTasks', CFG_DISPLAY_CATEGORY_TASKS, 'DisplayNotes', CFG_DISPLAY_CATEGORY_NOTES )

		## For Function / Class Arguments Callback Settings
		self.SetCfgAccessorFuncValues( 'ObjectOpeningDelimiter', CFG_OBJECT_ARGUMENT_OPENING_DELIMITER, 'ObjectEndingDelimiter', CFG_OBJECT_ARGUMENT_ENDING_DELIMITER, 'ObjectArgSplitDelimiter', CFG_OBJECT_ARGUMENT_SPLIT_DELIMITER, 'ObjectArgDefaultSplitDelimiter', CFG_OBJECT_ARGUMENT_DEFAULT_SPLIT_DELIMITER, 'ObjectArgJoinString', CFG_OBJECT_ARGUMENT_JOIN_DELIMITER, 'ObjectArgJoinPrefix', CFG_CLASSFUNC_ARG_REASSEMBLE_PREFIX, 'ObjectArgJoinSuffix', CFG_CLASSFUNC_ARG_REASSEMBLE_SUFFIX )

		## Replace 'function' or 'def' with something shorter such as 'f' - Only enabled if Search is populated...
		self.SetCfgAccessorFuncValues( 'FuncNameDefinitionSearch', CFG_FUNC_NAME_SEARCH, 'FuncNameDefinitionReplace', CFG_FUNC_NAME_REPLACE )

		## Alter Function / Class Args to display Data-Types, Args or Both and set the character display data...
		self.SetCfgAccessorFuncValues( 'DisplayArgDataTypes', CFG_DISPLAY_ARG_DATA_TYPE, 'DisplayArgDataTypesSpacer', CFG_DISPLAY_ARG_DATA_TYPE_SPACER_CHARS, 'DisplayArgDataTypesPrefix', CFG_DISPLAY_ARG_DATA_TYPE_PREFIX, 'DisplayArgDataTypesSuffix', CFG_DISPLAY_ARG_DATA_TYPE_SUFFIX )


		##
		## Call the Individual Code-Mapper Callback
		##
		self.OnSetupConfig( )


	##
	## Internal Handler / Hook Caller
	##
	def Setup( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'Setup' )

		self.CallFunctionHooks( 'Setup' )




	##
	## Internal Handler / Hook Caller
	##
	def Reset( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'Reset' )

		##
		## self.CallFunctionHooks( 'Reset' )
		self.PreOnReset( )
		self.OnReset( )
		self.PostOnReset( )


	##
	## Internal Processor
	##
	def OnReset( self ):
		## This is what is returned after everything has been processed..
		## self.output = ''

		## Entries counter
		self.entries = 0

		#-- Syntax Rules
		## self.data_syntax = [ ]
		self.output_order = [ ]

		## These are the categories we add things to...
		## self.categories = { }
		## self.category_types = { }
		self.data_hidden_categories = { }

		##
		## self.AccessorFuncHelpersDefined = { }


	##
	## Internal Helper - Initializes values to the AccessorFunctions created for this segment..
	##
	def OnInitInternalConfigAccessors( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnInitInternalConfigAccessors' )


	##
	## Internal Helper - Initializes values to the AccessorFunctions created for this segment..
	##
	def OnInitInternalAccessors( self ):
		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( 'OnInitInternalAccessors' )

		## Set the values...
		## self.SetActiveObjectCallback( )
		## self.SetActiveObjectXCodeMapper( self )


	##
	## Returns DATA_NONE, DATA_OUT_OF_RANGE, or code on a specific line..
	##
	def GetFileDataByLine( self, _line ):
		## Adjust the requested line by offsetting it -1 because line 1 is 0...
		_line = _line - 1

		## Return the file-data... DATA_NONE is the default response..
		_lines = self.GetFileData( )

		## Return the number of lines in the file... 0 is the default response..
		_count = self.GetFileLines( )

		## If data doesn't exist..
		if ( _lines == CONST_DATA_NONE or _count == 0 ):
			return CONST_DATA_NONE

		## If we're out of range..
		if ( _line > _count or _line < 0 ):
			return CONST_DATA_OUT_OF_RANGE

		## Fetch the data...
		_data = _lines[ _line ]

		## Return it...
		return _data


	##
	## Add a value to the category entry counter for the input category and value - value is defaulted to 1 so it can be called without a number with our add entry function to incrememnt the counter...
	##
	def AddCategoryEntryCount( self, _category, _value = 1 ):
		## Make sure the value being used is numerical, otherwise don't set it..
		if ( type( _value ) == int ):
			## Add our current _value together with our Getter value ( which will return 0 if none is set ) making this valid all the time..
			self.SetCategoryEntryCount( _category, _value + self.GetCategoryEntryCount( _category ) )


	##
	## Forces the value of the category count to be set if the input value is numeric. This controls all of the setting functionality of all helpers.
	##
	def SetCategoryEntryCount( self, _category, _value = 0 ):
		## If an entry wasn't made / doesn't exist, then set the value as 0...
		if ( self.category_count.get( str( _category ) ) == None ):
			self.category_count[ str( _category ) ] = 0

		## Make sure the value being used is numerical, otherwise don't set it..
		if ( type( _value ) == int ):
			self.category_count[ str( _category ) ] = _value


	##
	## Returns either 0 or the actual numeric value of the counter ( forced numeric by the setter, or forced to be 0 if a value was never set )
	##
	def GetCategoryEntryCount( self, _category ):
		## If an entry wasn't made / doesn't exist, then return 0 to ensure a number always returns...
		if ( self.category_count.get( str( _category ) ) == None ):
			return 0

		return ( self.category_count.get( str( _category ) ) )


	##
	## Returns True if the category has entries, False if it doesn't - useful for determining first entry, etc...
	##
	def HasCategoryEntries( self, _category ):
		return ( self.GetCategoryEntryCount( _category ) > 0 )


	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddSyntax( self, _category, _search_mode, *_patterns ):
		## Callback ForEach Arg...
		def Callback( self, _i, _pattern ):
			## Depending on whether the data-set is for comments, or not... add them to a different list...
			if ( _category == CATEGORY_COMMENT or _category == CATEGORY_COMMENT_BLOCK_BEGIN or _category == CATEGORY_COMMENT_BLOCK_FINISH ):
				## Comments are added to our comments list so we can process them in advance..
				self.data_syntax_comments.append( ( _category, _search_mode, _pattern ) )
			else:
				## Add the Syntax entry to our dataset.. Non-comment rules are added elsewhere...
				self.data_syntax.append( ( _category, _search_mode, _pattern ) )

			## Append the pattern to the string so we only output the debug message once per call instead of once per pattern added...
			return self.GetJoinedDelimitedString( _i, self.GetCfgObjectArgJoinPrefix( ), _pattern, '\'', '\'' ) ## return Acecool.logic.ternary( ( _i > 0 ), ', ', '' ) + '\'' + _pattern + '\''

		## Call our Callback Processor for VarArgs and return the data..
		_output = self.ProcessVarArgs( _patterns, Callback, 'AddSyntax', 'Category {1} [ id \'{2}\' ]; Patterns: {0}', self.GetCategoryName( _category ), str( _category ) )



	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddSyntaxRule( self, *_patterns ):
		pass


	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddHideCategoryEntry( self, *_categories ):
		## Callback ForEach Arg...
		def Callback( self, _i, _category ):
			## Add it to our table so a check can be used to determine whether or not the category is hidden ( exists in this table ) or not ( doesn't exist )...
			self.data_hidden_categories[ str( _category ) ] = True

			## Append the pattern to the string so we only output the debug message once per call instead of once per pattern added...
			return self.GetJoinedDelimitedString( _i, self.GetCfgObjectArgJoinPrefix( ), self.GetCategoryName( _category ) ) ## return Acecool.logic.ternary( ( _i > 0 ), ', ', '' ) + self.GetCategoryName( _category ) # + '[ id \'' + str( _category ) + '\' ]'

		## Call our Callback Processor for VarArgs and return the data..
		_output = self.ProcessVarArgs( _categories, Callback, 'HideCategory', 'Hiding: {0}' )


	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddCategoryOrderEntry( self, *_categories ):
		## Callback ForEach Arg...
		def Callback( self, _i, _category ):
			## Add the category entry to the output_order table so the back-end will know which order to output the data..
			self.output_order.append( ( _category, str( _category ), self.GetCategoryName( _category ), self.GetCategoryDesc( _category ) ) )

			## Append the pattern to the string so we only output the debug message once per call instead of once per pattern added...
			return self.GetJoinedDelimitedString( _i, self.GetCfgObjectArgJoinPrefix( ), self.GetCategoryName( _category ) ) ## return Acecool.logic.ternary( ( _i > 0 ), ', ', '' ) + self.GetCategoryName( _category ) # + '[ id \'' + str( _category ) + '\' ]'

		## Call our Callback Processor for VarArgs and return the data..
		_output = self.ProcessVarArgs( _categories, Callback, 'AddCategoryOrderEntry', 'Adding, in this order: {0}' )


	##
	## Add Syntax RuleSet ( _category is the type of data to retrieve such as function or comment.. mode is how we are to search and pattern is the text used to search)
	##
	def AddArgDataTypeEntry( self, _category_type, _datatype, *_args ):
		## Helpers which can be passed through Callback as it is generated in this scope..
		_type_str = str( _category_type )

		## As this only needs to happen ONCE because the Category Type doesn't change , I may as well do it out here to save the cost of running the logic every time Callback is called.. - Create the nested category if it doesn't exist.. This element is to ensure the right args are applied to the right object / function...
		if ( self.data_arg_conversion.get( _type_str, None ) == None ): self.data_arg_conversion[ _type_str ] = { }

		## Callback ForEach Arg...
		def Callback( self, _i, _arg ):
			## self.data_arg_conversion.append( ( _category_type, _id_str, _name ) )
			self.data_arg_conversion[ _type_str ][ _arg ] = _datatype

			## Append the pattern to the string so we only output the debug message once per call instead of once per pattern added...
			return self.GetJoinedDelimitedString( _i, self.GetCfgObjectArgJoinPrefix( ), _arg, '\'', '\'' ) ## return Acecool.logic.ternary( ( _i > 0 ), ', ', '' ) + '\'' + _arg + '\''

		## Call our Callback Processor for VarArgs and return the data..
		_output = self.ProcessVarArgs( _args, Callback, 'AddArgDataTypeEntry', 'Added Arg Conversion( s ) from: [ {0} ] > To > [ {1} ] in Category: {2}', str( _datatype ), self.GetCategoryName( _category_type ) )


	##
	## Adds a Typo / Fix Entry for commonly messed up data to look for in code...
	##
	def AddTypoFixEntry( self, _pattern, _error, _solution ):
		## _entry = {
		## 	'pattern':	_pattern,
		## 	'error':	_error,
		## 	'solution':	_solution
		## }

		## Entry...
		_entry = {
			_pattern,
			_error,
			_solution
		}

		## Add it..
		self.typo_list.append( _entry )


	##
	## Add Helpful information to the bottom of the XCodeMapper Output ( IF the user wants it ) such as contact information to submit bugs, ask for help, report issues, etc... Tips and tricks, etc...
	##
	def AddHelp( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_HELP, _line_number, _text, 0, 0, '' )


	##
	## Allows you to add Syntax Error Checking for your code and add it to a uniform category...
	##
	def AddError( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_ERROR, _line_number, _text, 0, 0, '' )


	##
	## Adds an entry to the Errors Example Category - this is to show the user how something should look in case they're using XCodeMapper as an educational utility...
	##
	def AddErrorExample( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_ERROR_EXAMPLE, _line_number, _text, 0, 0, '' )


	##
	## Allows you to add Warning Checking for your code and add it to a uniform category...
	##
	def AddWarning( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_WARNING, _line_number, _text, 0, 0, '' )


	##
	## Allows you to add a feature entry - to let the user know what features the current mapper is using......
	##
	def AddFeature( self, _text, _line_number = -1 ):
		self.AddEntry( CATEGORY_MAPPER_FEATURES, _line_number, _text, 0, 0, '' )


	##
	## Helper - Adds a blank-line to the Category ID of your choosing. Also hides line-number hotlink!
	##
	def AddBlankEntry( self, _category, _count = 1 ):
		## Allow multiple blank entries to be added at any given time...
		for _i in range( _count ):
			self.AddEntry( _category, '', self.GetCfgEntryBlank( ), 0, 0, '' )


	##
	## Helper - Aides in avoiding Stack Overflow by simplifying the data needed to add an entry so we have a way to detect if we're adding something from within the PreOnAddEntry Callback, etc...
	##
	def AddSimpleEntry( self, _category, _line_number, _code, _depth, _parent = None ):
		## Call AddEntry without setting the Pattern or the Mode as they're not used in the actual data that is set... This prevents a stack-overflow by preventing a repeat when you look at category and pattern...
		self.AddEntry( _category, _line_number, _code, _depth, None, None, _parent )


	##
	## Helper - Converts _arg and DataType to a string based on supplied configuration...
	##
	def GetDataTypeArgString( self, _arg, _category ):
		## Grab the configuration Data..
		_config = self.GetCfgDisplayArgDataTypes( )

		## If the config is set to show only the arg, then don't go any further..
		if ( _config == ENUM_DISPLAY_DATA_TYPE_DEFAULT ):
			return _arg
		else:
			## Grab the Category Type
			_type = str( self.GetCategoryType( _category ) )

			## See if we can read the list...
			_map = self.data_arg_conversion.get( _type, None )

			## If the category isn't set, then just return the argument as we've already know we can't continue as the list isn't initialized..
			if ( _map == None ):
				return _arg

			## See if the arg exists in the list... If it does, it has a translation to Data-Type..
			_datatype = _map.get( _arg, None )

			## If it doesn't exist, we can't go any further.. Return the submitted arg..
			if ( _datatype == None ):
				return _arg

			## Grab the Data-Type Prefix, Suffix, and Spacer
			_prefix = self.GetCfgDisplayArgDataTypesPrefix( )
			_suffix = self.GetCfgDisplayArgDataTypesSuffix( )
			_spacer = self.GetCfgDisplayArgDataTypesSpacer( )

			## If We are set to show BOTH, then we need to load the spacer between the Data-Type and the Arg
			if ( _config == ENUM_DISPLAY_DATA_TYPE_BOTH ):
				_arg = _prefix + _datatype + _suffix + _spacer + _arg
			elif ( _config == ENUM_DISPLAY_DATA_TYPE_ONLY ):
				_arg = _prefix + _datatype + _suffix

		return _arg


	##
	## Helper - Designed to reduce repitition... Used for OnFunction/ClassArgument Callbacks..
	##
	def ProcessEntryArgs( self, _category, _line_number, _code, _depth, _search_mode, _pattern, _func_name ):
		##
		if ( Acecool.string.indexOfExists( self.GetCfgObjectOpeningDelimiter( ), _code, True ) ):
			##
			## Fix: If multiple ( / )s are found, process each char using the old method - The exact scenario eludes me, but it was where two separate paren-sets where the first one opened and closed before the second one leading to the first paren from the first set being used and the last paren of the last set being used..
			##


			## Extract everything before the first (
			_pre_paren = Acecool.string.Strip( _code, Acecool.string.find( _code, self.GetCfgObjectOpeningDelimiter( ) ) + 1, len( _code ) )

			## Strip everything except including and after the last )
			_end_paren = Acecool.string.Strip( _code, 0, Acecool.string.rfind( self.GetCfgObjectEndingDelimiter( ), _code ) - 1 )

			## Grab everything between the ( )s
			_args = Acecool.string.SubStr( _code, len( _pre_paren ), len( _code ) - len( _end_paren ) ).strip( )

			##
			_args_raw = _args

			## Make sure we have args to process....
			if ( len( _args ) > 0 and _args != '' ):
				## Split the args...
				_args_raw = _args.split( self.GetCfgObjectArgSplitDelimiter( ) )

				## Counter Shifter - if '' is returned, then we want to remove it so shift the next into its place...
				_i_shift = 0

				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( 'ProcessEntryArgs', '\nTotal Args: ' + str( len( _args_raw ) ) + ' - args: [ "' + _args + '" ] -\tcode: "' + _code + '":' + str( _line_number ) )

				## Process all of the arguments - run the callback on each to allow for modification...
				for _i, _arg, in enumerate( _args_raw ):
					## Make sure the arg is without spaces before and after...
					_arg = _arg.strip( )

					## _data = self.OnFunctionArgument( _arg, _category, _line_number, _code, _depth, _search_mode, _pattern )

					## Run the callback - OnFunction/ClassArgument and determine whether or not the arg is to be modified!
					_data = getattr( self, _func_name )( _arg, _category, _line_number, _code, _depth, _search_mode, _pattern )

					## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
					_debug_a = 'arg: [ "' + str( _arg ) + '" ]'
					_debug_b = '> into > [ "' + str( _data ) + '" ]'
					self.print( 'ProcessEntryArgs', Acecool.string.FormatColumn( 25, _debug_a, len( _debug_b ) + 5, _debug_b ) )

					## Helper
					_overwrite = ( _data == '' or _data == None or _data == True or _data == False )

					## If our callback modifies the arg, then it'll return a string. If None or '' is returned, do nothing...
					if ( _data ):
						_args_raw[ _i + _i_shift ] = Acecool.logic.ternary( ( _data == None or _data == '' ), _arg, _data )


					## As the above takes precedence - we only modify with DataType + Arg IFF the above didn't modify anything...
					## Task: Modify the above code so everything gets put into GetDataTypeArgString - possibly, or a different function to slim this function down a little bit...
					if ( not _data or _data == _arg ):
						##
						self.print( 'ProcessEntryArgs', '_data was not used, now we look at the other option..: ' + _arg )

						## Set the Arg as either itself, or as the Data-Type only or combination with arg depending on settings...
						_args_raw[ _i + _i_shift ] = self.GetDataTypeArgString( _arg, _category )
			else:
				## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
				self.print( 'ProcessEntryArgs', '\nTotal Args: 0 -\tcode: "' + _code + '":' + str( _line_number ) )


			## Re-Assemble the code-line using my standard airy format...
			_code = _pre_paren + self.GetCfgObjectArgJoinPrefix( ) + self.GetCfgObjectArgJoinString( ).join( _args_raw ) + self.GetCfgObjectArgJoinSuffix( ) + _end_paren

		## Return the updated line...
		return _code


	##
	## Helper - Returns True / False depending on whether or not the _category is hidden through a self.AddHideCategoryEntry( _category ) call!
	##
	def IsCategoryHidden( self, _category ):
		##
		_data = self.data_hidden_categories.get( str( _category ), 'ENTRY_NOT_EXISTS' )

		##
		if ( _data == 'ENTRY_NOT_EXISTS' ):
			return False

		return True



	##
	## Internal - Gets the original depth and the new depth and provides the difference... This is so lines which has multiple tabs in the middle can be properly re-tabbed when tabs are subtracted from the beginning..
	##
	##d GetDeltaDepth( self, _category, _line_number, _code, _depth, _search_mode, _pattern ):
	def GetDeltaDepth( self, _depth, _calc_depth ):
		## Grab the calculated depth... If I choose to use all args
		## _calc_depth = self.OnCalcDepth( _category, _line_number, _code, _depth, _search_mode, _pattern )

		## If we add indentation, we return negative value to subtract that depth from the center.. If we subtract depth, we have to return positive to add that depth back to the center...
		## so _depth = 3, _calc = 2 == subtract 1 from start meaning add 1 to later.. positive ... _depth = 3, _calc = 4 meaning indent 1 more meaning subtract from center... 1
		## _depth_delta = _depth - _calc_depth

		## Return the difference...
		return _depth - _calc_depth


	##
	## Helper Function used to process VarArgs...
	##
	def ProcessVarArgs( self, _args, _callback, _print_category, _print_text, *_text_args ):
		## Set up out output / return value to a blank-string...
		_output = ''

		## Make sure we have something to process
		if ( _args != None and len( _args ) > 0 ):
			## Process the data...
			for _i, _arg in enumerate( _args ):
				_output = _output + _callback( self, _i, _arg )

		## Convert the _text_args to print-data... _output is always {0}
		_print = [ ]
		_print.append( str( _output ) )
		if ( len( _text_args ) > 0 ):
			for _, _value in enumerate( _text_args ):
				_print.append( str( _value ) )

		## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
		self.print( _print_category, ( _print_text.format( *_print ) ) )

		## Always return something
		return _output


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessSimpleVarArgs( self, _callback, *_args ):
		## Define the amount of iterations required...
		_varargs = len( _args )

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( isfunction( _callback ) and _args != None and _varargs > 0 ):

			## Loop through the varargs...
			for _index in range( _varargs ):
				## Grab the key / value for our index..
				_key = _args[ _index ]

				## Execute the callback with the key / value...
				_callback( self, _key )
		else:
			## Return false if we can't do anything..
			return False

		## Return true when done...
		return True


	##
	## Similar to ProcessVarArgs without text... Simple processor of keyed varargs...
	##
	def ProcessKeyVarArgs( self, _callback, *_args ):
		## Define the amount of iterations required...
		_vararg_pairs = math.floor( len( _args ) / 2 )

		## If callback is provided and arguments are provided and we have a valid number of arguments...
		if ( isfunction( _callback ) and _args != None and _vararg_pairs > 0 ):

			## Loop through the varargs...
			for _i in range( _vararg_pairs ):
				## Grab the appropriate index for the first element, ie the key...
				_index = ( _i * 2 )

				## Grab the key / value for our index..
				_key = _args[ _index ]
				_value = _args[ _index + 1 ]

				## Execute the callback with the key / value...
				_callback( self, _key, _value )
		else:
			## Return false if we can't do anything..
			return False

		## Return true when done...
		return True


	##
	## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	##
	def AccessorFuncEx( self, _name_prefix, *_args ):
		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name, _default ):
			_name_get		= 'self.Get' + _name_prefix + _name + '( _default, _skip_defaults )'
			_name_str		= 'self.Get' + _name_prefix + _name + 'ToString( _default )'
			_name_len		= 'self.Get' + _name_prefix + _name + 'Len( _default )'
			_name_lenstr	= 'self.Get' + _name_prefix + _name + 'LenToString( _default )'
			_name_set		= 'self.Set' + _name_prefix + _name + '( _value )'
			_name_reset		= 'self.Reset' + _name_prefix + _name + '( _default )'
			_name_isset		= 'self.Is' + _name_prefix + _name + 'Set( _true_on_default )'

			## Status Report for Debugging purposes.. If an error occurs, typically the error is in the vicinity of the last one of these to report in..
			## self.print( 'AccessorFuncEx', 'Creating functions: self.Get' + _name_prefix + _name + '( _default ) / self.Set' + _name_prefix + _name + '( _value ) / self.Get' + _name_prefix + _name + 'ToString( ) / self.Reset' + _name_prefix + _name + '( )' )
			self.print( 'AccessorFuncEx', 'Creating functions: ' + _name_get + ' / ' + _name_str + ' / ' + _name_len + ' / ' + _name_lenstr + ' / ' + _name_set + ' / ' + _name_reset + ' / ' + _name_isset )

			## Create them...
			return Acecool.util.AccessorFunc( self, _name_prefix + _name, _default, True, self.GetCfgDebugMode( ) )

		## Execute...
		self.ProcessKeyVarArgs( Callback, *_args )


	##
	## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	##
	def SetAccessorFuncValuesEx( self, _name_prefix, *_args ):
		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name, _value ):
			## Debugging..
			self.print( 'SetAccessorFuncValuesEx', 'Executing: ' + 'Set' + _name_prefix + _name + '( "' + str( _value ) + '" )' )

			## Execute the Set function for the AccessorFunc Names provided with their associated values...
			return getattr( self, 'Set' + _name_prefix + _name )( _value )

		## Execute...
		self.ProcessKeyVarArgs( Callback, *_args )


	##
	## Helper - Extra Functionality for setting all AccessorFunc Names provided to the Values provided..
	##
	def ResetAccessorFuncValuesEx( self, _name_prefix, *_args ):
		## Callback - Called for each _key as function name and _value pairing - used to run functions named, provided, with the input provided - single-arg mainly to set AccessorFunc values to their assigned names..
		def Callback( self, _name ):
			## Debugging..
			self.print( 'SetAccessorFuncValuesEx', 'Executing: ' + 'Reset' + _name_prefix + _name + '( )' )

			## Execute the Set function for the AccessorFunc Names provided with their associated values...
			return getattr( self, 'Reset' + _name_prefix + _name )( )

		## Execute...
		self.ProcessSimpleVarArgs( Callback, *_args )


	##
	## Create AccessorFuncs Get/Set<Name> + helpers
	##
	def AccessorFunc( self, _name, _default = None, *_args ):
		return self.AccessorFuncEx( '', _name, _default, *_args )


	##
	## self.AccessorFunc( 'Bleh', False ) == self.GetCfgBleh( _default_override ) / __.SetCfgBleh( _value ) / __.GetCfgBlehToString( ) / __.ResetCfgBleh( )
	##
	def ConfigAccessorFunc( self, _name, _default = None, *_args ):
		return self.AccessorFuncEx( 'Cfg', _name, _default, *_args )


	##
	## Sets all AccessorFunc Names provided to the values provided..
	##
	def SetAccessorFuncValues( self, _name, _value, *_args ):
		return self.SetAccessorFuncValuesEx( '', _name, _value, *_args )


	##
	## Sets all Config AccessorFunc Names provided to the values provided..
	##
	def SetCfgAccessorFuncValues( self, _name, _value, *_args ):
		return self.SetAccessorFuncValuesEx( 'Cfg', _name, _value, *_args )


	##
	## Resets all AccessorFuncs..
	##
	def ResetAccessorFuncValues( self, _name, *_args ):
		return self.ResetAccessorFuncValuesEx( '', _name, *_args )


	##
	## Resets all Config AccessorFuncs..
	##
	def ResetCfgAccessorFuncValues( self, _name, *_args ):
		return self.ResetAccessorFuncValuesEx( 'Cfg', _name, *_args )






##
## Deprecated code - Look at how much crap was replaced with a single line of code....
##

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'JSON'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/TypeScript/TypeScript.tmLanguage'
## else:
## map_syntax	= 'Packages/Python/Python.tmLanguage'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'Python'


## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.sublime-syntax'

## ## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/Python/Python.sublime-syntax'
## else:
## 	map_syntax	= 'Packages/Python/Python.sublime-syntax'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'TypeScript'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.tmLanguage'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/TypeScript/TypeScript.tmLanguage'
## else:
## 	map_syntax	= 'Packages/Python/Python.tmLanguage'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'Batch File'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.sublime-syntax'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/Batch File/Batch File.sublime-syntax'
## else:
## 	map_syntax	= 'Packages/Python/Python.tmLanguage'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'Lua'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.tmLanguage'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/GMod Lua/Lua.tmLanguage'
## elif ( ( 'Lua' in installed ) or ( 'Lua' not in ignored ) ):
## 	map_syntax	= 'Packages/Lua/Lua.sublime-syntax'
## else:
##     map_syntax	= 'Packages/Python/Python.tmLanguage'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'Markdown'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.sublime-syntax'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
##     ## map_syntax	= 'Packages/Markdown/MultiMarkdown.sublime-syntax'
##     map_syntax	= 'Packages/Markdown/Markdown.sublime-syntax'
## else: ## TODO: elif file exists...
## 	map_syntax	= 'Packages/User/CodeMap/custom_languages/md.sublime-syntax'
## else:
	## map_syntax	= 'Packages/Python/Python.sublime-syntax'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'PHP Source'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/PHP/PHP.sublime-syntax'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.sublime-syntax'

## Note: PHP Highlighting has trouble but JavaScript looks decent...
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/PHP/PHP Source.sublime-syntax'
## elif ( ( 'JavaScript' in installed ) or ( 'JavaScript' not in ignored ) ):
## 	map_syntax	= 'Packages/JavaScript/JavaScript.sublime-syntax'
## else:
## 	map_syntax	= 'Packages/Python/Python.sublime-syntax'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'AutoHotkey'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.tmLanguage'

## ## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
##     map_syntax	= 'Packages/AutoHotkey/AutoHotkey.tmLanguage'
## else:
##     map_syntax	= 'Packages/Python/Python.tmLanguage'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'Batch File'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.sublime-syntax'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/Batch File/Batch File.sublime-syntax'
## else:
## 	map_syntax	= 'Packages/Python/Python.tmLanguage'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'C++'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.sublime-syntax'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/C++.sublime-syntax'
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/C++/C++.sublime-syntax'
## else:
## 	map_syntax	= 'Packages/Python/Python.tmLanguage'

## Base Language - the language this mapper maps...
## MAPPING_LANGUAGE = 'JavaScript'

## Use the Syntax file within
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/syntax_files/' + MAPPING_LANGUAGE + '.sublime-syntax'

## Set this to .tmLanguage / .sublime-syntax / etc.. files in order to enable syntax highlighting within the Code - Map Panel!
## map_syntax = 'Packages/AcecoolSourceCodeNavigator/JavaScript.sublime-syntax'
## if ( ( MAPPING_LANGUAGE in installed ) or ( MAPPING_LANGUAGE not in ignored ) ):
## 	map_syntax	= 'Packages/JavaScript/JavaScript.sublime-syntax'
## else:
## 	map_syntax	= 'Packages/Python/Python.sublime-syntax'