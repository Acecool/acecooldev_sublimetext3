##
## XCodeMapper Definitions for an extension for the Sublime Text 3 Package CodeMap by oleg-shilo - Josh 'Acecool' Moser
##


##
## Declarations for Globals, CONSTants, ENUMeration, etc..
##

## True / False alternative definitions...
true									= True
false									= False
TRUE									= True
FALSE									= False

## New-Line Declaration Constants - can use in config if you want..
CONST_NEW_LINE_WINDOWS					= '\r\n'
CONST_NEW_LINE_UNIX						= '\n'
CONST_NEW_LINE_MAC_OS_9					= '\n'

## Data returned when line-number is out of range in certain functions..
CONST_DATA_OUT_OF_RANGE					= 'OUT_OF_RANGE'

## Default used for file-data
CONST_DATA_NONE							= 'DATA_NONE'

## Define the tab jump-points - Pi uses 8, Windows uses 4, etc.. Basically, how many space-chars are used per \t tab char...
CONST_TAB_WIDTH							= 4


##
## Imports
##

## Import Acecool's Library of useful functions and globals / constants...
## Note: The above are needed for the Library so importing above creates either a loop and prevents it from seeing or something else.... An issue...
from User.CodeMap.Acecool.AcecoolST3_Library import *


##
## Global Namespace Declarations for Globals, CONSTants, ENUMeration, etc.. Anything which needs to be shared across all of my applications in ST3...
##

## Global declarations for data-mapping...
## global MAP_SEARCH_TYPE, MAP_CATEGORIES, MAP_CATEGORIES_TYPE, MAP_CATEGORIES_DESC, MAP_REALMS, MAP_REALMS_DESC, MAP_MODES
## global CATEGORY_FUNCTION, CATEGORY_ERROR


##
## Important OverHead Information Containers
##

## Map tables...
MAP_SEARCH_TYPE							= { };
MAP_REALMS								= { };
MAP_REALMS_DESC							= { };
MAP_MODES								= { };

##
MAP_CATEGORIES							= { };
MAP_CATEGORIES_TYPE						= { };
MAP_CATEGORIES_DESC						= { };
MAP_CATEGORIES_PARENT					= { };
MAP_CATEGORIES_CHILDREN					= { };

##
MAP_LANGUAGE_VARIANTS_BASE_CLASS		= { };
MAP_LANGUAGE_VARIANTS_CLASS_NAME		= { };
MAP_LANGUAGE_VARIANTS_CLASS_DESC		= { };


##
## Internal Function Declarations
##


##
## Useful Helper - returns a number which is incremented each call.. Useful for setting up ENUMeration or Unique IDs for ENUMeration, etc..
##
ENUM_ID = -1
def ENUM( ):
	## Tell the language we're assigning ENUM_ID to the global namespace...
	global ENUM_ID

	## Increment the value...
	ENUM_ID = ENUM_ID + 1

	## Return it..
	return ENUM_ID


##
## Helper-Function to create the map all on one line...
##
def SetupSearchTypeMap( _name ):
	## Generate a new ENUM Unique ID for this category...
	_id = ENUM( )

	## print( '[ CreateSearchTypeMap ][ ID: ' + str( _id ) + '][ Name: ' + _name + ']' )

	MAP_SEARCH_TYPE[ _id ]				= _name

	return _id


##
## Helper-Function to create the map all on one line...
##
def SetupRealmMap( _name, _desc ):
	## Generate a new ENUM Unique ID for this category...
	_id = ENUM( )

	MAP_REALMS[ _id ]					= _name
	MAP_REALMS_DESC[ _id ]				= _desc

	return _id


##
## Helper-Function to create the map all on one line...
##
def SetupCategoryMap( _type, _name, _desc, _parent_id = None ):
	## Generate a new ENUM Unique ID for this category...
	_id = ENUM( )

	## Modified SetupCategoryMap to work for CategoryTypes - If _type is set to None, True or false use _id as the TYPE to ensure the other helper functions still map it..
	## Note: Types and Cats, ETC... use the same ENUMeration counter so there won't be collisions. I may alter this later to work like my AcecoolDev_Framework ENUMerator so it creates a new counter each time it is called and allows generating maps by submitting to the args - but the call can be quite long...
	_mapped_type = Acecool.logic.ternary( ( _type == None ), _id, _type )

	## print( '[ CreateCategoryMap ][ ID: ' + str( _id ) + '][ Type: ' + str( _type ) + '][ Name: ' + _name + '][ Desc: ' + _desc + ']' )

	## TODO: Convert this into a single table.... Add a map to track ids from text though...
	MAP_CATEGORIES[ _id ]				= _name
	MAP_CATEGORIES_DESC[ _id ]			= _desc
	MAP_CATEGORIES_TYPE[ _id ]			= _mapped_type
	MAP_CATEGORIES_PARENT[ _id ]		= _parent_id
	MAP_CATEGORIES_CHILDREN[ _id ]		= [ ]

	## If parent id is set, then add it as a child...
	if ( _parent_id != None ):
		## Add the current category to the children list of another...
		MAP_CATEGORIES_CHILDREN[ _parent_id ].append( _id )

	return _id


##
## Helper-Function to create the map all on one line...
##
def SetupLangVariantMap( _name, _base, _desc ):
	## Generate a new ENUM Unique ID for this category...
	_id = ENUM( )

	## TODO: Convert this into a single table...
	MAP_LANGUAGE_VARIANTS_CLASS_NAME[ _id ]	= _name
	MAP_LANGUAGE_VARIANTS_BASE_CLASS[ _id ]	= _base
	MAP_LANGUAGE_VARIANTS_CLASS_DESC[ _id ]	= _desc

	return _id


##
## Helper-Function to Grab the user-friendly output...
##
def GetLangVariantClassName( _id, _default = 'XCodeMapperBase' ):
	return Acecool.logic.ternary( ( MAP_LANGUAGE_VARIANTS_CLASS_NAME[ _id ] != None ), MAP_LANGUAGE_VARIANTS_CLASS_NAME[ _id ], _default )


##
## Helper-Function to Grab the user-friendly output...
##
def GetLangVariantClassBase( _id, _default = 'XCodeMapperBase' ):
	return Acecool.logic.ternary( ( MAP_LANGUAGE_VARIANTS_BASE_CLASS[ _id ] != None ), MAP_LANGUAGE_VARIANTS_BASE_CLASS[ _id ], _default )


##
## Helper-Function to Grab the user-friendly output...
##
def GetLangVariantClassDesc( _id, _default = 'XCodeMapperBase' ):
	return Acecool.logic.ternary( ( MAP_LANGUAGE_VARIANTS_CLASS_DESC[ _id ] != None ), MAP_LANGUAGE_VARIANTS_CLASS_DESC[ _id ], _default )


##
## Search Type Declarations
##

## Search Methods
SEARCH_TYPE_UNKNOWN						= SetupSearchTypeMap( 'Unknown' )
SEARCH_TYPE_STARTSWITH					= SetupSearchTypeMap( 'StartsWith' )
SEARCH_TYPE_ENDSWITH					= SetupSearchTypeMap( 'EndsWith' )
SEARCH_TYPE_REGEX						= SetupSearchTypeMap( 'RegEx' )
SEARCH_TYPE_CONTAINS					= SetupSearchTypeMap( 'Contains - Case-Sensitive' )
SEARCH_TYPE_CONTAINSI					= SetupSearchTypeMap( 'Contains' )


##
## Realm Declarations ( For where code is executed if Server / Client relationship exists in the code )
##

## Realms - for GLua mainly or other language which have SERVER / CLIENT distinction...
REALM_UNKNOWN							= SetupRealmMap( 'Unknown', 'Unknown or undetermined point of execution!' )
REALM_NOLOAD							= SetupRealmMap( 'NoLoad', 'Code prevented from executing on either the CLIENT or SERVER machines!' )
REALM_CLIENT							= SetupRealmMap( 'Client', 'Code executed on the CLIENT machine!' )
REALM_SERVER							= SetupRealmMap( 'Server', 'Code executed on the SERVER machine!' )
REALM_SHARED							= SetupRealmMap( 'Shared', 'Code executed on both the CLIENT and SERVER machines!' )


##
## Category Groups - This is an additive to the category base - this simply helps the script determine what to do - ie for functions: They will have a Callback set up to allow the dev to modify the arguments, for example.
## 	The type or group doesn't always need to be set, but it can help...
##
## Note: I may or may not implement this...
## 	Pros:
## 		Categories can be grouped for the special logic so instead of enforcing certain categories it'll open devs to create or use any category for anything and assign a group for specific logic they want allied to that group...
##
## 	Cons:
## 		More work, More overhead
##
## Comments are ignored, for the most part but being in this group means extra logic runs to strip the comment so if there is actual code on the line, it is revealed..
## Example: Block comments are removed so if code comes after it ends or before it started, we will see it... Or, if it is a single-line comment... if it is at the beginning
## 	then there's no use running logic on it, but if the index isn't 0 then there may be usable code on that line which we scrap out...

## Created to be used in SetupCategoryMap in the event pointing to oneself causes other issues such as a stack overflow...
CATEGORY_TYPE							= ENUM( )

## Comments
CATEGORY_TYPE_COMMENT					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Comment', 'Comment - This Category Type is meant to compile Comments and Comment-Blocks together in a like-category type while remaining the unique names and features..' )

## In Comment means a category that needs to run when a comment is detected..
CATEGORY_TYPE_IN_COMMENT				= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: In-Comment', 'Special - This allows search-terms within comments. Specically it is used for the Task-Type and Note-Type Categories so they can be extracted from comments..' )

## Ignnored from additional logic..
CATEGORY_TYPE_IGNORE					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Ignore', 'Special - This Category Type is used on elements which are to be ignored by everything... The Category becomes symbolic when this is applies to it..' )

## No idea how to categorize the data
CATEGORY_TYPE_UNKNOWN					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Unknown', 'Other - This Category Type is used as an \'Other\' type until a type can be created specifically for a new type - in short it is used as a development tool.' )

## File Import / Include / Require Category
CATEGORY_TYPE_IMPORT					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Import', 'Import - used for categories which specify file includes, requires, include?_once, require_once, import, etc...' )

## Object based code - Runs callback for argument manipulation
CATEGORY_TYPE_CLASS						= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Class / Object', 'Generic - This Category Type is specifically for Objects / Classes / Meta-Tables / etc.. Anything which can be replicated and sometimes used as libraries and / or with special features.. It is also used for Generic Objects..' )

## Function based code - Runs callback for argument based manipulation
CATEGORY_TYPE_CLASS_FUNCTION			= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Class Function', 'Similar to the Function type, this one is special in that it belongs to an object and therefore gets special treatment...' )

## Function based code - Runs callback for argument based manipulation
CATEGORY_TYPE_FUNCTION					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Function', 'Function - This Category Type is specifically used for Generic Methods / Functions / Definitions / etc..' )

## An anchor point in the code - Can be literal HTML style anchor, or an anchor used by GOTOs...
CATEGORY_TYPE_ANCHOR					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Anchor point', 'Anchors can be used for navigation, and to act as functions for a single call, recursion and iteration. They can be quite useful with the ability to adapt..' )

## As an Anchor marks a point, a GOTO goes to that point to use that anchor to emulate a function call, iteration, recursion, or non-linear passage through code.... Category Type Navigation is born... or Navigator?
CATEGORY_TYPE_NAVIGATOR					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Navigator / Navigation', 'Addition, Subtraction. Multiplication, Division. The inverse of one another, but also the complement. A Navigator / Navigation requires an Anchor.' )

## For functions being CALLED so the argument code doesn't run...
CATEGORY_TYPE_FUNCTION_CALL				= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Function Call', 'Function Call - This Category Type is used specifically for categories which track function calls, not definitions.' )

## Similar to function, except the argument manipulation comes AFTER expanding the accessorfunc into 1 or more created functions...
CATEGORY_TYPE_ACCESSOR_FUNCTION			= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: AccessorFunc', 'Special - This Category Type is used for functions which create other functions - This is a special type which processes the found AccessorFuncs through a special AccessorFunc Expansion Callback!' )

## Information only
CATEGORY_TYPE_INFO						= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Information', 'Data - This Category Type is used to track data such as Errors, Warnings, Notes in some situations, etc..' )

## Information only
CATEGORY_TYPE_DEBUGGING					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Debugging, Optimization & Development', 'Development - This Category Type is used for Debugging and development-based categories!' )

## User Interface / VGUI / UI / etc... HUD...
CATEGORY_TYPE_INTERFACE					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Interface', 'UI - This Category Type is used for categories that track Interfaces such as HUDs, UI, VGUI, etc..' )

## Key-Binding Type Category
CATEGORY_TYPE_HOTKEY					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: HotKey', 'HotKey - This Category Type is for categories which track key-binds, hotkeys, etc..' )

## Any 'other' Category with interactable elements.
CATEGORY_TYPE_INTERACTABLE				= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Interactable', 'Interaction - This Category Type is for categories which track generic interactions such as Key-Binds, Buttons, Etc..' )

## Formatting as with HT Markup Language, MarkDown, WYSIWYG / BBCode, Etc...
CATEGORY_TYPE_FORMATTING				= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Formatting', 'Formatting - This Category Type is for categories which handle text formatting - such as BBCode, MarkDown, HT Mark Up, etc..' )

## Formatting as with HT Markup Language, MarkDown, WYSIWYG / BBCode, Etc...
CATEGORY_TYPE_COMMAND					= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Command', 'Command - This Category Type is for categories which revolve around user commands, or other commands - for example Batch Files, Win + R commands, etc...' )

## Tracker type for indentations...
CATEGORY_TYPE_RULE						= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Rule', 'Rule - This Category Type is specifically for categories defined as rules which are used as logic gates..' )

## Tracker type for indentations...
CATEGORY_TYPE_RULE_INDENT				= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Rule for Indentation Occurrences', 'Rule - This Category Type is used to designate, by means of using which syntaxes use it, when an indentation occurs..' )

## Tracker type for unindenting..
CATEGORY_TYPE_RULE_UNINDENT				= SetupCategoryMap( CATEGORY_TYPE, 'Category Type: Rule for Unindentation Occurrences', 'Rule - This Category Type is used to designate, by means of using which syntaxes use it, when a detent occurs..' )




##
## Default Categories List - Note: The ENUM / ID needs to be unique and it needs to be a number. The group dictates the logic applied - such as: For AccessorFuncs, they don't get added by the standard script - instead,
## 	they are routed through a callback function which allows you to add 1 to many different functions depending on what that specific AccessorFunc does... Typically a Getter and Setter are created, other times it is
## 	easier than that and only 1 function is created - or it could expand into 10+ functions. For any type of function or class, the arguments are processed through callbacks so you can turn a function like this:
## 	function GM:PlayerDeath( _p, _w ) into function GM:PlayerDeath( <Player>, <Weapon> ) or  function GM:PlayerDeath( <Player> _p, <Weapon> _W ) in the CodeMap output panel.
##
## The category group should be the most basic form - the reason AccessorFunc isn't as group_function is because the AccessorFunc doesn't add itself as a function to the list - it adds others meaning the logic for function
## 	group would be applied to what it is adding since we don't want to modify the arguments from the AccessorFunc ( as we need to read them to see what options the dev used to create accurate entries ).
##
CATEGORY_NAME_UNKNOWN					= 'Unknown Category Name'
CATEGORY_DESC_UNKNOWN					= 'Unknown Category Description'



##
## Error Category Types - Error for syntax errors, etc... Unknown for information that can't be categorized... Warnings are for Coding standard issues, etc...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_UNKNOWN						= SetupCategoryMap( CATEGORY_TYPE_UNKNOWN,				'Unknown',										'For everything which can not be categorized, if you want to put it into a category and you don\'t want to create a new one to do so!' )



##
## Ignore Categories - Categories where, when things are added, aren't displayed .. they can be ( Unless I hard-code this rule ), but they're meant to be ignored...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_IGNORE							= SetupCategoryMap( CATEGORY_TYPE_IGNORE,				'Ignored Entries',								'Things you want to specifically prevent from being displayed in the CodeMap panel!' )


##
## XCodeMapper / Acecool Categories - These categories are specifically for Josh 'Acecool' Moser - ie "Tell a developer" type errors are output into these at the very end, for instance... Also used for internal debugging as reading syntax highlighted output in a larger output panel is sometimes preferred to the console - and with it being so bright...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_XCM_INFO						= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Info',							'XCodeMapper Internal Category' )
CATEGORY_XCM_ERROR						= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Error - Tell Acecool',			'XCodeMapper Internal Category' )
CATEGORY_XCM_DEBUG						= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Debugging',						'XCodeMapper Internal Category' )
CATEGORY_XCM_OPTIMIZATION				= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Optimization',					'XCodeMapper Internal Category' )

CATEGORY_XCM_SYNTAX_ENTRY				= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Syntax Entries',					'XCodeMapper Internal Category' )
CATEGORY_XCM_SYNTAX_RULE_ENTRY			= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Syntax Rule Entries',				'XCodeMapper Internal Category' )
CATEGORY_XCM_OUTPUT_ORDER_ENTRY			= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Category Output Order Entries',	'XCodeMapper Internal Category' )
CATEGORY_XCM_OUTPUT_ENTRY				= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Output Entry',					'XCodeMapper Internal Category' )
## CATEGORY_XCM_SYNTAX_ENTRY				= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Syntax Entry',					'XCodeMapper Internal Category' )
## CATEGORY_XCM_SYNTAX_ENTRY				= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'XCodeMapper: Syntax Entry',					'XCodeMapper Internal Category' )


## Aliases
CATEGORY_XCM_DEBUGGING = CATEGORY_XCM_DEBUG


##
## Debugging Categories - Categories meant for displaying debugging data...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_DEBUG							= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'Debugging',									'For anything relating to debugging - Debugging can slow execution down!' )
CATEGORY_OPTIMIZATION					= SetupCategoryMap( CATEGORY_TYPE_DEBUGGING,			'Optimization',									'print statements, file I/O, nested loops O( n^2+ ) but not always, etc.. can be incredibly taxing on a system..' )

## Aliases
CATEGORY_DEBUGGING = CATEGORY_DEBUG



##
## Information Category -- Declaration Categories ( Constants, ENUMeration, etc.. )
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_INFO							= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Information',									'For informational purposes..' )
CATEGORY_NETWORKING						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Networking',									'For informational purposes..' )
CATEGORY_WARNING						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Warnings List',								'Entries containing Coding Standard Issues, etc..' )
CATEGORY_ERROR							= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Errors List',									'This category is for debugging purposes - when you catch a syntax error, or something similar, add an entry to this category!' )
CATEGORY_ERROR_EXAMPLE					= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Error Correction Examples',					'This category is used to teach by way of displaying how the code should appear - used for Syntax errors, etc..!' )
CATEGORY_HELP							= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Help & Information',							'This category is to be used internally for XCodeMapper Universal Mapper to show helpful tips and contact information, etc..' )
CATEGORY_DEFINITION						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Definitions',									'Umbrella Category for entries containing CONSTants, ENUMeration, Definitions, etc..' )
CATEGORY_CONFIG							= SetupCategoryMap( CATEGORY_TYPE_INFO,					'ConFiGuration',								'For CFG_ / Configuration declarations..' )
CATEGORY_DIRECTIVE						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Directives',									'For AutoHotkey Directives - basically powerful script configuration in the form of simple commands..' )
CATEGORY_DEPRECATED						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Deprecated',									'For deprecated commands.' )
CATEGORY_CONSTANT						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'CONSTant',										'For CONSTant declarations..' )
CATEGORY_ENUMERATION					= SetupCategoryMap( CATEGORY_TYPE_INFO,					'ENUMeration',									'Fpr EMUMeration declarations..' )
CATEGORY_REGISTRY						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Registry',										'For Registry declarations..' )
CATEGORY_VARIABLE						= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Variable',										'For Variables - usage and declaration..' )

## Aliases
CATEGORY_CONFIGURATION					= CATEGORY_CONFIG




##
## Mapper specific - HELP, INFO, Features, etc...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_MAPPER_FEATURES				= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Active XCodeMapper Features',					'Specific Features the current mapper has..' )




##
## Categories which execute on lines containing comments and nothing else...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_TODO							= SetupCategoryMap( CATEGORY_TYPE_IN_COMMENT,			'Task List',									'Entries containing internal code comments / notes specifying alterations to be made, new features to integrate, and the like..' )
CATEGORY_NOTE							= SetupCategoryMap( CATEGORY_TYPE_IN_COMMENT,			'Notes',										'Entries designed as internal comments / notation..' )
CATEGORY_COMPLETED_TASK					= SetupCategoryMap( CATEGORY_TYPE_IN_COMMENT,			'Completed Tasks',								'Entries designed as internal comments / notation..' )




##
## Comment Categories
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_COMMENT						= SetupCategoryMap( CATEGORY_TYPE_COMMENT,				'Comments!',									'This is an entriless category - Comments are ignored unless they\'re TODO / Tasks based comments..' )
CATEGORY_COMMENT_BLOCK					= SetupCategoryMap( CATEGORY_TYPE_COMMENT,				'Block Comment',								'..' )
CATEGORY_COMMENT_BLOCK_BEGIN			= SetupCategoryMap( CATEGORY_TYPE_COMMENT,				'Block Comment StartPoint',						'..' )
CATEGORY_COMMENT_BLOCK_FINISH			= SetupCategoryMap( CATEGORY_TYPE_COMMENT,				'Block Comment EndPoint',						'..' )




##
## Class Categories
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_INCLUDE							= SetupCategoryMap( CATEGORY_TYPE_IMPORT,				'File Import: include',						'For Class declarations..' )
CATEGORY_INCLUDE_ONCE						= SetupCategoryMap( CATEGORY_TYPE_IMPORT,				'File Import: incluide_once',				'For Object declarations..' )
CATEGORY_REQUIRE							= SetupCategoryMap( CATEGORY_TYPE_IMPORT,				'File Import: require',						'For Class declarations..' )
CATEGORY_REQUIRE_ONCE						= SetupCategoryMap( CATEGORY_TYPE_IMPORT,				'File Import: require_once',				'For Object declarations..' )
CATEGORY_IMPORT								= SetupCategoryMap( CATEGORY_TYPE_IMPORT,				'File Includes / Imports',					'For Class declarations..' )




##
## Class Categories
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_CLASS							= SetupCategoryMap( CATEGORY_TYPE_CLASS,				'Class Headers',								'For Class declarations..' )
CATEGORY_OBJECT							= SetupCategoryMap( CATEGORY_TYPE_CLASS,				'Object',										'For Object declarations..' )
CATEGORY_METATABLE						= SetupCategoryMap( CATEGORY_TYPE_CLASS,				'MetaTable Object / Class',						'Meta-Tables are special Lua tables with additional functionality - they similar to objects as they can be cloned, can have static vars across all copies ( shared data which is identical across all ), public vars and somehwat private vars ( If you run a Trace in __index or __newindex you can prevent outside initiators from accessing certain data... )..' )




##
## Class Function Categories
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_CLASS_FUNCTION					= SetupCategoryMap( CATEGORY_TYPE_CLASS_FUNCTION,		'Class Functions',								'For Class Function declarations..' )




##
## Function Categories
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_FUNCTION						= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'function: Core',								'For function declarations..' )
CATEGORY_FUNCTION_LOCAL					= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'function: local',								'For local function declarations..' )
CATEGORY_FUNCTION_HELPER				= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'function: Helper',								'Functions comprised of calls to existing functions or a simple algorithm designed to make a task easier / shorter ( Like these category declarations )..' )
CATEGORY_FUNCTION_ACCESSOR				= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'function: AccessorFunc',									'Function Calls which create other functions - This category is SPECIAL! It uses a CALLBACK so that you can expand the AccessorFunc into the appropriate entries in XCodeMapper - ie 1 func becomes 2 or more entries..' )
CATEGORY_FUNCTION_META					= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'function: META_*',								'Function which is a child to an object / META_TABLE.' )
CATEGORY_FUNCTION_OTHER					= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'function: Other',								'For functions declared in a non-standard way such as x = function or _panel.Paint = function, etc..' )
CATEGORY_FUNCTION_INTERNAL				= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'function: Private / Internal',					'For functions which shouldn\' be called by the dev - ie only used internally within an object - similar to private but without enforcement..' )


## Aliases
CATEGORY_FUNCTION_PRIVATE				= CATEGORY_FUNCTION_INTERNAL



##
## A GOTO isn't exactly a function, and it isn't exactly not... You can set vars to act as the arg, and set a goto in the goto to return back to just after or before the goto was called to create iteration or recursion or just a function call... It is really only an anchor, but here I call it a function..
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_ANCHOR							= SetupCategoryMap( CATEGORY_TYPE_ANCHOR,				'GOTO Anchor / Function',						'A GOTO can act as a function for a single call, for recursion, they can be used for iteration and more - they\'re really anchor points, but here I title it a function because of their ability to adapt..' )
CATEGORY_GOTO							= SetupCategoryMap( CATEGORY_TYPE_NAVIGATOR,			'GOTO Anchor / Function',						'A GOTO can act as a function for a single call, for recursion, they can be used for iteration and more - they\'re really anchor points, but here I title it a function because of their ability to adapt..' )




##
## For functions being CALLED, not declared...
##
## Note: This will be removed, maybe, once I fix the issue with the Acecool.table.Args from function function so raw AND non-raw are sent through to the callback so I can check for " / 's, constants, enumeration, and the like...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_FUNCTION_CALLBACK				= SetupCategoryMap( CATEGORY_TYPE_FUNCTION,				'Callback function',							'For local functions which are executed via means of callback..' )
CATEGORY_CMD_CONSOLE					= SetupCategoryMap( CATEGORY_TYPE_FUNCTION_CALL,		'Console Command',								'Console Commands - in Lua these will be from concommand.Add function calls...' )
CATEGORY_FUNCTION_HOOK					= SetupCategoryMap( CATEGORY_TYPE_FUNCTION_CALL,		'function Hook',								'Similar to callback - this is a special function I created for hook.Add calls to be isolated..' )




##
## User / Client-Realm based Interface Categories... Can also be for other interfaces such as server text I/O, etc...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_VGUI							= SetupCategoryMap( CATEGORY_TYPE_INTERFACE,			'VGUI',											'Versatile Graphical User Interface Declarations' )
CATEGORY_INTERFACE						= SetupCategoryMap( CATEGORY_TYPE_INTERFACE,			'Interface',									'Interface Declarations' )
CATEGORY_USER_INTERFACE					= SetupCategoryMap( CATEGORY_TYPE_INTERFACE,			'UI',											'User Interface Declarations' )
CATEGORY_HUD							= SetupCategoryMap( CATEGORY_TYPE_INTERFACE,			'HUD',											'Heads Up Display Declarations' )




##
## Keys / Bindings / etc..
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_HOTKEY							= SetupCategoryMap( CATEGORY_TYPE_HOTKEY,				'Hotkey',										'User Interaction - Keys which trigger an event' )
CATEGORY_KEY_BIND						= SetupCategoryMap( CATEGORY_TYPE_HOTKEY,				'Key Bind',										'User Interaction - Keys bound to an event' )




##
## Text Formatting
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_FORMATTING_UNKNOWN				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Unknown',										'Unknown' )
CATEGORY_FORMATTING_OTHER				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Other',										'Other' )
CATEGORY_FORMATTING_EXAMPLE				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Example',										'Example' )
CATEGORY_FORMATTING_IMAGE				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Image',										'Image' )
CATEGORY_FORMATTING_LINK				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Link',											'Website URL / Link' )
CATEGORY_FORMATTING_QUOTE				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Quote',										'Quote' )
CATEGORY_FORMATTING_CODE_SNIPPET		= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Snippet',										'Code Snippet' )
CATEGORY_FORMATTING_BOLD				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Bold',											'Text formatted using BOLD.' )
CATEGORY_FORMATTING_ITALICS				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Italics',										'Text formatted using Italics.' )
CATEGORY_FORMATTING_STRIKETHROUGH		= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Strike-Through',								'Text formatted using Strike-Through.' )
CATEGORY_FORMATTING_TABLE				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Table',										'Data organized within a table.' )
CATEGORY_FORMATTING_LIST				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Itemized List',								'Standard List of Items..' )
CATEGORY_FORMATTING_LIST_DEFINITION		= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Definition List',								'List of Definitions..' )
CATEGORY_FORMATTING_LIST_BUTTONED		= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Bulleted List',								'Unordered List using buttons.' )
CATEGORY_FORMATTING_LIST_NUMBERED		= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Numbered List',								'Ordered List using numbers.' )
CATEGORY_FORMATTING_CODE				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Code',											'Code' )
CATEGORY_FORMATTING_TAG					= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'HTML / XML Tags',								'Tag commonly used with HTML, XML, and other languages.' )
CATEGORY_FORMATTING_HEADER				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Header',										'Header - H1 through H6' )
CATEGORY_FORMATTING_HEADER_1			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'H1',											'Header 1' )
CATEGORY_FORMATTING_HEADER_2			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'H2',											'Header 2' )
CATEGORY_FORMATTING_HEADER_3			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'H3',											'Header 3' )
CATEGORY_FORMATTING_HEADER_4			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'H4',											'Header 4' )
CATEGORY_FORMATTING_HEADER_5			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'H5',											'Header 5' )
CATEGORY_FORMATTING_HEADER_6			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'H6',											'Header 6' )
CATEGORY_FORMATTING_RULE				= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Rule',											'Rule' )
CATEGORY_FORMATTING_RULE_HORIZONTAL		= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Horizontal Rule',								'Horizontal Rule' )
CATEGORY_FORMATTING_RULE_VERTICAL		= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Vertical Rule',								'Vertical Rule' )
CATEGORY_FORMATTING_FOOTNOTE			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Footnote',										'Note at the bottom of the page..' )
CATEGORY_FORMATTING_LINE_BLOCK			= SetupCategoryMap( CATEGORY_TYPE_FORMATTING,			'Line Block',									'Line Block' )




##
## Command Categories ( For Batch Files, etc.. )
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)

CATEGORY_CMD_FILE						= SetupCategoryMap( CATEGORY_TYPE_COMMAND,				'Command: File',								'File based commands' )
CATEGORY_CMD_REGISTRY					= SetupCategoryMap( CATEGORY_TYPE_COMMAND,				'Command: Registry',							'Registry Based Commands' )
CATEGORY_CMD_ITERATION					= SetupCategoryMap( CATEGORY_TYPE_COMMAND,				'Command: Iteration',							'Iteration' )




##
## Special
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_REALM							= SetupCategoryMap( CATEGORY_TYPE_INFO,					'Realm of Execution',							'REALM_CLIENT / REALM_SERVER / REALM_SHARED used for languages which execute separately on the client vs the server - shared is for anything which is identical on both client and server but it does not mean they execute together..' )




##
## Code Rules -These are meanfor specific tracking actions in code... ie Indent when you see then, do ( in some cases ie.. do while may not require it depending how it is formulated so tracking the start value may be better: if, for, while, etc.. ), etc.. in Lua... { in many other languages. Un-indent when you see end in Lua or } in other languages... etc...
##

## ENUMeration of Category				Create Category Map(	CAT GROUP,						Category Name,									Category Description		)
CATEGORY_RULE_INDENT					= SetupCategoryMap( CATEGORY_TYPE_RULE,					'Code: Indent',									'Category RULE for Code... Similar to comments - it is not meant to be used as a data-containing category - it is meant to perform tracking functions... ie track value indent for depth when analyzing multiple lines of a function..' )
CATEGORY_RULE_UNINDENT					= SetupCategoryMap( CATEGORY_TYPE_RULE,					'Code: Un-Indent',								'Category RULE for Code... Similar to comments - it is not meant to be used as a data-containing category - it is meant to perform tracking functions... ie track value indent for depth when analyzing multiple lines of a function..' )




##
## Important Declarations
##

## Data-Types for Function / Class Arguments...
ENUM_DISPLAY_DATA_TYPE_DEFAULT			= ENUM( )
ENUM_DISPLAY_DATA_TYPE_BOTH				= ENUM( )
ENUM_DISPLAY_DATA_TYPE_ONLY				= ENUM( )
## ENUM_DISPLAY_DATA_TYPE_ARG						= ENUM( )



## Sorting methods
ENUM_SORT_METHOD_DEFAULT				= ENUM( )
ENUM_SORT_METHOD_LINE_NUMBER			= ENUM( )
ENUM_SORT_METHOD_LINE_NUMBER_DESC		= ENUM( )
ENUM_SORT_METHOD_ALPHABETICAL			= ENUM( )
ENUM_SORT_METHOD_ALPHABETICAL_DESC		= ENUM( )


##
## How the Code Line gets displayed with a column spacing...
##

## Default means the :1234 line number hugs the code... It's a bit bulky, this method..
ENUM_INDENT_CODE_COLUMN_MODE_DEFAULT	= ENUM( )

## The line is always CUT at the exact size defined, and then the code is affixed to the end - This is clean in most cases but you can lose data from being seen ( although it is nice if you want to keep the panel a certain size without scrollbars )
ENUM_INDENT_CODE_COLUMN_MODE_TRUNCATE	= ENUM( )

## This is the best of both worlds... It adheres to the column Truncate does, unless the length of the code would end up cutting off data... Then instead of cutting off data, the line is simply affixed at the end line default...
ENUM_INDENT_CODE_COLUMN_MODE_ORGANIZED 	= ENUM( )