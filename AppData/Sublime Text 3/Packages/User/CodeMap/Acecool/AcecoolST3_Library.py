##
## Acecool Development Library for Sublime Text 3 - Josh 'Acecool' Moser
##


# File I/O
import codecs

## Sublime Text 3 API / Library
import sublime

##
import sublime_plugin

##
import imp

## System Library
import sys

## Regular Expressions for Search and Replace...
import re

## Arrays
import array

## Math functions - This shouldn't be required for floor / ceil... etc... odd..
import math

##
import os

## Import Libraries
## import imp
## import importlib

## Hopefully there is a method around this as I'm not using recursion...
## sys.setrecursionlimit( 2143 )

## Import the definitions ( Categories, enumerators, etc... everything definition based )
from User.CodeMap.Acecool.XCodeMapper_Definitions import *


##
## Global Namespace Declarations for Globals, CONSTants, ENUMeration, etc.. Anything which needs to be shared across all of my applications in ST3...
##

## 						syntax, _ = os.path.splitext(os.path.basename(view.settings().get('syntax')))
## 'base_file': '${packages}/Default/Preferences.sublime-settings',
##                'user_file': os.path.join(sublime.packages_path(), 'User', syntax + '.sublime-settings'),

## ##
## ## Declarations for Globals, CONSTants, ENUMeration, etc..
## ##

## ## True / False alternative definitions...
## true						= True
## false						= False
## TRUE						= True
## FALSE						= False

## ## New-Line Declaration Constants - can use in config if you want..
## CONST_NEW_LINE_WINDOWS		= '\r\n'
## CONST_NEW_LINE_UNIX			= '\n'
## CONST_NEW_LINE_MAC_OS_9		= '\n'


##
## Function Declarations
##


## ##
## ## Useful Helper - returns a number which is incremented each call.. Useful for setting up ENUMeration or Unique IDs for ENUMeration, etc..
## ##
## ENUM_ID = -1
## def ENUM( ):
## 	## Tell the language we're assigning ENUM_ID to the global namespace...
## 	global ENUM_ID

## 	## Increment the value...
## 	ENUM_ID = ENUM_ID + 1

## 	## Return it..
## 	return ENUM_ID



##
## Helper Function - Returns True if the supplied value is a function...
##
def isfunction( _func ):
	## Grab the type-of the function we're looking at and convert it to a string...
	_typeof = str( type( _func ) )

	## Return true or false
	return ( _typeof == "<class 'function'>" or _typeof == "<class 'method'>" )


##
## Helper Function - Returns True if the supplied value is a string...
##
def isstring( _func ):
	## Grab the type-of the function we're looking at and convert it to a string...
	_typeof = str( type( _func ) )

	## Return true or false
	return ( _typeof == "<class 'string'>" )


##
## Acecool NameSpace..
##
class Acecool:
	__name__ = 'AcecoolST3_Library'

	## ##
	## ## Constants table to store important information as globals don't seem to pass through...
	## ##
	## class CONSTANTS:
	## 	## Alternative True / False contants
	## 	true									= True
	## 	false									= False
	## 	TRUE									= True
	## 	FALSE									= False

	## 	## New-Lines used for file-line-delimitation...
	## 	NEW_LINE_WINDOWS						= '\r\n'
	## 	NEW_LINE_UNIX							= '\n'
	## 	NEW_LINE_MAC_OS_9						= '\n'


	##
	## RunTime Environment - used for data which is thrown away after use...
	##
	class RunTime:
		##
		## For when we need an instance where all vars are set / reset..
		##
		def __init__( ):
			pass


		##
		## Helper - Generate a new ENUM id each time it is called...
		##
		ENUM_ID = -1
		def ENUM( ):
			Acecool.RunTime.ENUM_ID = Acecool.RunTime.ENUM_ID + 1

			return Acecool.RunTime.ENUM_ID


	##
	##
	##
	class file:
		pass


	##
	##
	##
	class sublime:
		##
		##
		##
		def GetPackageFolder( ):
			return 'AcecoolSourceCodeNavigator'


		##
		##
		##
		def GetDefaultSyntaxFilePath( ):
			return os.path.join( 'Packages', Acecool.sublime.GetDefaultSimpleSyntaxFilePath( ) )


		##
		##
		##
		def GetDefaultSimpleSyntaxFilePath( ):
			return os.path.join( 'Python', 'Python.sublime-syntax' )


		##
		##
		##
		def GetSyntaxFilePath( _package, _file = '' ):
			##
			print( ' >> GetSyntaxFilePath > Attempting to locate Syntax Highlighter File...' )

			##
			if ( _file == '' ):
				_file = _package

			## Base Path through AppData\Sublime Text 3\Packages\ - Unfortunately CodeMap doesn't support using the full-path.... which is why I can't use it in the return statement...
			_base_path = sublime.packages_path( )

			## Keep repetitions down to define it once up here and use them twice each below... more if additional folders are added...
			_file_tm = _file + '.tmLanguage'
			_file_sy = _file + '.sublime-syntax'

			## Table containing possible locations and file-names...
			_syntax_files = [ ]

			## Acecool/syntax_files/<FileName>.<tmLanguage/sublime-syntax>
			_syntax_files.append( os.path.join( Acecool.sublime.GetPackageFolder( ), 'syntax_files', _file_sy ) )
			_syntax_files.append( os.path.join( Acecool.sublime.GetPackageFolder( ), 'syntax_files', _file_tm ) )

			## <PackageName>/<FileName>.<tmLanguage/sublime-syntax>
			_syntax_files.append( os.path.join( _package, _file_sy ) )
			_syntax_files.append( os.path.join( _package, _file_tm ) )

			## Default...
			_syntax_files.append( Acecool.sublime.GetDefaultSimpleSyntaxFilePath( ) )

			## Process the locations above in search of the right one...
			for _path in _syntax_files:
				##
				## print( '\t\t>> Trying: ' + _path )

				##
				if ( os.path.exists( os.path.join( sublime.packages_path( ), _path ) ) ):
					##
					print( '\t\t>> Setting Syntax Highlighter File: ' + _path )

					##
					return 'Packages/' + _path


	##
	## Table Operations Library
	##
	class table:
		##
		##
		##
		def GetValue( _args, _args_count, _index, _default ):
			_arg = ''; ## _index += 1;

			if ( _default != '' or str( _default ) != 'nil' ) and _arg == '':
				_arg = _default
			if ( _args_count > _index and str( _args[ _index ] ) != 'nil' ):
				_arg = str( _args[ _index ] )

			return _arg


		##
		## Takes a Dictionary ( uses { } ) and returns a tuple ( uses [ ] ) map which is sorted - basic index values convert to string key so
		##
		## TODO: Create this...
		##
		## ## Usage:
		##
		## _dict = { "My":{ "z":True, "a":False }, "ABC":{ "z":False, "a":True } }
		##
		## ## This becomes [ 1: "ABC", 2: "My" ]
		## _tuple_map = Acecool.table.DictToTupleMap( _dict, SORT_METHOD_A_Z )
		##
		## ## So, in our loop we can use range which does 1, then 2 - it means we need a lengthier call.. Or we can use a for-each method...
		## for _i in range( len( _tuple_map ) ):
		## 	## _tuple_map for 1 is ABC so we convert it... - Returns ABC as { "z":False, "a":True } then My as { "z":True, "a":False }
		##  _dict_data = _dict[ _tuple_map[ _i ] ]
		##
		##
		def DictToTupleMap( _dict ):
			## Create our Tuple
			_tuple = [ ]

			## Go through the dictionary one at a time...
			for _key, _value in enumerate( _dict ):
				print( 'Tuple > Dict[ ' + str( _key ) + ' ] = ' + str( _value ) )



		##
		## Redesign of the code below...
		##
		def FromCodeDefinitionToComponents( _code, _args_delimeter = ',', _start_paren_char = '(', _finish_paren_char = ')' ):
			## Determine how many parens are there...
			_start_char_count = _code.count( _start_paren_char )
			_end_char_count = _code.count( _finish_paren_char )

			##
			## Notes: If function CALL then preserve args ( quotes, etc.. except white-space between args ) if definition then allow skip chars...
			##

			## If we have the same number of each...
			if ( _start_char_count == _end_char_count ):
				## If we have 1 of each then
				if ( _start_char_count == 1 ):
					## Because there is 1 of each we can break it down into component form by means of <FUNC/CLASS-COMBO> <NAME><START><ARGS><END> or <NAME> = <FUNC/CLASS-COMBO><START><ARGS><END> by simply splitting the string up...

					## Just in case we're not starting from 0...
					_begin = Acecool.string.indexOf( _start_paren_char, _text )
					_finish = Acecool.string.indexOf( _finish_paren_char, _text )


				## More than one of each with the same count - we need to determine scope...
				else:
					## We have to process each character... Depth 1 means function.. Depth > 1 means part of an arg so preserve everything... It can be split using another call... Everything outside of <START> and <END> is left to be the name / id... we only break it into component form, nothing else..
					print( )
			else:
				## We have an odd number of parens - ie
				##
				if ( _start_char_count > 1 or _end_char_count > 1 ):
					print( )


		##
		## Acecool.table.FromFunctionArgsString - Function to take a string of function arguments and turn it into a table
		##
		## TODO: Change so depth 0 chars aren't used, then primary_depth 1 args are taken, depth 2 is returned in a different table, 3 and so on... but 0 is ignored... { }s and [ ]s count as a different type of depth ( table, or so on )..
		## Note: Spaces should be allowed in after the first char and before the last to allow for x = y default arg value systems...
		##
		def FromFunctionArgsString( _text ):
			if not Acecool.util.isset( _text ):
				return '', 0

			##
			## Config
			##

			## Chars and Cap ( Starting and Ending ) Chars we want to skip...
			_cfg_skip_chars = { }
			_cfg_skip_cap_chars = { }

			## Chars to skip at ALL times regardless of location within the string...
			## _cfg_skip_chars[ "{" ] = True
			## _cfg_skip_chars[ "}" ] = True

			## Chars we want to skip regardless of where they are in the string...
			_cfg_skip_cap_chars[ ' ' ] = True
			_cfg_skip_cap_chars[ "'" ] = True
			_cfg_skip_cap_chars[ '"' ] = True

			## Tracks the current "Depth" based on brackets / braces
			_depth			= 0

			## Tracks the number of words we've accumulated in our table
			_args_count		= 0

			## Tracks the number of chars in the current word
			_char_count		= 0

			## Holds the current word until it is ready to be imploded as a string and inserted into our words table..
			_arg			= [ ]
			_arg_raw		= [ ]

			## Holds all of the arguments we've accumulated
			_args			= [ ]
			_args_raw		= [ ]

			## Current Arg Flags
			_arg_isstring	= False

			## Just in case we're not starting from 0...
			_begin = Acecool.string.indexOf( '(', _text )
			_finish = Acecool.string.indexOf( ')', _text )

			## Function declarations can be an argument - make sure these aren't included...
			_begin_func = Acecool.string.indexOf( 'function(', _text )
			## _finish = Acecool.logic.ternary( _finish <= len( _text ), _finish, len( _text ) )

			## print( "Debugging: _text = '" + _text + "' / _begin = " + str( _begin ) + " / _finish = " + str( _finish ) )

			## Clean
			## if ( _begin > 0 ):
				## if ( _begin_func > 0 ):
				## 	_text = Acecool.string.SubStrEx( _text, _begin, _begin_func )
				## 	_text = _text + 'FUNC_REF'
				## else:
			_text = Acecool.string.SubStrEx( _text, _begin, _finish )

			## if (  ):
			## 	_text = Acecool.string.EndChars( _text, _finish )

			## trick to remove special-case of trailing chars
			for _key, _char in enumerate( _text + "," ):
				## Increment the key..
				## _key += 1

				## Manage Brace Depth
				if _char == "{" or _char == "(" or _char == "[":
					_depth += 1
				elif _char == "}" or _char == ")" or _char == "]":
					_depth -= 1

				## If we've encountered a char we should skip, then skip it..
				if _cfg_skip_chars.get( _char, False ) == True:
					## _arg_raw.append( _char )
					_arg_raw.append( '*' )
					continue

				## If we've reached a comma.. we turn out current work collected chars into an actual word for our table and reset the collector for the next word...
				if _char == "," and _depth < 1:
					## If We're not in a bracket
					if _depth == 0:
						## Assemble the word and strip left / right spaces...
						_arg = "" . join( _arg ).strip( )
						_arg_raw = "" . join( _arg_raw ).strip( )

						## Add the current word to our table of words
						_args.append( _arg )
						_args_raw.append( _arg_raw )

						## Increment our counter
						_args_count += 1

						## Reset the word so we can begin assembling a new word
						_arg = [ ]
						_arg_raw = [ ]

						## Reset Arg Flags
						_char_count = 0
						_arg_isstring = False

				## else - ie If the char isn't a comma or the depth is >= 1 - meaning commas will only be added if depth is 1 or more and other data will be added all the time
				else:
					## Helpers... _head_char == first char of arg, _tail_char == last char of arg, _nil_depth == No Depth, _skip_char == Bad Character ( Determine whether or not to skip )
					_head_char = _char_count == 0
					_tail_char = _text[_key:_key + 1] == ','
					_nil_depth = _depth == 0
					_skip_char = _cfg_skip_cap_chars.get( _char, False ) == False

					## Helpers so we don't need to repeat _arg append... In short, if we're on a cap-character which is bad skip it...
					## If head_char or nil depth or tail char encountered... or not
					## if ( ( _head_char or _nil_depth and _tail_char ) or not ( _head_char or _nil_depth and _tail_char ) ) and _skip_char or not _nil_depth:
					if _skip_char or not _nil_depth:
						## Add the current character to our word string
						_arg.append( _char )
						_char_count += 1

					## if _char == '"' or _char == "'" or _skip_char or not _nil_depth:
					## if not _nil_depth:
					_arg_raw.append( _char )

			return _args, _args_count, _args_raw


		##
		##
		##
		def KeySet( *_varargs ):
			##
			_tab = { }

			##
			for _value in _varargs:
				## print( ' >>> ' + str( _value ) )
				_tab[ str( _value ) ] = True

			##
			return _tab


		##
		##
		##
		def HasKeySet( _tab, _key ):
			## if ( _key != None and _tab[ str( _key ) ] == True ):
			return _tab.get( str( _key ), False )
				## return True

			## return False


	##
	## String Operations Library
	##
	class string( str ):
		##
		##
		##
		def Safe( _text ):
			_text = _text.replace( ':', '_' )
			_text = _text.replace( '\\', '_' )
			_text = _text.replace( '/', '_' )
			_text = _text.replace( ' ', '_' )
			_text = _text.replace( '__', '_' )

			return _text


		##
		## Returns the filename with file extension from path/to/file.ext
		##
		def GetFilename( _file ):
			_data = _file.split( '\\' )
			return _data[ len( _data ) - 1 ] or ""


		##
		## Returns the file extension from path/to/file.ext
		##
		def GetExt( _file ):
			_data = _file.split( '.' )
			return _data[ len( _data ) - 1 ] or ""


		##
		## Acecool.string.repeat
		##
		def repeat( _chars, _count ):
			_text = ''

			for _ in range( _count ):
				_text = _text + _chars

			return _text


		##
		## Alias
		##
		## def contains( )


		##
		## Returns the index of the _needle word first-char if discovered - otherwise it returns -1
		##
		def indexOf( _needle, _haystack ):
			return _haystack.find( _needle ) + 1


		##
		## Returns True or False depending if _needle exists in _haystack or not..
		##
		def indexOfExists( _needle, _haystack, _case_sensitive ):
			if not _case_sensitive:
				return ( Acecool.string.indexOf( _needle, _haystack ) > 0 )
			else:
				return ( Acecool.string.indexOf( _needle.lower( ), _haystack.lower( ) ) > 0 )


		##
		## Returns the string in reverse
		##
		def reverse( _text ):
		  return _text[ : : - 1 ]


		##
		## SubString replacement
		##
		def SubStr( _text, _start, _end ):
			return _text[ _start : _end ]


		##
		## Strips out text found at _index
		##
		def Strip( _text, _index, _len ):
			if ( _index > 0 ):
				return Acecool.string.SubStr( _text, 0, _index ) + Acecool.string.SubStr( _text, _index + _len, len( _text ) )
			else:
				return Acecool.string.SubStr( _text, _len, len( _text ) )


		##
		## Strips all instances of _needle from _haystack
		##
		def StripAll( _needle, _haystack, _case_sensitive = True ):
			## Do while... ie run at least once...
			while( True ):
				## Make sure the data exists before running strip and repeating..
				_exists = Acecool.string.indexOfExists( _needle, _haystack, _case_sensitive )

				## It exists, lets do it...
				if ( _exists ):
					## It exists.. Grab the first index of where it is located...
					_index = Acecool.string.indexOf( _needle, _haystack ) - 1

					## Strip our needle from the haystack
					_haystack = Acecool.string.Strip( _haystack, _index, len( _needle ) )
				else:
					## We're done... exit loop...
					break

			return _haystack


		##
		## Finds all instances of _needle from _haystack
		##
		def FindAll( _needle, _haystack ):
			##
			_start = 0
			_found = [ ]
			_exclude_found = { }
			_regex = re.compile( _needle )

			_exclude = Acecool.table.KeySet( 'end', 'for', 'in', 'do', 'then', 'else', 'elseif', 'elif', 'if', 'local', 'return', 'break', 'true', 'false', 'nil', 'function' )

			## Do while... ie run at least once...
			while( True ):
				## Make sure the data exists before running strip and repeating.., len( _haystack ) + 1
				_result = _regex.search( _haystack, _start )

				## It exists, lets do it...
				if ( _result != None ): #and _result.start( ) > 0 and _result.end( ) > 0 ):
					## Update start pos
					_start = _result.end( ) + 1
					_data = str( _result.group( 1 ).strip( ) )

					##
					if ( _data.find( ':' ) > 0 ):
						_data = _data.split( ':' )[ 0 ]

					## (?!end|then|else|elif|if|local|return|break|true|false)
					## _data.find( ':' ) > 0 or
					## if not ( _data == 'end' or _data == 'then' or _data == 'else' or _data == 'elif' or _data == 'if' or _data == 'local' or _data == 'return' or _data == 'break' or _data == 'true' or _data == 'false' ):
					##
					if not ( _data.find( '..' ) > 0 or _data.endswith( '"' ) or _data.endswith( "'" ) or _data.startswith( '"' ) or _data.startswith( "'" ) or _data.endswith( "(" ) or Acecool.string.HasQuotes( _data ) or Acecool.table.HasKeySet( _exclude_found, _data ) or Acecool.table.HasKeySet( _exclude, _data ) ):
						## Add the result...
						_found.append( _data )
						_exclude_found[ _data ] = True

						## print( str( _data ) )
				else:
					## We're done... exit loop...
					break

			if ( len( _found ) > 0 ):
				return _found
			else:
				return None

		##
		## Removes Quotes from a string...
		##
		def StripQuotes( _text ):
			if ( _text.startswith( "'" ) and _text.endswith( "'" ) or _text.startswith( '"' ) and _text.endswith( '"' ) ):
				_text = Acecool.string.SubStr( _text, 1, len( _text ) - 1 )

			return _text


		##
		## Returns the type of quotes used by a string..
		##
		def GetQuotesType( _text ):
			if ( not _text or _text == '' ):
				return ''

			if ( _text.startswith( "'" ) and _text.endswith( "'" ) ):
				return "'"

			if ( _text.startswith( '"' ) and _text.endswith( '"' ) ):
				return '"'

			return ''


		##
		## Returns whether or not a particular string has quotes within it.
		##
		def HasQuotes( _text ):
			_type = Acecool.string.GetQuotesType( _text )

			return Acecool.logic.ternary( _type == '"' or _type == "'", True, False )


		##
		## SubString replacement
		##
		def SubStrEx( _text, _start, _end ):
			return _text[ _start : _end - 1 ]


		##
		## Returns the n starting chars from the string
		##
		def StartChars( _text, _count ):
			return _text[ : _count ]


		##
		## Returns the n ending chars from the string
		##
		def EndChars( _text, _count ):
			## One way to do it
			## _len = len( _text )
			## return _text[ _len - _count : ]

			## And another way...
			return _text[ - _count : ]


		##
		## Returns everything except the last _count chars in the string...
		##
		def NotEndChars( _text, _count ):
			return _text[ : - _count ]


		##
		## BUILT-IN - Returns whether or not our string starts with certain text
		##
		def StartsWith( _needle, _haystack ):
			return _haystack.startswith( _needle )


		##
		## BUILT-IN - Returns whether or not our string ends with certain text
		##
		def EndssWith( _needle, _haystack ):
			return _haystack.endswith( _needle )


		##
		## Helper function which lets you define width for each text, and it repeats it for you so you can repeat text and all columns will be the same width
		##
		def FormatColumn( _width = 25, _text = '', *_varargs ):
			## Make sure our text is a string
			_text = str( _text )

			## For each tab used, calculate how many spaces should be used with minimum of 1 and maximum of 4 being the range depending which snap-point is used.. Then strip that tab and add spaces in its place..
			_text = _text.expandtabs( CONST_TAB_WIDTH )

			## Count how many additional arguments we have
			_count = len( _varargs )

			## Since our Ex function, this, must use a paired-system, we make sure the rounded division is > 0
			_more = ( round( _count / 2, 0 ) > 0 )

			## How many repeating chars do we need to create?
			_reps = ( _width - len( _text ) )

			## Build a string to fill the empty column space with spaces so everything lines up - as long as the right font is used ( where all chars are the same size )
			_empty = Acecool.string.repeat( ' ', _reps )

			## Now we ensure our text is limited to the _width size - data going over is truncated...
			_data = Acecool.string.SubStr( _text + ( Acecool.logic.ternary( _reps > 0, _empty, _empty ) ), 0, _width )

			## If we have more cars
			if ( _more ):
				## Recursive call by shifting our VarArgs left so they populate _width and _text - then add the result to the data var... This only stops when no more paired options are left...
				_data = _data + Acecool.string.FormatColumn( *_varargs )

			## Return the data..
			return _data


		##
		## Helper function which lets you define width once, and it repeats it for you so you can repeat text and all columns will be the same width
		##
		def FormatSimpleColumn( _width = 25, *_varargs ):
			## Count how many text elements we have...
			_count = len( _varargs )

			## Set up our return var
			_data = ''

			## If we have at least 1 text element to set-up into a column
			if ( _count > 0 ):
				## Then we loop through each vararg
				for _text in range( _varargs ):
					## And we use a pseudo recursive call on the FormatColumnEx function - extra args...
					_data = _data + Acecool.string.FormatColumn( _width, _text )

			## Return the data..
			return _data


	##
	## math Library
	##
	class math:
		##
		## Snaps a value to the closest point...
		##
		def SnapTo( _value, _interval, _always_increment = True ):
			if ( not _interval or _interval == 0 ): return _value

			_div = _value / _interval
			_snapped = 0

			if ( _always_increment == True ):
				_snapped = round( _div )
			else:
				_snapped = ceil( _div )

			_snapped = _snapped * _interval
			return _snapped


		##
		## Acecool.string.repeat
		##
		def repeat( _chars, _count ):
			_output = ''
			for _ in range( _count ):
				_output = _output + _chars
			return _output


		##
		## Helper function to process the basic math functions..
		##
		def simple( _mode, _a, *_z ):
			_value = _a

			if ( len( _z ) > 0 ):
				for _b in ( _z ):
					## Addition / Subtract
					if ( _mode == 'add' ):
						_value = _value + _b
					if ( _mode == 'sub' ):
						_value = _value - _b

					## Multiply / Division
					if ( _mode == 'mul' ):
						_value = _value * _b
					if ( _mode == 'div' ):
						_value = _value / _b

					## Modulus
					if ( _mode == 'mod' ):
						_value = _value % _b

					## Power of
					if ( _mode == 'pow' ):
						_value = _value ** _b

					## Exponent
					if ( _mode == 'exp' ):
						_value = _value ^ _b

			return _value


		##
		## Helpers for basic actions... Addition, Subtracting / Multiplication, Division, Modulus / Power / Exponent
		##
		def add( a, *_z ): return Acecool.math.simple( 'add', _a, *_z )
		def sub( a, *_z ): return Acecool.math.simple( 'sub', _a, *_z )
		def mul( a, *_z ): return Acecool.math.simple( 'mul', _a, *_z )
		def div( a, *_z ): return Acecool.math.simple( 'div', _a, *_z )
		def mod( a, *_z ): return Acecool.math.simple( 'mod', _a, *_z )
		def pow( a, *_z ): return Acecool.math.simple( 'pow', _a, *_z )
		def exp( a, *_z ): return Acecool.math.simple( 'exp', _a, *_z )


	##
	## File Operations Library
	##
	class file:
		##
		##
		##
		def Read( _file, _char_new_line = CONST_NEW_LINE_UNIX ):
			_lines = None

			try:
				with codecs.open( _file, "r", encoding = 'utf8' ) as _f:
					_lines = _f.read( ).split( _char_new_line )
			except Exception as _error:
				print( '[ Acecool > file > Read ] Error:', _error )

			return _lines, len( _lines )


	##
	## Utility Library
	##
	class util:
		##
		##
		##
		## def example( ):
		## 	print( )


		##
		##
		##
		def ImportInvalidlyNamed( _base, _name_invalid, _name ):
			with open( _base + _name_invalid + '.py', 'rb' ) as _data:
				_name = imp.load_module( _name, _data, _name_invalid, ( '.py', 'rb', imp.PY_SOURCE ) )

			return _name


		##
		## return Acecool.logic.ternary( not _data and _data != False, False, True )
		##
		def isset( _data = None ):
			if ( _data == None ):
				return False

			_isset = True
			try:
				_data
			except NameError:
				_isset = False

			return _isset


		##
		## Create Get/Set<Name>( ) functions and supporting helpers..
		##
		def AccessorFunc( self, _name, _default, _initialize = True, _debug = False ):
			##
			_data_key = 'accessorfunc_data'

			## Set up the function names to be used...
			_name_get		= 'Get' + _name
			_name_str		= 'Get' + _name + 'ToString'
			_name_len		= 'Get' + _name + 'Len'
			_name_lenstr	= 'Get' + _name + 'LenToString'
			_name_set		= 'Set' + _name
			_name_reset		= 'Reset' + _name
			_name_isset		= 'Is' + _name + 'Set'

			## Debugging
			if ( _debug == True or _debug == 'AccessorFunc' or _debug == 'Acecool.util.AccessorFunc' ):
				_call_get		= 'self.' + _name_get + '( _default, _skip_defaults )'
				_call_str		= 'self.' + _name_str + '( _default )'
				_call_len		= 'self.' + _name_len + '( _default )'
				_call_lenstr	= 'self.' + _name_lenstr + '( _default )'
				_call_set		= 'self.' + _name_set + '( _value )'
				_call_reset		= 'self.' + _name_reset + '( _default )'
				_call_isset		= 'self.' + _name_isset + '( _true_on_default )'

				##
				_width = 70

				## print( Acecool.string.FormatColumn( 25, '[ Acecool.util.AccessorFuncs ]', len( _call_get ) + 5, _call_get, len( _call_str ) + 5, _call_str, len( _call_len ) + 5, _call_len, len( _call_set ) + 5, _call_set, len( _call_reset ) + 5, _call_reset, len( _call_isset ) + 5, _call_isset ) )
				print( Acecool.string.FormatColumn( 34, '[ Acecool.util.AccessorFuncs ]', _width + 2, _call_get, _width - 6, _call_str, _width - 11, _call_len, _width - 3, _call_lenstr, _width - 16, _call_set, _width - 12, _call_reset, _width - 4, _call_isset ) )

			try:
				self.accessorfunc_data
			except:
				self.accessorfunc_data = { }
			## if ( not Acecool.util.isset( self.accessorfunc_data ) ):
			## 	self.accessorfunc_data = { }


			##
			## function self.Get<Name>( _default )
			##
			## Args: Override Default as _i_default - This overrides the default value in favor of an on a per-call default setting basis..
			## Args: Ignore ALL Defaults as _no_default - This overrides all default values in favor of returning None if a value isn't set...
			##
			def GetName( _i_default = None, _skip_defaults = False ):
				## Debugging
				## print( '[ AccessorFunc ] ' + _name_get + ' is being called!' ) ( Acecool.logic.ternary( _i_default != None, _i_default, _default ) )

				## If the override default isn't set - use the global default for this AccessorFunc - An accurate default should be defined ( many devs forget to use an on-per-call-basis defaults which is why the global default exists and it prevents repetitiveness b y not requiring the same default each call )
				if ( _i_default == None ):
					_i_default = _default

				## Read the data from the database / table - Use DATA_NOT_FOUND string if it doesn't exist...
				_data = self.accessorfunc_data.get( _name, 'DATA_NOT_FOUND' )

				## If the data is set, return our data.. Otherwise - if no defaults is set then we return None otherwise we return the default value ( either global or this-calls' local override )
				_data = Acecool.logic.ternary( ( _data != 'DATA_NOT_FOUND' ), _data, Acecool.logic.ternary( ( _skip_defaults == False ), _i_default, None ) )

				## Return the data!
				return _data

			## function self.Get<Name>( _default )
			GetName.__name__ = _name_get
			setattr( self, _name_get, GetName )


			##
			## function self.Get<Name>ToString( _default ) - Returns the data in string-format..
			##
			def GetNameToString( _i_default = None ):
				return str( GetName( _i_default ) )

			## function self.Get<Name>ToString( )
			GetNameToString.__name__ = _name_str;
			setattr( self, _name_str, GetNameToString )


			##
			## function self.Get<Name>Len( _default ) - Returns the Length of characters of the data
			##
			def GetNameLen( _i_default = '' ):
				return len( GetNameToString( _i_default ) )

			## function self.Get<Name>Len( )
			GetNameLen.__name__ = _name_len;
			setattr( self, _name_len, GetNameLen )


			##
			## function self.Get<Name>Len( _default ) - Returns the Length of characters of the data
			##
			def GetNameLenStr( _i_default = '' ):
				return str( GetNameLen( _i_default ) )

			## function self.Get<Name>Len( )
			GetNameLenStr.__name__ = _name_lenstr;
			setattr( self, _name_lenstr, GetNameLenStr )


			##
			## function self.Set<Name>( _default ) - Assigns a value to the referenced key / index location in our table...
			##
			def SetName( _value ):
				self.accessorfunc_data[ _name ] = _value
				return self

			## function self.Set<Name>( _default )
			SetName.__name__ = _name_set;
			setattr( self, _name_set, SetName )


			##
			## function self.Reset<Name>( _default ) - Resets the value to a local default or the global default...
			##
			def ResetName( _i_default = None ):
				_value = _i_default
				if ( _value == None ):
					_value = _default

				return SetName( _value )

			## function self.Reset<Name>( _default )
			ResetName.__name__ = _name_reset;
			setattr( self, _name_reset, ResetName )


			##
			## function self.Is<Name>Set( _default ) - Returns whether or not the data is set.. Set means the value isn't None and isn't using the default return...
			##
			def IsNameSet( _true_on_default = False ):
				_data = GetName( None, not _true_on_default )

				## If the Raw data is None then continue.. If we return True if default is Set and Default isn't set then return false...
				if ( _data == None ) and ( _true_on_default == False or _default != None ):
					return False

				return True

			## function self.Get<Name>Len( )
			IsNameSet.__name__ = _name_isset;
			setattr( self, _name_isset, IsNameSet )


			## If we're to Initialize the data...
			if ( _initialize ):
				## Initialize the data to the default state.. - self[ _name_reset ]( ) or ResetName( ) or getattr( self, _name_set )( _default ) should work!
				getattr( self, _name_reset )( )


	##
	## Logic Library
	##
	class logic:
		##
		## Ternary Helper Function - Follows a more traditional pattern: ( <statement> ) ? true : false; / ( statement ) and true or false / etc..
		##
		def ternary( _statement, _true, _false ):
			return ( _false, _true )[ _statement ]


		##
		##
		##
		## def example( ):
		## 	print( )


	##
	##
	##
	class Debugging:
		##
		## testing( 'this', 'little', 'puppy', 'went', 'to', 'market' )
		##
		## Testing testing( varargs ) function:
		## 0 testing this
		## 1 testing little
		## 2 testing puppy
		## 3 testing went
		## 4 testing to
		## 5 testing market
		## Now passing directly to another function using _args
		## ('this', 'little', 'puppy', 'went', 'to', 'market')
		##
		## Now passing directly to another function using *_args
		## this little puppy went to market
		##
		def testing( *_args ):
			print( 'Testing testing( varargs ) function: ' )
			for key, value in enumerate( _args ):
				print( str( key ) + ' testing ' + str( value ) )

			print( 'Now passing directly to another function using _args' )
			print( _args )
			print( 'Now passing directly to another function using *_args' )
			print( *_args )

			## Running this way is identical to Lua: _func( unpack( _tab ) ) and works perfectly...
			testing_call( *_args )

			## Running this way leaves 5 args empty - error... Because it sends _args as a table instead of unpacked data..
			## testing_call( _args )


		##
		## Test to see how to run func( unpack( _tab ) ) correctly...
		##
		def testing_call( _this, _little, _piggly, _went, _to, _market ):
			print( _this + _little + _piggly + _went + _to + _market )

			_text = 'testing a b c d e f'
			_index = Acecool.string.indexOf( 'e', _text )
			print( str( _index ) )


			_text = Acecool.string.FormatColumn( 25, 'Testing 25 Len', 50, '50 Len this time', 10, 'And 10 for this', 10, 'again 10', 10, 'again 10' )
			print( 'FormatColumn: ' + _text )
			_text = Acecool.string.FormatSimpleColumn( 25, '25 for each', 'and every single', 'option', 'yay' )
			print( 'FormatSimpleColumn: ' + _text )



		##
		## Runs the test...
		##
		def testing_exec( ):
			testing( 'this', 'little', 'piggly', 'went', 'to', 'market' )

	## End Acecool Namespace