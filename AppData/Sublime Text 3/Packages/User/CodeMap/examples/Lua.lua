--
-- XCodeMapper Example File for Language: Lua using the Garry's Mod and Vanilla Lua Mappings - Josh 'Acecool' Moser
--


--
-- Simulate this as a different file...
--
do -- Start funcitons.lua or whatever..
	--
	-- Helper Function for CreateAccessorFuncs - This creates a setter function with restrictions and a callback to modify the data...
	--
	function CreateAccessorFuncSetter( _tab, _name, _force_values, _force_types, _key, _callback )
		-- If _tab isn't a table, we have a problem...
		if ( not istable( _tab ) ) then return false, 'Error - _tab isn\'t a table' ); end

		-- Create the function...
		_tab[ _name ] = function( _value )
			-- Grab the __data location
			local __data = self.__data;

			-- Grab the callback data...
			local _callback_value = nil
			if ( isfunction( _callback ) ) then
				-- Call the callback letting it know the current value, and what is currently being set...
				_callback_value = _callback( _value, __data[ _key ] )
			end

			-- Now, lets see if they work - then choose which one to use... If callback doesn't work and value doesn't - set nothing.. Set standard data first, set callback if it works second, or alternative..






			-- TODO: Finish adding this... which requires a few more functions..







			-- return self to allow Set-Chaining...
			return _tab;
		end
	end


	--
	-- Helper Function for CreateAccessorFuncs - This creates a getter function which doesn't need restrictions, and a callback to modify the data...
	-- Note: We do not need to force values or types in the getter because the setter does that when it sets the value to our reference point... The getter only grabs the data at that point with the option of manipulating that data on return through a callback...
	--
	function CreateAccessorFuncGetter( _tab, _name, _key, _callback )
		-- If _tab isn't a table, we have a problem...
		if ( not istable( _tab ) ) then return false, 'Error - _tab isn\'t a table' ); end

		-- Create the function...
		_tab[ _name ] = function(_default )
			local _data = self.__data[ _key ]

			-- If _callback is set then we see if we can use it...
			if ( isfunction( _callback ) ) then
				-- Call callback with the _data ( as current _value ) and _default value this Getter was called with...
				_callback_data = _callback( _data, _default );

				-- If our callback data is set as something then return it...
				if ( _callback_data == false or _callback_data ) then
					return _callback_data;
				end
			end

			-- Return the data stored at _key if it is set.. otherwise return default..
			return ( _data == false or _data ) and _data or _default
		end
	end


	--
	-- Define a helper function called AccessorFunc to somewhat meet Garry's Mod definition using a better layout...
	-- Note: We get rid of _key in this declaration because we'll use name as our key which won't be repeated without overwriting... We use _tab.__data[ _key ] to store data..
	--
	function CreateAccessorFuncs( _tab, _name, _force_values, _force_types, _getter_prefix, _callback_setter, _callback_getter )
		-- Note: _key isn't really necessary because this AccessorFunc definition doesn't let you set the prefix for the Getter, and it doesn't let you define a callback
		-- key is useful if you define the callback and the callback provides the data stored at that key and lets you mod the value before setting or do something with the getter data...
		-- Example: Adding G/SetReps ... Adding HasReps to use G/SetReps key... HasReps Getter callback function( _value, _default_not_needed_here ) return _value > 0; end... which becomes useful and short... We just added HasReps to G/SetReps Getter/Setters in a single line..
		-- For the above Example to work, we need a helper function which AccessorFunc calls twice and we need _key to go somewhere where accessible...

		-- If _tab isn't a table, we have a problem... Our helpers can check it...

		-- If our __data location doesn't exist, create it...
		if ( not istable( _tab.__data ) ) then _tab.__data = { }; end

		-- Define the Getter Prefix - If it is set, then use it otherwise use 'Get' - Note: This also means it can be set as '' meaning nothing and that is fine...
		local _getter_prefix = ( isstring( _getter_prefix ) and _getter_prefix or 'Get' );

		-- Define the key: as key_<FuncNameLowercase>
		local _key = 'key_' .. string.lower( _name );

		-- Create the Getter - Prefix the Getter Prefix to the _name, send the _key and also add the _callback for the Getter if one exists...
		CreateAccessorFuncGetter( _tab, _getter_prefix .. _name, _key, _callback_getter )

		-- Create the Setter
		CreateAccessorFuncSetter( _tab, 'Set' .. _name, _force_values, _force_types, _key, _callback_setter )
	end
end -- End simulated file for functions.lua or whatever..


--
-- TimeKeeper Object / Library
--
TimeKeeper = TimeKeeper || { };
TimeKeeper.__index = TimeKeeper;
TimeKeeper.__newindex = TimeKeeper;


--
-- Define AccessorFuncs used by our program - function AccessorFunc( _tab, _key, _name, _crude_force_type )
--

-- Creates Get/SetTime( _value_or_default ) and forces the set value to be a number...
CreateAccessorFuncs( TimeKeeper, 'time_key', 'Time', FORCE_NUMBER )

-- Creates Get/SetName( _value_or_default ) and forces the set value to be a string...
CreateAccessorFuncs( TimeKeeper, 'name_key', 'Name', FORCE_STRING )

-- Creates Get/SetExists( _value_or_default ) and forces the set value to be a boolean...
CreateAccessorFuncs( TimeKeeper, 'exists_key', 'Exists', FORCE_BOOL )

-- Creates Get/SetBlah( _value_or_default ) and doesn't imply any restrictions on the set value...
CreateAccessorFuncs( TimeKeeper, 'else_key', 'Blah' )

-- Creates Get/SetBlahBlah( _value_or_default ) and doesn't imply any restrictions on the set value...
CreateAccessorFuncs( TimeKeeper, 'else_key', 'BlahBlah' )

-- Creates Get/SetBlahBlahBlah( _value_or_default ) and doesn't imply any restrictions on the set value...
CreateAccessorFuncs( TimeKeeper, 'else_key', 'BlahBlahBlah' )


--
-- Create a new copy of self... Allow an alternative table to be passed in to be used as the object table...
--
function TimeKeeper:New( _object )
	-- Define the object table to use for the new object...
	local __object = _object || { };

	-- Make our ordinary table into a meta-table ( TimeKeeper - meaning we can use self or TimeKeeper because of : func def.. if we used . then we'd need to specify self for each func but we'd still be able to use : to call )...
	setmetatable( __object, self );

	-- Call any functions to set any values...
	-- ..
	-- xx

	-- ..
	-- yy

	-- Return our object..
	return __object;
end


--
-- Our main function...
--
function main( ... )
	-- Variable Definition
	local _var = 'test';


end