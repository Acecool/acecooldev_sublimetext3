﻿;;
;; AutoHotkey Script Example to show off XCodeMapper - Josh 'Acecool' Moser
;;
;; Note: ! == ALT, ^ == Control, + == SHIFT, # == WIN
;;


;;
;;
;;
!+#^F1::
	MsgBox, [ Ctrl ] [ Alt ] [ Shift ] [ Win ] F1 was pressed!
return


;;
;;
;;
^!+F2::
	MsgBox, [ Ctrl ] [ Alt ] [ Shift ] F2 was pressed!
return


;;
;;
;;
^+F3::
	MsgBox, [ Ctrl ] [ Shift ] F3 was pressed!
return


;;
;; F21 to Ctrl + C - ie COPY
;;
F21::
	Send, ^c
return


;;
;; F22 to Ctrl + V - ie PASTE
;;
F22::
	Send, ^v
return


;;
;; F23 to Alt + C - ie OPEN CONSOLE
;;
F23::
	Send, !c
return


;;
;; F24 to Restart / Start Sublime Test 3
;;
F24::
	;; If the exe is running, close and run WinCloseWait for the next command..
	; Note: This will only work if using AcecoolAHK_Framework - Alternatively you can use WinGet ahk_exe sublime_text.exe then WinClose WinCloseWait then Run...
	; window.CloseIfExeExists( "sublime_text.exe" )

	;; If the exe is running, close and run WinCloseWait for the next command..
	_id := WinExist( "ahk_exe sublime_text.exe", "", "", "" )
	if ( _id != "" )
	{
		WinGet, _return, , "", "", "", ""
		WinClose
		WinWaitClose
	}

	;; Re-Open ST3
	Run, "C:\Program Files\Sublime Text 3\sublime_text.exe"
return


;;
;; AutoHotkey Script - Josh 'Acecool' Moser
;;


GotoTag:
	MsgBox, Hey, it worked!
return


;;
;; OnInit / Setup
;;
OnInit_library_window( )
{
	window.example( )
}


;;
;; Variables, CONSTants, ENUMerations, etc..
;;
class window
{
	;;
	;; Example Function to demonstrate functions in this library...
	;;
	example( )
	{

	}


	;;
	;;
	;;
	_____( )
	{

	}


	;;
	;;
	;; _cmd: ID, IDLast, PID, ProcessName, ProcessPath, Count, List, MinMax, ControlList, ControlListHwnd, Transparent, TransColor, Style or ExStyle
	;;
	Get( _cmd := "ID", _title := "", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		WinGet, _return, %_cmd%, %_title%, %_text%, %_exclude_title%, %_exclude_text%
		return _return
	}


	;;
	;;
	;;
	GetByClass( _cmd := "", _title := "", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		WinGet, _return, "ahk_class " . %_cmd%, %_title%, %_text%, %_exclude_title%, %_exclude_text%
		return _return
	}


	;;
	;;
	;;
	GetByExe( _cmd := "", _title := "", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		WinGet, _return, "ahk_exe " . %_cmd%, %_title%, %_text%, %_exclude_title%, %_exclude_text%
		return _return
	}


	;;
	;;
	;;
	CloseIfExists( _search )
	{
		;;
		_win := window.IfExist( _search )

		; #IfWinActive ahk_class PX_WINDOW_CLASS, ahk_exe sublime_text.exe
		if ( _win != false )
		{
			;;
			WinClose

			;;
			WinWaitClose
		}
	}


	;;
	;;
	;;
	CloseIfExeExists( _search )
	{
		this.CloseIfExists( "ahk_exe " . _search )
	}


	;;
	;;
	;;
	CloseIfClassExists( _search )
	{
		this.CloseIfExists( "ahk_class " . _search )
	}


	;;
	;;
	;;
	Size( _title )
	{
		WinGetPos, _x, _y, _w, _h, %_title%
		_return := _w ";" _h

		return _return
	}


	;;
	;;
	;;
	GetWinIDUnderMouse( )
	{
		MouseGetPos, , , _win_id, _control
		return _win_id
	}


	;;
	;;
	;;
	GetWinClassUnderMouse( )
	{
		_id := this.GetWinIDUnderMouse( )
		WinGetClass, _class, ahk_id %_id%

		return _class
	}


	;;
	;; Returns true if the window is active, false otherwise.
	;;
	IfActive( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		_id := WinActive( _title, _text, _exclude_title, _exclude_text )
		if ( _id != "" )
			return _id

		return false
	}


	;;
	;; Returns true if the window is not active, false otherwise.
	;;
	IfNotActive( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return !this.IfWinActive( _title, _text, _exclude_title, _exclude_text )
	}


	;;
	;; Returns true if the window, by class name, is active, false otherwise.
	;;
	IfClassActive( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return this.IfWinActive( "ahk_class " . _title, _text, _exclude_title, _exclude_text )
	}


	;;
	;; Returns true if the window, by class name, is not active, false otherwise.
	;;
	IfClassNotActive( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return !this.IfClassActive( _title, _text, _exclude_title, _exclude_text )
	}


	;;
	;; Returns true if the window exists, false otherwise.
	;;
	IfExist( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		_id := WinExist( _title, _text, _exclude_title, _exclude_text )
		if ( _id != "" )
			return _id

		return false
	}


	;;
	;; Returns true if the window doesn't exist, false otherwise.
	;;
	IfNotExist( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return !this.IfExist( _title, _text, _exclude_title, _exclude_text )
	}


	;;
	;; Returns true if the window, by class name, exists, false otherwise.
	;;
	IfClassExist( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return this.IfExist( "ahk_class " . _title, _text, _exclude_title, _exclude_text )
	}


	;;
	;; Returns true if the window, by class name, doesn't exist, false otherwise.
	;;
	IfClassNotExist( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return !this.IfClassExist( _title, _text, _exclude_title, _exclude_text )
	}


	;;
	;; Returns true if the window, by class name, exists, false otherwise.
	;;
	IfExeExist( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return this.IfExist( "ahk_exe " . _title, _text, _exclude_title, _exclude_text )
	}


	;;
	;; Returns true if the window, by class name, doesn't exist, false otherwise.
	;;
	IfExeNotExist( _title := "Task Manager", _text := "", _exclude_title := "", _exclude_text := "" )
	{
		return !this.IfExeExist( _title, _text, _exclude_title, _exclude_text )
	}
}