


##
## Depth 0 == RESET and - Set Depth + 1 ( When you see Depth + 1 in comments it means found at Depth + 1... )
##
class TestingA:
	##
	## Depth + 1 ( This means found at depth + 1 )
	##
	def TestingAFunc( ):
		print( )


	##
	## Depth + 1 ( Class found at depth + 1 means we append active, which is A, to stack and set B as current active. We also set active depth + 1 - which becomes 2 - depth + 1 is still used throughout - it is from last class )
	##
	class TestingB:
		##
		## Depth + 1
		##
		def TestingBFunc( ):
			print( )


		##
		## Depth + 1 - append B, set active C
		##
		class TestingC:
			##
			## Depth + 1
			##
			def TestingCFunc( ):
				print( )

		##
		## Depth - 1 == SetActive( stack.pop( ) == B ) and set current depth == depth which is 2 ( here's where I say depth instead of depth+1 even though it is still depth + 1 fro B - it is Depth compared to read-order )
		##
		def TestingBFunc2( ):
			print()


		##
		## Depth
		##
		def TestingBFunc3( ):
			print()


	##
	## Depth - 1 == SetActive( stack.pop( ) == A ) and set current depth = depth which is 1
	##
	def TestingAFunc2( ):
		print()


##
## Depth == 0 ( also depth -1 ) == RESET and SetActive( D )
##
class TestingD( TestingA ):
	##
	## Depth + 1
	##
	def TestingD1( ):
		print( )


	##
	## Depth + 1
	##
	def TestingD2( ):
		print( )


	##
	## Depth + 1
	##
	def TestingD3( ):
		print( )


##
## Depth == 0 ( Still Depth - 1 because depth + 1 is set from the depth of the class to simplify logic ) == RESET and SetActive( E )
##
## Note: For now on I won't show Depth + 1 but I'll still show what happens to the stack for the the data ...
##
class TestingE( TestingA ):
	def TestingE1( ):
		print( )
	def TestingE2( ):
		print( )
	def TestingE3( ):
		print( )


	##
	## Depth + 1 == Append( E ), SetActive( F )
	##
	class TestingF( TestingA ):
		def TestingF1( ):
			print( )
		def TestingF2( ):
			print( )
		def TestingF3( ):
			print( )


	##
	## Depth == REPLACE F == SetActive( G ) -  meaning F isn't appended to the stack because this class is defined outside of its scope so we won't need to track backwards through it...
	##
	class TestingG( TestingA ):
		def TestingG3( ):
			print( )
		def TestingG2( ):
			print( )
		def TestingG1( ):
			print( )


		##
		## Depth + 1 == Append( G ), SetActive( H )
		##
		class TestingH( TestingA ):
			def TestingH1( ):
				print( )
			def TestingH2( ):
				print( )
			def TestingH3( ):
				print( )


			##
			## Depth + 1 == Append( H ), SetActive( I )
			##
			class TestingI( TestingA ):
				def TestingI1( ):
					print( )
				def TestingI2( ):
					print( )
				def TestingI3( ):
					print( )


				##
				## Depth + 1 == Append( I ), SetActive( J )
				##
				class TestingJ( TestingA ):
					def TestingJ1( ):
						print( )
					def TestingJ2( ):
						print( )
					def TestingJ3( ):
						print( )



					##
					## Depth + 1 - Altered to do this to show the loop popping... SetActive( K ), Append( J )
					## CANCEL - ALTERED THIS LINE, READ ABOVE - Depth - 1 - For loop is used in case multiple steps are taken... Bsically in this case it runs 1 time ( Difference is 2, but as ActiveClass is not in stack we don't run it )
					##
					class TestingK( TestingA ):
						def TestingK1( ):
							print( )
						def TestingK2( ):
							print( )
						def TestingK3( ):
							print( )



	##
	## Depth - 5 ( Loop 5 - 1 == 4 times - We do don't get F because F was replaced and we don't replace E because we're in E - additionally I didn't go to depth 0 because at depth 0 I simply wipe the slate clean... )
	## Loop( 1, Active( K ), SetActive( Pop( ) == J ) ) - Basically for the first iteration the previous active was K... The Pop result is J and J is set as new active class...
	## Loop( 2, Active( J ), SetActive( Pop( ) == I ) )
	## Loop( 3, Active( I ), SetActive( Pop( ) == H ) )
	## Loop( 4, Active( H ), SetActive( Pop( ) == G ) ) - Meaning G is set as active leaving E as parent... Then G is replaced by L
	##
	class TestingL( TestingA ):
		def TestingL1( ):
			print( )
		def TestingL2( ):
			print( )
		def TestingL3( ):
			print( )




	##
	##
	##
	class TestingM( TestingA ):
		def TestingM1( ):
			print( )
		def TestingM2( ):
			print( )
		def TestingM3( ):
			print( )


##
##
##
def testing_NotInClass( ):
	print( )