//
// XCodeMapper Example File for Language: Lua using the Garry's Mod and Vanilla Lua Mappings - Josh 'Acecool' Moser
//


//
// TimeKeeper Object / Library
//
TimeKeeper = TimeKeeper || { };
TimeKeeper.__index = TimeKeeper;
TimeKeeper.__newindex = TimeKeeper;


//
// Define AccessorFunc Function Declarations used by our TimeKeeper ( This isn't any different than defining it directly as with New ) - function AccessorFunc( _tab, _key, _name, _crude_force_type )
//

// Creates Get/SetTime( _value_or_default ) and forces the set value to be a number...
AccessorFunc( TimeKeeper, 'time_key', 'Time', FORCE_NUMBER )

// Creates Get/SetName( _value_or_default ) and forces the set value to be a string...
AccessorFunc( TimeKeeper, 'name_key', 'Name', FORCE_STRING )

// Creates Get/SetExists( _value_or_default ) and forces the set value to be a boolean...
AccessorFunc( TimeKeeper, 'exists_key', 'Exists', FORCE_BOOL )

// Creates Get/SetBlah( _value_or_default ) and forces the set value to be a number...
AccessorFunc( TimeKeeper, 'reps_key', 'Reps', FORCE_NUMBER )

// If AccessorFunc used helpers, this could automatically grab the value ( and know what the default value is supposed to be )... Return either true ( only if reps is a number and reps is above 0 ) or false if reps isn't set as a number or is 0 or lower..
// Note: This shows up as an error / warning for the arguments because I look for the first ( and last ).. at least only until the string parser is set ( which will be used if more than 1 ( and ) exists to keep efficiency as high as possible )
function TimeKeeper:HasReps( ) return ( ( isnumber( self:GetReps( ) ) ) and self:GetReps( ) > 0 or false ); end

// Creates Get/SetBlahBlah( _value_or_default ) and doesn't imply any restrictions on the set value...
AccessorFunc( TimeKeeper, 'else_key', 'BlahBlah' )

// Creates Get/SetBlahBlahBlah( _value_or_default ) and doesn't imply any restrictions on the set value...
AccessorFunc( TimeKeeper, 'else_key', 'BlahBlahBlah' )


//
// Create a new copy of self... Allow an alternative table to be passed in to be used as the object table...
//
function TimeKeeper:New( _object, _reps, _name )
	// Define the object table to use for the new object...
	local __object = _object || { };

	// Make our ordinary table into a meta-table ( TimeKeeper - meaning we can use self or TimeKeeper because of : func def.. if we used . then we'd need to specify self for each func but we'd still be able to use : to call )...
	setmetatable( __object, self );

	// Set the name... We use __object because self is still TimeKeeper in this call, not our new __object...
	__object:SetName( _name || 'Example TimeKeeper' );

	// Set the reps
	__object:SetReps( ( ( isnumber( _reps ) ) and _reps || 1 ) );

	// Return our object.. Any call using it outside such as _timekeeper = TimeKeeper:New( ); can then be used so _timekeeper:SetName( 'Blah' ) and self will work for that object... It doesn't work here because it would set up default / static values.. as self isn't an object reference here...
	return __object;
end


//
//
//



//
// Our main function...
//
function main( ... )
	// Variable Definitions
	local _name = 'Example TimeKeeper';
	local _reps = 1;



	// Create a new copy of TimeKeeper.. Note _timekeeper becomes the object reference and is used as self in all : calls...
	// Note: TimeKeeper.SetReps( _timekeeper, 1 ) and _timekeeper.SetReps( _timekeeper, 1 ) are both identical to _timekeeper:SetReps( 1 ) and can be called on functions defined using Object:Name( x ) or Object.Name( self, x )
	_timekeeper = TimeKeeper:New( );
	_timekeeper:SetName( _nam,e );
	_timekeeper:SetReps( _reps );
end