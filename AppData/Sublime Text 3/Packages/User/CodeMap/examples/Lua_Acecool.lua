//
// XCodeMapper Example File for Language: Lua using the AcecoolDef_Framework, Garry's Mod and Vanilla Lua Mappings - Josh 'Acecool' Moser
//


//
// Setup CONSTants, ENUMeration, etc..
//

// CONST - Default Start Time for when none is given, or someone calls StartTime without setting the data or calling New..
// NOTE: When the default time is set to -1, then the timekeeper will not start unless Start is called ( which sets the start-time to the current time using the appropriate
//	time function set by mode ) - This shouldn't be tampered with otherwise default behavior in the framework would be altered, and the default code would be affected..
local TIMEKEEPER_DEFAULT_TIME_STARTED			= -1;

// CONST - Default Duration for when none is given, or someone calls Duration without setting the data or calling New..
local TIMEKEEPER_DEFAULT_DURATION				= 3;


// CONST - Default Delay for when none is given, or someone calls Delay or Expiry / StartTime without setting the data or calling New..
local TIMEKEEPER_DEFAULT_DELAY					= 0;

// Default Repetitions = 0 because we only want it to run once..
local TIMEKEEPER_DEFAULT_REPETITIONS			= 1;


//
// TimeKeeper Object / Library
//
TimeKeeper = TimeKeeper || { };
TimeKeeper.__index = TimeKeeper;
TimeKeeper.__newindex = TimeKeeper;


//
// Define AccessorFuncs used by our program - function AccessorFunc( _tab, _key, _name, _crude_force_type )
//

//
Acecool:AccessorFunc( TimeKeeper, "Delay", TIMEKEEPER_DEFAULT_DELAY, TYPE_NUMBER, nil, "" );

// GetStartTime / SetStartTime -
Acecool:AccessorFunc( TimeKeeper, "StartTime", TIMEKEEPER_DEFAULT_TIME_STARTED, TYPE_NUMBER, nil );

// Duration / SetDuration -
Acecool:AccessorFunc( TimeKeeper, "Duration", TIMEKEEPER_DEFAULT_DURATION, { TYPE_NUMBER, TYPE_FUNCTION }, nil, "" );

// GetReps / SetReps / HasReps / AddReps / StartRep -
Acecool:AccessorFunc( TimeKeeper, "Reps", TIMEKEEPER_DEFAULT_REPETITIONS, TYPE_NUMBER, nil );


//
// Create a new copy of self... Allow an alternative table to be passed in to be used as the object table...
//
function TimeKeeper:New( _object, _reps, _delay )
	// Define the object table to use for the new object...
	local __object = _object || { };

	// Make our ordinary table into a meta-table ( TimeKeeper - meaning we can use self or TimeKeeper because of : func def.. if we used . then we'd need to specify self for each func but we'd still be able to use : to call )...
	setmetatable( __object, self );

	// Set Repetitions - I don't need to check if it is a number, or even if it is defined, before inputting _reps because the setter handles that... It also uses a default for all Getter calls so if _reps is nil, it'll use the default of 1 to allow the timekeeper to function...
	__object:SetReps( _reps );

	// Set the delay before starting - Delay counts down from the time it is set - If no delay is set, then the start time becomes the time the first time Start is called ( for draw.This / render.This / hook.AddTemp - it is the first time that that particular hook is called )...
	__object:SetDelay( _delay )

	// Return our object..
	return __object;
end


//
// Our main function...
//
function main( ... )
	// Variable Definition
	local _var = 'test';
end