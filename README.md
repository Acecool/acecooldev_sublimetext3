# AcecoolDev_SublimeText3

All of my files for SublimeText 3 - Modifications to ST3, other Packages, and more!

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//
// CodeMap extension for GLua - Josh 'Acecool' Moser
//

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// View the Source-Code and use a program such as Source-Tree to clone it from

	https://bitbucket.org/Acecool/acecooldev_sublimetext3/src


// OR Download the Repo directly from

	https://bitbucket.org/Acecool/acecooldev_sublimetext3/downloads/


// Install the following package within Sublime Text 3

	CodeMap


// Next from the menu goto:

	> Preferences > Package Settings > CodeMap > Edit Settings


// On the USER site, make sure syntaxes has the contents commented out.. it must look like this - or be empty:

    // defined mappers with preferred extension, universal is used as a backup
    // for undefined syntaxes; to use the defined mappers below, they must be
    // present in this list. Don't put anything in this list that isn't
    // defined below.
    "syntaxes":     [
                        // ["universal",   ""],
                        // ["text",        "txt"],
                        // ["python",      "py"]
                    ],


// Go to:

	%AppData%\Sublime Text 3\Packages\User\


// If CodeMap doesn't exist:

	Reinstall CodeMap, and you may need to follow the optional steps below to extract in... once done proceed to the next step!


// Take the ZIP file from above, open it and move the files into the appropriate folders!

	Move the files into the correct folders!


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

//
// For Help & Support
//

// Either Go to the following website

	https://bitbucket.org/Acecool/acecooldev_sublimetext3/issues?status=new&status=open


// AND Click on

	Create Issue


// OR Go directly to the following Create-New Ticket URL

	https://bitbucket.org/Acecool/acecooldev_sublimetext3/issues/new


// Next

	Enter an informitive title letting me know in a few words what the problem, or request, is..


// Next

	Select the 'Kind' of report it is from the dropdown menu: [ bug, enhancement, proposal, task ]

		Select 'task'

		Select 'proposal'		For all other suggestions not applicable by other categories.

		Select 'enhancement'	If you're suggesting a feature to improve upon the output format, optimization, etc..

		Select 'bug'			If there is any type of problem ranging from a typo to crashing..


// Next

	Select the 'Priority' from the dropdown menu

		Select 'blocker'		If there is an issue preventing the CodeMap panel from being populated, crashes, etc.. ( Before submission attempt to close and re-open the panel by using ALT + M - if it doesn't resolve the issue continue with the report )

		Select 'critical'		If there is a large issue or bug occurring which isn't a crash but deeply impacts the functionality of the mapper. A Feature request can be critical for things such as language support, etc... something which impacts a large number of users.

		Select 'major'			If there is a large issue or bug occurring which isn't a crash and doesn't seem to warrant the 'critical' tag. Feature requests which are major impact or would benefit a large number of people but not as serious as language support - such as adding support for multiple languages within a file which could also be considered critical at your discretion, files such as .htm, .html, .php and .asp would be considered files which use multiple languages such as Markup Language and JavaScript for these files and depending on the extension adding PHP or ASP.

		Select 'minor'			If there is a small issue or bug occurring which isn't a crash and doesn't severely impact functionality of the mapper. Minor feature requests are along the line of adding addition search algorithms, etc..

		Select 'trivial'		For matters concerning the polish of the extension such as a typo or other issue which doesn't impact functionality. Trivial feature requests are along the line of altering the default category output order, adding new default categories, etc....


// Next

	Enter a description of the problem along with steps to reproduce it along with the console log.

	OR

	Enter a description of the feature or language you'd like supported along with providing a list of file-extensions used by the language and other details such as what you'd like to
	earmark in the CodeMap panel on the right. You do not need to produce a console log for this type of submission - ignore the steps below.


// To obtain the console log

	Press ALT + C which is the default key combination in Sublime Text 3


// Alternatively

	Look at the bottom left of Sublime Text 3. Click the small icon in the status-bar with an outline of a box with an additional line closer to the bottom - and then click on console.


// Next

	Copy and paste the console log into the description box in between the following characters:

	```code

		PASTE HERE

	```

------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//
// [ Optional - 1 ] Important if you plan on using GMod Lua Package - Lua and GMod Lua packages appear to interfere with each-other with both of tham active - I highly recommend
//					disabling the Vanilla Lua package by completing the following steps... Note that code_map.lua.py in CodeMap\custom_mappers\ is for GMod Lua, but it will also
//					work with Vanilla Lua - it simply contains code to earmark and / or interact / extend certain Garry's Mod Lua functions such as AccessorFunc. It also contains
//					code for AcecoolDev_Framework - my Game and Addon Development Platform / Framework which can also work as a redistributable. I've also including a vanilla Lua
//					CodeMap file named: code_map.vlua.py in custom_mappers. If you're using Vanilla Lua, I'd suggest using this version instead by renaming it..
//

// Install Package

	GMod Lua


// Open Sublime Text Settings

	> Preferences > Settings


// Add "Lua" to "ignored_packages" - yours should look similar to one of these ( Note positions of ,s - the last entry doesn't have a comma, all others do )
// Note: If you don't have an "ignored_packages": line in the right panel of the window which opens when opening settings... Copy the FIRST option into the settings file..

	"ignored_packages":
	[
		"Lua"
	],


// Note: If you do have "ignored_packages": line, make sure to add "Lua", OR "Lua" to the text between the brackets. Ensure ALL lines except the last one has a comma at the end.

	"ignored_packages":
	[
		"Lua",
		"Lua",
		"Lua",
		"Lua",
		"Lua"
	],


// Note: If you settings file is empty, copy and paste the following into the right panel and CTRL + S to save:
// Note: Just as above, the last entry doesn't use a comma here either - the entry table ends at ] ... If there is something after this, then the square-bracket would require a comma after it.

{
	"ignored_packages":
	[
		"Lua"
	]
}


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//
// [ Optional - 2 ] If you want to use Vanilla Lua XCodeMapper for CodeMap - I've included a code_map.vlua.py file inside of User\CodeMap\custom_mappers\ which removes all of the
//					GLua and AcecoolDev_Framework interaction logic. Complete this optional setup step to do that.
//

// Open folder:

	%AppData%\Sublime Text 3\Packages\User\CodeMap\custom_mappers\


// Rename File:

	FROM
		code_map.lua.py

	TO
		code_map.alua.py


// Rename File:
	FROM
		code_map.vlua.py

	TO
		code_map.lua.py


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//
// [ Optional - 3 ] If you want to use Garry's Mod Lua XCodeMapper for CodeMap without the AcecoolDev_Framework interaction logic - I've included a code_map.glua.py file inside
//					of User\CodeMap\custom_mappers\ which removes all of the AcecoolDev_Framework interaction logic leaving only Vanilla Lua / GMod Lua logic. Complete this optional
//					setup stage to do that.
//

// Open folder:

	%AppData%\Sublime Text 3\Packages\User\CodeMap\custom_mappers\


// Rename File:

	FROM
		code_map.lua.py

	TO
		code_map.alua.py


// Rename File:

	FROM
		code_map.glua.py

	TO
		code_map.lua.py


------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//
// [ Optional - 4 ] Extract CodeMap Package - Only necessary if you plan on modifying the CodeMap back-end - it isn't required for this mod...
//

// Next, install the following package:

	Extract Sublime Package


// Go to:

	%AppData%\Sublime Text 3\Installed Packages\


// Double click on ( make sure you open with Sublime Text 3 ):

	CodeMap.sublime-package


// It will ask you whether or not you want to extract the package:

	Choose yes